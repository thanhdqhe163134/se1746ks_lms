﻿CREATE
DATABASE LMS;
go
USE LMS;
go

-- Tạo bảng Books
CREATE TABLE Books
(
    book_id          INT IDENTITY(1,1) PRIMARY KEY,
    title            NVARCHAR(255),
    author           NVARCHAR(50),
    total_copies     INT,
    copies_borrowed  INT,
    copies_available INT,
    description      NVARCHAR( MAX),
    IMG              NVARCHAR(100),
    created_date     DATETIME,
    created_user     NVARCHAR(20),
    updated_date     DATETIME,
    updated_user     NVARCHAR(20),
    deleted_date     datetime,
    deleted_user     nvarchar(20),
    is_deleted       BIT
);

CREATE TABLE Publisher(
publisher_id INT PRIMARY KEY,
name NVARCHAR(50),
created_date DATETIME,
created_user NVARCHAR(20),
updated_date DATETIME,
updated_user NVARCHAR(20),
    deleted_date     datetime,
    deleted_user     nvarchar(20),
    is_deleted       BIT
);

CREATE TABLE BookEdition(
ISBN NVARCHAR(20) PRIMARY KEY,
book_id INT,
publisher_id INT,
publish_year INT,
language NVARCHAR(20),
created_date DATETIME,
created_user NVARCHAR(20),
updated_date DATETIME,
updated_user NVARCHAR(20),
    deleted_date     datetime,
    deleted_user     nvarchar(20),
    is_deleted       BIT,
FOREIGN KEY (book_id) REFERENCES Books (book_id),
FOREIGN KEY (publisher_id) REFERENCES Publisher (publisher_id)
);


CREATE TABLE BookDetail
(
    book_detail_id  NVARCHAR(10) PRIMARY KEY,
    ISBN	NVARCHAR(20),
    created_date DATETIME,
	created_user NVARCHAR(20),
    status       NVARCHAR(255),
    is_borrowed  BIT,
	is_returned BIT,
    updated_date DATETIME,
    updated_user NVARCHAR(20),
    deleted_date datetime,
    deleted_user nvarchar(20),
    is_deleted       BIT,
	FOREIGN KEY (ISBN) REFERENCES BookEdition (ISBN),
);






-- Tạo bảng Users
CREATE TABLE Accounts
(
    user_id      INT IDENTITY(1,1) PRIMARY KEY,
    username     NVARCHAR(20),
    password     NVARCHAR(255),
    student_id   NVARCHAR(10),
    full_name    NVARCHAR(50),
    dob          DATE,
    email        NVARCHAR(50),
    phone_number NVARCHAR(15),
    address      NVARCHAR(255),
    role         NVARCHAR(10),
    IMG          NVARCHAR(100),
    created_date DATETIME,
    updated_date DATETIME,
    updated_user NVARCHAR(20),
    deleted_date datetime,
    deleted_user nvarchar(20),
    is_deleted   BIT

);

CREATE TABLE RateLimiting (
    user_id INT PRIMARY KEY,
    request_count INT,
    last_request_time DATETIME,
	failed_attempts INT DEFAULT 0,
    created_date DATETIME DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES Accounts (user_id)
);

-- Tạo bảng Loans
CREATE TABLE Loans
(
    loan_id       INT IDENTITY(1,1) PRIMARY KEY,
    user_id       INT,
    book_detail_id     NVARCHAR(10),
    borrow_date   DATETIME,
    due_date      DATE,
    returned_date DATETIME,
	status NVARCHAR(MAX),
    created_date  DATETIME,
    created_user  NVARCHAR(20),
	updated_date DATETIME,
	updated_user NVARCHAR(20),
    expried_date  DATE,
    is_returned   BIT,
    is_request   BIT
    FOREIGN KEY (user_id) REFERENCES Accounts (user_id),
    FOREIGN KEY (book_detail_id) REFERENCES BookDetail (book_detail_id)
);

-- Tạo bảng Fine
CREATE TABLE Fine
(
    fine_id     INT IDENTITY(1,1) PRIMARY KEY,
    loan_id     INT,
    fine_amount MONEY, 
    description NVARCHAR(MAX),
    created_date DATETIME,
    created_user NVARCHAR(20),
	updated_date DATETIME,
	updated_user NVARCHAR(20),
    is_paid BIT,
    FOREIGN KEY (loan_id) REFERENCES Loans (loan_id)
    );


-- Tạo bảng Genres
CREATE TABLE Genres
(
    genre_id   INT IDENTITY(1,1) PRIMARY KEY,
    genre_name NVARCHAR(50),
	created_date DATETIME,
	created_user NVARCHAR(20),
	  deleted_date datetime,
    deleted_user nvarchar(20),
    is_deleted   BIT

);

CREATE TABLE BookGenre
(
    book_id  INT,
    genre_id INT,
	created_date DATETIME,
	created_user NVARCHAR(20),
	  deleted_date datetime,
    deleted_user nvarchar(20),
    is_deleted   BIT,
    PRIMARY KEY (book_id, genre_id),
    FOREIGN KEY (book_id) REFERENCES Books (book_id) ON DELETE CASCADE,
    FOREIGN KEY (genre_id) REFERENCES Genres (genre_id) ON DELETE CASCADE
);

-- Tạo bảng Request
CREATE TABLE Request
(
    request_id   INT IDENTITY(1,1) PRIMARY KEY,
    loan_id      INT,
    request_date DATE,
    description  NVARCHAR(255),
    created_date DATETIME,
    created_user NVARCHAR(20),
	approved_user NVARCHAR(20),
	approved_date DATETIME,
    is_approved  BIT,
    FOREIGN KEY (loan_id) REFERENCES LOANS (loan_id),
);

-- Tạo bảng Comment
CREATE TABLE Comment
(
    comment_id   INT IDENTITY(1,1) PRIMARY KEY,
    user_id      INT,
    book_id      INT,
    content      NVARCHAR( MAX),
    rep_comment  INT,
    created_date DATETIME,
    created_user NVARCHAR(20),
    updated_date DATETIME,
    updated_user NVARCHAR(20),
    deleted_date datetime,
    deleted_user nvarchar(20),
    is_deleted   BIT,
    FOREIGN KEY (book_id) REFERENCES Books (book_id),
    FOREIGN KEY (user_id) REFERENCES Accounts (user_id)
);

CREATE TABLE Returned(
returned_id  INT IDENTITY(1,1) PRIMARY KEY,
    loan_id INT UNIQUE,
book_condition NVARCHAR(MAX),
 created_date DATETIME,
    created_user NVARCHAR(20),
    updated_date DATETIME,
    updated_user NVARCHAR(20),
FOREIGN KEY (loan_id) REFERENCES LOANS (loan_id),
);



GO

CREATE TRIGGER trg_UpdateBookCounts
ON BookDetail
AFTER INSERT, DELETE, UPDATE
AS
BEGIN
    SET NOCOUNT ON;

    -- Declare variables
    DECLARE @bookId INT;
    DECLARE @totalCopies INT;
    DECLARE @borrowedCopies INT;
    DECLARE @availableCopies INT;

    -- Handle insert or update
    IF (SELECT COUNT(*) FROM inserted) > 0
    BEGIN
        SELECT @bookId = b.book_id
        FROM inserted i
        INNER JOIN BookEdition b ON i.ISBN = b.ISBN;

        SELECT @totalCopies = COUNT(*),
               @borrowedCopies = COUNT(CASE WHEN is_borrowed = 1 THEN 1 ELSE NULL END)
        FROM BookDetail
        WHERE ISBN IN (SELECT ISBN FROM BookEdition WHERE book_id = @bookId) AND is_deleted = 0;

        SET @availableCopies = @totalCopies - @borrowedCopies;

        -- Update the Books table
        UPDATE Books
        SET total_copies = @totalCopies,
            copies_borrowed = @borrowedCopies,
            copies_available = @availableCopies
        WHERE book_id = @bookId;
    END

    -- Handle delete
    IF (SELECT COUNT(*) FROM deleted) > 0
    BEGIN
        SELECT @bookId = b.book_id
        FROM deleted d
        INNER JOIN BookEdition b ON d.ISBN = b.ISBN;

        SELECT @totalCopies = COUNT(*),
               @borrowedCopies = COUNT(CASE WHEN is_borrowed = 1 THEN 1 ELSE NULL END)
        FROM BookDetail
        WHERE ISBN IN (SELECT ISBN FROM BookEdition WHERE book_id = @bookId) AND is_deleted = 0;

        SET @availableCopies = @totalCopies - @borrowedCopies;

        -- Update the Books table
        UPDATE Books
        SET total_copies = @totalCopies,
            copies_borrowed = @borrowedCopies,
            copies_available = @availableCopies
        WHERE book_id = @bookId;
    END
END;


GO




-- Insert data into Books table
INSERT INTO Books (title, author, description, IMG, created_date, created_user, updated_date, updated_user, is_deleted, total_copies, copies_borrowed, copies_available)
VALUES 
('Harry Potter and the Sorcerer''s Stone', 'J.K. Rowling', 'A magical adventure.', 'https://i.imgur.com/fXXpcve.jpg', GETDATE(), 'admin', GETDATE(), 'admin', 0, 3, 0, 3),
('The Great Gatsby', 'F. Scott Fitzgerald', 'A classic novel.', 'https://i.imgur.com/Kg0Kl8p.jpg', GETDATE(), 'admin', GETDATE(), 'admin', 0, 3, 0, 3),
('To Kill a Mockingbird', 'Harper Lee', 'A compelling story.', 'https://i.imgur.com/9fw99Qa.jpg', GETDATE(), 'admin', GETDATE(), 'admin', 0, 3, 0, 3),
('1984', 'George Orwell', 'Dystopian novel set in Airstrip One.', 'https://i.imgur.com/d2TbCuB.jpg',  GETDATE(), 'admin', GETDATE(), 'admin', 0, 4, 0, 4),
('Pride and Prejudice', 'Jane Austen', 'A romantic novel of manners.', 'https://i.imgur.com/TjlGfYX.jpg', GETDATE(), 'admin', GETDATE(), 'admin', 0, 5, 0, 5),
('The Catcher in the Rye', 'J.D. Salinger', 'A story about adolescent Holden Caulfield.', 'https://i.imgur.com/ie2SagR.jpg', GETDATE(), 'admin', GETDATE(), 'admin', 0, 4, 0, 4),
('The Hobbit', 'J.R.R. Tolkien', 'A fantasy novel and children''s book.', 'https://i.imgur.com/6yt572f.jpg', GETDATE(), 'admin', GETDATE(), 'admin', 0, 5, 0, 5),
('Fahrenheit 451', 'Ray Bradbury', 'Dystopian novel set in a future American society.', 'https://i.imgur.com/Mlgv6oE.jpg', GETDATE(), 'admin', GETDATE(), 'admin', 0, 4, 0, 4),
('Moby Dick', 'Herman Melville', 'A sailor Ishmael narrates the pursuit of Moby Dick.', 'https://i.imgur.com/AYkqIdM.jpg', GETDATE(), 'admin', GETDATE(), 'admin', 0, 4, 0, 4),
('War and Peace', 'Leo Tolstoy', 'A novel chronicling the French invasion of Russia.', 'https://i.imgur.com/nSLgREK.jpg', GETDATE(), 'admin', GETDATE(), 'admin', 0, 6, 0, 6);


INSERT INTO Publisher (publisher_id, name, created_date, created_user,is_deleted)
VALUES
(1, 'Scholastic', GETDATE(), 'admin',0),
(2, 'Scribner',GETDATE(), 'admin',0),
(3, 'HarperCollins',GETDATE(), 'admin',0),
(4, 'Random House', GETDATE(), 'admin', 0),
(5, 'Penguin', GETDATE(), 'admin', 0),
(6, 'Hachette', GETDATE(), 'admin', 0),
(7, 'Macmillan', GETDATE(), 'admin', 0),
(8, 'Simon & Schuster', GETDATE(), 'admin', 0),
(9, 'Oxford University Press', GETDATE(), 'admin', 0),
(10, 'Cambridge University Press', GETDATE(), 'admin', 0);

INSERT INTO BookEdition (ISBN, book_id, publisher_id, publish_year, language,is_deleted) 
VALUES
('97860419-01-2022', 1, 1, 2022, 'EngLish',0),
('97814842-02-2019', 2, 2, 2019, 'EngLish',0),
('97814843-03-2019', 3, 3, 2019, 'EngLish',0),
  ('978-0-7432-7356-5', 1, 1, 2012, 'English', 0),
('978-0-7432-7357-6', 1, 4, 2014, 'English', 0),
('978-0-7432-7358-7', 2, 5, 2015, 'English', 0),
('978-0-7432-7359-8', 2, 6, 2016, 'English', 0),
('978-0-7432-7360-9', 3, 7, 2017, 'English', 0),
('978-0-7432-7361-0', 3, 8, 2018, 'English', 0),
('978-0-7432-7362-1', 4, 9, 2019, 'English', 0),
('978-0-7432-7363-2', 4, 10, 2021, 'English', 0),
('978-0-7432-7364-3', 5, 1, 2013, 'English', 0),
('978-0-7432-7365-4', 5, 2, 2015, 'English', 0),
('978-0-7432-7366-5', 6, 3, 2011, 'English', 0),
('978-0-7432-7367-6', 6, 4, 2013, 'English', 0),
('978-0-7432-7368-7', 7, 5, 2010, 'English', 0),
('978-0-7432-7369-8', 7, 6, 2022, 'English', 0),
('978-0-7432-7370-9', 8, 7, 2023, 'English', 0),
('978-0-7432-7371-0', 8, 8, 2023, 'English', 0),
('978-0-7432-7372-1', 9, 9, 2011, 'English', 0),
('978-0-7432-7373-2', 9, 10, 2012, 'English', 0),
('978-0-7432-7374-3', 10, 1, 2010, 'English', 0),
('978-0-7432-7375-4', 10, 2, 2011, 'English', 0);



-- Insert data into BookDetail table
INSERT INTO BookDetail (book_detail_id,ISBN, created_date, created_user, status, is_borrowed, 
                         updated_date, updated_user,is_deleted)
VALUES 
('00001abcde', '978-0-7432-7356-5',   GETDATE(), 'admin', 'New', 0, GETDATE(), 'admin', 0),
('00002abcde', '978-0-7432-7357-6',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('00003abcde', '978-0-7432-7357-6',   GETDATE(), 'admin', 'New', 0, GETDATE(), 'admin', 0),
('0000Aabcde', '978-0-7432-7358-7',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('0000Babcde', '978-0-7432-7359-8',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('0000Cabcde', '978-0-7432-7359-8',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('0000Dabcde', '978-0-7432-7360-9',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('0000Eabcde', '978-0-7432-7362-1',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('0000Fabcde', '978-0-7432-7363-2',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('0000Gabcde', '978-0-7432-7364-3',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('0000Habcde', '978-0-7432-7365-4',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('0000Iabcde', '978-0-7432-7365-4',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('0000Jabcde', '978-0-7432-7366-5',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('0000Kabcde', '978-0-7432-7367-6',   GETDATE(), 'admin', 'New', 0, GETDATE(), 'admin', 0),
('0000Labcde', '978-0-7432-7368-7',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('0000Mabcde', '978-0-7432-7369-8',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('0000Nabcde', '978-0-7432-7369-8',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('0000Oabcde', '978-0-7432-7370-9',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('0000Pabcde', '978-0-7432-7371-0',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('0000Qabcde', '978-0-7432-7372-1',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('0000Rabcde', '978-0-7432-7373-2',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('0000Sabcde', '978-0-7432-7373-2',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('0000Tabcde', '978-0-7432-7374-3',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('0000Uabcde', '978-0-7432-7375-4',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('0000Vabcde', '978-0-7432-7375-4',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('0000Wabcde', '978-0-7432-7374-3',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('0000Xabcde', '978-0-7432-7375-4',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('0000Yabcde', '978-0-7432-7375-4',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('0000Zabcde', '978-0-7432-7360-9',   GETDATE(), 'admin', 'New', 0, GETDATE(), 'admin', 0),
('00010abcde', '978-0-7432-7361-0',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('00011abcde', '978-0-7432-7362-1',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('00012abcde', '978-0-7432-7363-2',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('00013abcde', '978-0-7432-7364-3',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('00014abcde', '978-0-7432-7365-4',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('00015abcde', '978-0-7432-7366-5',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('00016abcde', '978-0-7432-7367-6',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('00017abcde', '978-0-7432-7368-7',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('00018abcde', '978-0-7432-7369-8',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('00019abcde', '978-0-7432-7370-9',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('00020abcde', '978-0-7432-7371-0',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0),
('00021abcde', '978-0-7432-7372-1',   GETDATE(), 'admin', 'New', 0,  GETDATE(), 'admin', 0);


-- Insert data into Accounts table (Users)
INSERT INTO Accounts (username, password, full_name, email, phone_number, address, role, IMG, created_date,
                      updated_date, updated_user, is_deleted)
VALUES  ('admin', '123456', 'Admin User', 'admin@example.com', '555-555-5555', '789 Oak St', 'admin', '',
        GETDATE(), GETDATE(), 'admin', 0),
		       ('librarian', '123456', 'Jane Smith', 'jane.smith@example.com', '987-654-3210', '456 Elm St', 'librarian', '',
        GETDATE(), GETDATE(), 'admin', 0),
('user', '123456', 'John Doe', 'john.doe@example.com', '123-456-7890', '123 Main St', 'user', '',
        GETDATE(), GETDATE(), 'admin', 0);

      

-- Insert data into Genres table
INSERT INTO Genres (genre_name, created_date, created_user, is_deleted)
VALUES 
('Fantasy', GETDATE(), 'admin', 0),
('Classics', GETDATE(), 'admin', 0),
('Fiction', GETDATE(), 'admin', 0),
('Adventure', GETDATE(), 'admin', 0),
('Mystery', GETDATE(), 'admin', 0),
('Romance', GETDATE(), 'admin', 0),
('Science Fiction', GETDATE(), 'admin', 0),
('Historical Fiction', GETDATE(), 'admin', 0),
('Thriller', GETDATE(), 'admin', 0),
('Non-Fiction', GETDATE(), 'admin', 0);

INSERT INTO BookGenre (book_id, genre_id, created_date, created_user, is_deleted)
VALUES  (1, 1, GETDATE(), 'admin', 0),
(1, 3, GETDATE(), 'admin', 0),
(1, 4, GETDATE(), 'admin', 0),
(2, 2, GETDATE(), 'admin', 0),
(2, 5, GETDATE(), 'admin', 0),
(2, 8, GETDATE(), 'admin', 0),
(3, 3, GETDATE(), 'admin', 0),
(3, 7, GETDATE(), 'admin', 0),
(3, 9, GETDATE(), 'admin', 0),
(4, 1, GETDATE(), 'admin', 0),
(4, 4, GETDATE(), 'admin', 0),
(4, 6, GETDATE(), 'admin', 0),
(5, 2, GETDATE(), 'admin', 0),
(5, 3, GETDATE(), 'admin', 0),
(5, 8, GETDATE(), 'admin', 0),
(6, 1, GETDATE(), 'admin', 0),
(6, 4, GETDATE(), 'admin', 0),
(6, 5, GETDATE(), 'admin', 0),
(7, 6, GETDATE(), 'admin', 0),
(7, 7, GETDATE(), 'admin', 0),
(7, 9, GETDATE(), 'admin', 0),
(8, 1, GETDATE(), 'admin', 0),
(8, 3, GETDATE(), 'admin', 0),
(8, 10, GETDATE(), 'admin', 0),
(9, 2, GETDATE(), 'admin', 0),
(9, 5, GETDATE(), 'admin', 0),
(9, 7, GETDATE(), 'admin', 0),
(10, 4, GETDATE(), 'admin', 0),
(10, 6, GETDATE(), 'admin', 0),
(10, 8, GETDATE(), 'admin', 0);


-- Insert data into Comment table
INSERT INTO Comment (user_id, book_id, content, rep_comment, created_date, created_user, updated_date, updated_user,
                     is_deleted)
VALUES (1, 1, 'Great book!', 0, GETDATE(), 'user', NULL, NULL, 0),
       (2, 1, 'I love this series!', 1, GETDATE(), 'librarian', NULL, NULL, 0),
       (1, 2, 'Classic literature at its best.', 0, GETDATE(), 'user', NULL, NULL, 0);


