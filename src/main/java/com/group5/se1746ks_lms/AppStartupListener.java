package com.group5.se1746ks_lms;

import com.group5.se1746ks_lms.dao.AccountDAO;
import com.group5.se1746ks_lms.util.DailyTaskScheduler;
import de.svws_nrw.ext.jbcrypt.BCrypt;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;


@WebListener
public class AppStartupListener implements ServletContextListener {

    private AccountDAO accountDAO;

    private DailyTaskScheduler taskScheduler;


    @Override
    public void contextInitialized(ServletContextEvent sce) {
        accountDAO = new AccountDAO();
        ensureAdminAccount();
        ensureUserAccount();
        ensureLibrarianAccount();
        taskScheduler = new DailyTaskScheduler();
        taskScheduler.startDailyTask();
    }

    private void ensureAdminAccount() {
        String username = "admin";
        String defaultPassword = "123456";
        String role = "admin";
        String hashedPassword = BCrypt.hashpw(defaultPassword, BCrypt.gensalt());

        if (accountDAO.existsByUsername(username)) {
            accountDAO.updatePassword(username, hashedPassword, role);
        } else {
            accountDAO.createAccount(username, hashedPassword, role);
        }
    }

    private void ensureUserAccount() {
        String username = "user";
        String defaultPassword = "123456";
        String role = "user";
        String hashedPassword = BCrypt.hashpw(defaultPassword, BCrypt.gensalt());

        if (accountDAO.existsByUsername(username)) {
            accountDAO.updatePassword(username, hashedPassword, role);
        } else {
            accountDAO.createAccount(username, hashedPassword, role);
        }
    }

    private void ensureLibrarianAccount() {
        String username = "librarian";
        String defaultPassword = "123456";
        String role = "librarian";
        String hashedPassword = BCrypt.hashpw(defaultPassword, BCrypt.gensalt());

        if (accountDAO.existsByUsername(username)) {
            accountDAO.updatePassword(username, hashedPassword, role);
        } else {
            accountDAO.createAccount(username, hashedPassword, role);
        }
    }





    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        if (taskScheduler != null) {
            taskScheduler.stop();
        }    }
}
