package com.group5.se1746ks_lms.model;

import java.util.Date;

public class BookDetail {
   private String book_detail_id;
   private String ISBN;
   private String status;

   private boolean is_borrowed;

   private Date created_date;
    private String created_user;
    private Date updated_date;
    private String updated_user;
    private Date deleted_date;
    private String deleted_user;
    private boolean is_deleted;

    public BookDetail() {

    }

    public BookDetail(String book_detail_id, String ISBN, String status, boolean is_borrowed, Date created_date, String created_user, Date updated_date, String updated_user, Date deleted_date, String deleted_user, boolean is_deleted) {
        this.book_detail_id = book_detail_id;
        this.ISBN = ISBN;
        this.status = status;
        this.is_borrowed = is_borrowed;
        this.created_date = created_date;
        this.created_user = created_user;
        this.updated_date = updated_date;
        this.updated_user = updated_user;
        this.deleted_date = deleted_date;
        this.deleted_user = deleted_user;
        this.is_deleted = is_deleted;
    }

    public boolean getIs_borrowed() {
        return is_borrowed;
    }

    public void setIs_borrowed(boolean is_borrowed) {
        this.is_borrowed = is_borrowed;
    }


    public String getBook_detail_id() {
        return book_detail_id;
    }

    public void setBook_detail_id(String book_detail_id) {
        this.book_detail_id = book_detail_id;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreated_date() {
        return created_date;
    }

    public void setCreated_date(Date created_date) {
        this.created_date = created_date;
    }

    public String getCreated_user() {
        return created_user;
    }

    public void setCreated_user(String created_user) {
        this.created_user = created_user;
    }

    public Date getUpdated_date() {
        return updated_date;
    }

    public void setUpdated_date(Date updated_date) {
        this.updated_date = updated_date;
    }

    public String getUpdated_user() {
        return updated_user;
    }

    public void setUpdated_user(String updated_user) {
        this.updated_user = updated_user;
    }

    public Date getDeleted_date() {
        return deleted_date;
    }

    public void setDeleted_date(Date deleted_date) {
        this.deleted_date = deleted_date;
    }

    public String getDeleted_user() {
        return deleted_user;
    }

    public void setDeleted_user(String deleted_user) {
        this.deleted_user = deleted_user;
    }

    public boolean getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(boolean is_deleted) {
        this.is_deleted = is_deleted;
    }
}
