package com.group5.se1746ks_lms.model;


import jakarta.persistence.*;

import java.sql.Date;
import java.util.Set;
import java.sql.Timestamp;


@Entity
@Table(name = "Books")
public class Books {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long book_id;
    private String title;
    private String author;
    private int total_copies;
    private int copies_borrowed;
    private int copies_available;
    private String description;
    private String IMG;
    private Timestamp created_date;
    private String created_user;
    private Timestamp updated_date;
    private String updated_user;
    private boolean is_deleted;

    private String isbn;

    private String publisher;

    private String language;

    private int year;



    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(
            name = "BookGenre",
            joinColumns = {@JoinColumn(name = "book_id")},
            inverseJoinColumns = {@JoinColumn(name = "genre_id")}
    )
    private Set<Genres> genres;


    public Books() {
    }

    public Books(Long book_id, String title, String author, int total_copies, int copies_borrowed, int copies_available, String description, String IMG, Timestamp created_date, String created_user, Timestamp updated_date, String updated_user, boolean is_deleted, Set<Genres> genres) {
        this.book_id = book_id;
        this.title = title;
        this.author = author;
        this.total_copies = total_copies;
        this.copies_borrowed = copies_borrowed;
        this.copies_available = copies_available;
        this.description = description;
        this.IMG = IMG;
        this.created_date = created_date;
        this.created_user = created_user;
        this.updated_date = updated_date;
        this.updated_user = updated_user;
        this.is_deleted = is_deleted;
        this.genres = genres;
    }


    public Books(String title, String author, int total_copies, int available_copies, int borrowed_copies, String created_user, Timestamp created_date, String updated_user, Timestamp updated_date, boolean is_deleted, String description, String img) {
        this.title = title;
        this.author = author;
        this.total_copies = total_copies;
        this.copies_borrowed = available_copies;
        this.copies_available = borrowed_copies;
        this.created_user = created_user;
        this.created_date = created_date;
        this.updated_user = updated_user;
        this.updated_date = updated_date;
        this.is_deleted = is_deleted;
        this.description = description;
        this.IMG = img;
    }

    public Books(String title, String author, String created_user, Timestamp created_date, String updated_user, Timestamp updated_date, boolean is_deleted, String description, String img) {
        this.title = title;
        this.author = author;
        this.created_user = created_user;
        this.created_date = created_date;
        this.updated_user = updated_user;
        this.updated_date = updated_date;
        this.is_deleted = is_deleted;
        this.description = description;
        this.IMG = img;
    }

    public Books(Long book_id, String title, String author, int total_copies, int copies_borrowed, int copies_available, String description, String IMG, Timestamp created_date, String created_user, Timestamp updated_date, String updated_user, boolean is_deleted, String isbn, String publisher, String language, int year, Set<Genres> genres) {
        this.book_id = book_id;
        this.title = title;
        this.author = author;
        this.total_copies = total_copies;
        this.copies_borrowed = copies_borrowed;
        this.copies_available = copies_available;
        this.description = description;
        this.IMG = IMG;
        this.created_date = created_date;
        this.created_user = created_user;
        this.updated_date = updated_date;
        this.updated_user = updated_user;
        this.is_deleted = is_deleted;
        this.isbn = isbn;
        this.publisher = publisher;
        this.language = language;
        this.year = year;
        this.genres = genres;
    }

    public Long getBook_id() {
        return book_id;
    }

    public void setBook_id(Long book_id) {
        this.book_id = book_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getTotal_copies() {
        return total_copies;
    }

    public void setTotal_copies(int total_copies) {
        this.total_copies = total_copies;
    }

    public int getCopies_borrowed() {
        return copies_borrowed;
    }

    public void setCopies_borrowed(int copies_borrowed) {
        this.copies_borrowed = copies_borrowed;
    }

    public int getCopies_available() {
        return copies_available;
    }

    public void setCopies_available(int copies_available) {
        this.copies_available = copies_available;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIMG() {
        return IMG;
    }

    public void setIMG(String IMG) {
        this.IMG = IMG;
    }

    public Timestamp getCreated_date() {
        return created_date;
    }

    public void setCreated_date(Timestamp created_date) {
        this.created_date = created_date;
    }

    public String getCreated_user() {
        return created_user;
    }

    public void setCreated_user(String created_user) {
        this.created_user = created_user;
    }

    public Timestamp getUpdated_date() {
        return updated_date;
    }

    public void setUpdated_date(Timestamp updated_date) {
        this.updated_date = updated_date;
    }

    public String getUpdated_user() {
        return updated_user;
    }

    public void setUpdated_user(String updated_user) {
        this.updated_user = updated_user;
    }

    public boolean getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(boolean is_deleted) {
        this.is_deleted = is_deleted;
    }

    public Set<Genres> getGenres() {
        return genres;
    }

    public void setGenres(Set<Genres> genres) {
        this.genres = genres;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
