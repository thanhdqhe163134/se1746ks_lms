package com.group5.se1746ks_lms.model;


import jakarta.persistence.*;

import java.sql.Timestamp;
import java.util.Set;

@Entity
@Table(name = "Genres")
public class Genres {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long genre_id;
    private String genre_name;


    @ManyToMany(mappedBy = "genres")
    private Set<Books> books;

    public Genres() {
    }

    public Genres(Long genre_id, String genre_name, Set<Books> books) {
        this.genre_id = genre_id;
        this.genre_name = genre_name;
        this.books = books;
    }

    public Long getGenre_id() {
        return genre_id;
    }

    public void setGenre_id(Long genre_id) {
        this.genre_id = genre_id;
    }

    public String getGenre_name() {
        return genre_name;
    }

    public void setGenre_name(String genre_name) {
        this.genre_name = genre_name;
    }

    public Set<Books> getBooks() {
        return books;
    }

    public void setBooks(Set<Books> books) {
        this.books = books;
    }
}
