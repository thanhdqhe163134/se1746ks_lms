package com.group5.se1746ks_lms.model;

public class Request {
    private long request_id;
    private long loan_id;
    private java.sql.Date request_date;
    private String description;
    private java.sql.Timestamp created_date;
    private String created_user;
    private boolean is_approved;

    public Request(long request_id, long loan_id, java.sql.Date request_date, String description, java.sql.Timestamp created_date, String created_user, boolean is_approved) {
        this.request_id = request_id;
        this.loan_id = loan_id;
        this.request_date = request_date;
        this.description = description;
        this.created_date = created_date;
        this.created_user = created_user;
        this.is_approved = is_approved;
    }

    public Request() {
    }

    public long getRequest_id() {
        return request_id;
    }

    public void setRequest_id(long request_id) {
        this.request_id = request_id;
    }

    public long getLoan_id() {
        return loan_id;
    }

    public void setLoan_id(long loan_id) {
        this.loan_id = loan_id;
    }

    public java.sql.Date getRequest_date() {
        return request_date;
    }

    public void setRequest_date(java.sql.Date request_date) {
        this.request_date = request_date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }



    public java.sql.Timestamp getCreated_date() {
        return created_date;
    }

    public void setCreated_date(java.sql.Timestamp created_date) {
        this.created_date = created_date;
    }

    public String getCreated_user() {
        return created_user;
    }

    public void setCreated_user(String created_user) {
        this.created_user = created_user;
    }

    public boolean getIs_approved() {
        return is_approved;
    }

    public void setIs_approved(boolean is_approved) {
        this.is_approved = is_approved;
    }

}
