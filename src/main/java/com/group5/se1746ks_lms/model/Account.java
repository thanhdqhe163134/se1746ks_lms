package com.group5.se1746ks_lms.model;


import java.sql.Timestamp;
import java.util.Date;


public class Account {

    private Long user_id;

    private String username;

    private String password;

    private String student_id;

    private String full_name;

    private Date dob;

    private String email;

    private String phone_number;

    private String address;

    private String role;

    private String img;

    private Timestamp created_date;

    private Timestamp updated_date;

    private String updated_user;

    private Timestamp deleted_date;

    private String deleted_user;

    private Boolean is_deleted;

    public Account() {
    }

    public Account(Long user_id, String username, String password, String student_id, String full_name, Date dob, String email, String phone_number, String address, String role, String img, Timestamp created_date, Timestamp updated_date, String updated_user, Timestamp deleted_date, String deleted_user, Boolean is_deleted) {
        this.user_id = user_id;
        this.username = username;
        this.password = password;
        this.student_id = student_id;
        this.full_name = full_name;
        this.dob = dob;
        this.email = email;
        this.phone_number = phone_number;
        this.address = address;
        this.role = role;
        this.img = img;
        this.created_date = created_date;
        this.updated_date = updated_date;
        this.updated_user = updated_user;
        this.deleted_date = deleted_date;
        this.deleted_user = deleted_user;
        this.is_deleted = is_deleted;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Timestamp getCreated_date() {
        return created_date;
    }

    public void setCreated_date(Timestamp created_date) {
        this.created_date = created_date;
    }

    public Timestamp getUpdated_date() {
        return updated_date;
    }

    public void setUpdated_date(Timestamp updated_date) {
        this.updated_date = updated_date;
    }

    public String getUpdated_user() {
        return updated_user;
    }

    public void setUpdated_user(String updated_user) {
        this.updated_user = updated_user;
    }

    public Timestamp getDeleted_date() {
        return deleted_date;
    }

    public void setDeleted_date(Timestamp deleted_date) {
        this.deleted_date = deleted_date;
    }

    public String getDeleted_user() {
        return deleted_user;
    }

    public void setDeleted_user(String deleted_user) {
        this.deleted_user = deleted_user;
    }

    public Boolean getIs_deleted() {
        return is_deleted;
    }

    public Boolean getIs_deleted(boolean b) {
        return is_deleted;
    }

    public void setIs_deleted(Boolean is_deleted) {
        this.is_deleted = is_deleted;
    }
}
