package com.group5.se1746ks_lms.model;


import java.sql.Timestamp;

public class Fine {

private int fine_id;
private int loans_id;
private double fine_amount;
private String description;

private Timestamp created_date;

private Timestamp paid_date;

private String updated_user;

private String created_user;
private boolean is_paid;

    public Fine(int fine_id, int loans_id, double fine_amount, String description, Timestamp created_date,  String created_user, Timestamp paid_date, String updated_user,  boolean is_paid) {
        this.fine_id = fine_id;
        this.loans_id = loans_id;
        this.fine_amount = fine_amount;
        this.description = description;
        this.created_date = created_date;
        this.created_user = created_user;
        this.paid_date = paid_date;
        this.updated_user = updated_user;
        this.is_paid = is_paid;
    }

    public Fine() {
    }

    public Timestamp getPaid_date() {
        return paid_date;
    }

    public void setPaid_date(Timestamp paid_date) {
        this.paid_date = paid_date;
    }

    public String getUpdated_user() {
        return updated_user;
    }

    public void setUpdated_user(String updated_user) {
        this.updated_user = updated_user;
    }

    public int getFine_id() {
        return fine_id;
    }

    public void setFine_id(int fine_id) {
        this.fine_id = fine_id;
    }

    public int getLoans_id() {
        return loans_id;
    }

    public void setLoans_id(int loans_id) {
        this.loans_id = loans_id;
    }

    public double getFine_amount() {
        return fine_amount;
    }

    public void setFine_amount(double fine_amount) {
        this.fine_amount = fine_amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getCreated_date() {
        return created_date;
    }

    public void setCreated_date(Timestamp created_date) {
        this.created_date = created_date;
    }

    public boolean getIs_paid() {
        return is_paid;
    }

    public void setIs_paid(boolean is_paid) {
        this.is_paid = is_paid;
    }

    public String getCreated_user() {
        return created_user;
    }

    public void setCreated_user(String created_user) {
        this.created_user = created_user;
    }
}
