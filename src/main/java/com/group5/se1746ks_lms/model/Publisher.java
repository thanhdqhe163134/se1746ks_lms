package com.group5.se1746ks_lms.model;

import java.util.Date;
import java.util.List;

public class Publisher {
    private long publisher_id;
    private String publisher_name;
    private Date created_date;
    private String created_user;
    private Date updated_date;
    private String updated_user;
    private Date deleted_date;
    private String deleted_user;
    private boolean is_deleted;

    private List<BookEdition> bookEdition;

    public Publisher(long publisher_id, String publisher_name, Date created_date, String created_user, Date updated_date, String updated_user, Date deleted_date, String deleted_user, boolean is_deleted, List<BookEdition> bookEdition) {
        this.publisher_id = publisher_id;
        this.publisher_name = publisher_name;
        this.created_date = created_date;
        this.created_user = created_user;
        this.updated_date = updated_date;
        this.updated_user = updated_user;
        this.deleted_date = deleted_date;
        this.deleted_user = deleted_user;
        this.is_deleted = is_deleted;
        this.bookEdition = bookEdition;
    }

    public Publisher() {

    }

    public Publisher(long publisher_id, String publisher_name, Date created_date, String created_user, Date updated_date, String updated_user, Date deleted_date, String deleted_user, boolean is_deleted) {
        this.publisher_id = publisher_id;
        this.publisher_name = publisher_name;
        this.created_date = created_date;
        this.created_user = created_user;
        this.updated_date = updated_date;
        this.updated_user = updated_user;
        this.deleted_date = deleted_date;
        this.deleted_user = deleted_user;
        this.is_deleted = is_deleted;
    }

    public List<BookEdition> getBookEdition() {
        return bookEdition;
    }

    public void setBookEdition(List<BookEdition> bookEdition) {
        this.bookEdition = bookEdition;
    }

    public long getPublisher_id() {
        return publisher_id;
    }

    public void setPublisher_id(long publisher_id) {
        this.publisher_id = publisher_id;
    }

    public String getPublisher_name() {
        return publisher_name;
    }

    public void setPublisher_name(String publisher_name) {
        this.publisher_name = publisher_name;
    }

    public Date getCreated_date() {
        return created_date;
    }

    public void setCreated_date(Date created_date) {
        this.created_date = created_date;
    }

    public String getCreated_user() {
        return created_user;
    }

    public void setCreated_user(String created_user) {
        this.created_user = created_user;
    }

    public Date getUpdated_date() {
        return updated_date;
    }

    public void setUpdated_date(Date updated_date) {
        this.updated_date = updated_date;
    }

    public String getUpdated_user() {
        return updated_user;
    }

    public void setUpdated_user(String updated_user) {
        this.updated_user = updated_user;
    }

    public Date getDeleted_date() {
        return deleted_date;
    }

    public void setDeleted_date(Date deleted_date) {
        this.deleted_date = deleted_date;
    }

    public String getDeleted_user() {
        return deleted_user;
    }

    public void setDeleted_user(String deleted_user) {
        this.deleted_user = deleted_user;
    }

    public boolean getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(boolean is_deleted) {
        this.is_deleted = is_deleted;
    }


}
