package com.group5.se1746ks_lms.model;

import java.sql.Timestamp;
import java.util.Date;

public class BookEdition {
    private String ISBN;
    private long book_id;
    private Long publisher_id;
    private int publish_year;
    private String language;
    private Timestamp created_date;
    private String created_user;
    private Timestamp updated_date;
    private String updated_user;
    private Timestamp deleted_date;
    private String deleted_user;
    private boolean is_deleted;
    private int total_copies;
    private int available_copies;
    private int borrowed_copies;

public BookEdition() {
}

    public BookEdition(String ISBN, long book_id, Long publisher_id, int publish_year, String language, Timestamp created_date, String created_user, Timestamp updated_date, String updated_user, Timestamp deleted_date, String deleted_user, boolean is_deleted, int total_copies, int available_copies, int borrowed_copies) {
        this.ISBN = ISBN;
        this.book_id = book_id;
        this.publisher_id = publisher_id;
        this.publish_year = publish_year;
        this.language = language;
        this.created_date = created_date;
        this.created_user = created_user;
        this.updated_date = updated_date;
        this.updated_user = updated_user;
        this.deleted_date = deleted_date;
        this.deleted_user = deleted_user;
        this.is_deleted = is_deleted;
        this.total_copies = total_copies;
        this.available_copies = available_copies;
        this.borrowed_copies = borrowed_copies;
    }

    public BookEdition(String ISBN, long book_id, Long publisher_id, int publish_year, String language, Timestamp created_date, String created_user, Timestamp updated_date, String updated_user, Timestamp deleted_date, String deleted_user, boolean is_deleted) {
        this.ISBN = ISBN;
        this.book_id = book_id;
        this.publisher_id = publisher_id;
        this.publish_year = publish_year;
        this.language = language;
        this.created_date = created_date;
        this.created_user = created_user;
        this.updated_date = updated_date;
        this.updated_user = updated_user;
        this.deleted_date = deleted_date;
        this.deleted_user = deleted_user;
        this.is_deleted = is_deleted;
    }

    public BookEdition(String ISBN, long book_id, Long publisher_id, int publish_year, String language) {
        this.ISBN = ISBN;
        this.book_id = book_id;
        this.publisher_id = publisher_id;
        this.publish_year = publish_year;
        this.language = language;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public long getBook_id() {
        return book_id;
    }

    public void setBook_id(long book_id) {
        this.book_id = book_id;
    }


    public Long getPublisher_id() {
        return publisher_id;
    }

    public void setPublisher_id(Long publisher_id) {
        this.publisher_id = publisher_id;
    }

    public int getPublish_year() {
        return publish_year;
    }

    public void setPublish_year(int publish_year) {
        this.publish_year = publish_year;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Date getCreated_date() {
        return created_date;
    }

    public void setCreated_date(Timestamp created_date) {
        this.created_date = created_date;
    }

    public String getCreated_user() {
        return created_user;
    }

    public void setCreated_user(String created_user) {
        this.created_user = created_user;
    }

    public Date getUpdated_date() {
        return updated_date;
    }

    public void setUpdated_date(Timestamp updated_date) {
        this.updated_date = updated_date;
    }

    public String getUpdated_user() {
        return updated_user;
    }

    public void setUpdated_user(String updated_user) {
        this.updated_user = updated_user;
    }

    public Date getDeleted_date() {
        return deleted_date;
    }

    public void setDeleted_date(Timestamp deleted_date) {
        this.deleted_date = deleted_date;
    }

    public String getDeleted_user() {
        return deleted_user;
    }

    public void setDeleted_user(String deleted_user) {
        this.deleted_user = deleted_user;
    }

    public boolean getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(boolean is_deleted) {
        this.is_deleted = is_deleted;
    }

    public boolean isIs_deleted() {
        return is_deleted;
    }

    public int getTotal_copies() {
        return total_copies;
    }

    public void setTotal_copies(int total_copies) {
        this.total_copies = total_copies;
    }

    public int getAvailable_copies() {
        return available_copies;
    }

    public void setAvailable_copies(int available_copies) {
        this.available_copies = available_copies;
    }

    public int getBorrowed_copies() {
        return borrowed_copies;
    }

    public void setBorrowed_copies(int borrowed_copies) {
        this.borrowed_copies = borrowed_copies;
    }
}
