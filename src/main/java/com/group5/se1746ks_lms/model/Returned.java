package com.group5.se1746ks_lms.model;

import java.sql.Timestamp;

public class Returned {
    int returned_id;
    int loans_id;
    String book_condition;
    Timestamp created_date;
    String created_user;
    Timestamp updated_date;
    String updated_user;

    public Returned(int returned_id, int loans_id, String book_condition, Timestamp created_date, String created_user, Timestamp updated_date, String updated_user) {
        this.returned_id = returned_id;
        this.loans_id = loans_id;
        this.book_condition = book_condition;
        this.created_date = created_date;
        this.created_user = created_user;
        this.updated_date = updated_date;
        this.updated_user = updated_user;
    }

    public int getReturned_id() {
        return returned_id;
    }

    public void setReturned_id(int returned_id) {
        this.returned_id = returned_id;
    }

    public int getLoans_id() {
        return loans_id;
    }

    public void setLoans_id(int loans_id) {
        this.loans_id = loans_id;
    }

    public String getBook_condition() {
        return book_condition;
    }

    public void setBook_condition(String book_condition) {
        this.book_condition = book_condition;
    }

    public Timestamp getCreated_date() {
        return created_date;
    }

    public void setCreated_date(Timestamp created_date) {
        this.created_date = created_date;
    }

    public String getCreated_user() {
        return created_user;
    }

    public void setCreated_user(String created_user) {
        this.created_user = created_user;
    }

    public Timestamp getUpdated_date() {
        return updated_date;
    }

    public void setUpdated_date(Timestamp updated_date) {
        this.updated_date = updated_date;
    }

    public String getUpdated_user() {
        return updated_user;
    }

    public void setUpdated_user(String updated_user) {
        this.updated_user = updated_user;
    }
}
