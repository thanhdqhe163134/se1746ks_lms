package com.group5.se1746ks_lms.model;

import java.sql.Timestamp;
import java.util.Date;

public class Loans {
    private int loans_id, user_id, book_id;
    private Date  due_date, request_date, expried_date;

    private Timestamp borrow_date, return_date,  create_date, update_date, updated_user;
    private String create_user, book_detail_id, status;
    private boolean is_returned, is_approved;
    private String title, img;

    private String student_id;

    private boolean is_request;

    private Date new_due_date;

    private long request_id;

    private String request_status;

    public Loans() {
    }

    public Loans(int loans_id, int user_id, String book_detail_id, Timestamp borrow_date, Date due_date, Timestamp return_date, String status, Timestamp create_date, Date request_date, Date expried_date, String create_user, boolean is_returned, boolean is_approved, boolean is_request, Date new_due_date, String request_status) {
        this.loans_id = loans_id;
        this.user_id = user_id;
        this.book_detail_id = book_detail_id;
        this.book_id = book_id;
        this.borrow_date = borrow_date;
        this.due_date = due_date;
        this.return_date = return_date;
        this.status = status;
        this.create_date = create_date;
        this.request_date = request_date;
        this.expried_date = expried_date;
        this.create_user = create_user;
        this.is_returned = is_returned;
        this.is_approved = is_approved;
        this.is_request = is_request;
        this.new_due_date = new_due_date;
        this.request_status = request_status;
    }

    public Loans(int loans_id, int user_id, String book_code, Timestamp borrow_date, Date due_date, Timestamp return_date, String status, Timestamp create_date, Date expiry, String create_user, boolean is_returned, boolean is_approved) {
        this.loans_id = loans_id;
        this.user_id = user_id;
        this.book_detail_id = book_code; // Đã thay đổi tên tham số từ book_code thành book_detail_id để phù hợp với lớp Loans
        this.borrow_date = borrow_date;
        this.due_date = due_date;
        this.return_date = return_date;
        this.status = status;
        this.create_date = create_date;
        this.expried_date = expiry; // Đã thay đổi tên tham số từ expiry thành expried_date để phù hợp với lớp Loans
        this.create_user = create_user;
        this.is_returned = is_returned;
        this.is_approved = is_approved;
        this.is_request = false; // Giá trị mặc định cho is_request là false

    }


    public Loans(int user_id, String book_detail_id, Timestamp borrow_date, Date due_date, Timestamp return_date, String status, Timestamp create_date, Date request_date, Date expried_date, String create_user, boolean is_returned, boolean is_approved, boolean is_request) {
        this.user_id = user_id;
        this.book_detail_id = book_detail_id;
        this.book_id = book_id;
        this.borrow_date = borrow_date;
        this.due_date = due_date;
        this.return_date = return_date;
        this.status = status;
        this.create_date = create_date;
        this.request_date = request_date;
        this.expried_date = expried_date;
        this.create_user = create_user;
        this.is_returned = is_returned;
        this.is_approved = is_approved;
        this.is_request = is_request;
    }
    public Loans(int loans_id, int user_id, String book_detail_id, int book_id, String title, String img, Timestamp borrow_date, Date due_date, Timestamp return_date, String status, Timestamp create_date, Date request_date, Date expried_date, String create_user, boolean is_returned, boolean is_approved, boolean is_request, Date new_due_date, long request_id
    ) {
        this.loans_id = loans_id;
        this.user_id = user_id;
        this.book_detail_id = book_detail_id;
        this.book_id = book_id;
        this.title = title;
        this.img = img;
        this.borrow_date = borrow_date;
        this.due_date = due_date;
        this.return_date = return_date;
        this.status = status;
        this.create_date = create_date;
        this.request_date = request_date;
        this.expried_date = expried_date;
        this.create_user = create_user;
        this.is_returned = is_returned;
        this.is_approved = is_approved;
        this.is_request = is_request;
        this.new_due_date = new_due_date;
        this.request_id = request_id;
    }

    public Loans(int parseInt, String book, Timestamp valueOf, java.sql.Date valueOf1, Timestamp valueOf2, Date valueOf3, String status, boolean b, boolean b1, boolean parseBoolean) {
        this.user_id = parseInt;
        this.book_detail_id = book;
        this.borrow_date = valueOf;
        this.due_date = valueOf1;
        this.create_date = valueOf2;
        this.expried_date = valueOf3;
        this.status = status;
        this.is_returned = b;
        this.is_approved = b1;
        this.is_request = parseBoolean;

    }

    public int getLoans_id() {
        return loans_id;
    }

    public void setLoans_id(int loans_id) {
        this.loans_id = loans_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getBook_id() {
        return book_id;
    }

    public void setBook_id(int book_id) {
        this.book_id = book_id;
    }

    public Date getDue_date() {
        return due_date;
    }

    public void setDue_date(Date due_date) {
        this.due_date = due_date;
    }

    public Date getRequest_date() {
        return request_date;
    }

    public void setRequest_date(Date request_date) {
        this.request_date = request_date;
    }

    public Date getExpried_date() {
        return expried_date;
    }

    public void setExpried_date(Date expried_date) {
        this.expried_date = expried_date;
    }

    public Timestamp getBorrow_date() {
        return borrow_date;
    }

    public void setBorrow_date(Timestamp borrow_date) {
        this.borrow_date = borrow_date;
    }

    public Timestamp getReturn_date() {
        return return_date;
    }

    public void setReturn_date(Timestamp return_date) {
        this.return_date = return_date;
    }

    public Timestamp getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Timestamp create_date) {
        this.create_date = create_date;
    }

    public String getCreate_user() {
        return create_user;
    }

    public void setCreate_user(String create_user) {
        this.create_user = create_user;
    }

    public String getBook_detail_id() {
        return book_detail_id;
    }

    public void setBook_detail_id(String book_detail_id) {
        this.book_detail_id = book_detail_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean getIs_returned() {
        return is_returned;
    }

    public void setIs_returned(boolean is_returned) {
        this.is_returned = is_returned;
    }

    public boolean getIs_approved() {
        return is_approved;
    }

    public void setIs_approved(boolean is_approved) {
        this.is_approved = is_approved;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public boolean getIs_request() {
        return is_request;
    }

    public void setIs_request(boolean is_request) {
        this.is_request = is_request;
    }

    public Timestamp getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Timestamp update_date) {
        this.update_date = update_date;
    }

    public Timestamp getUpdated_user() {
        return updated_user;
    }

    public void setUpdated_user(Timestamp updated_user) {
        this.updated_user = updated_user;
    }

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public Date getNew_due_date() {
        return new_due_date;
    }

    public void setNew_due_date(Date new_due_date) {
        this.new_due_date = new_due_date;
    }

    public long getRequest_id() {
        return request_id;
    }

    public void setRequest_id(long request_id) {
        this.request_id = request_id;
    }

    public String getRequest_status() {
        return request_status;
    }

    public void setRequest_status(String request_status) {
        this.request_status = request_status;
    }
}

