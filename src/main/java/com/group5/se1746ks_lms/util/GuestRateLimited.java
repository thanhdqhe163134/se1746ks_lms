package com.group5.se1746ks_lms.util;

import jakarta.servlet.http.HttpSession;

public class GuestRateLimited {
    private static final long WAIT_TIME_MILLIS = 5 * 1000; // 5 giây
    private static final long PENALTY_TIME_MILLIS =  60 * 1000; // 1 phút
    private static final int MAX_REQUESTS = 5;
    public static String RateLimited(HttpSession session) {
        Integer requestCount = (Integer) session.getAttribute("requestCount");
        Long lastRequestTime = (Long) session.getAttribute("lastRequestTime");

        if (requestCount == null) {
            requestCount = 0;
        }

        long currentTime = System.currentTimeMillis();
        long timeSinceLastRequest = lastRequestTime != null ? currentTime - lastRequestTime : Long.MAX_VALUE;

        if (requestCount > MAX_REQUESTS && timeSinceLastRequest < PENALTY_TIME_MILLIS) {
            return "You have made too many requests. Please try again in  " + (PENALTY_TIME_MILLIS - timeSinceLastRequest)/1000  + " seconds.";
        } else if (timeSinceLastRequest < WAIT_TIME_MILLIS) {
            return "Please wait " + (WAIT_TIME_MILLIS - timeSinceLastRequest) / 1000 + " seconds before making another request.";
        }

        // Tăng số lần yêu cầu và cập nhật thời gian của lần yêu cầu cuối cùng
        requestCount++;
        session.setAttribute("requestCount", requestCount);
        session.setAttribute("lastRequestTime", currentTime);

        return null; // không có lỗi
    }



}
