package com.group5.se1746ks_lms.util;

import com.group5.se1746ks_lms.dao.AccountDAO;
import com.group5.se1746ks_lms.dao.BookDetailDAO;
import com.group5.se1746ks_lms.dao.LoansDAO;
import com.group5.se1746ks_lms.model.Account;
import com.group5.se1746ks_lms.model.Loans;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class DailyTaskScheduler {
    private static final int INITIAL_DELAY = 0; // No delay for the first execution
    private static final int PERIOD = 24; // Repeat every 24 hours

    public void startDailyTask() {
        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();

        Runnable task = () -> {
            LoansDAO loansDAO = new LoansDAO();
            AccountDAO accountDAO = new AccountDAO();
            BookDetailDAO bookDetailDAO = new BookDetailDAO();
            Account account = new Account();
            List<Long> loansIdList = loansDAO.getOverdueLoans();
            if (loansIdList.size() > 0) {
                for (Long loansId : loansIdList) {
                    loansDAO.updateStatus(loansId, "Overdue", "System");
                    account = accountDAO.getAccountByLoansId(loansId);
                    if (account != null && account.getEmail() != null) {
                        String subject = "Overdue Notification";
                        String link = "http://localhost:8080/library/home";
                        String body = "<strong> <p>Hello, " + account.getFull_name() + "</p> </strong>" +
                                "<p>Your book is overdue. Please check which book borrow is overdue in</p>" +
                                "<strong><p><a href='" + link + "' style='color: #4CAF50; text-decoration: none; font-weight: bold;'>FPTU Library Website</a></p></strong>" +
                                "<p>Thank you</p>" +
                                "<strong><p>FPTU Library</p></strong>";
                        EmailUtil.sendEmail(account.getEmail(), subject, body);
                    }
                }
            }
            loansIdList = loansDAO.getCloseToDueDateLoans();
            if (loansIdList.size() > 0) {
                for (Long loansId : loansIdList) {
                    account = accountDAO.getAccountByLoansId(loansId);
                    if (account != null && account.getEmail() != null) {
                        String subject = "Due Date Notification";
                        String link = "http://localhost:8080/library/home";
                        String body = "<strong> <p>Hello, " + account.getFull_name() + "</p> </strong>" +
                                "<p>Your book is close to due date. Please check which book borrow is close to due date in</p>" +
                                "<strong><p><a href='" + link + "' style='color: #4CAF50; text-decoration: none; font-weight: bold;'>FPTU Library Website</a></p></strong>" +
                                "<p>Thank you</p>" +
                                "<strong><p>FPTU Library</p></strong>";
                        EmailUtil.sendEmail(account.getEmail(), subject, body);
                    }
                }
            }
            loansIdList = loansDAO.getExpriedLoans();
            if (loansIdList.size() > 0) {
                for (Long loansId : loansIdList) {
                    loansDAO.updateStatus2(loansId, "Expired", "System");
                    bookDetailDAO.updateBookDetailIsAvailable(loansId);
                    account = accountDAO.getAccountByLoansId(loansId);
                    if (account != null && account.getEmail() != null) {
                        String subject = "Expried Notification";
                        String link = "http://localhost:8080/library/home";
                        String body = "<strong> <p>Hello, " + account.getFull_name() + "</p> </strong>" +
                                "<p>Your book is expried. You need to borrow that book again if you want</p>" +
                                "<strong><p><a href='" + link + "' style='color: #4CAF50; text-decoration: none; font-weight: bold;'>FPTU Library Website</a></p></strong>" +
                                "<p>Thank you</p>" +
                                "<strong><p>FPTU Library</p></strong>";
                        EmailUtil.sendEmail(account.getEmail(), subject, body);
                    }
                }
            }

        };


                // Calculate the delay until 12:00 AM from the current time
                long delay = calculateDelayUntilMidnight();

                scheduler.scheduleAtFixedRate(task, INITIAL_DELAY, PERIOD, TimeUnit.HOURS);
            }



    private long calculateDelayUntilMidnight() {
        Calendar midnight = Calendar.getInstance();
        midnight.add(Calendar.DAY_OF_MONTH, 1);
        midnight.set(Calendar.HOUR_OF_DAY, 0);
        midnight.set(Calendar.MINUTE, 0);
        midnight.set(Calendar.SECOND, 0);
        midnight.set(Calendar.MILLISECOND, 0);

        long delayMillis = midnight.getTimeInMillis() - System.currentTimeMillis();
        return TimeUnit.MILLISECONDS.toHours(delayMillis);
    }


    public void stop() {

    }
}

