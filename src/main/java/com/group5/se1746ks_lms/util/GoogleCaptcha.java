package com.group5.se1746ks_lms.util;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.Response;

public class GoogleCaptcha {

    public boolean verifyCaptcha(String token) {
        String secretKey = "6LeIeOwoAAAAACOLFr8UNMSvrOfHCRim0wAVSMIV";
        String apiUrl = "https://www.google.com/recaptcha/api/siteverify";

        Form formData = new Form();
        formData.param("secret", secretKey);
        formData.param("response", token);

        Client client = ClientBuilder.newClient();
        Response response = client.target(apiUrl).request().post(Entity.form(formData));

        if (response.getStatus() == 200) {
            String body = response.readEntity(String.class);
            return body.contains("\"success\": true");
        } else {
            return false;
        }
    }

}
