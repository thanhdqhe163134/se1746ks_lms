package com.group5.se1746ks_lms.util;

import com.group5.se1746ks_lms.model.Account;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.classic.methods.HttpUriRequestBase;
import org.apache.hc.client5.http.entity.UrlEncodedFormEntity;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.NameValuePair;
import org.apache.hc.core5.http.ParseException;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.apache.hc.core5.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GoogleOAuth {
    public Account getUserInforFromToken(String code) {
        String clientId = "213027209983-jis4nemjiibhbpp19rbvoq929sp207dv.apps.googleusercontent.com";
        String clientSecret = "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";
        String redirectURI = "http://localhost:8080/library/google-login";
        String grantType = "authorization_code";

        // Step 1: Exchange authorization code for access token
        String tokenEndpoint = "https://oauth2.googleapis.com/token";

        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("code", code));
        params.add(new BasicNameValuePair("client_id", clientId));
        params.add(new BasicNameValuePair("client_secret", clientSecret));
        params.add(new BasicNameValuePair("redirect_uri", redirectURI));
        params.add(new BasicNameValuePair("grant_type", grantType));

        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpUriRequestBase request = new HttpPost(tokenEndpoint);
            request.setEntity(new UrlEncodedFormEntity(params));

            try (CloseableHttpResponse response = httpClient.execute(request)) {
                String jsonResponse = EntityUtils.toString(response.getEntity());
                JSONObject jsonObject = new JSONObject(jsonResponse);
                String accessToken = jsonObject.getString("access_token");

                // Step 2: Use access token to fetch user info
                String userInfoEndpoint = "https://www.googleapis.com/oauth2/v1/userinfo?access_token=" + accessToken;
                try (CloseableHttpResponse userInfoResponse = httpClient.execute(new HttpGet(userInfoEndpoint))) {  // Changed this line
                    String userInfoJson = EntityUtils.toString(userInfoResponse.getEntity());
                    JSONObject userInfoObject = new JSONObject(userInfoJson);
                    Account account = new Account();
                    account.setEmail( userInfoObject.getString("email"));
                    account.setFull_name(userInfoObject.getString("name"));
                    account.setImg(userInfoObject.getString("picture"));

                    return account;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public String getGoogleLoginURL() {
        // Replace with your Google Client ID and redirect URL
        String clientId = "213027209983-jis4nemjiibhbpp19rbvoq929sp207dv.apps.googleusercontent.com";
        String redirectURI = "http://localhost:8080/library/google-login";
        String scope = "email profile";
        String accessType = "offline";
        String responseType = "code";
        String state = JWTUtil.createToken("some_random_data");

        return "https://accounts.google.com/o/oauth2/auth?" +
                "client_id=" + clientId +
                "&redirect_uri=" + redirectURI +
                "&scope=" + scope +
                "&access_type=" + accessType +
                "&response_type=" + responseType +
                "&state=" + state;
    }

}
