package com.group5.se1746ks_lms.dao;

import com.group5.se1746ks_lms.model.Account;
import com.group5.se1746ks_lms.util.DBConnect;

import java.sql.*;
import java.util.ArrayList;

public class AccountDAO extends DBConnect {
    Connection connection = DBConnect.getConnection();

    public Account selectAccountByUsername(String username) {
        String SELECT_ACCOUNT_BY_USERNAME = "SELECT * FROM Accounts WHERE username = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ACCOUNT_BY_USERNAME);
            preparedStatement.setString(1, username);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return mapToAccount(resultSet);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Account selectAccountByUserEmail(String email) {
        String SELECT_ACCOUNT_BY_USERNAME = "SELECT * FROM Accounts WHERE email = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ACCOUNT_BY_USERNAME);
            preparedStatement.setString(1, email);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return mapToAccount(resultSet);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Account mapToAccount(ResultSet resultSet) throws SQLException {
        Account account = new Account();
        account.setUser_id(resultSet.getLong("user_id"));
        account.setUsername(resultSet.getString("username"));
        account.setPassword(resultSet.getString("password"));
        account.setStudent_id(resultSet.getString("student_id"));
        account.setFull_name(resultSet.getString("full_name"));
        account.setDob(resultSet.getDate("dob"));
        account.setEmail(resultSet.getString("email"));
        account.setPhone_number(resultSet.getString("phone_number"));
        account.setAddress(resultSet.getString("address"));
        account.setRole(resultSet.getString("role"));
        account.setImg(resultSet.getString("img"));
        account.setCreated_date(resultSet.getTimestamp("created_date"));
        account.setUpdated_date(resultSet.getTimestamp("updated_date"));
        account.setUpdated_user(resultSet.getString("updated_user"));
        account.setDeleted_date(resultSet.getTimestamp("deleted_date"));
        account.setDeleted_user(resultSet.getString("deleted_user"));
        account.setIs_deleted(resultSet.getBoolean("is_deleted"));
        return account;
    }

    public boolean existsByUsername(String username) {
        String QUERY = "SELECT * FROM Accounts WHERE username = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(QUERY);
            preparedStatement.setString(1, username);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                return resultSet.next();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void createAccount(String username, String hashedPassword, String role) {
        String INSERT_ADMIN = "INSERT INTO Accounts (username, password, role) VALUES (?, ?, ?)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_ADMIN);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, hashedPassword);
            preparedStatement.setString(3, role);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updatePassword(String username, String hashedPassword, String role) {
        String UPDATE_PASSWORD = "UPDATE Accounts SET password = ?, role = ? WHERE username = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_PASSWORD);
            preparedStatement.setString(1, hashedPassword);
            preparedStatement.setString(2, role);
            preparedStatement.setString(3, username);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Account> getAccountListPerPage(int currentPage, int itemsPerPage) {
        ArrayList<Account> acc = new ArrayList<>();
        String sqlStr = "SELECT * FROM Accounts WHERE is_deleted = 0 ORDER BY user_id OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        try {
            PreparedStatement pstm = connection.prepareStatement(sqlStr);
            int offset = (currentPage - 1) * itemsPerPage;
            pstm.setInt(1, offset);
            pstm.setInt(2, itemsPerPage);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                Account a = mapToAccount(rs);
                acc.add(a);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return acc;
    }

    public ArrayList<Account> getAccountList() {
        ArrayList<Account> acc = new ArrayList<>();
        String sqlStr = "SELECT * FROM Accounts WHERE is_deleted = 0";
        try {
            PreparedStatement pstm = connection.prepareStatement(sqlStr);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                Account a = mapToAccount(rs);
                acc.add(a);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return acc;
    }

    public Account getAccountById(String id) {
        String sqlStr = "SELECT * FROM Accounts WHERE user_id = ?";
        try {
            PreparedStatement pstm = connection.prepareStatement(sqlStr);
            pstm.setInt(1, Integer.parseInt(id));
            ResultSet rs = pstm.executeQuery();
            if (rs.next()) {
                Account a = mapToAccount(rs);
                return a;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<Account> sortAccountList(String sort, int currentPage, int itemsPerPage) {
        ArrayList<Account> acc = new ArrayList<>();
        String sqlStr = "SELECT * FROM Accounts WHERE is_deleted = 0 ORDER BY created_date " + sort + " OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        try {
            PreparedStatement pstm = connection.prepareStatement(sqlStr);
            int offset = (currentPage - 1) * itemsPerPage;
            pstm.setInt(1, offset);
            pstm.setInt(2, itemsPerPage);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                Account a = mapToAccount(rs);
                acc.add(a);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return acc;
    }

    public void createNewAccount(Account a) {
        String sqlStr = "INSERT INTO Accounts (username, password, student_id, full_name, dob, email, phone_number, address, role, created_date, is_deleted) VALUES (?,?, ?,?, ?, ?, ?, ?, ?, ?, ?)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            preparedStatement.setString(1, a.getUsername());
            preparedStatement.setString(2, a.getPassword());
            preparedStatement.setString(3, a.getStudent_id());
            preparedStatement.setString(4, a.getFull_name());
            preparedStatement.setString(5, String.valueOf(a.getDob()));
            preparedStatement.setString(6, a.getEmail());
            preparedStatement.setString(7, a.getPhone_number());
            preparedStatement.setString(8, a.getAddress());
            preparedStatement.setString(9, a.getRole());
            preparedStatement.setString(10, String.valueOf(a.getCreated_date()));
            preparedStatement.setBoolean(11, a.getIs_deleted());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateAccount(String id, String username, String student_id, String fullname, Date dob, String email, String phone, String address, String role, Date update_date, String update_user) {
        String sqlStr = "UPDATE Accounts SET username = ?, student_id = ?, full_name = ?, dob = ?, email = ?, phone_number = ?, address = ?," +
                "role = ?, updated_date = ?, updated_user = ? WHERE user_id = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            preparedStatement.setString(11, id);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, student_id);
            preparedStatement.setString(3, fullname);
            preparedStatement.setDate(4, dob);
            preparedStatement.setString(5, email);
            preparedStatement.setString(6, phone);
            preparedStatement.setString(7, address);
            preparedStatement.setString(8, role);
            preparedStatement.setDate(9, update_date);
            preparedStatement.setString(10, update_user);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAccount(String id) {
        String sqlStr = "UPDATE Accounts SET is_deleted = 1 WHERE user_id = ?";
        try {
            PreparedStatement pstm = connection.prepareStatement(sqlStr);
            pstm.setInt(1, Integer.parseInt(id));
            int affectedRows = pstm.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Account> searchAccountByUsername(String username) {
        ArrayList<Account> acc = new ArrayList<>();
        String sqlStr = "SELECT * FROM Accounts WHERE username LIKE ? AND is_deleted = 0";
        try {
            PreparedStatement pstm = connection.prepareStatement(sqlStr);
            pstm.setString(1, "%" + username + "%");
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                Account a = mapToAccount(rs);
                acc.add(a);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return acc;
    }

    public void changePassword (String id, String hashPassword) {
        String sqlStr = "UPDATE Accounts SET [password] = ? WHERE user_id = ?";
        try {
            PreparedStatement pstm = connection.prepareStatement(sqlStr);
            pstm.setInt(2, Integer.parseInt(id));
            pstm.setString(1, hashPassword);
            int affectedRows = pstm.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getEmailByUsername(String username) {
        String QUERY = "SELECT email FROM Accounts WHERE username = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(QUERY);
            preparedStatement.setString(1, username);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return resultSet.getString("email");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void updatePasswordByUsername(String username, String password) {
        String QUERY = "UPDATE Accounts SET password = ? WHERE username = ?";
       try{
              PreparedStatement preparedStatement = connection.prepareStatement(QUERY);
              preparedStatement.setString(1, password);
              preparedStatement.setString(2, username);
              preparedStatement.executeUpdate();
         } catch (SQLException e) {
              e.printStackTrace();
       }


    }

    public boolean isRateLimited(long userId) throws InterruptedException {
        int MAX_REQUESTS = 10;
        int TIME_WINDOW = 60 * 1000; // 60s in milliseconds
        String sql = "SELECT request_count, last_request_time, failed_attempts FROM RateLimiting WHERE user_id = ?";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setLong(1, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()) {
                int request_count = resultSet.getInt("request_count");
                Timestamp last_request_time = resultSet.getTimestamp("last_request_time");
                int failedAttempts = resultSet.getInt("failed_attempts");
                long currentTime = System.currentTimeMillis();

                if (request_count > MAX_REQUESTS && (currentTime - last_request_time.getTime()) < TIME_WINDOW) {
                    // Exponential Backoff
                    int delay = (int) Math.pow(2, failedAttempts) * 1000;
                    Thread.sleep(delay);

                    // Tăng số lần thất bại
                    String updateSql = "UPDATE RateLimiting SET failed_attempts = failed_attempts + 1 WHERE user_id = ?";
                    PreparedStatement updateStmt = connection.prepareStatement(updateSql);
                    updateStmt.setLong(1, userId);
                    updateStmt.executeUpdate();

                    return true;
                } else {
                    // Cập nhật request_count, last_request_time và đặt lại failed_attempts
                    String updateSql = "UPDATE RateLimiting SET request_count = request_count + 1, last_request_time = CURRENT_TIMESTAMP, failed_attempts = 0 WHERE user_id = ?";
                    PreparedStatement updateStmt = connection.prepareStatement(updateSql);
                    updateStmt.setLong(1, userId);
                    updateStmt.executeUpdate();

                    return false;
                }
            } else {
                // Tạo bản ghi mới cho user_id này
                String insertSql = "INSERT INTO RateLimiting (user_id, request_count, last_request_time, failed_attempts) VALUES (?, 1, CURRENT_TIMESTAMP, 0)";
                PreparedStatement insertStmt = connection.prepareStatement(insertSql);
                insertStmt.setLong(1, userId);
                insertStmt.executeUpdate();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public void createAccountByUserInfor(Account account) {
        String sqlStr = "INSERT INTO Accounts (username, password, student_id,full_name, email, role , img, created_date, is_deleted) VALUES (?,?,?, ?, ?, ?, ?, ?, ?)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            preparedStatement.setString(1, account.getUsername());
            preparedStatement.setString(2, account.getPassword());
            preparedStatement.setString(3, account.getStudent_id());
            preparedStatement.setString(4, account.getFull_name());
            preparedStatement.setString(5, account.getEmail());
            preparedStatement.setString(6, account.getRole());
            preparedStatement.setString(7, account.getImg());
            preparedStatement.setTimestamp(8, account.getCreated_date());
            preparedStatement.setBoolean(9, account.getIs_deleted());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public Account selectAccountByEmail(String userEmail) {
        String SELECT_ACCOUNT_BY_EMAIL = "SELECT * FROM Accounts WHERE email = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ACCOUNT_BY_EMAIL);
            preparedStatement.setString(1, userEmail);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return mapToAccount(resultSet);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int CountLoans(long userId) {
        String sql = "SELECT COUNT(*) AS count FROM Loans WHERE user_id = ? AND is_returned = 0";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setLong(1, userId);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return resultSet.getInt("count");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int CountRequest(long user_id, long loans_id) {
        String sql = "SELECT COUNT(*) AS count FROM Request INNER JOIN Loans ON Request.loan_id = Loans.loan_id WHERE Loans.user_id = ? AND loans.loan_id = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setLong(1, user_id);
            preparedStatement.setLong(2, loans_id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return resultSet.getInt("count");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public Account getAccountByLoansId(Long loansId) {
        String sql = "SELECT * FROM Accounts INNER JOIN Loans ON Accounts.user_id = Loans.user_id WHERE Loans.loan_id = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setLong(1, loansId);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return mapToAccount(resultSet);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}

