package com.group5.se1746ks_lms.dao;

import com.group5.se1746ks_lms.model.Fine;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.group5.se1746ks_lms.util.DBConnect.getConnection;

public class FineDAO {
    Connection connection = getConnection();


    public List<Fine> getFineByUserId(int user_id) {
        String sql = "SELECT f.fine_id, f.loan_id, f.fine_amount, f.description, f.created_date, f.created_user, f.updated_date, f.updated_user, f.is_paid FROM fine f JOIN loans l ON f.loan_id = l.loan_id WHERE l.user_id = ? AND f.is_paid = 0";
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setLong(1, user_id);
            ResultSet rs = preparedStatement.executeQuery();
            List<Fine> fineList = new ArrayList<>();
            while (rs.next()) {
                Fine fine = new Fine();
                fine.setFine_id(rs.getInt("fine_id"));
                fine.setLoans_id(rs.getInt("loan_id"));
                fine.setFine_amount(rs.getInt("fine_amount"));
                fine.setDescription(rs.getString("description"));
                fine.setCreated_date(rs.getTimestamp("created_date"));
                fine.setCreated_user(rs.getString("created_user"));
                fine.setPaid_date(rs.getTimestamp("updated_date"));
                fine.setUpdated_user(rs.getString("updated_user"));
                fine.setIs_paid(rs.getBoolean("is_paid"));
                fineList.add(fine);
            }
            return fineList;
        } catch (SQLException e) {
            e.printStackTrace();

        }
        return null;
    }

    public void addFine(Fine fine) {
        String sql = "INSERT INTO fine (loan_id, fine_amount, created_date, created_user, is_paid, description ) VALUES (?, ?, ?, ?, ?,?)";
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, fine.getLoans_id());
            preparedStatement.setDouble(2, fine.getFine_amount());
            preparedStatement.setTimestamp(3, fine.getCreated_date());
            preparedStatement.setString(4, fine.getCreated_user());
            preparedStatement.setBoolean(5, false);
            preparedStatement.setString(6, fine.getDescription());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public List<Fine> getAllFine() {
        String sql = "SELECT * FROM fine";
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet rs = preparedStatement.executeQuery();
            List<Fine> fineList = new ArrayList<>();
            while (rs.next()) {
                Fine fine = new Fine();
                fine.setFine_id(rs.getInt("fine_id"));
                fine.setLoans_id(rs.getInt("loan_id"));
                fine.setFine_amount(rs.getInt("fine_amount"));
                fine.setDescription(rs.getString("description"));
                fine.setCreated_date(rs.getTimestamp("created_date"));
                fine.setCreated_user(rs.getString("created_user"));
                fine.setPaid_date(rs.getTimestamp("updated_date"));
                fine.setUpdated_user(rs.getString("updated_user"));
                fine.setIs_paid(rs.getBoolean("is_paid"));
                fineList.add(fine);
            }
            return fineList;
        } catch (SQLException e) {
            e.printStackTrace();

        }
        return null;
    }

    public void payFine(String fine_id, String username) {
        String sql= "UPDATE fine SET is_paid = 1, updated_date = ?, updated_user = ? WHERE fine_id = ?";
        try {
            // Prepare the statement to update the fine
            PreparedStatement pstmt = connection.prepareStatement(sql);

            // Set the updated_date to the current timestamp
            pstmt.setTimestamp(1, new Timestamp(System.currentTimeMillis()));

            // Set the updated_user to the given username
            pstmt.setString(2, username);

            // Set the fine_id to the given fine_id
            pstmt.setString(3, fine_id);

            // Execute the update
            int affectedRows = pstmt.executeUpdate();

            if (affectedRows == 0) {
                // No rows affected, meaning the fine_id was not found
                throw new SQLException("Paying fine failed, no rows affected.");
            }

            // If we reach here, the fine has been paid successfully
        } catch (SQLException e) {
            // Handle any SQL errors
            e.printStackTrace();
            // Consider whether to rethrow the exception or handle it
        }
    }


    public int getTotalPages(int itemsPerPage) {
        String sql = "SELECT COUNT(*) FROM fine";
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            int totalItems = rs.getInt(1);
            int totalPages = (int) Math.ceil((double) totalItems / itemsPerPage);
            return totalPages;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public List<Fine> getAllFinePagingSort(int start, int itemsPerPage, String sortBy) {
        String sql ="SELECT * FROM fine ORDER BY created_date "+sortBy +"  OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, start);
            preparedStatement.setInt(2, itemsPerPage);
            ResultSet rs = preparedStatement.executeQuery();
            List<Fine> fineList = new ArrayList<>();
            while (rs.next()) {
                Fine fine = new Fine();
                fine.setFine_id(rs.getInt("fine_id"));
                fine.setLoans_id(rs.getInt("loan_id"));
                fine.setFine_amount(rs.getInt("fine_amount"));
                fine.setDescription(rs.getString("description"));
                fine.setCreated_date(rs.getTimestamp("created_date"));
                fine.setCreated_user(rs.getString("created_user"));
                fine.setPaid_date(rs.getTimestamp("updated_date"));
                fine.setUpdated_user(rs.getString("updated_user"));
                fine.setIs_paid(rs.getBoolean("is_paid"));
                fineList.add(fine);
            }
            return fineList;
        } catch (SQLException e) {
            e.printStackTrace();

        }
        return null;
    }

    public List<Fine> getAllFinePaging(int start, int itemsPerPage) {
        String sql ="SELECT * FROM fine ORDER BY created_date DESC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, start);
            preparedStatement.setInt(2, itemsPerPage);
            ResultSet rs = preparedStatement.executeQuery();
            List<Fine> fineList = new ArrayList<>();
            while (rs.next()) {
                Fine fine = new Fine();
                fine.setFine_id(rs.getInt("fine_id"));
                fine.setLoans_id(rs.getInt("loan_id"));
                fine.setFine_amount(rs.getInt("fine_amount"));
                fine.setDescription(rs.getString("description"));
                fine.setCreated_date(rs.getTimestamp("created_date"));
                fine.setCreated_user(rs.getString("created_user"));
                fine.setPaid_date(rs.getTimestamp("updated_date"));
                fine.setUpdated_user(rs.getString("updated_user"));
                fine.setIs_paid(rs.getBoolean("is_paid"));
                fineList.add(fine);
            }
            return fineList;
        } catch (SQLException e) {
            e.printStackTrace();

        }
        return null;
    }

    public List<Fine> getFineByUserIdPagingSort(int parseInt, int start, int itemsPerPage, String sortBy) {
        String sql ="SELECT * FROM fine f JOIN loans l ON f.loan_id = l.loan_id WHERE l.user_id = ? ORDER BY f.created_date "+sortBy +"  OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, parseInt);
            preparedStatement.setInt(2, start);
            preparedStatement.setInt(3, itemsPerPage);
            ResultSet rs = preparedStatement.executeQuery();
            List<Fine> fineList = new ArrayList<>();
            while (rs.next()) {
                Fine fine = new Fine();
                fine.setFine_id(rs.getInt("fine_id"));
                fine.setLoans_id(rs.getInt("loan_id"));
                fine.setFine_amount(rs.getInt("fine_amount"));
                fine.setDescription(rs.getString("description"));
                fine.setCreated_date(rs.getTimestamp("created_date"));
                fine.setCreated_user(rs.getString("created_user"));
                fine.setPaid_date(rs.getTimestamp("updated_date"));
                fine.setUpdated_user(rs.getString("updated_user"));
                fine.setIs_paid(rs.getBoolean("is_paid"));
                fineList.add(fine);
            }
            return fineList;
        } catch (SQLException e) {
            e.printStackTrace();

        }
        return null;
    }

    public List<Fine> getFineByUserIdPaging(int parseInt, int start, int itemsPerPage) {
        String sql ="SELECT * FROM fine f JOIN loans l ON f.loan_id = l.loan_id WHERE l.user_id = ? ORDER BY f.created_date DESC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, parseInt);
            preparedStatement.setInt(2, start);
            preparedStatement.setInt(3, itemsPerPage);
            ResultSet rs = preparedStatement.executeQuery();
            List<Fine> fineList = new ArrayList<>();
            while (rs.next()) {
                Fine fine = new Fine();
                fine.setFine_id(rs.getInt("fine_id"));
                fine.setLoans_id(rs.getInt("loan_id"));
                fine.setFine_amount(rs.getInt("fine_amount"));
                fine.setDescription(rs.getString("description"));
                fine.setCreated_date(rs.getTimestamp("created_date"));
                fine.setCreated_user(rs.getString("created_user"));
                fine.setPaid_date(rs.getTimestamp("updated_date"));
                fine.setUpdated_user(rs.getString("updated_user"));
                fine.setIs_paid(rs.getBoolean("is_paid"));
                fineList.add(fine);
            }
            return fineList;
        } catch (SQLException e) {
            e.printStackTrace();

        }
        return null;
    }

    public int getTotalPagesByUserId(int parseInt, int itemsPerPage) {
        String sql = "SELECT COUNT(*) FROM fine f JOIN loans l ON f.loan_id = l.loan_id WHERE l.user_id = ?";
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, parseInt);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            int totalItems = rs.getInt(1);
            int totalPages = (int) Math.ceil((double) totalItems / itemsPerPage);
            return totalPages;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
}

