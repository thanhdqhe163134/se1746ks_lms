package com.group5.se1746ks_lms.dao;

import com.group5.se1746ks_lms.model.BookDetail;
import com.group5.se1746ks_lms.util.DBConnect;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BookDetailDAO extends DBConnect {
    Connection connection = getConnection();

    public ArrayList<BookDetail> getBookDetail() {
        ArrayList<BookDetail> bookDetails = new ArrayList<>();
        return bookDetails;
    }

    public List<BookDetail> getBookDetailByISBN(String isbn) {
        List<BookDetail> bookDetails = new ArrayList<>();
        String sql = "SELECT * FROM BookDetail WHERE ISBN = ? AND is_borrowed = 0";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, isbn);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                BookDetail bookDetail = new BookDetail();
                bookDetail.setBook_detail_id(resultSet.getString("book_detail_id"));
                bookDetail.setISBN(resultSet.getString("ISBN"));
                bookDetail.setIs_borrowed(resultSet.getBoolean("is_borrowed"));
                bookDetail.setStatus(resultSet.getString("status"));
                bookDetail.setCreated_user(resultSet.getString("created_user"));
                bookDetail.setCreated_date(resultSet.getTimestamp("created_date"));
                bookDetail.setUpdated_user(resultSet.getString("updated_user"));
                bookDetail.setUpdated_date(resultSet.getTimestamp("updated_date"));
                if(!bookDetail.getIs_borrowed()){
                    bookDetails.add(bookDetail);
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return bookDetails;
    }

    public List<BookDetail> getBookDetailByISBN2(String isbn) {
        List<BookDetail> bookDetails = new ArrayList<>();
        String sql = "SELECT * FROM BookDetail WHERE ISBN = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, isbn);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                BookDetail bookDetail = new BookDetail();
                bookDetail.setBook_detail_id(resultSet.getString("book_detail_id"));
                bookDetail.setISBN(resultSet.getString("ISBN"));
                bookDetail.setIs_borrowed(resultSet.getBoolean("is_borrowed"));
                bookDetail.setStatus(resultSet.getString("status"));
                bookDetail.setCreated_user(resultSet.getString("created_user"));
                bookDetail.setCreated_date(resultSet.getTimestamp("created_date"));
                bookDetail.setUpdated_user(resultSet.getString("updated_user"));
                bookDetail.setUpdated_date(resultSet.getTimestamp("updated_date"));
                bookDetail.setIs_deleted(resultSet.getBoolean("is_deleted"));
                bookDetails.add(bookDetail);

            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return bookDetails;
    }

    public String getISBNWithMaxQuantity(String bookId) {
        String isbn = null;
        String sql = "SELECT TOP 1 be.ISBN\n" +
                "FROM BookDetail bd\n" +
                "INNER JOIN BookEdition be ON bd.ISBN = be.ISBN\n" +
                "WHERE be.book_id = ? AND bd.is_deleted = 0\n" +
                "GROUP BY be.ISBN\n" +
                "ORDER BY COUNT(*) DESC;";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, bookId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                isbn = resultSet.getString("ISBN");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return isbn;
    }

    public void updateBookDetailStatus(String book_detail_id, String username) {
        String sql = "UPDATE BookDetail SET is_borrowed = 0,  updated_user = ?, updated_date = ? WHERE book_detail_id = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, username);
            preparedStatement.setTimestamp(2, new java.sql.Timestamp(System.currentTimeMillis()));
            preparedStatement.setString(3, book_detail_id);
            preparedStatement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }



    public void updateBookDetailIsAvailable(Long loansId) {
        String sql = "UPDATE BookDetail SET is_borrowed = 0 WHERE book_detail_id = (SELECT book_detail_id FROM Loans WHERE loan_id = ?)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setLong(1, loansId);
            preparedStatement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void deleteBookDetail(String bookDetailId) {
        String sql = "UPDATE BookDetail SET is_deleted = 1 WHERE book_detail_id = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, bookDetailId);
            preparedStatement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

}
