package com.group5.se1746ks_lms.dao;

import com.group5.se1746ks_lms.model.Genres;
import com.group5.se1746ks_lms.util.DBConnect;

import java.sql.*;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class GenresDAO {

    public List<Genres> getAllGenres() {
        String QUERY = "SELECT * FROM Genres where is_deleted = 0";
        List<Genres> genresList = new LinkedList<>();
        try (Connection connection = DBConnect.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(QUERY);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                Genres genre = new Genres();
                genre.setGenre_id(resultSet.getLong("genre_id"));
                genre.setGenre_name(resultSet.getString("genre_name"));
                genresList.add(genre);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return genresList;
    }

    public Set<Genres> getGenresByIds(String[] ids) {
        String QUERY = "SELECT * FROM Genres WHERE genre_id = ? and is_deleted = 0";
        Set<Genres> genresSet = new HashSet<>();
        try (Connection connection = DBConnect.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(QUERY)) {
            for (String id : ids) {
                preparedStatement.setLong(1, Long.parseLong(id));
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        Genres genre = new Genres();
                        genre.setGenre_id(resultSet.getLong("genre_id"));
                        genre.setGenre_name(resultSet.getString("genre_name"));
                        genresSet.add(genre);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return genresSet;
    }

    public Genres create(String name) {
        String QUERY = "INSERT INTO Genres (genre_name, created_date, created_user, is_deleted) VALUES (?, ?, ?, ?)";
        Genres genre = null;
        try (Connection connection = DBConnect.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(QUERY, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, name);
            preparedStatement.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
            preparedStatement.setString(3, "admin");
            preparedStatement.setBoolean(4, false);
            preparedStatement.executeUpdate();
            try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    genre = new Genres();
                    genre.setGenre_id(resultSet.getLong(1));
                    genre.setGenre_name(name);


                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return genre;
    }

    public Genres findByName(String newGenreName) {
        String QUERY = "SELECT * FROM Genres WHERE genre_name = ? and is_deleted = 0    ";
        Genres genre = null;
        try (Connection connection = DBConnect.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(QUERY)) {
            preparedStatement.setString(1, newGenreName);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    genre = new Genres();
                    genre.setGenre_id(resultSet.getLong("genre_id"));
                    genre.setGenre_name(resultSet.getString("genre_name"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return genre;
    }

    public void deleteGenreById(int genre_id) {
        String QUERY = "UPDATE Genres SET is_deleted = 1 WHERE genre_id = ?";
        try (Connection connection = DBConnect.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(QUERY)) {
            preparedStatement.setInt(1, genre_id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {

        }

    }
}
