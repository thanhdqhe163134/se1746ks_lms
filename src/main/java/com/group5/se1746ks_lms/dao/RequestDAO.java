package com.group5.se1746ks_lms.dao;

import com.group5.se1746ks_lms.model.Request;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.group5.se1746ks_lms.util.DBConnect.getConnection;

public class RequestDAO {
    Connection connection = getConnection();


    public void createRequest(long loans_id, Date newDueDate, String description, String username) {
        try {
            connection.setAutoCommit(false);
            String sql = "INSERT INTO request (loan_id, request_date, description, created_date, created_user,  is_approved ) VALUES (?, ?, ?, ?,?, ?)";
            try (PreparedStatement insertRequestStmt = connection.prepareStatement(sql)) {
                insertRequestStmt.setLong(1, loans_id);
                insertRequestStmt.setDate(2, newDueDate);
                insertRequestStmt.setString(3, description);
                insertRequestStmt.setTimestamp(4, new java.sql.Timestamp(System.currentTimeMillis()));
                insertRequestStmt.setString(5, username);
                insertRequestStmt.setBoolean(6, false);
                insertRequestStmt.executeUpdate();

            }
            connection.commit();
        } catch (SQLException ex) {
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public List<Request> getRequestsByUser_id(long user_id) {
        List<Request> requests = new ArrayList<>();
        String sql = "SELECT * FROM Request INNER JOIN Loans ON Request.loan_id = Loans.loan_id WHERE Loans.user_id = ?";
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setLong(1, user_id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Request request = new Request();
                request.setRequest_id(resultSet.getLong("request_id"));
                request.setLoan_id(resultSet.getLong("loan_id"));
                request.setRequest_date(resultSet.getDate("request_date"));
                request.setDescription(resultSet.getString("description"));
                request.setCreated_date(resultSet.getTimestamp("created_date"));
                request.setCreated_user(resultSet.getString("created_user"));
                request.setIs_approved(resultSet.getBoolean("is_approved"));
                requests.add(request);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return requests;
    }

    public void approveRequest(long request_id, String username) {
        try {
            connection.setAutoCommit(false);
            String sql = "UPDATE request SET is_approved = true WHERE request_id = ? AND created_user = ?";
            try (PreparedStatement approveRequestStmt = connection.prepareStatement(sql)) {
                approveRequestStmt.setLong(1, request_id);
                approveRequestStmt.setString(2, username);
                approveRequestStmt.executeUpdate();
            }
            connection.commit();
        } catch (SQLException ex) {
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public void removeRequest(long request_id, String username) {
        try {
            connection.setAutoCommit(false);
            String sql = "DELETE FROM request WHERE request_id = ? AND created_user = ?";
            try (PreparedStatement removeRequestStmt = connection.prepareStatement(sql)) {
                removeRequestStmt.setLong(1, request_id);
                removeRequestStmt.setString(2, username);
                removeRequestStmt.executeUpdate();
            }
            connection.commit();
        } catch (SQLException ex) {
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public Request getRequest(int loans_id) {
        String sql = "SELECT * \n" +
                "FROM Request \n" +
                "WHERE loan_id = ? \n" +
                "AND is_approved = 1 \n" +
                "AND created_date = (\n" +
                "    SELECT MAX(created_date) \n" +
                "    FROM Request \n" +
                "    WHERE loan_id = ?\n" +
                "    AND is_approved = 1\n" +
                ");\n";
        try{
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, loans_id);
            statement.setInt(2, loans_id);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                Request request = new Request();
                request.setRequest_id(resultSet.getLong("request_id"));
                request.setLoan_id(resultSet.getLong("loan_id"));
                request.setRequest_date(resultSet.getDate("request_date"));
                request.setDescription(resultSet.getString("description"));
                request.setCreated_date(resultSet.getTimestamp("created_date"));
                request.setCreated_user(resultSet.getString("created_user"));
                request.setIs_approved(resultSet.getBoolean("is_approved"));
                return request;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }


    public List<Request> getAllRequests() {
        List<Request> requests = new ArrayList<>();
        String sql = "SELECT * FROM Request ORDER BY is_approved ASC";
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Request request = new Request();
                request.setRequest_id(resultSet.getLong("request_id"));
                request.setLoan_id(resultSet.getLong("loan_id"));
                request.setRequest_date(resultSet.getDate("request_date"));
                request.setDescription(resultSet.getString("description"));
                request.setCreated_date(resultSet.getTimestamp("created_date"));
                request.setCreated_user(resultSet.getString("created_user"));
                request.setIs_approved(resultSet.getBoolean("is_approved"));
                requests.add(request);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return requests;
    }

    public Request getRequestByLoan_id(int loans_id) {
        String sql = "WITH RankedRequests AS (\n" +
                "    SELECT\n" +
                "        request_id,\n" +
                "        loan_id,\n" +
                "        request_date,\n" +
                "        description,\n" +
                "        created_date,\n" +
                "        created_user,\n" +
                "        approved_user,\n" +
                "        approved_date,\n" +
                "        is_approved,\n" +
                "        ROW_NUMBER() OVER(PARTITION BY loan_id ORDER BY created_date DESC) as rn\n" +
                "    FROM\n" +
                "        Request\n" +
                "    WHERE\n" +
                "    loan_id = ?\n" +
                ")\n" +
                "SELECT *\n" +
                "FROM\n" +
                "    RankedRequests\n" +
                "WHERE\n" +
                "    rn = 1;\n";
        try{
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, loans_id);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                Request request = new Request();
                request.setRequest_id(resultSet.getLong("request_id"));
                request.setLoan_id(resultSet.getLong("loan_id"));
                request.setRequest_date(resultSet.getDate("request_date"));
                request.setDescription(resultSet.getString("description"));
                request.setCreated_date(resultSet.getTimestamp("created_date"));
                request.setCreated_user(resultSet.getString("created_user"));
                request.setIs_approved(resultSet.getBoolean("is_approved"));
                return request;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return null;
    }

    public Request getRequestById(long request_id) {
        String sql = "SELECT * FROM Request WHERE request_id = ?";
        try{
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, request_id);
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()){
                Request request = new Request();
                request.setRequest_id(resultSet.getLong("request_id"));
                request.setLoan_id(resultSet.getLong("loan_id"));
                request.setRequest_date(resultSet.getDate("request_date"));
                request.setDescription(resultSet.getString("description"));
                request.setCreated_date(resultSet.getTimestamp("created_date"));
                request.setCreated_user(resultSet.getString("created_user"));
                request.setIs_approved(resultSet.getBoolean("is_approved"));
                return request;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);

        }
        return null;
    }

    public void updateRequestStatus(long request_id, String description, String username, boolean is_approved) {
        try {
            connection.setAutoCommit(false);
            String sql = "UPDATE request SET is_approved = ?, approved_user = ?, approved_date = ?, description = ? WHERE request_id = ?";
            try (PreparedStatement updateRequestStmt = connection.prepareStatement(sql)) {
                updateRequestStmt.setBoolean(1, is_approved);
                updateRequestStmt.setString(2, username);
                updateRequestStmt.setTimestamp(3, new java.sql.Timestamp(System.currentTimeMillis()));
                updateRequestStmt.setString(4, description);
                updateRequestStmt.setLong(5, request_id);
                updateRequestStmt.executeUpdate();
            }
            connection.commit();
        } catch (SQLException ex) {
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    public List<Request> getAllRequestsWithPaging(int start, int itemsPerPage) {
        List<Request> requests = new ArrayList<>();
        String sql = "SELECT * FROM Request ORDER BY is_approved ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, start);
            statement.setInt(2, itemsPerPage);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Request request = new Request();
                request.setRequest_id(resultSet.getLong("request_id"));
                request.setLoan_id(resultSet.getLong("loan_id"));
                request.setRequest_date(resultSet.getDate("request_date"));
                request.setDescription(resultSet.getString("description"));
                request.setCreated_date(resultSet.getTimestamp("created_date"));
                request.setCreated_user(resultSet.getString("created_user"));
                request.setIs_approved(resultSet.getBoolean("is_approved"));
                requests.add(request);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return requests;
    }

    public List<Request> getRequestsByUser_idPaging(long user_id, int start, int itemsPerPage) {
        List<Request> requests = new ArrayList<>();
        String sql = "SELECT * FROM Request INNER JOIN Loans ON Request.loan_id = Loans.loan_id WHERE Loans.user_id = ? ORDER BY is_approved ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setLong(1, user_id);
            statement.setInt(2, start);
            statement.setInt(3, itemsPerPage);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Request request = new Request();
                request.setRequest_id(resultSet.getLong("request_id"));
                request.setLoan_id(resultSet.getLong("loan_id"));
                request.setRequest_date(resultSet.getDate("request_date"));
                request.setDescription(resultSet.getString("description"));
                request.setCreated_date(resultSet.getTimestamp("created_date"));
                request.setCreated_user(resultSet.getString("created_user"));
                request.setIs_approved(resultSet.getBoolean("is_approved"));
                requests.add(request);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return requests;
    }

    public int getTotalPages(int itemsPerPage) {
        int totalPages = 0;
        String sql = "SELECT COUNT(*) FROM Request";
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int totalItems = resultSet.getInt(1);
                totalPages = (int) Math.ceil((double) totalItems / itemsPerPage);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return totalPages;
    }
    public int getTotalPagesByUser_id( long user_id, int itemsPerPage) {
        int totalPages = 0;
        String sql = "SELECT COUNT(*) FROM Request INNER JOIN Loans ON Request.loan_id = Loans.loan_id WHERE Loans.user_id = "+user_id+"";
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int totalItems = resultSet.getInt(1);
                totalPages = (int) Math.ceil((double) totalItems / itemsPerPage);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return totalPages;
    }


    public List<Request> getRequestsByUser_idPagingSort(long parseLong, int start, int itemsPerPage, String sortBy) {
        List<Request> requests = new ArrayList<>();
        String sql = "SELECT * FROM Request R INNER JOIN Loans L ON R.loan_id = L.loan_id WHERE L.user_id = ? ORDER BY  R.created_date "+ sortBy+ " OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setLong(1, parseLong);
            statement.setInt(2, start);
            statement.setInt(3, itemsPerPage);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Request request = new Request();
                request.setRequest_id(resultSet.getLong("request_id"));
                request.setLoan_id(resultSet.getLong("loan_id"));
                request.setRequest_date(resultSet.getDate("request_date"));
                request.setDescription(resultSet.getString("description"));
                request.setCreated_date(resultSet.getTimestamp("created_date"));
                request.setCreated_user(resultSet.getString("created_user"));
                request.setIs_approved(resultSet.getBoolean("is_approved"));
                requests.add(request);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return requests;
    }

    public List<Request> getAllRequestsWithPagingSort(int start, int itemsPerPage, String sortBy) {
        List<Request> requests = new ArrayList<>();
        String sql = "SELECT * FROM Request  ORDER BY created_date "+ sortBy+ " OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, start);
            statement.setInt(2, itemsPerPage);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Request request = new Request();
                request.setRequest_id(resultSet.getLong("request_id"));
                request.setLoan_id(resultSet.getLong("loan_id"));
                request.setRequest_date(resultSet.getDate("request_date"));
                request.setDescription(resultSet.getString("description"));
                request.setCreated_date(resultSet.getTimestamp("created_date"));
                request.setCreated_user(resultSet.getString("created_user"));
                request.setIs_approved(resultSet.getBoolean("is_approved"));
                requests.add(request);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return requests;
    }


}

