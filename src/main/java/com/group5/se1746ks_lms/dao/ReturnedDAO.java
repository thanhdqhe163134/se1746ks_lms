package com.group5.se1746ks_lms.dao;

import com.group5.se1746ks_lms.util.DBConnect;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ReturnedDAO {
    Connection connection = DBConnect.getConnection();

    public void returnBook(long loanId, String bookCondition, String username) {
        String sql = "INSERT INTO returned (loan_id, book_condition, created_date, created_user) VALUES (?, ?, ?,?)";
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setLong(1, loanId);
            preparedStatement.setString(2, bookCondition);
            preparedStatement.setTimestamp(3, new java.sql.Timestamp(System.currentTimeMillis()));
            preparedStatement.setString(4, username);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


}
