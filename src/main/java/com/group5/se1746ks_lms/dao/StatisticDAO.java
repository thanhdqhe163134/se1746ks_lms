package com.group5.se1746ks_lms.dao;

import com.group5.se1746ks_lms.util.DBConnect;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static com.group5.se1746ks_lms.util.DBConnect.getConnection;

public class StatisticDAO {
    Connection connection = getConnection();

    public int getTotalBooks() {
        String sqlStr = "SELECT COUNT(*) FROM Books where is_deleted = 0";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }

    public int getTotalBookEdition() {
        String sqlStr = "SELECT COUNT(*) FROM BookEdition where is_deleted = 0";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }

    public int getTotalBookCopies() {
        String sqlStr = "SELECT COUNT(*) FROM BookDetail where is_deleted = 0";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }

    //get total book detail that is not borrowed
    public int getTotalBookDetailAvailable() {
        String sqlStr = "SELECT COUNT(*) FROM BookDetail where is_deleted = 0 and is_borrowed = 0";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }

    //get total book detail that is borrowed
    public int getTotalBookDetailBorrowed() {
        String sqlStr = "SELECT COUNT(*) FROM BookDetail where is_deleted = 0 and is_borrowed = 1";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }

    //get total book detail that is borrowed in ? month of ? year
    public int getTotalBookDetailBorrowedInMonth(int month, int year) {
        String sqlStr = "SELECT COUNT(*) FROM BookDetail bd, Loans l WHERE bd.book_detail_id = l.book_detail_id AND MONTH(l.borrowed_date) = ? AND YEAR(l.borrowed_date) = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            preparedStatement.setInt(1, month);
            preparedStatement.setInt(2, year);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }

    //get total book detail that is returned in ? month of ? year
    public int getTotalBookDetailReturnedInMonth(int month, int year) {
        String sqlStr = "SELECT COUNT(*) FROM BookDetail bd, Loans l WHERE bd.book_detail_id = l.book_detail_id AND MONTH(l.returned_date) = ? AND YEAR(l.returned_date) = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            preparedStatement.setInt(1, month);
            preparedStatement.setInt(2, year);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }

    //get total book detail that is borrowed in ? year
    public int getTotalBookDetailBorrowedInYear(int year) {
        String sqlStr = "SELECT COUNT(*) FROM BookDetail bd, Loans l WHERE bd.book_detail_id = l.book_detail_id AND YEAR(l.borrowed_date) = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            preparedStatement.setInt(1, year);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }

    //get total book detail that is returned in ? year
    public int getTotalBookDetailReturnedInYear(int year) {
        String sqlStr = "SELECT COUNT(*) FROM BookDetail bd, Loans l WHERE bd.book_detail_id = l.book_detail_id AND YEAR(l.returned_date) = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            preparedStatement.setInt(1, year);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }
    //get total book detail that is borrowed in ? year
    public int getTotalBookDetailBorrowedInYear(int year, int month) {
        String sqlStr = "SELECT COUNT(*) FROM BookDetail bd, Loans l WHERE bd.book_detail_id = l.book_detail_id AND YEAR(l.borrowed_date) = ? AND MONTH(l.borrowed_date) = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            preparedStatement.setInt(1, year);
            preparedStatement.setInt(2, month);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
        	throw new RuntimeException(e);
        }
        return 0;
    }

    //get total book detail that added in ? month of ? year
    public Map<Integer, Integer> getTotalBookTypeAddedEachDay(int month, int year, String bookType) {
        String sqlStr = "SELECT DAY(created_date) AS day, COUNT(*) AS count FROM " + bookType +
                " WHERE MONTH(created_date) = ? AND YEAR(created_date) = ?" +
                " GROUP BY DAY(created_date)";
        Map<Integer, Integer> dailyCount = new HashMap<>();
        try (Connection connection = DBConnect.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlStr)) {
            preparedStatement.setInt(1, month);
            preparedStatement.setInt(2, year);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    dailyCount.put(resultSet.getInt("day"), resultSet.getInt("count"));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return dailyCount;
    }


    //get total book detail that added in ? year
    public Map<Integer, Integer> getTotalBookTypeAddedEachMonth(int year, String bookType) {
        String sqlStr = "SELECT MONTH(created_date) AS month, COUNT(*) AS count FROM " + bookType +
                " WHERE YEAR(created_date) = ? GROUP BY MONTH(created_date)";
        Map<Integer, Integer> monthlyCount = new HashMap<>();
        try (Connection connection = DBConnect.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlStr)) {
            preparedStatement.setInt(1, year);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    monthlyCount.put(resultSet.getInt("month"), resultSet.getInt("count"));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return monthlyCount;
    }


    public int getTotalUser() {
        String sqlStr = "SELECT COUNT(*) FROM Accounts where    is_deleted = 0";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }

    public int getTotalUserInMonth(int month, int year) {
        String sqlStr = "SELECT COUNT(*) FROM Accounts WHERE MONTH(created_date) = ? AND YEAR(created_date) = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            preparedStatement.setInt(1, month);
            preparedStatement.setInt(2, year);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
        	throw new RuntimeException(e);
        }
        return 0;
    }

    public int getTotalRequest() {
        String sqlStr = "SELECT COUNT(*) FROM Request";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }

    public int getTotalApprovedRequest() {
        String sqlStr = "SELECT COUNT(*) FROM Request WHERE is_approved = 1";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }

    //get total Loan in ? month of ? year
    public int getTotalLoanInMonth(int month, int year) {
        String sqlStr = "SELECT COUNT(*) FROM Loans WHERE MONTH(created_date) = ? AND YEAR(created_date) = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            preparedStatement.setInt(1, month);
            preparedStatement.setInt(2, year);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
        	throw new RuntimeException(e);
        }
        return 0;
    }

    //get total Loan in ? year
    public int getTotalLoanInYear(int year) {
        String sqlStr = "SELECT COUNT(*) FROM Loans WHERE YEAR(created_date) = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            preparedStatement.setInt(1, year);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
        	throw new RuntimeException(e);
        }
        return 0;
    }

    //get total Loans that overdue in ? month of ? year
    public int getTotalLoanOverdueInMonth(int month, int year) {
        String sqlStr = "SELECT COUNT(*) FROM Loans WHERE MONTH(due_date) = ? AND YEAR(due_date) = ? AND Status = 'Overdue'";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            preparedStatement.setInt(1, month);
            preparedStatement.setInt(2, year);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
        	throw new RuntimeException(e);
        }
        return 0;
    }

    //get total Loans that overdue in ? year
    public int getTotalLoanOverdueInYear(int year) {
        String sqlStr = "SELECT COUNT(*) FROM Loans WHERE YEAR(due_date) = ? AND Status = 'Overdue'";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            preparedStatement.setInt(1, year);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
        	throw new RuntimeException(e);
        }
        return 0;
    }

    //get total Loans that expired in ? month of ? year
    public int getTotalLoanExpiredInMonth(int month, int year) {
        String sqlStr = "SELECT COUNT(*) FROM Loans WHERE MONTH(due_date) = ? AND YEAR(due_date) = ? AND Status = 'Expired'";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            preparedStatement.setInt(1, month);
            preparedStatement.setInt(2, year);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException e){
        	throw new RuntimeException(e);
        }
        return 0;
    }

    //get total Loans that expired in ? year
    public int getTotalLoanExpiredInYear(int year) {
        String sqlStr = "SELECT COUNT(*) FROM Loans WHERE YEAR(due_date) = ? AND Status = 'Expired'";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            preparedStatement.setInt(1, year);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException e){
        	throw new RuntimeException(e);
        }
        return 0;
    }

    public int getTotalLoans() {
        String sqlStr = "SELECT COUNT(*) FROM Loans";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }

    public int getTotalLoansOverdue() {
        String sqlStr = "SELECT COUNT(*) FROM Loans where status = 'Overdue'";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }
    public int getTotalLoansReturned() {
        String sqlStr = "SELECT COUNT(*) FROM Loans where status = 'Returned'";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }


    public Map<Integer, Integer> getTotalLoansAddedEachMonth(int years) {
        String sqlStr = "SELECT MONTH(created_date) AS month, COUNT(*) AS count FROM Loans WHERE YEAR(created_date) = ? GROUP BY MONTH(created_date)";
        Map<Integer, Integer> monthlyCount = new HashMap<>();
        try (Connection connection = DBConnect.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlStr)) {
            preparedStatement.setInt(1, years);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    monthlyCount.put(resultSet.getInt("month"), resultSet.getInt("count"));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return monthlyCount;
    }

    public Map<Integer, Integer> getTotalLoansAddedEachDay(int month, int year) {
        String sqlStr = "SELECT DAY(created_date) AS day, COUNT(*) AS count FROM Loans WHERE MONTH(created_date) = ? AND YEAR(created_date) = ? GROUP BY DAY(created_date)";
        Map<Integer, Integer> dailyCount = new HashMap<>();
        try (Connection connection = DBConnect.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlStr)) {
            preparedStatement.setInt(1, month);
            preparedStatement.setInt(2, year);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    dailyCount.put(resultSet.getInt("day"), resultSet.getInt("count"));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return dailyCount;
    }

    public int getTotalUsersActive() {
        String sqlStr = "SELECT COUNT(*) FROM Accounts WHERE is_deleted = 1";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return  resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }

    public Map<Integer, Integer> getTotalUsersAddedEachMonth(int years) {
        String sqlStr = "SELECT MONTH(created_date) AS month, COUNT(*) AS count FROM Accounts WHERE YEAR(created_date) = ? GROUP BY MONTH(created_date)";
        Map<Integer, Integer> monthlyCount = new HashMap<>();
        try (Connection connection = DBConnect.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlStr)) {
            preparedStatement.setInt(1, years);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    monthlyCount.put(resultSet.getInt("month"), resultSet.getInt("count"));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return monthlyCount;
    }

    public Map<Integer, Integer> getTotalUsersAddedEachDay(int month, int year) {
        String sqlStr = "SELECT DAY(created_date) AS day, COUNT(*) AS count FROM Accounts WHERE MONTH(created_date) = ? AND YEAR(created_date) = ? GROUP BY DAY(created_date)";
        Map<Integer, Integer> dailyCount = new HashMap<>();
        try (Connection connection = DBConnect.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlStr)) {
            preparedStatement.setInt(1, month);
            preparedStatement.setInt(2, year);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    dailyCount.put(resultSet.getInt("day"), resultSet.getInt("count"));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return dailyCount;
    }

    public int getTotalRequestsPending() {
        String sqlStr = "SELECT COUNT(*) FROM Request WHERE is_approved = 0 AND description = 'Pending'";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return  resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }

    public int getTotalRequestsApproved() {
        String sqlStr = "SELECT COUNT(*) FROM Request WHERE is_approved = 1 AND description = 'Approved'";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return  resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }

    public int getTotalRequestsDenied() {
        String sqlStr = "SELECT COUNT(*) FROM Request WHERE is_approved = 0 AND description = 'Rejected'";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return  resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }

    public Map<Integer, Integer> getTotalRequestsAddedEachMonth(int years) {
        String sqlStr = "SELECT MONTH(created_date) AS month, COUNT(*) AS count FROM Request WHERE YEAR(created_date) = ? GROUP BY MONTH(created_date)";
        Map<Integer, Integer> monthlyCount = new HashMap<>();
        try (Connection connection = DBConnect.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlStr)) {
            preparedStatement.setInt(1, years);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    monthlyCount.put(resultSet.getInt("month"), resultSet.getInt("count"));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return monthlyCount;
    }

    public Map<Integer, Integer> getTotalRequestsAddedEachDay(int month, int year) {
        String sqlStr = "SELECT DAY(created_date) AS day, COUNT(*) AS count FROM Request WHERE MONTH(created_date) = ? AND YEAR(created_date) = ? GROUP BY DAY(created_date)";
        Map<Integer, Integer> dailyCount = new HashMap<>();
        try (Connection connection = DBConnect.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlStr)) {
            preparedStatement.setInt(1, month);
            preparedStatement.setInt(2, year);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    dailyCount.put(resultSet.getInt("day"), resultSet.getInt("count"));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return dailyCount;
    }

    public int getTotalFines() {
        String sqlStr = "SELECT COUNT(*) FROM Fine";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return  resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }

    public int getTotalFinesPaid() {
        String sqlStr = "SELECT COUNT(*) FROM Fine WHERE is_paid = 1";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return  resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }

    public int getTotalFinesUnpaid() {
        String sqlStr = "SELECT COUNT(*) FROM Fine WHERE is_paid = 0";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return  resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }

    public Map<Integer, Integer> getTotalFinesAddedEachMonth(int years) {
        String sqlStr = "SELECT MONTH(created_date) AS month, COUNT(*) AS count FROM Fine WHERE YEAR(created_date) = ? GROUP BY MONTH(created_date)";
        Map<Integer, Integer> monthlyCount = new HashMap<>();
        try (Connection connection = DBConnect.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlStr)) {
            preparedStatement.setInt(1, years);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    monthlyCount.put(resultSet.getInt("month"), resultSet.getInt("count"));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return monthlyCount;
    }

    public Map<Integer, Integer> getTotalFinesAddedEachDay(int month, int year) {
        String sqlStr = "SELECT DAY(created_date) AS day, COUNT(*) AS count FROM Fine WHERE MONTH(created_date) = ? AND YEAR(created_date) = ? GROUP BY DAY(created_date)";
        Map<Integer, Integer> dailyCount = new HashMap<>();
        try (Connection connection = DBConnect.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sqlStr)) {
            preparedStatement.setInt(1, month);
            preparedStatement.setInt(2, year);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    dailyCount.put(resultSet.getInt("day"), resultSet.getInt("count"));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return dailyCount;
    }
}
