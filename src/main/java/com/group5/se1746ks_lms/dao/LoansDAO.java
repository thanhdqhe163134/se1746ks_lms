package com.group5.se1746ks_lms.dao;

import com.group5.se1746ks_lms.model.Loans;
import com.group5.se1746ks_lms.util.DBConnect;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class LoansDAO extends DBConnect {
    Connection connection = getConnection();

    public ArrayList<Loans> getLoansList() {
        ArrayList<Loans> loans = new ArrayList<>();
        String sqlStr = "SELECT l.*, b.title, b.img, be.* " +
                "FROM Loans l " +
                "JOIN BookDetail bd ON l.book_detail_id = bd.book_detail_id " +
                "JOIN BookEdition be ON bd.ISBN = be.ISBN " +
                "JOIN Books b ON be.book_id = b.book_id";

        try (
                PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
                ResultSet resultSet = preparedStatement.executeQuery();
        ) {
            while (resultSet.next()) {
                Loans l = new Loans();
                l.setLoans_id((int) resultSet.getLong("loan_id"));
                l.setUser_id((int) resultSet.getLong("user_id"));
                l.setBook_detail_id(resultSet.getString("book_detail_id"));
                l.setTitle(resultSet.getString("title"));
                l.setImg(resultSet.getString("img"));
                l.setStatus(resultSet.getString("status"));
                l.setBorrow_date(resultSet.getTimestamp("borrow_date"));
                l.setDue_date(resultSet.getDate("due_date"));
                l.setCreate_date(resultSet.getTimestamp("created_date"));
                l.setCreate_user(resultSet.getString("created_user"));
                l.setReturn_date(resultSet.getTimestamp("returned_date"));
                l.setExpried_date(resultSet.getTimestamp("expried_date"));
                l.setIs_returned(resultSet.getBoolean("is_returned"));
                l.setIs_approved(resultSet.getBoolean("is_request"));
                loans.add(l);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return loans;
    }

    public ArrayList<Loans> getLoansListbyUser_id(long user_id) {
        ArrayList<Loans> loans = new ArrayList<>();
        String sqlStr = "SELECT l.*, b.title, b.img, b.book_id, be.* " +
                "FROM Loans l " +
                "JOIN BookDetail bd ON l.book_detail_id = bd.book_detail_id " +
                "JOIN BookEdition be ON bd.ISBN = be.ISBN " +
                "JOIN Books b ON be.book_id = b.book_id where l.user_id = " + user_id;

        try (
                PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
                ResultSet resultSet = preparedStatement.executeQuery();
        ) {
            while (resultSet.next()) {
                Loans l = new Loans();
                l.setLoans_id(resultSet.getInt(1));
                l.setUser_id(resultSet.getInt(2));
                l.setBook_detail_id(resultSet.getString(3));
                l.setBook_id(resultSet.getInt(17));
                l.setTitle(resultSet.getString(15));
                l.setImg(resultSet.getString(16));
                l.setBorrow_date(resultSet.getTimestamp(4));
                l.setDue_date(resultSet.getDate(5));
                l.setStatus(resultSet.getString(7));
                l.setCreate_date(resultSet.getTimestamp(8));
                l.setCreate_user(resultSet.getString(9));
                l.setReturn_date(resultSet.getTimestamp(6));
                l.setExpried_date(resultSet.getDate(12));
                l.setIs_returned(resultSet.getBoolean(13));
                l.setIs_request(resultSet.getBoolean(14));
                loans.add(l);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return loans;
    }

    public ArrayList<Loans> searchLoansByBookCode (String query) {
        ArrayList<Loans> loans = new ArrayList<>();
        String sqlStr = "SELECT * FROM Loans l, Books b, BookDetail bd " +
                "WHERE l.book_detail_id = bd.book_detail_id AND bd.book_id = b.book_id AND l.detail_id LIKE ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            preparedStatement.setString(1, "%" + query + "%");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Loans l = new Loans();
                l.setLoans_id((int) resultSet.getLong(1));
                l.setUser_id((int) resultSet.getLong(2));
                l.setBook_detail_id(resultSet.getString(3));
                l.setTitle(resultSet.getString(13));
                l.setImg(resultSet.getString(20));
                l.setBorrow_date(resultSet.getTimestamp(4));
                l.setDue_date(resultSet.getDate(5));
                l.setCreate_date(resultSet.getTimestamp(6));
                l.setCreate_user(resultSet.getString(7));
                l.setReturn_date(resultSet.getTimestamp(8));
                l.setExpried_date(resultSet.getTimestamp(9));
                l.setIs_returned(resultSet.getBoolean(10));
                l.setIs_approved(resultSet.getBoolean(11));
                loans.add(l);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return loans;
    }

    public ArrayList<Loans> searchLoansByBookTitle (String query) {
        ArrayList<Loans> loans = new ArrayList<>();
        String sqlStr = "SELECT * FROM Loans l, Books b, BookDetail bd " +
                "WHERE l.book_detail_id = bd.book_detail_id AND bd.book_id = b.book_id AND bd.title LIKE ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            preparedStatement.setString(1, "%" + query + "%");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Loans l = new Loans();
                l.setLoans_id((int) resultSet.getLong(1));
                l.setUser_id((int) resultSet.getLong(2));
                l.setBook_detail_id(resultSet.getString(3));
                l.setTitle(resultSet.getString(13));
                l.setImg(resultSet.getString(20));
                l.setBorrow_date(resultSet.getTimestamp(4));
                l.setDue_date(resultSet.getTimestamp(5));
                l.setCreate_date(resultSet.getTimestamp(6));
                l.setCreate_user(resultSet.getString(7));
                l.setReturn_date(resultSet.getTimestamp(8));
                l.setExpried_date(resultSet.getTimestamp(9));
                l.setIs_returned(resultSet.getBoolean(10));
                l.setIs_approved(resultSet.getBoolean(11));
                loans.add(l);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return loans;
    }

    public ArrayList<Loans> searchLoansByRecordId (String query) {
        ArrayList<Loans> loans = new ArrayList<>();
        String sqlStr = "SELECT * FROM Loans l, Books b, BookDetail bd " +
                "WHERE l.book_detail_id = bd.book_detail_id AND bd.book_id = b.book_id AND l.loan_id LIKE ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            preparedStatement.setString(1, "%" + query + "%");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Loans l = new Loans();
                l.setLoans_id((int) resultSet.getLong(1));
                l.setUser_id((int) resultSet.getLong(2));
                l.setBook_detail_id(resultSet.getString(3));
                l.setTitle(resultSet.getString(13));
                l.setImg(resultSet.getString(20));
                l.setBorrow_date(resultSet.getTimestamp(4));
                l.setDue_date(resultSet.getTimestamp(5));
                l.setCreate_date(resultSet.getTimestamp(6));
                l.setCreate_user(resultSet.getString(7));
                l.setReturn_date(resultSet.getTimestamp(8));
                l.setExpried_date(resultSet.getTimestamp(9));
                l.setIs_returned(resultSet.getBoolean(10));
                l.setIs_approved(resultSet.getBoolean(11));
                loans.add(l);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return loans;
    }

    public ArrayList<Loans> searchLoansByStudentId(String query) {
        ArrayList<Loans> loans = new ArrayList<>();
        String sqlStr = "SELECT * FROM Loans l, Books b, BookDetail bd,  BookEdition be, Accounts a\n" +
                "                WHERE l.book_detail_id = bd.book_detail_id AND bd.ISBN = be.ISBN AND be.book_id = b.book_id AND l.user_id = a.user_id AND a.student_id LIKE ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            preparedStatement.setString(1, "%" + query + "%");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Loans l = new Loans();
                l.setLoans_id((int) resultSet.getLong("loan_id"));
                l.setUser_id((int) resultSet.getLong("user_id"));
                l.setBook_detail_id(resultSet.getString("book_detail_id"));
                l.setTitle(resultSet.getString("title"));
                l.setImg(resultSet.getString("img"));
                l.setBorrow_date(resultSet.getTimestamp("borrow_date"));
                l.setDue_date(resultSet.getTimestamp("due_date"));
                l.setCreate_date(resultSet.getTimestamp("created_date"));
                l.setCreate_user(resultSet.getString("created_user"));
                l.setReturn_date(resultSet.getTimestamp("returned_date"));
                l.setExpried_date(resultSet.getTimestamp("expried_date"));
                l.setIs_returned(resultSet.getBoolean("is_returned"));
                l.setIs_request(resultSet.getBoolean("is_request"));
                loans.add(l);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return loans;
    }


    public void borrowBook(String bookDetailId, long userId, String username) {
        // Đảm bảo rằng không có dữ liệu không hợp lệ được cung cấp
        if (bookDetailId == null || userId <= 0) {
            throw new IllegalArgumentException("Invalid input parameters.");
        }

        Connection conn = null;
        PreparedStatement checkStmt = null;
        PreparedStatement insertLoanStmt = null;
        PreparedStatement updateBookDetailStmt = null;

        try {
            // Lấy một connection từ pool và bắt đầu transaction
            conn = getConnection();
            conn.setAutoCommit(false);
            conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);

            // Kiểm tra xem cuốn sách đã được mượn chưa
            String checkSql = "SELECT * FROM BookDetail WITH (UPDLOCK, ROWLOCK) WHERE book_detail_id = ? AND is_borrowed = 0";
            checkStmt = conn.prepareStatement(checkSql);
            checkStmt.setString(1, bookDetailId);
            ResultSet rs = checkStmt.executeQuery();
            if (!rs.next()) {
                throw new SQLException("Book is already borrowed.");
            }

            // Tạo một mục mới trong bảng Loans
            String insertSql = "INSERT INTO Loans (user_id, book_detail_id, due_date, expried_date, status, created_date, created_user, is_returned, is_request) "
                    + "VALUES (?, ?, ?, ?, ?, CURRENT_TIMESTAMP, ?, 0, 0)";
            insertLoanStmt = conn.prepareStatement(insertSql);
            insertLoanStmt.setLong(1, userId);
            insertLoanStmt.setString(2, bookDetailId);
            insertLoanStmt.setDate(3, new Date(System.currentTimeMillis() + 16 * 24 * 60 * 60 * 1000)); // due date 15 days later
            insertLoanStmt.setDate(4, new Date(System.currentTimeMillis() + 3 * 24 * 60 * 60 * 1000)); // expried date 3 days later
            insertLoanStmt.setString(5, "Reserved");
            insertLoanStmt.setString(6, username);
            insertLoanStmt.executeUpdate();

            // Cập nhật trạng thái của cuốn sách trong bảng BookDetail
            String updateSql = "UPDATE BookDetail SET is_borrowed = 1 WHERE book_detail_id = ?";
            updateBookDetailStmt = conn.prepareStatement(updateSql);
            updateBookDetailStmt.setString(1, bookDetailId);
            int updatedRows = updateBookDetailStmt.executeUpdate();
            if (updatedRows == 0) {
                throw new SQLException("Failed to update book detail, no rows affected.");
            }

            // Commit transaction
            conn.commit();
        } catch (SQLException ex) {
            // Trong trường hợp có lỗi, rollback transaction
            if (conn != null) {
                try {
                    conn.rollback();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            ex.printStackTrace();
            throw new RuntimeException("Failed to borrow book: " + ex.getMessage());
        } finally {
            // Đảm bảo đóng tất cả các resources và reset lại auto commit
            close(conn, checkStmt, insertLoanStmt, updateBookDetailStmt);
        }
    }

    private void close(Connection conn, Statement... statements) {
        for (Statement stmt : statements) {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        if (conn != null) {
            try {
                conn.setAutoCommit(true);
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public Loans getLoansById(String loanId) {
        Loans l = new Loans();

        String sqlStr = "SELECT l.*, b.title, b.img, be.* " +
                "FROM Loans l " +
                "JOIN BookDetail bd ON l.book_detail_id = bd.book_detail_id " +
                "JOIN BookEdition be ON bd.ISBN = be.ISBN " +
                "JOIN Books b ON be.book_id = b.book_id where l.loan_id = " + loanId;

        try (
                PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
                ResultSet resultSet = preparedStatement.executeQuery();
        ) {
            while (resultSet.next()) {
                l.setLoans_id((int) resultSet.getLong("loan_id"));
                l.setUser_id((int) resultSet.getLong("user_id"));
                l.setBook_detail_id(resultSet.getString("book_detail_id"));
                l.setTitle(resultSet.getString("title"));
                l.setImg(resultSet.getString("img"));
                l.setStatus(resultSet.getString("status"));
                l.setBorrow_date(resultSet.getTimestamp("borrow_date"));
                l.setDue_date(resultSet.getDate("due_date"));
                l.setCreate_date(resultSet.getTimestamp("created_date"));
                l.setCreate_user(resultSet.getString("created_user"));
                l.setReturn_date(resultSet.getTimestamp("returned_date"));
                l.setExpried_date(resultSet.getTimestamp("expried_date"));
                l.setIs_returned(resultSet.getBoolean("is_returned"));
                l.setIs_request(resultSet.getBoolean("is_request"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return l;
    }


    public ArrayList<Loans> sortLoansList(String sort, int currentPage, int itemsPerPage) {
        ArrayList<Loans> loans = new ArrayList<>();
        String sqlStr = "SELECT * FROM Loans l, Books b, BookDetail bd, BookEdition be, Accounts a " +
                "WHERE l.book_detail_id = bd.book_detail_id AND bd.ISBN = be.ISBN AND be.book_id = b.book_id  AND l.user_id = a.user_id " +
                "ORDER BY l.created_date " + sort + " OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        try {
            PreparedStatement pstm = connection.prepareStatement(sqlStr);
            int offset = (currentPage - 1) * itemsPerPage;
            pstm.setInt(1, offset);
            pstm.setInt(2, itemsPerPage);
            ResultSet resultSet = pstm.executeQuery();
            while (resultSet.next()) {
                Loans l = new Loans();
                l.setLoans_id(resultSet.getInt(1));
                l.setUser_id(resultSet.getInt(2));
                l.setBook_detail_id(resultSet.getString(3));
                l.setBook_id(resultSet.getInt(15));
                l.setTitle(resultSet.getString(16));
                l.setImg(resultSet.getString(22));
                l.setBorrow_date(resultSet.getTimestamp(4));
                l.setDue_date(resultSet.getDate(5));
                l.setStatus(resultSet.getString(7));
                l.setCreate_date(resultSet.getTimestamp(8));
                l.setCreate_user(resultSet.getString(9));
                l.setReturn_date(resultSet.getTimestamp(6));
                l.setExpried_date(resultSet.getDate(12));
                l.setIs_returned(resultSet.getBoolean(13));
                l.setIs_request(resultSet.getBoolean(14));
                l.setStudent_id(resultSet.getString("student_id"));
                loans.add(l);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return loans;
    }


    public ArrayList<Loans> getLoansListPerPage(int currentPage, int itemsPerPage) {
        ArrayList<Loans> loans = new ArrayList<>();
        String sqlStr = "WITH LatestRequests AS (\n" +
                "    SELECT loan_id, created_date, description AS request_description, -- Dùng bí danh ở đây\n" +
                "           ROW_NUMBER() OVER(PARTITION BY loan_id ORDER BY created_date DESC) as rn\n" +
                "    FROM Request\n" +
                ")\n" +
                "SELECT l.*, bd.*, be.*, b.*, a.*, lr.request_description -- Chỉ định bí danh khi chọn cột\n" +
                "FROM Loans l\n" +
                "         JOIN BookDetail bd ON l.book_detail_id = bd.book_detail_id\n" +
                "         JOIN BookEdition be ON bd.ISBN = be.ISBN\n" +
                "         JOIN Books b ON be.book_id = b.book_id\n" +
                "         LEFT JOIN Request r ON l.loan_id = r.loan_id\n" +
                "         LEFT JOIN LatestRequests lr ON r.loan_id = lr.loan_id AND r.created_date = lr.created_date\n" +
                "         JOIN Accounts a ON l.user_id = a.user_id\n" +
                "WHERE lr.rn = 1 OR lr.rn IS NULL\n" +
                "ORDER BY r.created_date DESC, l.loan_id\n" +
                "OFFSET ? ROWS FETCH NEXT ? ROWS ONLY\n";
        try {
            PreparedStatement pstm = connection.prepareStatement(sqlStr);
            int offset = (currentPage - 1) * itemsPerPage;
            pstm.setInt(1, offset);
            pstm.setInt(2, itemsPerPage);
            ResultSet resultSet = pstm.executeQuery();
            while (resultSet.next()) {
                Loans l = new Loans();
                l.setLoans_id(resultSet.getInt("loan_id"));
                l.setUser_id(resultSet.getInt("user_id"));
                l.setBook_detail_id(resultSet.getString("book_detail_id"));
                l.setBook_id(resultSet.getInt("book_id"));
                l.setTitle(resultSet.getString("title"));
                l.setImg(resultSet.getString("img"));
                l.setBorrow_date(resultSet.getTimestamp("borrow_date"));
                l.setDue_date(resultSet.getDate("due_date"));
                l.setStatus(resultSet.getString("status"));
                l.setCreate_date(resultSet.getTimestamp(8));
                l.setCreate_user(resultSet.getString(9));
                l.setReturn_date(resultSet.getTimestamp(6));
                l.setExpried_date(resultSet.getDate("expried_date"));
                l.setIs_returned(resultSet.getBoolean("is_returned"));
                l.setIs_request(resultSet.getBoolean("is_request"));
                l.setStudent_id(resultSet.getString("student_id"));
                l.setRequest_status(resultSet.getString("request_description"));
                loans.add(l);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return loans;
    }


    public ArrayList<Loans> getLoansListPerPage2(int currentPage, int itemsPerPage) {
        ArrayList<Loans> loans = new ArrayList<>();
        String sqlStr = "SELECT \n" +
                "    l.*, \n" +
                "    b.*, \n" +
                "    bd.*, \n" +
                "    be.*, \n" +
                "    a.*, \n" +
                "    r.description AS request_description, -- Đổi tên cột description từ bảng Request\n" +
                "    r.* -- Lấy tất cả các cột khác từ bảng Request\n" +
                "FROM \n" +
                "    Loans l\n" +
                "JOIN \n" +
                "    BookDetail bd ON l.book_detail_id = bd.book_detail_id\n" +
                "JOIN \n" +
                "    BookEdition be ON bd.ISBN = be.ISBN\n" +
                "JOIN \n" +
                "    Books b ON be.book_id = b.book_id\n" +
                "JOIN \n" +
                "    Accounts a ON l.user_id = a.user_id\n" +
                "JOIN \n" +
                "    Request r ON l.loan_id = r.loan_id\n" +
                "ORDER BY \n" +
                "    l.loan_id\n" +
                "OFFSET \n" +
                "    ? ROWS FETCH NEXT ? ROWS ONLY\n";
        try {
            PreparedStatement pstm = connection.prepareStatement(sqlStr);
            int offset = (currentPage - 1) * itemsPerPage;
            pstm.setInt(1, offset);
            pstm.setInt(2, itemsPerPage);
            ResultSet resultSet = pstm.executeQuery();
            while (resultSet.next()) {
                Loans l = new Loans();
                l.setLoans_id(resultSet.getInt("loan_id"));
                l.setUser_id(resultSet.getInt("user_id"));
                l.setBook_detail_id(resultSet.getString("book_detail_id"));
                l.setBook_id(resultSet.getInt("book_id"));
                l.setTitle(resultSet.getString("title"));
                l.setImg(resultSet.getString("img"));
                l.setBorrow_date(resultSet.getTimestamp("borrow_date"));
                l.setDue_date(resultSet.getDate("due_date"));
                l.setStatus(resultSet.getString("status"));
                l.setCreate_date(resultSet.getTimestamp(8));
                l.setCreate_user(resultSet.getString(9));
                l.setReturn_date(resultSet.getTimestamp(6));
                l.setExpried_date(resultSet.getDate("expried_date"));
                l.setIs_returned(resultSet.getBoolean(13));
                l.setIs_request(resultSet.getBoolean(14));
                l.setRequest_status(resultSet.getString("request_description"));
                l.setStudent_id(resultSet.getString("student_id"));

                loans.add(l);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return loans;
    }



    public List<Loans> getLoansListPerPagebyUser_id(long user_id, int currentPage, int itemsPerPage) {
        ArrayList<Loans> loans = new ArrayList<>();
        String sqlStr = "SELECT * FROM Loans l, Books b, BookDetail bd, BookEdition be " +
                "WHERE l.book_detail_id = bd.book_detail_id AND bd.ISBN = be.ISBN AND be.book_id = b.book_id AND l.user_id = "+ user_id  +
                "ORDER BY loan_id OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        try {
            PreparedStatement pstm = connection.prepareStatement(sqlStr);
            int offset = (currentPage - 1) * itemsPerPage;
            pstm.setInt(1, offset);
            pstm.setInt(2, itemsPerPage);
            ResultSet resultSet = pstm.executeQuery();
            while (resultSet.next()) {
                Loans l = new Loans();
                l.setLoans_id(resultSet.getInt(1));
                l.setUser_id(resultSet.getInt(2));
                l.setBook_detail_id(resultSet.getString(3));
                l.setBook_id(resultSet.getInt(15));
                l.setTitle(resultSet.getString(16));
                l.setImg(resultSet.getString(22));
                l.setBorrow_date(resultSet.getTimestamp(4));
                l.setDue_date(resultSet.getDate(5));
                l.setStatus(resultSet.getString(7));
                l.setCreate_date(resultSet.getTimestamp(8));
                l.setCreate_user(resultSet.getString(9));
                l.setReturn_date(resultSet.getTimestamp(6));
                l.setExpried_date(resultSet.getDate(12));
                l.setIs_returned(resultSet.getBoolean(13));
                l.setIs_request(resultSet.getBoolean(14));
                loans.add(l);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return loans;
    }

    public void createNewLoans(Loans l) {
        String sqlStr = "INSERT INTO Loans (user_id, book_detail_id, borrow_date, due_date, returned_date, status, created_date, created_user, request_date, expried_date, is_returned, is_approved)" +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            preparedStatement.setInt(1, l.getUser_id());
            preparedStatement.setString(2, l.getBook_detail_id());
            preparedStatement.setTimestamp(3,  l.getBorrow_date());
            preparedStatement.setDate(4, (Date) l.getDue_date());
            preparedStatement.setTimestamp(5,  l.getReturn_date());
            preparedStatement.setString(6, l.getStatus());
            preparedStatement.setTimestamp(7, l.getCreate_date());
            preparedStatement.setString(8, l.getCreate_user());
            preparedStatement.setDate(9, (Date) l.getExpried_date());
            preparedStatement.setBoolean(10, l.getIs_returned());
            preparedStatement.setBoolean(11, l.getIs_request());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void updateBorrowDate(String loans_id, String status, Timestamp timestamp, String username) {
        String sqlStr = "UPDATE Loans SET borrow_date = ?, status = ?, updated_user = ?, updated_date = ? WHERE loan_id = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            preparedStatement.setTimestamp(1, timestamp);
            preparedStatement.setString(2, status);
            preparedStatement.setString(3, username);
            preparedStatement.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
            preparedStatement.setString(5, loans_id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateStatus(long loans_id, String status, String username) {
        String sqlStr = "UPDATE Loans SET status = ?, updated_user = ?, updated_date = ? WHERE loan_id = ?";
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            preparedStatement.setString(1, status);
            preparedStatement.setString(2, username);
            preparedStatement.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
            preparedStatement.setLong(4, loans_id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void updateStatusReturned(String loans_id, String status, String username, Timestamp return_date) {
        String sqlStr = "UPDATE Loans SET status = ?, updated_user = ?, updated_date = ?, returned_date = ?, is_returned = 0 WHERE loan_id = ?";
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            preparedStatement.setString(1, status);
            preparedStatement.setString(2, username);
            preparedStatement.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
            preparedStatement.setTimestamp(4, return_date);
            preparedStatement.setString(5, loans_id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateStatusReturned2(String loans_id, String status, String username, Timestamp return_date) {
        String sqlStr = "UPDATE Loans SET status = ?, updated_user = ?, updated_date = ?, returned_date = ?, is_returned = 1 WHERE loan_id = ?";
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            preparedStatement.setString(1, status);
            preparedStatement.setString(2, username);
            preparedStatement.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
            preparedStatement.setTimestamp(4, return_date);
            preparedStatement.setString(5, loans_id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Long> getOverdueLoans() {
        String sql = "WITH LatestApprovedRequests AS (\n" +
                "    SELECT loan_id, request_date,\n" +
                "           ROW_NUMBER() OVER(PARTITION BY loan_id ORDER BY request_date DESC) as rn\n" +
                "    FROM Request\n" +
                "    WHERE is_approved = 1\n" +
                ")\n" +
                "SELECT l.loan_id\n" +
                "FROM Loans l\n" +
                "         LEFT JOIN LatestApprovedRequests r ON l.loan_id = r.loan_id\n" +
                "WHERE l.is_returned = 0 AND l.status = 'Borrowing' AND (\n" +
                "        (l.is_request = 0 AND l.due_date < ?) OR\n" +
                "        (l.is_request = 1 AND ((r.rn = 1 AND r.request_date <  ?) OR (r.loan_id IS NULL AND l.due_date <  ?)))\n" +
                "    )\n";
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            java.sql.Date sqlDate = new java.sql.Date(System.currentTimeMillis());
            preparedStatement.setDate(1,sqlDate);
            preparedStatement.setDate(2, sqlDate);
            preparedStatement.setDate(3, sqlDate);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Long> overdueLoans = new ArrayList<>();
            while (resultSet.next()) {
                overdueLoans.add(resultSet.getLong(1));
            }
            return overdueLoans;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    public Date getDueDate(long parseLong) {
        String sqlStr = "SELECT due_date FROM Loans WHERE loan_id = ?";
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            preparedStatement.setLong(1, parseLong);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                return resultSet.getDate(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void updateIsRequest(long loans_id, boolean b) {
        String sqlStr = "UPDATE Loans SET is_request = ? WHERE loan_id = ?";
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            preparedStatement.setBoolean(1, b);
            preparedStatement.setLong(2, loans_id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public long getLoansIDByBookDetailId(String book_detail_id) {
        String sqlStr = "SELECT loan_id FROM Loans WHERE book_detail_id = ? AND is_returned = 0";
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            preparedStatement.setString(1, book_detail_id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                return resultSet.getLong(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
return 0;
    }


    public List<Long> getCloseToDueDateLoans() {
        String sql = "WITH LatestApprovedRequests AS (\n" +
                "    SELECT loan_id, request_date,\n" +
                "           ROW_NUMBER() OVER(PARTITION BY loan_id ORDER BY request_date DESC) as rn\n" +
                "    FROM Request\n" +
                "    WHERE is_approved = 1\n" +
                ")\n" +
                "SELECT l.loan_id\n" +
                "FROM Loans l\n" +
                "         LEFT JOIN LatestApprovedRequests r ON l.loan_id = r.loan_id\n" +
                "WHERE l.is_returned = 0 AND l.status = 'Borrowing' AND (\n" +
                "        (l.is_request = 0 AND l.due_date < ?) OR\n" +
                "        (l.is_request = 1 AND ((r.rn = 1 AND r.request_date <  ?) OR (r.loan_id IS NULL AND l.due_date <  ?)))\n" +
                "    )\n";
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            java.sql.Date sqlDate = new java.sql.Date(System.currentTimeMillis() + 3 * 24 * 60 * 60 * 1000);
            preparedStatement.setDate(1,sqlDate);
            preparedStatement.setDate(2, sqlDate);
            preparedStatement.setDate(3, sqlDate);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Long> overdueLoans = new ArrayList<>();
            while (resultSet.next()) {
                overdueLoans.add(resultSet.getLong(1));
            }
            return overdueLoans;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Long> getExpriedLoans() {
        String sql = "Select loan_id from Loans where status = 'Reserved' and is_returned = 0 and expried_date < ?";
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            java.sql.Date sqlDate = new java.sql.Date(System.currentTimeMillis());
            preparedStatement.setDate(1,sqlDate);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Long> overdueLoans = new ArrayList<>();
            while (resultSet.next()) {
                overdueLoans.add(resultSet.getLong(1));
            }
            return overdueLoans;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void updateStatus2(long loans_id, String status, String username) {
        String sqlStr = "UPDATE Loans SET status = ?, updated_user = ?, updated_date = ?, is_returned = 1 WHERE loan_id = ?";
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            preparedStatement.setString(1, status);
            preparedStatement.setString(2, username);
            preparedStatement.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
            preparedStatement.setLong(4, loans_id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Loans> searchLoans(String by, String query) {
        ArrayList<Loans> loansList = new ArrayList<>();
        String sql = "SELECT l.*, b.title, b.img, b.book_id, a.student_id, r.description AS request_description  FROM Loans l " +
                "JOIN BookDetail bd ON l.book_detail_id = bd.book_detail_id " +
                "JOIN BookEdition be ON bd.ISBN = be.ISBN " +
                "JOIN Books b ON be.book_id = b.book_id " +
                "JOIN Accounts a ON l.user_id = a.user_id "+
                "JOIN Request r ON l.loan_id = r.loan_id ";

        // Điều kiện tìm kiếm cần thêm thông tin từ bảng Accounts nếu tìm kiếm theo student_id
        if ("student_id".equals(by)) {
            sql += "WHERE a.student_id LIKE ?";
        } else if ("bookCode".equals(by)) {
            sql += "WHERE l.book_detail_id LIKE ?";
        } else if ("bookTitle".equals(by)) {
            sql += "WHERE b.title LIKE ?";
        } else if ("recordId".equals(by)) {
            sql += "WHERE l.loan_id = ?";
        } else {
            throw new IllegalArgumentException("Search category not supported.");
        }

        try (Connection conn = this.getConnection(); // Giả sử bạn đã có phương thức getConnection
             PreparedStatement stmt = conn.prepareStatement(sql)) {

            // Đặt tham số cho câu lệnh SQL
            if (by.equals("recordId")) {
                stmt.setInt(1, Integer.parseInt(query));
            } else {
                stmt.setString(1, "%" + query + "%");
            }

            // Thực hiện truy vấn
            try (ResultSet resultSet = stmt.executeQuery()) {
                while (resultSet.next()) {
                    Loans l = new Loans();
                    l.setLoans_id(resultSet.getInt("loan_id"));
                    l.setUser_id(resultSet.getInt("user_id"));
                    l.setBook_detail_id(resultSet.getString("book_detail_id"));
                    l.setBook_id(resultSet.getInt("book_id"));
                    l.setTitle(resultSet.getString("title"));
                    l.setImg(resultSet.getString("img"));
                    l.setBorrow_date(resultSet.getTimestamp("borrow_date"));
                    l.setDue_date(resultSet.getDate("due_date"));
                    l.setStatus(resultSet.getString("status"));
                    l.setCreate_date(resultSet.getTimestamp(8));
                    l.setCreate_user(resultSet.getString(9));
                    l.setReturn_date(resultSet.getTimestamp(6));
                    l.setExpried_date(resultSet.getDate("expried_date"));
                    l.setIs_returned(resultSet.getBoolean("is_returned"));
                    l.setIs_request(resultSet.getBoolean("is_request"));
                    l.setStudent_id(resultSet.getString("student_id"));
                    l.setRequest_status(resultSet.getString("request_description"));

                    loansList.add(l);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            // Xử lý ngoại lệ: Bạn có thể muốn log lỗi hoặc ném một unchecked exception
        } catch (NumberFormatException e) {
            // Xử lý trường hợp 'query' không phải là số khi tìm kiếm theo 'recordId'
            e.printStackTrace();
            // Xử lý ngoại lệ: Bạn có thể muốn log lỗi hoặc thông báo lỗi tới người dùng
        }

        return loansList;
    }

    public String getStatus(String loans_id) {
        String sqlStr = "SELECT status FROM Loans WHERE loan_id = ?";
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStr);
            preparedStatement.setString(1, loans_id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                return resultSet.getString(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
