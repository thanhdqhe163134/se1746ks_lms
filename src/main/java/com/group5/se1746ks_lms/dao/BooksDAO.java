package com.group5.se1746ks_lms.dao;

import com.group5.se1746ks_lms.model.*;
import com.group5.se1746ks_lms.util.DBConnect;

import java.sql.*;
import java.util.*;

public class BooksDAO {

    public List<Books> getAllBooks(int currentPage, int itemsPerPage, String sortBy) {
        List<Books> bookList = new ArrayList<>();
        String orderByClause = "b.book_id";
        if ("newest".equals(sortBy)) {
            orderByClause = "b.created_date DESC";
        } else if ("oldest".equals(sortBy)) {
            orderByClause = "b.created_date ASC";
        } else if ( "az".equals(sortBy)) {
            orderByClause = "b.title";
        } else if ("za".equals(sortBy)){
            orderByClause = "b.title DESC";
        }
        String sql = "SELECT * FROM Books b WHERE b.is_deleted = 0 ORDER BY " + orderByClause + " OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";

        Connection connection = DBConnect.getConnection();
        if (connection != null) {
            try (PreparedStatement statement = connection.prepareStatement(
                    sql)) {

                int offset = (currentPage - 1) * itemsPerPage;

                statement.setInt(1, offset);
                statement.setInt(2, itemsPerPage);

                try (ResultSet resultSet = statement.executeQuery()) {
                    while (resultSet.next()) {
                        Books book = new Books();
                        book.setBook_id(resultSet.getLong("book_id"));
                        book.setTitle(resultSet.getString("title"));
                        book.setAuthor(resultSet.getString("author"));
                        book.setTotal_copies(resultSet.getInt("total_copies"));
                        book.setCopies_borrowed(resultSet.getInt("copies_borrowed"));
                        book.setCopies_available(resultSet.getInt("copies_available"));
                        book.setIMG(resultSet.getString("IMG"));
                        book.setIs_deleted(resultSet.getBoolean("is_deleted"));
                        if (!book.getIs_deleted()) {
                            book.setGenres(getGenresForBook(book.getBook_id(), connection));
                            bookList.add(book);
                        }
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return bookList;
    }

    public int getTotalBooksCount() {
        int count = 0;
        Connection connection = DBConnect.getConnection();
        if (connection != null) {
            try (Statement statement = connection.createStatement()) {
                String sql = "SELECT COUNT(*) FROM Books WHERE is_deleted = 0";
                try (ResultSet resultSet = statement.executeQuery(sql)) {
                    if (resultSet.next()) {
                        count = resultSet.getInt(1);
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return count;
    }


    private Set<Genres> getGenresForBook(Long bookId, Connection connection) {
        Set<Genres> genresSet = new HashSet<>();
        String sql = "SELECT g.*, bg.is_deleted\n" +
                "FROM Genres g\n" +
                "JOIN BookGenre bg ON g.genre_id = bg.genre_id\n" +
                "WHERE bg.book_id = ? AND bg.is_deleted = 0;\n";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, bookId);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    Genres genre = new Genres();
                    genre.setGenre_id(resultSet.getLong("genre_id"));
                    genre.setGenre_name(resultSet.getString("genre_name"));

                        genresSet.add(genre);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return genresSet;
    }


    public Books getBookById(Long bookId) {
        Books book = new Books();
        Connection connection = DBConnect.getConnection();
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM Books WHERE book_id = ?")) {
                preparedStatement.setLong(1, bookId);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        book.setBook_id(resultSet.getLong("book_id"));
                        book.setTitle(resultSet.getString("title"));
                        book.setAuthor(resultSet.getString("author"));
                        book.setDescription(resultSet.getString("description"));
                        book.setTotal_copies(resultSet.getInt("total_copies"));
                        book.setCopies_borrowed(resultSet.getInt("copies_borrowed"));
                        book.setCopies_available(resultSet.getInt("copies_available"));
                        book.setIMG(resultSet.getString("IMG"));
                        book.setUpdated_date(resultSet.getTimestamp("updated_date"));

                        book.setUpdated_user(resultSet.getString("updated_user"));
                        book.setCreated_date(resultSet.getTimestamp("created_date"));
                        book.setCreated_user(resultSet.getString("created_user"));
                        book.setIs_deleted(resultSet.getBoolean("is_deleted"));
                        if (!book.getIs_deleted()) {
                            book.setGenres(getGenresForBook(book.getBook_id(), connection));
                        }
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return book;
    }

    public List<Books> searchBooksByTitle(String query) {
        List<Books> bookList = new ArrayList<>();
        Connection connection = DBConnect.getConnection();
        if (connection != null) {
            try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM Books WHERE title LIKE ?")) {
                statement.setString(1, "%" + query + "%");
                try (ResultSet resultSet = statement.executeQuery()) {
                    while (resultSet.next()) {
                        Books book = new Books();
                        book.setBook_id(resultSet.getLong("book_id"));
                        book.setTitle(resultSet.getString("title"));
                        book.setAuthor(resultSet.getString("author"));
                        book.setTotal_copies(resultSet.getInt("total_copies"));
                        book.setCopies_borrowed(resultSet.getInt("copies_borrowed"));
                        book.setCopies_available(resultSet.getInt("copies_available"));
                        book.setIMG(resultSet.getString("IMG"));
                        book.setIs_deleted(resultSet.getBoolean("is_deleted"));
                        if (!book.getIs_deleted()) {
                            book.setGenres(getGenresForBook(book.getBook_id(), connection));
                            bookList.add(book);
                        }
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return bookList;
    }

    public boolean deleteBook(int book_id) {
        Connection connection = DBConnect.getConnection();
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("UPDATE Books SET is_deleted = 1 WHERE book_id = ?")) {
                preparedStatement.setInt(1, book_id);
                preparedStatement.executeUpdate();
                return true;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public Long addBook(Books book, String[] genres) {
        Connection connection = DBConnect.getConnection();
        if (connection != null) {
            String sql = "INSERT INTO Books (title, author, total_copies, copies_borrowed, copies_available, description, IMG, created_date, created_user, is_deleted) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {

                preparedStatement.setString(1, book.getTitle());
                preparedStatement.setString(2, book.getAuthor());
                preparedStatement.setInt(3, book.getTotal_copies());
                preparedStatement.setInt(4, book.getCopies_borrowed());
                preparedStatement.setInt(5, book.getCopies_available());
                preparedStatement.setString(6, book.getDescription());
                preparedStatement.setString(7, book.getIMG());
                preparedStatement.setTimestamp(8, new java.sql.Timestamp(book.getCreated_date().getTime()));
                preparedStatement.setString(9, book.getUpdated_user());
                preparedStatement.setBoolean(10, book.getIs_deleted());

                int affectedRows = preparedStatement.executeUpdate();
                if (affectedRows > 0) {
                    try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                        if (generatedKeys.next()) {
                            Long bookId = generatedKeys.getLong(1);
                            addBookGenres(bookId, genres, connection);
                            return bookId;
                        }
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private void addBookGenres(Long bookId, String[] genres, Connection connection) {
        String sql = "INSERT INTO BookGenre (book_id, genre_id, is_deleted, created_date, created_user) VALUES (?, ?, ?, ?, ?)";
        String checkSql = "SELECT * FROM BookGenre WHERE book_id = ? AND genre_id = ?";
        String updateSql = "UPDATE BookGenre SET is_deleted = 0 WHERE book_id = ? AND genre_id = ?";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql);
             PreparedStatement checkStatement = connection.prepareStatement(checkSql);
             PreparedStatement updateStatement = connection.prepareStatement(updateSql)) {
            for (String genreId : genres) {
                // Check if the record already exists
                checkStatement.setLong(1, bookId);
                checkStatement.setLong(2, Long.parseLong(genreId));
                ResultSet resultSet = checkStatement.executeQuery();
                if (resultSet.next()) {
                    // Update the is_deleted field to 0
                    updateStatement.setLong(1, bookId);
                    updateStatement.setLong(2, Long.parseLong(genreId));
                    updateStatement.executeUpdate();
                    continue;
                }

                // If record does not exist, insert new record
                preparedStatement.setLong(1, bookId);
                preparedStatement.setLong(2, Long.parseLong(genreId));
                preparedStatement.setBoolean(3, false);
                preparedStatement.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
                preparedStatement.setString(5, "admin");
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



    public List<Books> getBooksByGenreId(String genre_id, int currentPage, int itemsPerPage, String sortBy) {
        List<Books> bookList = new ArrayList<>();
        String orderByClause = "b.book_id";
        if ("newest".equals(sortBy)) {
            orderByClause = "b.created_date DESC";
        } else if ("oldest".equals(sortBy)) {
            orderByClause = "b.created_date ASC";
        } else if ( "az".equals(sortBy)) {
            orderByClause = "b.title";
        } else if ("za".equals(sortBy)){
        orderByClause = "b.title DESC";
        }

        String sql = "SELECT * FROM Books b JOIN BookGenre bg ON b.book_id = bg.book_id WHERE bg.genre_id = ? AND b.is_deleted = 0 ORDER BY " + orderByClause + " OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";

        Connection connection = DBConnect.getConnection();
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setString(1, genre_id);
                int offset = (currentPage - 1) * itemsPerPage;
                preparedStatement.setInt(2, offset);
                preparedStatement.setInt(3, itemsPerPage);

                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        Books book = new Books();
                        book.setBook_id(resultSet.getLong("book_id"));
                        book.setTitle(resultSet.getString("title"));
                        book.setAuthor(resultSet.getString("author"));
                        book.setTotal_copies(resultSet.getInt("total_copies"));
                        book.setCopies_borrowed(resultSet.getInt("copies_borrowed"));
                        book.setCopies_available(resultSet.getInt("copies_available"));
                        book.setIMG(resultSet.getString("IMG"));
                        book.setIs_deleted(resultSet.getBoolean("is_deleted"));
                        if (!book.getIs_deleted()) {
                            book.setGenres(getGenresForBook(book.getBook_id(), connection));
                            bookList.add(book);
                        }
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return bookList;
    }

    public void updateBookGenres(Books book, String[] genres) {
        Connection connection = DBConnect.getConnection();
        if (connection != null) {
            try {
                // Xóa tất cả các genres hiện tại của sách
                try (PreparedStatement preparedStatement = connection.prepareStatement("UPDATE BookGenre SET is_deleted = 1 WHERE book_id = ?")) {
                    preparedStatement.setLong(1, book.getBook_id());
                    preparedStatement.executeUpdate();
                }

                // Thêm các genres mới cho sách
                addBookGenres(book.getBook_id(), genres, connection);

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void updateBook(Books book) {
        Connection connection = DBConnect.getConnection();
        if (connection != null) {
            String sql = "UPDATE Books SET title = ?, author = ?, description = ?, IMG = ?, Updated_user =  ?, Updated_date = ? WHERE book_id = ?";
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setString(1, book.getTitle());
                preparedStatement.setString(2, book.getAuthor());
                preparedStatement.setString(3, book.getDescription());
                preparedStatement.setString(4, book.getIMG());
                preparedStatement.setString(5, book.getUpdated_user());
                preparedStatement.setTimestamp(6, new java.sql.Timestamp(book.getUpdated_date().getTime()));
                preparedStatement.setLong(7, book.getBook_id());

                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public List<Books> getBooksByAuthor(String author, int currentPage, int itemsPerPage, String sortBy) {
        List<Books> bookList = new ArrayList<>();
        String orderByClause = "b.book_id";
        if ("newest".equals(sortBy)) {
            orderByClause = "b.created_date DESC";
        } else if ("oldest".equals(sortBy)) {
            orderByClause = "b.created_date ASC";
        } else if ( "az".equals(sortBy)) {
            orderByClause = "b.title";
        } else if ("za".equals(sortBy)){
            orderByClause = "b.title DESC";
        }
        String sql = "SELECT * FROM Books WHERE author LIKE ? ORDER BY " + orderByClause + " OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        Connection connection = DBConnect.getConnection();
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setString(1, "%" + author + "%");
                int offset = (currentPage - 1) * itemsPerPage;
                preparedStatement.setInt(2, offset);
                preparedStatement.setInt(3, itemsPerPage);

                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        Books book = new Books();
                        book.setBook_id(resultSet.getLong("book_id"));
                        book.setTitle(resultSet.getString("title"));
                        book.setAuthor(resultSet.getString("author"));
                        book.setTotal_copies(resultSet.getInt("total_copies"));
                        book.setCopies_borrowed(resultSet.getInt("copies_borrowed"));
                        book.setCopies_available(resultSet.getInt("copies_available"));
                        book.setIMG(resultSet.getString("IMG"));
                        book.setIs_deleted(resultSet.getBoolean("is_deleted"));
                        if (!book.getIs_deleted()) {
                            book.setGenres(getGenresForBook(book.getBook_id(), connection));
                            bookList.add(book);
                        }
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return bookList;
    }


    public List<Books> getBooksByPublisher(String publisher, int currentPage, int itemsPerPage, String sortBy) {
        List<Books> bookList = new ArrayList<>();
        String orderByClause = "b.book_id";
        if ("newest".equals(sortBy)) {
            orderByClause = "b.created_date DESC";
        } else if ("oldest".equals(sortBy)) {
            orderByClause = "b.created_date ASC";
        } else if ( "az".equals(sortBy)) {
            orderByClause = "b.title";
        } else if ("za".equals(sortBy)){
            orderByClause = "b.title DESC";
        }
        String sql = "SELECT * FROM Books WHERE publisher LIKE ? ORDER BY " + orderByClause + " OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        Connection connection = DBConnect.getConnection();
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setString(1, "%" + publisher + "%");
                int offset = (currentPage - 1) * itemsPerPage;
                preparedStatement.setInt(2, offset);
                preparedStatement.setInt(3, itemsPerPage);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        Books book = new Books();
                        book.setBook_id(resultSet.getLong("book_id"));
                        book.setTitle(resultSet.getString("title"));
                        book.setAuthor(resultSet.getString("author"));
                        book.setTotal_copies(resultSet.getInt("total_copies"));
                        book.setCopies_borrowed(resultSet.getInt("copies_borrowed"));
                        book.setCopies_available(resultSet.getInt("copies_available"));
                        book.setIMG(resultSet.getString("IMG"));
                        book.setIs_deleted(resultSet.getBoolean("is_deleted"));
                        if (!book.getIs_deleted()) {
                            book.setGenres(getGenresForBook(book.getBook_id(), connection));
                            bookList.add(book);
                        }
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return bookList;
    }

    public int getTotalBooksCountByAuthor(String author) {
        int count = 0;
        Connection connection = DBConnect.getConnection();
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT COUNT(*) FROM Books WHERE author LIKE ? AND is_deleted = 0")) {
                preparedStatement.setString(1, "%" + author + "%");
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        count = resultSet.getInt(1);
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return count;
    }

    public int getTotalBooksCountByPublisher(String publisher) {
        int count = 0;
        Connection connection = DBConnect.getConnection();
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT COUNT(*) FROM Books WHERE publisher LIKE ? AND is_deleted = 0")) {
                preparedStatement.setString(1, "%" + publisher + "%");
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        count = resultSet.getInt(1);
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return count;
    }

    public int getTotalBooksCountByGenreId(String genre_id) {
        int count = 0;
        Connection connection = DBConnect.getConnection();
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT COUNT(*) FROM Books b JOIN BookGenre bg ON b.book_id = bg.book_id WHERE bg.genre_id = ? AND b.is_deleted = 0")) {
                preparedStatement.setString(1, genre_id);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                            count = resultSet.getInt(1);
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return count;
    }

    public List<Publisher> getPublishersByBookId(Long bookId) {
        Connection conn = DBConnect.getConnection();
        List<Publisher> publisherList = new ArrayList<>();
        try {
            // Query for Publishers with the bookId
            String sql = "SELECT DISTINCT Publisher.* FROM Publisher JOIN BookEdition ON Publisher.publisher_id = BookEdition.publisher_id WHERE BookEdition.book_id = ? AND Publisher.is_deleted = 0";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setLong(1, bookId);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                // Add Publishers to publisherList
                Publisher publisher = new Publisher();
                publisher.setPublisher_id(rs.getLong("publisher_id"));
                publisher.setPublisher_name(rs.getString("name"));
                boolean is_deleted = rs.getBoolean("is_deleted");
                if (!is_deleted) {
                    publisherList.add(publisher);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return publisherList;
    }

    public List<BookEdition> getBookEditionsByPublisherId(long publisher_id, Long book_id) {
        Connection conn = DBConnect.getConnection();
        List<BookEdition> bookEditionList = new ArrayList<>();
        try {
            String sql = "SELECT * FROM BookEdition WHERE book_id = ? AND publisher_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setLong(1, book_id);
            stmt.setLong(2, publisher_id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                BookEdition bookEdition = new BookEdition();
                bookEdition.setBook_id(rs.getLong("book_id"));
                bookEdition.setPublisher_id(rs.getLong("publisher_id"));
                bookEdition.setPublish_year(rs.getInt("publish_year"));
                bookEdition.setISBN(rs.getString("ISBN"));
                bookEdition.setLanguage(rs.getString("language"));
                bookEdition.setUpdated_date(rs.getTimestamp("updated_date"));
                bookEdition.setCreated_user(rs.getString("created_user"));
                bookEdition.setUpdated_date(rs.getTimestamp("updated_date"));
                bookEdition.setUpdated_user(rs.getString("updated_user"));
                bookEdition.setDeleted_date(rs.getTimestamp("deleted_date"));
                bookEdition.setDeleted_user(rs.getString("deleted_user"));
                bookEdition.setIs_deleted(rs.getBoolean("is_deleted"));

                // Calculate total, available, and borrowed copies
                String isbn = rs.getString("ISBN");
                String countSql = "SELECT COUNT(*) AS total, " +
                        "SUM(CASE WHEN is_borrowed = 0 THEN 1 ELSE 0 END) AS available, " +
                        "SUM(CASE WHEN is_returned = 1 THEN 1 ELSE 0 END) AS borrowed " +
                        "FROM BookDetail WHERE ISBN = ? AND is_deleted = 0";
                PreparedStatement countStmt = conn.prepareStatement(countSql);
                countStmt.setString(1, isbn);
                ResultSet countRs = countStmt.executeQuery();
                if (countRs.next()) {
                    bookEdition.setTotal_copies(countRs.getInt("total"));
                    bookEdition.setAvailable_copies(countRs.getInt("available"));
                    bookEdition.setBorrowed_copies(countRs.getInt("borrowed"));
                }

                if (!bookEdition.getIs_deleted()) {
                    bookEditionList.add(bookEdition);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return bookEditionList;
    }

    public List<Publisher> getAllPublishers() {
        Connection conn = DBConnect.getConnection();
        List<Publisher> publisherList = new ArrayList<>();
        try{
            String sql = "SELECT * FROM Publisher WHERE is_deleted = 0";
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Publisher publisher = new Publisher();
                publisher.setPublisher_id(rs.getLong("publisher_id"));
                publisher.setPublisher_name(rs.getString("name"));
                publisherList.add(publisher);
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return publisherList;
        }


    public Long searchBooksIdByISBN(String query) {
        Long bookId = 0L;
        Connection connection = DBConnect.getConnection();
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT book_id FROM BookEdition WHERE ISBN = ? AND is_deleted = 0")) {
                preparedStatement.setString(1, query);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        bookId = resultSet.getLong("book_id");
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return bookId;

    }

    public Long getBookIdByTitle(String title) {
        Long bookId = 0L;
        Connection connection = DBConnect.getConnection();
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT book_id FROM Books WHERE title = ?")) {
                preparedStatement.setString(1, title);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        bookId = resultSet.getLong("book_id");
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return bookId;
    }

    public List<BookEdition> getAllBookEditions() {
        Connection conn = DBConnect.getConnection();
        List<BookEdition> bookEditionList = new ArrayList<>();
        try{
            String sql = "SELECT * FROM BookEdition";
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                BookEdition bookEdition = new BookEdition();
                bookEdition.setBook_id(rs.getLong("book_id"));
                bookEdition.setPublisher_id(rs.getLong("publisher_id"));
                bookEdition.setPublish_year(rs.getInt("publish_year"));
                bookEdition.setISBN(rs.getString("ISBN"));
                bookEdition.setLanguage(rs.getString("language"));
                bookEdition.setCreated_date(rs.getTimestamp("created_date"));
                bookEdition.setCreated_user(rs.getString("created_user"));
                bookEdition.setUpdated_date(rs.getTimestamp("updated_date"));
                bookEdition.setUpdated_user(rs.getString("updated_user"));
                bookEdition.setDeleted_date(rs.getTimestamp("deleted_date"));
                bookEdition.setDeleted_user(rs.getString("deleted_user"));
                bookEdition.setIs_deleted(rs.getBoolean("is_deleted"));
                if(!bookEdition.getIs_deleted()){
                    bookEditionList.add(bookEdition);
                }
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return bookEditionList;
    }


    public void updateBookEdition(BookEdition bookEdition) {
        Connection connection = DBConnect.getConnection();
        if (connection != null) {
            String sql = "UPDATE BookEdition SET publish_year = ?, language = ?, updated_user = ?, updated_date = ? WHERE isbn = ?";
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setInt(1, bookEdition.getPublish_year());
                preparedStatement.setString(2, bookEdition.getLanguage());
                preparedStatement.setString(3, bookEdition.getUpdated_user());
                preparedStatement.setTimestamp(4, new java.sql.Timestamp(bookEdition.getUpdated_date().getTime()));
                preparedStatement.setString(5, bookEdition.getISBN());

                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public List<BookDetail> getBookByISBN(String isbn) {
        Connection conn = DBConnect.getConnection();
        List<BookDetail> bookDetailList = new ArrayList<>();
        try{
            String sql = "SELECT * FROM BookDetail WHERE isbn = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, isbn);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                BookDetail bookDetail = new BookDetail();
                bookDetail.setBook_detail_id(rs.getString("book_detail_id"));
                bookDetail.setISBN(rs.getString("isbn"));
                bookDetail.setStatus(rs.getString("status"));
                bookDetail.setIs_borrowed(rs.getBoolean("is_borrowed"));
                bookDetail.setCreated_date(rs.getTimestamp("created_date"));
                bookDetail.setCreated_user(rs.getString("created_user"));
                bookDetail.setUpdated_date(rs.getTimestamp("updated_date"));
                bookDetail.setUpdated_user(rs.getString("updated_user"));
                bookDetail.setDeleted_date(rs.getTimestamp("deleted_date"));
                bookDetail.setDeleted_user(rs.getString("deleted_user"));
                bookDetail.setIs_deleted(rs.getBoolean("is_deleted"));
                if(!bookDetail.getIs_deleted()){
                    bookDetailList.add(bookDetail);
                }
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return bookDetailList;
    }

    public void addBookEdition(BookEdition bookEdition) {
        Connection conn = DBConnect.getConnection();

        try{
            String sql = "INSERT INTO BookEdition (ISBN, book_id, publisher_id, publish_year, language, created_date, created_user, is_deleted) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, bookEdition.getISBN());
            stmt.setLong(2, bookEdition.getBook_id());
            stmt.setLong(3, bookEdition.getPublisher_id());
            stmt.setInt(4, bookEdition.getPublish_year());
            stmt.setString(5, bookEdition.getLanguage());

            stmt.setTimestamp(6, new java.sql.Timestamp(bookEdition.getCreated_date().getTime()));
            stmt.setString(7, bookEdition.getCreated_user());
            stmt.setBoolean(8, bookEdition.getIs_deleted());
            stmt.executeUpdate();

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    public boolean existBookDetail(String book) {
        Connection conn = DBConnect.getConnection();
        try{
            String sql = "SELECT * FROM BookDetail WHERE book_detail_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, book);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return true;
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return false;
    }

    public List<Books> searchBooks(String query, String type) {
        List<Books> bookList = new ArrayList<>();
        Connection connection = DBConnect.getConnection();
        if (connection != null) {
            try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM Books WHERE " + type + " LIKE ?")) {
                statement.setString(1, "%" + query + "%");
                try (ResultSet resultSet = statement.executeQuery()) {
                    while (resultSet.next()) {
                        Books book = new Books();
                        book.setBook_id(resultSet.getLong("book_id"));
                        book.setTitle(resultSet.getString("title"));
                        book.setAuthor(resultSet.getString("author"));
                        book.setTotal_copies(resultSet.getInt("total_copies"));
                        book.setCopies_borrowed(resultSet.getInt("copies_borrowed"));
                        book.setCopies_available(resultSet.getInt("copies_available"));
                        book.setIMG(resultSet.getString("IMG"));
                        book.setIs_deleted(resultSet.getBoolean("is_deleted"));
                        if (!book.getIs_deleted()) {
                            book.setGenres(getGenresForBook(book.getBook_id(), connection));
                            bookList.add(book);
                        }
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return bookList;
    }


    public  List<Books> searchBooks2(String query, String type) {
        List<Books> bookList = new ArrayList<>();
        Connection connection = DBConnect.getConnection();
        if (connection != null) {
            String searchColumn;
            switch (type) {
                case "year":
                case "language":
                case "isbn":
                    searchColumn = "e." + type;
                    break;
                case "publisher":
                    searchColumn = "p.name";
                    break;
                default:
                    throw new IllegalArgumentException("Search type not supported.");
            }

            String sql = "SELECT\n" +
                    "    b.book_id,\n" +
                    "    b.title,\n" +
                    "    b.author,\n" +
                    "    b.total_copies,\n" +
                    "    b.copies_borrowed,\n" +
                    "    b.copies_available,\n" +
                    "    b.description,\n" +
                    "    b.IMG,\n" +
                    "    b.created_date,\n" +
                    "    b.created_user,\n" +
                    "    b.updated_date,\n" +
                    "    b.updated_user,\n" +
                    "    b.deleted_date,\n" +
                    "    b.deleted_user,\n" +
                    "    b.is_deleted,\n" +
                    "    p.name AS publisher,\n" +
                    "    e.ISBN,\n" +
                    "    e.publish_year,\n" +
                    "    e.language\n" +
                    "FROM\n" +
                    "    Books b\n" +
                    "        JOIN\n" +
                    "    BookEdition e ON b.book_id = e.book_id\n" +
                    "        JOIN\n" +
                    "    Publisher p ON e.publisher_id = p.publisher_id\n" +
                    "WHERE\n" +
                    "        " + searchColumn + " LIKE ? AND\n" +
                    "        b.is_deleted = 0 AND\n" +
                    "        e.is_deleted = 0 AND\n" +
                    "        p.is_deleted = 0;\n";

            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setString(1, "%" + query + "%");
                try (ResultSet resultSet = statement.executeQuery()) {
                    while (resultSet.next()) {

                        Books book = new Books();
                        book.setBook_id(resultSet.getLong("book_id"));
                        book.setTitle(resultSet.getString("title"));
                        book.setAuthor(resultSet.getString("author"));
                        book.setIMG(resultSet.getString("IMG"));
                        book.setPublisher(resultSet.getString("publisher"));
                        book.setIsbn(resultSet.getString("ISBN"));
                        book.setYear(resultSet.getInt("publish_year"));
                        book.setLanguage(resultSet.getString("language"));
                        book.setIs_deleted(resultSet.getBoolean("is_deleted"));
                        if (!book.getIs_deleted()) {
                            book.setGenres(getGenresForBook(book.getBook_id(), connection));
                            bookList.add(book);
                        }
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return bookList;
    }

    public List<Books> getBooksByTitle(String title, int currentPage, int itemsPerPage) {
        List<Books> bookList = new ArrayList<>();
        Connection connection = DBConnect.getConnection();
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM Books WHERE title LIKE ? ORDER BY book_id OFFSET ? ROWS FETCH NEXT ? ROWS ONLY")) {
                preparedStatement.setString(1, "%" + title + "%");
                int offset = (currentPage - 1) * itemsPerPage;
                preparedStatement.setInt(2, offset);
                preparedStatement.setInt(3, itemsPerPage);

                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        Books book = new Books();
                        book.setBook_id(resultSet.getLong("book_id"));
                        book.setTitle(resultSet.getString("title"));
                        book.setAuthor(resultSet.getString("author"));
                        book.setTotal_copies(resultSet.getInt("total_copies"));
                        book.setCopies_borrowed(resultSet.getInt("copies_borrowed"));
                        book.setCopies_available(resultSet.getInt("copies_available"));
                        book.setIMG(resultSet.getString("IMG"));
                        book.setIs_deleted(resultSet.getBoolean("is_deleted"));
                        if (!book.getIs_deleted()) {
                            book.setGenres(getGenresForBook(book.getBook_id(), connection));
                            bookList.add(book);
                        }
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return bookList;
    }

    public int getTotalBooksCountByTitle(String title) {
        int count = 0;
        Connection connection = DBConnect.getConnection();
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT COUNT(*) FROM Books WHERE title LIKE ? AND is_deleted = 0")) {
                preparedStatement.setString(1, "%" + title + "%");
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        count = resultSet.getInt(1);
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return count;
    }


    public void addBookDetail(BookDetail bookDetail) {
        Connection conn = DBConnect.getConnection();

        try{
            String sql = "INSERT INTO BookDetail (book_detail_id, isbn, status, is_borrowed, created_date, created_user, is_deleted) VALUES (?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, bookDetail.getBook_detail_id());
            stmt.setString(2, bookDetail.getISBN());
            stmt.setString(3, bookDetail.getStatus());
            stmt.setBoolean(4, bookDetail.getIs_borrowed());
            stmt.setTimestamp(5, new java.sql.Timestamp(bookDetail.getCreated_date().getTime()));
            stmt.setString(6, bookDetail.getCreated_user());
            stmt.setBoolean(7, bookDetail.getIs_deleted());
            stmt.executeUpdate();

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    public boolean existBookEdition(String isbn) {
        Connection conn = DBConnect.getConnection();
        try{
            String sql = "SELECT * FROM BookEdition WHERE isbn = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, isbn);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return true;
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return false;
    }
}



