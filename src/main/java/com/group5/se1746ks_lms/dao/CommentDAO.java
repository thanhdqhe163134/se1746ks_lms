package com.group5.se1746ks_lms.dao;

import com.group5.se1746ks_lms.model.Comment;
import com.group5.se1746ks_lms.util.DBConnect;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

public class CommentDAO {
    public List<Comment> getAllCommentsByBookId(Long book_id) {
        List<Comment> comments = new ArrayList<>();
        Connection connection = DBConnect.getConnection();
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM comment WHERE book_id = ? ORDER BY created_date")) {
                preparedStatement.setLong(1, book_id);
                java.sql.ResultSet rs = preparedStatement.executeQuery();
                while (rs.next()) {
                    Comment comment = new Comment();
                    comment.setComment_id(rs.getLong("comment_id"));
                    comment.setUser_id(rs.getInt("user_id"));
                    comment.setBook_id(rs.getLong("book_id"));
                    comment.setContent(rs.getString("content"));
                    comment.setRep_comment(rs.getLong("rep_comment"));
                    comment.setCreated_date(rs.getDate("created_date"));
                    comment.setCreated_user(rs.getString("created_user"));
                    comment.setUpdated_date(rs.getDate("updated_date"));
                    comment.setUpdated_user(rs.getString("updated_user"));
                    comment.setIs_deleted(rs.getBoolean("is_deleted"));
                    if (comment.getIs_deleted() == true) {
                        comment.setContent("This comment has been deleted");
                    }
                    comment.setIMG(getIMG(rs.getString("user_id")));

                    comments.add(comment);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
        return comments;
    }

    public String getIMG(String user_id) {
        String img = "";
        Connection connection = DBConnect.getConnection();
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT img FROM Accounts WHERE user_id = ?")) {
                preparedStatement.setString(1, user_id);
                java.sql.ResultSet rs = preparedStatement.executeQuery();
                while (rs.next()) {
                    img = rs.getString("img");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return img;
    }

    public void deleteComment(Long comment_id, String username) {
        Connection connection = DBConnect.getConnection();
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("UPDATE comment SET is_deleted = 1, updated_date = ?, updated_user = ? WHERE comment_id = ?")) {
                preparedStatement.setDate(1, new java.sql.Date(new java.util.Date().getTime()));
                preparedStatement.setString(2, username);
                preparedStatement.setLong(3, comment_id);
                preparedStatement.executeUpdate();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void updateComment(Long comment_id, String content, String username) {
        Connection connection = DBConnect.getConnection();
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("UPDATE comment SET content = ?, updated_date = ?, updated_user = ? WHERE comment_id = ?")) {
                preparedStatement.setString(1, content);
                preparedStatement.setDate(2, new java.sql.Date(new java.util.Date().getTime()));
                preparedStatement.setString(3, username);
                preparedStatement.setLong(4, comment_id);
                preparedStatement.executeUpdate();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public void CreateComment(String content, String username, Long book_id, Long rep_comment, Long user_id) {
        Connection connection = DBConnect.getConnection();
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO comment (user_id, book_id, content, rep_comment, created_date, created_user, updated_date, updated_user, is_deleted) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)")) {
                preparedStatement.setLong(1, user_id);
                preparedStatement.setLong(2, book_id);
                preparedStatement.setString(3, content);
                preparedStatement.setLong(4, rep_comment);
                preparedStatement.setDate(5, new java.sql.Date(new java.util.Date().getTime()));
                preparedStatement.setString(6, username);
                preparedStatement.setDate(7, new java.sql.Date(new java.util.Date().getTime()));
                preparedStatement.setString(8, username);
                preparedStatement.setBoolean(9, false);
                preparedStatement.executeUpdate();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public void restoreComment(Long comment_id) {
        Connection connection = DBConnect.getConnection();
        if (connection != null) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("UPDATE comment SET is_deleted = false, updated_date = ? WHERE comment_id = ?")) {
                preparedStatement.setDate(1, new java.sql.Date(new java.util.Date().getTime()));
                preparedStatement.setLong(2, comment_id);
                preparedStatement.executeUpdate();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
