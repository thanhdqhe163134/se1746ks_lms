package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.BooksDAO;
import com.group5.se1746ks_lms.model.Books;
import com.group5.se1746ks_lms.model.Genres;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@WebServlet(name = "HomeSearchServlet", value = "/homeSearch")
public class HomeSearchServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String query = request.getParameter("query");
        String type = request.getParameter("type");
        BooksDAO booksDAO = new BooksDAO();
        if(query.equals("")) {
            return;
        }
        if(type.equals("title") || type.equals("author" ) || type.equals("genre") || type.equals("all")){
            List<Books>  bookList = booksDAO.searchBooks(query, type);
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();
            for (Books book : bookList) {
                out.println("<div class='book-card2' onclick=\"window.location.href='book?book_id=" + book.getBook_id() + "'\">");
                out.println("<img src='" + book.getIMG() + "' alt='" + book.getTitle() + "' class='book-img2'>");
                out.println("<h3 class='book-title2'>" + book.getTitle() + "</h3>");
                if(type.equals("author")) out.println("<strong> <p class='book-author2'>" + book.getAuthor() + "</p> </strong>");
                else out.println("<p class='book-author2'>" + book.getAuthor() + "</p>");
                out.println("<div class='book-genres2'>");
                Set<Genres> genres = book.getGenres();
                genres = genres.stream().sorted(Comparator.comparing(Genres::getGenre_name)).collect(Collectors.toCollection(LinkedHashSet::new));
                for (Genres genre : genres) {
                    out.println("<span class='genre2'>" + genre.getGenre_name() + "</span>");
                }
                out.println("</div>");
                out.println("</div>");
            }
        } else if(type.equals("isbn") || type.equals("year") || type.equals("language") || type.equals("publisher")){
            List<Books>  bookList = booksDAO.searchBooks2(query, type);
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();
            for (Books book : bookList) {
                out.println("<div class='book-card2' onclick=\"window.location.href='book?book_id=" + book.getBook_id() + "'\">");
                out.println("<img src='" + book.getIMG() + "' alt='" + book.getTitle() + "' class='book-img2'>");
                out.println("<h3 class='book-title2'>" + book.getTitle() + "</h3>");
                out.println("<p class='book-author2'>" + book.getAuthor() + "</p>");
                out.println("<strong> <p class='book-publisher2'>" + book.getPublisher() + "</p> </strong>");
                out.println("<strong><p class='book-year2'>" + book.getYear() + "</p></strong>");
                out.println("<strong><p class='book-language2'>" + book.getLanguage() + "</p></strong>");
                if(type.equals("isbn")) out.println("<strong><p class='book-isbn'>" + book.getIsbn() + "</p></strong>");
                out.println("<div class='book-genres2'>");
                Set<Genres> genres = book.getGenres();
                genres = genres.stream().sorted(Comparator.comparing(Genres::getGenre_name)).collect(Collectors.toCollection(LinkedHashSet::new));
                for (Genres genre : genres) {
                    out.println("<span class='genre2'>" + genre.getGenre_name() + "</span>");
                }
                out.println("</div>");
                out.println("</div>");
            }

        }
    }



        @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
