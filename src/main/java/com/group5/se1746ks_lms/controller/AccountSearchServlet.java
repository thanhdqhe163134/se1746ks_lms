package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.AccountDAO;
import com.group5.se1746ks_lms.model.Account;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

@WebServlet(name = "AccountSearchServlet", value = "/searchAccounts")
public class AccountSearchServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String query = request.getParameter("query");
        AccountDAO accountDAO = new AccountDAO();
        ArrayList<Account> accounts = accountDAO.searchAccountByUsername(query);

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        for (Account acc:accounts) {
            out.println("<tr><td>" + acc.getUser_id() + "</td>");
            out.println("<td>" + acc.getStudent_id() + "</td>");
            out.println("<td>" + acc.getUsername() + "</td>");
            out.println("<td>" + acc.getFull_name() + "</td>");
            out.println("<td>" + acc.getEmail() + "</td>");
            out.println("<td>" + acc.getPhone_number() + "</td>");
            out.println("<td>" + acc.getRole() + "</td>");
            out.println("<td>" + acc.getCreated_date() + "</td>");
            out.println("<td class='action'><a href='accDetail?id=" + acc.getUser_id() + "' class='view'>");
            out.println("<span><i class='fa-solid fa-eye'></i>View</span></a>");
            out.println("<a href='deleteAcc?id=" + acc.getUser_id() + "' class='delete'>");
            out.println("<span><i class='fa-solid fa-trash-can'></i>Delete</span></a></td></tr>");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
