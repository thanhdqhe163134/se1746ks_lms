package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.BooksDAO;
import com.group5.se1746ks_lms.dao.CommentDAO;
import com.group5.se1746ks_lms.dao.GenresDAO;
import com.group5.se1746ks_lms.model.*;
import com.group5.se1746ks_lms.util.ImgurUploader;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 10,      // 10MB
        maxRequestSize = 1024 * 1024 * 50)   // 50MB
@WebServlet("/book")
public class BookServlet extends HttpServlet {
    BooksDAO booksDAO = new BooksDAO();

    GenresDAO genresDAO = new GenresDAO();

    CommentDAO commentDAO = new CommentDAO();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Comment> comments = new ArrayList<>();
        String error = request.getParameter("error");
        if (error != null) {
            request.setAttribute("error", error);
        }
        String success = request.getParameter("success");
        if (success != null) {
            request.setAttribute("success", success);
        }
        String title = request.getParameter("title");
        String isbn = request.getParameter("isbn");
        Long book_id = 0L;
        if (title != null) {
            book_id = booksDAO.getBookIdByTitle(title);
            if (book_id == 0L) {
                Genres genre = new Genres();
                request.setAttribute("allGenres", genresDAO.getAllGenres());
                List<Publisher> allPublishers = booksDAO.getAllPublishers();
                request.setAttribute("allPublishers", allPublishers);
                Books book = new Books();
                request.setAttribute("book", book);
                request.setAttribute("genre", genre);
                String isExist = "true";
                request.setAttribute("isExist", isExist);

                request.getRequestDispatcher("views/BookAdd.jsp").forward(request, response);
                return;
            }
        }
        if (isbn != null) {
            book_id = booksDAO.searchBooksIdByISBN(isbn);
            if (book_id == 0L) {
                Genres genre = new Genres();
                request.setAttribute("allGenres", genresDAO.getAllGenres());

                Books book = new Books();
                request.setAttribute("book", book);
                request.setAttribute("genre", genre);
                String isExist = "false";
                request.setAttribute("isExist", isExist);
                request.getRequestDispatcher("views/BookAdd.jsp").forward(request, response);
                return;

            }
        } else {
            book_id = Long.parseLong(request.getParameter("book_id"));
        }
        String action = request.getParameter("action");
        request.setAttribute("book", booksDAO.getBookById(book_id));
        request.setAttribute("allGenres", genresDAO.getAllGenres());
        String sortOrder = request.getParameter("sortOrder");
        comments = commentDAO.getAllCommentsByBookId(book_id);
        List<Comment> rootComments = new ArrayList<>();
        for (Comment comment : comments) {
            if (comment.getRep_comment() == 0) {
                rootComments.add(comment);
            }
        }
        if ("newest".equals(sortOrder)) {
            rootComments.sort(Comparator.comparing(Comment::getCreated_date).reversed());
        } else {
            rootComments.sort(Comparator.comparing(Comment::getCreated_date));
        }

        request.setAttribute("comments", comments);
        request.setAttribute("rootComments", rootComments);
        List<Publisher> publishers = booksDAO.getPublishersByBookId(book_id);
        List<Publisher> allPublishers = booksDAO.getAllPublishers();
        List<BookEdition> allBookEditions = booksDAO.getAllBookEditions();
        request.setAttribute("allPublishers", allPublishers);
        request.setAttribute("bookEditions", allBookEditions);
        request.setAttribute("publishers", publishers);
        if (action != null && action.equals("add")) {
            String isExist = "true";
            request.setAttribute("isExist", isExist);
            request.getRequestDispatcher("views/BookAdd.jsp").forward(request, response);
        } else {
            request.getRequestDispatcher("views/BookDetail.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String publsiher_idStr = request.getParameter("publisher_id");
            long publsiher_id = 0L;
            long book_id = 0L;
            String book_idStr = request.getParameter("book_id");
            if(book_idStr != null){
                book_id = Long.parseLong(book_idStr);


            }
            if(publsiher_idStr != null){
                publsiher_id = Long.parseLong(publsiher_idStr);
                String updated_user = request.getParameter("updated_user");
                String[] ChangedIsbn = request.getParameterValues("isbn");
                String[] ChangedPublish_year = request.getParameterValues("publish_year");
                String[] ChangedLanguage = request.getParameterValues("language");

                List<BookEdition> bookEditions = new ArrayList<>();
                for (int i = 0; i < ChangedIsbn.length; i++) {
                    BookEdition bookEdition = new BookEdition();
                    bookEdition.setISBN(ChangedIsbn[i]);
                    bookEdition.setPublish_year(Integer.parseInt(ChangedPublish_year[i]));
                    bookEdition.setLanguage(ChangedLanguage[i]);
                    bookEditions.add(bookEdition);
                }
                List<BookEdition> bookEdition = booksDAO.getBookEditionsByPublisherId(publsiher_id, book_id);
                for (int i = 0; i < bookEdition.size(); i++) {
                    bookEditions.get(i).setUpdated_user(updated_user);
                    bookEditions.get(i).setUpdated_date(new Timestamp(System.currentTimeMillis()));
                    if (!bookEdition.get(i).getISBN().equals(bookEditions.get(i).getISBN())) {
                        booksDAO.updateBookEdition(bookEditions.get(i));
                    } else if (!bookEdition.get(i).getLanguage().equals(bookEditions.get(i).getLanguage())) {
                        booksDAO.updateBookEdition(bookEditions.get(i));
                    } else if (bookEdition.get(i).getPublish_year() != bookEditions.get(i).getPublish_year()) {
                        booksDAO.updateBookEdition(bookEditions.get(i));
                    } else if (bookEdition.get(i).getPublish_year() == bookEditions.get(i).getPublish_year() && bookEdition.get(i).getLanguage().equals(bookEditions.get(i).getLanguage()) && bookEdition.get(i).getISBN().equals(bookEditions.get(i).getISBN())) {
                        continue;
                    }
                }
            }

            Books book = booksDAO.getBookById(book_id);
            if (book != null) {
                // Update the book's fields
                book.setTitle(request.getParameter("title"));
                book.setAuthor(request.getParameter("author"));
                book.setDescription(request.getParameter("description"));
                book.setUpdated_user(request.getParameter("updated_user"));
                book.setUpdated_date(new Timestamp(System.currentTimeMillis()));

                Part part = request.getPart("image");
                if (part != null && part.getSize() > 0 && part.getSubmittedFileName() != null && !part.getSubmittedFileName().isEmpty()) {
                    InputStream imageStream = part.getInputStream();
                    String imgURL = ImgurUploader.uploadToImgur(imageStream);
                    book.setIMG(imgURL);
                }

                String[] selectedGenres = request.getParameterValues("genres");
                String[] newGenresNames = request.getParameterValues("newGenres");

                List<String> allGenresIds = selectedGenres != null ? new ArrayList<>(Arrays.asList(selectedGenres)) : new ArrayList<>();

                if (newGenresNames != null) {
                    for (String newGenreName : newGenresNames) {
                        if (newGenreName != null && !newGenreName.trim().isEmpty()) {
                            Genres existingGenre = genresDAO.findByName(newGenreName);
                            if (existingGenre == null) {
                                Genres newGenre = genresDAO.create(newGenreName);
                                allGenresIds.add(String.valueOf(newGenre.getGenre_id()));
                            } else {
                                allGenresIds.add(String.valueOf(existingGenre.getGenre_id()));
                            }
                        }
                    }
                }

                booksDAO.updateBookGenres(book, allGenresIds.toArray(new String[0]));

                booksDAO.updateBook(book);

                response.sendRedirect("book?book_id=" + book_id);
            } else {
                response.sendRedirect("errorPage.jsp");
            }
        } catch (NumberFormatException e) {
            response.sendRedirect("errorPage.jsp");
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }
}