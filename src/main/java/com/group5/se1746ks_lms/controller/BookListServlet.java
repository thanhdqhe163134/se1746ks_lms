package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.BooksDAO;
import com.group5.se1746ks_lms.dao.GenresDAO;
import com.group5.se1746ks_lms.model.Books;
import com.group5.se1746ks_lms.model.Genres;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.List;


@WebServlet("/books")
public class BookListServlet extends HttpServlet {
    BooksDAO booksDAO = new BooksDAO();

    GenresDAO genresDAO = new GenresDAO();


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int currentPage = 1;
        int itemsPerPage = 8;
        if (req.getParameter("currentPage") != null) {
            currentPage = Integer.parseInt(req.getParameter("currentPage"));
        }
        if (req.getParameter("itemsPerPage") != null) {
            itemsPerPage = Integer.parseInt(req.getParameter("itemsPerPage"));
        }


        List<Books> bookList = null;
        List<Genres> allGenres = genresDAO.getAllGenres();
        allGenres.sort((Genres g1, Genres g2) -> g1.getGenre_name().compareTo(g2.getGenre_name()));


        String genre_id = req.getParameter("genre_id");
        String title = req.getParameter("title");
        String author = req.getParameter("author");
        String publisher = req.getParameter("publisher");

        int totalBooks = 0;
        int totalPages = 0;
        if (currentPage < 1) {
            currentPage = 1;
        }

        if (genre_id != null) {
            totalBooks = booksDAO.getTotalBooksCountByGenreId(genre_id);
        } else if (author != null) {
            totalBooks = booksDAO.getTotalBooksCountByAuthor(author);
        } else if (publisher != null) {
            totalBooks = booksDAO.getTotalBooksCountByPublisher(publisher);
        } else if (title != null) {
            totalBooks = booksDAO.getTotalBooksCountByTitle(title);
        } else {
            totalBooks = booksDAO.getTotalBooksCount();
        }



        totalPages = (int) Math.ceil((double) totalBooks / itemsPerPage);
        currentPage = (currentPage > totalPages) ? Math.max(totalPages, 1) : currentPage;

        String sortBy = req.getParameter("sortBy");
        if (genre_id != null) {
            bookList = booksDAO.getBooksByGenreId(genre_id, currentPage, itemsPerPage, sortBy);
        } else if (author != null) {
            bookList = booksDAO.getBooksByAuthor(author, currentPage, itemsPerPage, sortBy);
        } else if (publisher != null) {
            bookList = booksDAO.getBooksByPublisher(publisher, currentPage, itemsPerPage, sortBy);
        } else if(title != null){
            bookList = booksDAO.getBooksByTitle(title, currentPage, itemsPerPage);
        } else {
            bookList = booksDAO.getAllBooks(currentPage, itemsPerPage, sortBy);
        }


        if(totalPages == 0){
            totalPages = 1;
        }
        req.setAttribute("sortBy", sortBy);
        req.setAttribute("totalPages", totalPages);
        req.setAttribute("currentPage", currentPage);
        req.setAttribute("itemsPerPage", itemsPerPage);
        req.setAttribute("genre_id", genre_id);
        req.setAttribute("author", author);
        req.setAttribute("publisher", publisher);
        req.setAttribute("allGenres", allGenres);
        req.setAttribute("bookList", bookList);
        req.getRequestDispatcher("views/BookList.jsp").forward(req, resp);
    }


}
