package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.BookDetailDAO;
import com.group5.se1746ks_lms.dao.FineDAO;
import com.group5.se1746ks_lms.dao.LoansDAO;
import com.group5.se1746ks_lms.dao.ReturnedDAO;
import com.group5.se1746ks_lms.model.Fine;
import com.group5.se1746ks_lms.model.Loans;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@WebServlet(name = "ReturnFormServlet", value = "/returnForm")
public class ReturnFormServlet extends HttpServlet {
    ReturnedDAO returnedDAO = new ReturnedDAO();
    LoansDAO loansDAO = new LoansDAO();

    BookDetailDAO bookDetailDAO = new BookDetailDAO();

    FineDAO fineDAO = new FineDAO();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String loans_id = request.getParameter("loan_id");
        String status = loansDAO.getStatus(loans_id);

        if(loans_id != null){
            request.setAttribute("loans_id", loans_id);
            request.getRequestDispatcher("views/ReturnedCondition.jsp").forward(request, response);
        }


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String loanIdStr = request.getParameter("loan_id");
        String bookCondition = request.getParameter("book_condition");
        String username = request.getParameter("username");
        long loanId = Long.parseLong(loanIdStr);
        String fineOption = request.getParameter("fine_option");

        Loans loan = loansDAO.getLoansById(loanIdStr);

        if ("lost".equals(fineOption)) {
            int Lost = 100000;
            Fine fine = new Fine();
            fine.setLoans_id((int) loanId);
            fine.setFine_amount(Lost);
            fine.setDescription("Lost book");
            fine.setCreated_date(new java.sql.Timestamp(System.currentTimeMillis()));
            fine.setCreated_user(username);
            fine.setIs_paid(false);
            fineDAO.addFine(fine);

            loansDAO.updateStatusReturned2(String.valueOf(loanId), "Lost", username, new Timestamp(System.currentTimeMillis()));

            response.sendRedirect("fine?user_id=" + loan.getUser_id());


        } else if ("damage".equals(fineOption)) {
                int damage = 50000;
                Fine fine = new Fine();
                fine.setLoans_id((int) loanId);
                fine.setFine_amount(damage);
                fine.setDescription(bookCondition);
                fine.setCreated_date(new java.sql.Timestamp(System.currentTimeMillis()));
                fine.setCreated_user(username);
                fine.setIs_paid(false);
                fineDAO.addFine(fine);

            loansDAO.updateStatusReturned2(String.valueOf(loanId), "Returned", username, new Timestamp(System.currentTimeMillis()));

            bookDetailDAO.updateBookDetailStatus(loan.getBook_detail_id(), username);

            returnedDAO.returnBook(loanId, bookCondition, username);

            response.sendRedirect("fine?user_id=" + loan.getUser_id());


        } else if("No".equalsIgnoreCase(fineOption) && loan.getStatus().equalsIgnoreCase("Overdue")) {
            LocalDate dueDate = null;
            Calendar cal = Calendar.getInstance();

            if (loan.getIs_request()) {
                cal.setTime(loan.getNew_due_date());
                dueDate = LocalDate.of(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH));
            } else if (!loan.getIs_request() && loan.getStatus().equalsIgnoreCase("Overdue")) {
                cal.setTime(loan.getDue_date());
                dueDate = LocalDate.of(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH));
            }

            LocalDate currentDate = LocalDate.now();
            long daysLate = ChronoUnit.DAYS.between(dueDate, currentDate);

            if (daysLate > 0) {
                int fineAmount = (int) daysLate * 5000;
                Fine fine = new Fine();
                fine.setLoans_id((int) loanId);
                fine.setFine_amount(fineAmount);
                fine.setDescription("Overdue book for " + daysLate + " days");
                fine.setCreated_date(new java.sql.Timestamp(System.currentTimeMillis()));
                fine.setCreated_user(username);
                fine.setIs_paid(false);
                fineDAO.addFine(fine);
            }
            loansDAO.updateStatusReturned2(String.valueOf(loanId), "Returned", username, new Timestamp(System.currentTimeMillis()));

            bookDetailDAO.updateBookDetailStatus(loan.getBook_detail_id(), username);

            returnedDAO.returnBook(loanId, bookCondition, username);

            response.sendRedirect("fine?user_id=" + loan.getUser_id());

        } else {
            loansDAO.updateStatusReturned2(String.valueOf(loanId), "Returned", username, new Timestamp(System.currentTimeMillis()));

            bookDetailDAO.updateBookDetailStatus(loan.getBook_detail_id(), username);

            returnedDAO.returnBook(loanId, bookCondition, username);

            response.sendRedirect("loansDetail?loans_id=" + loanId);
        }
    }

}
