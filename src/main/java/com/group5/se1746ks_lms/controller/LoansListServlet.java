package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.LoansDAO;
import com.group5.se1746ks_lms.dao.RequestDAO;
import com.group5.se1746ks_lms.model.Loans;
import com.group5.se1746ks_lms.model.Request;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "LoansListServlet", value = "/loans")
public class LoansListServlet extends HttpServlet {
    LoansDAO loansDAO = new LoansDAO();
    RequestDAO requestDAO = new RequestDAO();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int currentPage = 1;
        int itemsPerPage = 5;

        if (request.getParameter("currentPage") != null) {
            currentPage = Integer.parseInt(request.getParameter("currentPage"));
        }
        if (request.getParameter("itemsPerPage") != null) {
            itemsPerPage = Integer.parseInt(request.getParameter("itemsPerPage"));
        }

        String searchBy = request.getParameter("searchBy");
        if(searchBy != null ){
            String query = request.getParameter("query");
            LoansDAO loansDAO = new LoansDAO();
            ArrayList<Loans> loans = new ArrayList<>();

            if (searchBy.equals("bookCode")) {
                loans = loansDAO.searchLoansByBookCode(query);
            } else if (searchBy.equals("bookTitle")) {
                loans = loansDAO.searchLoansByBookTitle(query);
            } else if (searchBy.equals("recordId")) {
                loans = loansDAO.searchLoansByRecordId(query);
            } else if (searchBy.equals("student_id")) {
                loans = loansDAO.searchLoansByStudentId(query);
            }

            List<Request> requests = new ArrayList<>();
            for(Loans loan : loans){
                Request req = requestDAO.getRequestByLoan_id(loan.getLoans_id());
                if(req != null)requests.add(req);
            }




            request.setAttribute("requests", requests);
            request.setAttribute("totalPages", 1);
            request.setAttribute("currentPage", currentPage);
            request.setAttribute("itemsPerPage", itemsPerPage);
            request.setAttribute("sortBy", null);
            request.setAttribute("loans", loans);
            request.getRequestDispatcher("views/LoansList.jsp").forward(request,response);


            request.getRequestDispatcher("views/LoansList.jsp").forward(request,response);
            return;


        }


        // Đầu tiên lấy tổng số loans để tính totalPages
        ArrayList<Loans> allLoans = loansDAO.getLoansList();
        int totalLoans = allLoans.size();
        int totalPages = (int) Math.ceil((double) totalLoans / itemsPerPage);

        // Điều chỉnh currentPage nếu cần
        currentPage = currentPage > totalPages ? Math.max(totalPages, 1) : currentPage;

        String sortBy = request.getParameter("sortBy");
        ArrayList<Loans> loansPerPage;

        if (sortBy != null && sortBy.equals("newest")) {
            loansPerPage = loansDAO.sortLoansList("DESC", currentPage, itemsPerPage);
        } else if (sortBy != null && sortBy.equals("oldest")) {
            loansPerPage = loansDAO.sortLoansList("ASC", currentPage, itemsPerPage);
        } else {
            loansPerPage = loansDAO.getLoansListPerPage(currentPage, itemsPerPage);
        }
        if(loansPerPage.size() == 0)
            loansPerPage = loansDAO.getLoansListPerPage2(currentPage, itemsPerPage);
        for(Loans loans : loansPerPage){
            Request req = requestDAO.getRequestByLoan_id(loans.getLoans_id());
            if(req != null && req.getIs_approved()){
                loans.setNew_due_date(req.getRequest_date());
            } else if(req != null && !req.getIs_approved()){
                loans.setRequest_id(req.getRequest_id());
            }
        }

        request.setAttribute("totalPages", totalPages);
        request.setAttribute("currentPage", currentPage);
        request.setAttribute("itemsPerPage", itemsPerPage);
        request.setAttribute("sortBy", sortBy);
        request.setAttribute("loans", loansPerPage);
        request.getRequestDispatcher("views/LoansList.jsp").forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }
}
