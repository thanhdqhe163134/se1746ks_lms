package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.AccountDAO;
import com.group5.se1746ks_lms.model.Account;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.sql.Date;

@WebServlet(name = "AccountDetailServlet", value = "/accDetail")
public class AccountDetailServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        AccountDAO accountDAO = new AccountDAO();
        String id = request.getParameter("id");
        Account a = accountDAO.getAccountById(id);
        request.setAttribute("a", a);
        String editMode = request.getParameter("editMode");
        if (editMode == null) {
            request.getRequestDispatcher("views/AccountDetails.jsp").forward(request, response);
        } else {
            request.getRequestDispatcher("views/AccountEdit.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
