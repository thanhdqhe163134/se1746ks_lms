package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.RequestDAO;
import com.group5.se1746ks_lms.model.Request;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;

@WebServlet(name = "RequestApproveServlet", value = "/approveRequest")
public class RequestApproveServlet extends HttpServlet {
    Request req = new Request();
    RequestDAO requestDAO = new RequestDAO();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String request_idStr = request.getParameter("request_id");
        if(request_idStr != null){
            long request_id = Long.parseLong(request_idStr);
            req = requestDAO.getRequestById(request_id);
            request.setAttribute("request", req);
            request.getRequestDispatcher("views/RequestApprove.jsp").forward(request, response);
        }
        else{
            response.sendRedirect("requestList");
        }


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String request_idStr = request.getParameter("request_id");
        String description = request.getParameter("description");

        String username = request.getParameter("username");
        String is_approved = request.getParameter("is_approved");
        if (request_idStr != null && description != null) {
            if(description.equals("") && is_approved.equals("true")){
                description = "Approved";
            } else if(description.equals("") && is_approved.equals("false")){
                description = "Rejected";
            }
            long request_id = Long.parseLong(request_idStr);
            boolean is_approvedBool = Boolean.parseBoolean(is_approved);
            requestDAO.updateRequestStatus(request_id, description,username,is_approvedBool);
            response.sendRedirect("loans");


        }
    }
}
