package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.BooksDAO;
import com.group5.se1746ks_lms.dao.GenresDAO;
import com.group5.se1746ks_lms.model.Books;
import com.group5.se1746ks_lms.model.Genres;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@WebServlet("/searchBooks")
public class BookSearchServlet2 extends HttpServlet {
    BooksDAO booksDAO = new BooksDAO();
    GenresDAO genresDAO = new GenresDAO();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
        int currentPage = 1;
        int itemsPerPage = 4;
        if (req.getParameter("currentPage") != null) {
            currentPage = Integer.parseInt(req.getParameter("currentPage"));
        }
        if (req.getParameter("itemsPerPage") != null) {
            itemsPerPage = Integer.parseInt(req.getParameter("itemsPerPage"));
        }


        List<Books> bookList = null;
        List<Genres> allGenres = genresDAO.getAllGenres();
        allGenres.sort((Genres g1, Genres g2) -> g1.getGenre_name().compareTo(g2.getGenre_name()));

        int totalBooks = 0;
        int totalPages = 0;
        if (currentPage < 1) {
            currentPage = 1;
        }
        String query = req.getParameter("query");
        String type = req.getParameter("type");
        if (query != null && type != null) {
            if (type.equals("author")) {
                totalBooks = booksDAO.getTotalBooksCountByAuthor(query);
            } else if (type.equals("publisher")) {
                totalBooks = booksDAO.getTotalBooksCountByPublisher(query);
            } else {
                totalBooks = booksDAO.getTotalBooksCount();
            }
        } else {
            totalBooks = booksDAO.getTotalBooksCount();
        }
        totalPages = (int) Math.ceil((double) totalBooks / itemsPerPage);
        if (currentPage > totalPages) {
            currentPage = totalPages;
        }
        if (query != null && type != null) {
            if (type.equals("author")) {
                bookList = booksDAO.getBooksByAuthor(query, currentPage, itemsPerPage, null);
            } else if (type.equals("publisher")) {
                bookList = booksDAO.getBooksByPublisher(query, currentPage, itemsPerPage, null);
            } else {
                bookList = booksDAO.getAllBooks(currentPage, itemsPerPage, null);
            }
        } else {
            bookList = booksDAO.getAllBooks(currentPage, itemsPerPage, null);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
