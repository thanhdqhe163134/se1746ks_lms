package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.AccountDAO;
import com.group5.se1746ks_lms.dao.BookDetailDAO;
import com.group5.se1746ks_lms.dao.LoansDAO;
import com.group5.se1746ks_lms.model.BookDetail;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Random;

@WebServlet("/bookBorrow")
public class BookBorrowServlet extends HttpServlet {
    BookDetailDAO bookDetailDAO = new BookDetailDAO();

    AccountDAO accountDAO = new AccountDAO();

    LoansDAO Loan = new LoansDAO();


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userIdStr = request.getParameter("user_id");
        long userId = 0L;

        if(userIdStr != null){
            userId = Long.parseLong(userIdStr);
        }
        String username = request.getParameter("username");
        String isbn = request.getParameter("ISBN");
        String bookId = request.getParameter("book_id");

        try {
            if(accountDAO.CountLoans(userId) >= 5){
                String error = "You have borrowed 5 books. Please return them before borrowing more.";
                response.sendRedirect("book?book_id=" + bookId  + "&error=" + error);
                return;
            }
            if (accountDAO.isRateLimited(userId)) {
                String error = "Too many requests. Please try again later.";
                response.sendRedirect("book?book_id=" + bookId  + "&error=" + error);
                return;
                    }

            if (isbn == null) {
                isbn = bookDetailDAO.getISBNWithMaxQuantity(bookId);
                if (isbn == null) {
                    String error = "No available copies of the book. Please choose another edition.";
                    response.sendRedirect("book?book_id=" + bookId  + "&error=" + error);
                    return;

                }
            }

            List<BookDetail> bookDetailList = bookDetailDAO.getBookDetailByISBN(isbn);
            if (bookDetailList.isEmpty()) {
                // Nếu không có sách nào, thông báo lỗi
                String error = "No available copies of the book. Please choose another edition.";
                response.sendRedirect("book?book_id=" + bookId  + "&error=" + error);
                return;
            }
            Random random = new Random();
            BookDetail bookDetail = bookDetailList.get(random.nextInt(bookDetailList.size()));
            Loan.borrowBook(bookDetail.getBook_detail_id(), userId, username);
            request.setAttribute("success", "Book borrowed successfully.");
            response.sendRedirect("loansUser?user_id=" + userId);




        } catch (Exception e) {
            response.getWriter().write("An error occurred: " + e.getMessage());
        }
    }
}