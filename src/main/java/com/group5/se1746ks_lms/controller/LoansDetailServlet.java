package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.LoansDAO;
import com.group5.se1746ks_lms.dao.RequestDAO;
import com.group5.se1746ks_lms.model.Loans;
import com.group5.se1746ks_lms.model.Request;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;

@WebServlet(name = "LoansDetailServlet", value = "/loansDetail")
public class LoansDetailServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoansDAO loansDAO = new LoansDAO();
        RequestDAO requestDAO = new RequestDAO();
        String id = request.getParameter("loans_id");
        Loans l = loansDAO.getLoansById(id);
        if (l.getIs_request()) {
            Request req = requestDAO.getRequest(l.getLoans_id());
            request.setAttribute("req", req);
        }

        request.setAttribute("l", l);
        String editMode = request.getParameter("editMode");
        if (editMode == null) {
            request.getRequestDispatcher("views/LoansDetail.jsp").forward(request, response);
        } else {
            request.getRequestDispatcher("views/LoansEdit.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
