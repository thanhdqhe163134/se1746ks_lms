package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.FineDAO;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;

@WebServlet(name = "FinePayServlet", value = "/finePay")
public class FinePayServlet extends HttpServlet {
    FineDAO fineDAO = new FineDAO();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String fine_id = request.getParameter("fine_id");
        String username = request.getParameter("username");
        String user_id = request.getParameter("user_id");



            fineDAO.payFine(fine_id, username);
            response.sendRedirect("fine");
        }


}
