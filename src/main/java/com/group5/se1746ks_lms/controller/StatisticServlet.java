package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.StatisticDAO;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.util.Map;

@WebServlet(name = "StatisticServlet", value = "/statistic")
public class StatisticServlet extends HttpServlet {
    StatisticDAO statisticDAO = new StatisticDAO();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String type = request.getParameter("type");
        if (type != null && type.equals("")) type = null;
        String bookType = request.getParameter("bookType");
        if (bookType != null && bookType.equals("")) bookType = null;
        String monthStr = request.getParameter("month");
        if (monthStr != null && monthStr.equals("")) monthStr = null;
        String yearStr = request.getParameter("year");
        if (yearStr != null && yearStr.equals("")) yearStr = null;

        if (type == null || type.equals("book")) {
            int totalBooks = statisticDAO.getTotalBooks();
            int totalBookEditions = statisticDAO.getTotalBookEdition();
            int totalBookCopies = statisticDAO.getTotalBookCopies();
            int totalBookBorrowed = statisticDAO.getTotalBookDetailBorrowed();
            int totalBookAvailable = statisticDAO.getTotalBookDetailAvailable();
            request.setAttribute("totalBooks", totalBooks);
            request.setAttribute("totalBookEditions", totalBookEditions);
            request.setAttribute("totalBookCopies", totalBookCopies);
            request.setAttribute("totalBookBorrowed", totalBookBorrowed);
            request.setAttribute("totalBookAvailable", totalBookAvailable);

            if (monthStr == null && yearStr == null && bookType == null) {
                int years = 2023;
                Map<Integer, Integer> monthlyBookAdded = statisticDAO.getTotalBookTypeAddedEachMonth(years, "Books");
                request.setAttribute("monthlyBookAdded", monthlyBookAdded);
            } else if (yearStr != null && bookType != null && monthStr == null) {
                int year = Integer.parseInt(yearStr);
                Map<Integer, Integer> monthlyBookAdded = statisticDAO.getTotalBookTypeAddedEachMonth(year, bookType);
                request.setAttribute("monthlyBookAdded", monthlyBookAdded);

            } else if (yearStr != null && bookType != null) {
                int year = Integer.parseInt(yearStr);
                int month = Integer.parseInt(monthStr);
                Map<Integer, Integer> dailyBookAdded = statisticDAO.getTotalBookTypeAddedEachDay(month, year, bookType);
                request.setAttribute("dailyBookAdded", dailyBookAdded);
                Map<Integer, Integer> monthlyBookAdded = statisticDAO.getTotalBookTypeAddedEachMonth(year, bookType);
                request.setAttribute("monthlyBookAdded", monthlyBookAdded);
            }
            request.getRequestDispatcher("views/Statistic.jsp").forward(request, response);
        } else if (type.equals("loans")) {
            int totalLoans = statisticDAO.getTotalLoans();
            int totalLoansReturned = statisticDAO.getTotalLoansReturned();
            int totalLoansOverdue = statisticDAO.getTotalLoansOverdue();
            request.setAttribute("totalLoans", totalLoans);
            request.setAttribute("totalLoansReturned", totalLoansReturned);
            request.setAttribute("totalLoansOverdue", totalLoansOverdue);

            if (monthStr == null && yearStr == null) {
                int years = 2023;
                Map<Integer, Integer> monthlyLoansAdded = statisticDAO.getTotalLoansAddedEachMonth(years);
                request.setAttribute("monthlyBookAdded", monthlyLoansAdded);
            } else if (yearStr != null && monthStr == null) {
                int year = Integer.parseInt(yearStr);
                Map<Integer, Integer> monthlyLoansAdded = statisticDAO.getTotalLoansAddedEachMonth(year);
                request.setAttribute("monthlyBookAdded", monthlyLoansAdded);

            } else if (yearStr != null && monthStr != null) {
                int year = Integer.parseInt(yearStr);
                int month = Integer.parseInt(monthStr);
                Map<Integer, Integer> dailyLoansAdded = statisticDAO.getTotalLoansAddedEachDay(month, year);
                request.setAttribute("dailyBookAdded", dailyLoansAdded);
                Map<Integer, Integer> monthlyLoansAdded = statisticDAO.getTotalLoansAddedEachMonth(year);
                request.setAttribute("monthlyBookAdded", monthlyLoansAdded);
            }
            request.getRequestDispatcher("views/StatisticLoans.jsp").forward(request, response);
        } else if (type.equals("accounts")) {
            int totalUsers = statisticDAO.getTotalUser();
            int totalUsersNonActive = statisticDAO.getTotalUsersActive();
            request.setAttribute("totalUsers", totalUsers);
            request.setAttribute("totalUsersNonActive", totalUsersNonActive);

            if (monthStr == null && yearStr == null) {
                int years = 2023;
                Map<Integer, Integer> monthlyUsersAdded = statisticDAO.getTotalUsersAddedEachMonth(years);
                request.setAttribute("monthlyBookAdded", monthlyUsersAdded);
            } else if (yearStr != null && monthStr == null) {
                int year = Integer.parseInt(yearStr);
                Map<Integer, Integer> monthlyUsersAdded = statisticDAO.getTotalUsersAddedEachMonth(year);
                request.setAttribute("monthlyBookAdded", monthlyUsersAdded);
            } else if (yearStr != null && monthStr != null) {
                int year = Integer.parseInt(yearStr);
                int month = Integer.parseInt(monthStr);
                Map<Integer, Integer> dailyUsersAdded = statisticDAO.getTotalUsersAddedEachDay(month, year);
                request.setAttribute("dailyBookAdded", dailyUsersAdded);
                Map<Integer, Integer> monthlyUsersAdded = statisticDAO.getTotalUsersAddedEachMonth(year);
                request.setAttribute("monthlyBookAdded", monthlyUsersAdded);
            }
            request.getRequestDispatcher("views/StatisticAccounts.jsp").forward(request, response);
        } else if (type.equals("request")) {
            int totalRequests = statisticDAO.getTotalRequest();
            int totalRequestsPending = statisticDAO.getTotalRequestsPending();
            int totalRequestsApproved = statisticDAO.getTotalRequestsApproved();
            int totalRequestsDenied = statisticDAO.getTotalRequestsDenied();
            request.setAttribute("totalRequests", totalRequests);
            request.setAttribute("totalRequestsPending", totalRequestsPending);
            request.setAttribute("totalRequestsApproved", totalRequestsApproved);
            request.setAttribute("totalRequestsDenied", totalRequestsDenied);

            if (monthStr == null && yearStr == null) {
                int years = 2023;
                Map<Integer, Integer> monthlyRequestsAdded = statisticDAO.getTotalRequestsAddedEachMonth(years);
                request.setAttribute("monthlyBookAdded", monthlyRequestsAdded);
            } else if (yearStr != null && monthStr == null) {
                int year = Integer.parseInt(yearStr);
                Map<Integer, Integer> monthlyRequestsAdded = statisticDAO.getTotalRequestsAddedEachMonth(year);
                request.setAttribute("monthlyBookAdded", monthlyRequestsAdded);
            } else if (yearStr != null && monthStr != null) {
                int year = Integer.parseInt(yearStr);
                int month = Integer.parseInt(monthStr);
                Map<Integer, Integer> dailyRequestsAdded = statisticDAO.getTotalRequestsAddedEachDay(month, year);
                request.setAttribute("dailyBookAdded", dailyRequestsAdded);
                Map<Integer, Integer> monthlyRequestsAdded = statisticDAO.getTotalRequestsAddedEachMonth(year);
                request.setAttribute("monthlyBookAdded", monthlyRequestsAdded);
            }
            request.getRequestDispatcher("views/StatisticRequest.jsp").forward(request, response);
        } else if (type.equals("fine")) {
            int totalFines = statisticDAO.getTotalFines();
            int totalFinesPaid = statisticDAO.getTotalFinesPaid();
            int totalFinesUnpaid = statisticDAO.getTotalFinesUnpaid();
            request.setAttribute("totalFines", totalFines);
            request.setAttribute("totalFinesPaid", totalFinesPaid);
            request.setAttribute("totalFinesUnpaid", totalFinesUnpaid);

            if (monthStr == null && yearStr == null) {
                int years = 2023;
                Map<Integer, Integer> monthlyFinesAdded = statisticDAO.getTotalFinesAddedEachMonth(years);
                request.setAttribute("monthlyBookAdded", monthlyFinesAdded);
            } else if (yearStr != null && monthStr == null) {
                int year = Integer.parseInt(yearStr);
                Map<Integer, Integer > monthlyFinesAdded = statisticDAO.getTotalFinesAddedEachMonth(year);
                request.setAttribute("monthlyBookAdded", monthlyFinesAdded);
            } else if (yearStr != null && monthStr != null) {
                int year = Integer.parseInt(yearStr);
                int month = Integer.parseInt(monthStr);
                Map<Integer, Integer> dailyFinesAdded = statisticDAO.getTotalFinesAddedEachDay(month, year);
                request.setAttribute("dailyBookAdded", dailyFinesAdded);
                Map<Integer, Integer> monthlyFinesAdded = statisticDAO.getTotalFinesAddedEachMonth(year);
                request.setAttribute("monthlyBookAdded", monthlyFinesAdded);
            }
            request.getRequestDispatcher("views/StatisticFine.jsp").forward(request, response);

        }
    }
}