package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.GenresDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebServlet("/deleteGenre")
public class GenreDeleteServlet extends HttpServlet {
    GenresDAO genresDAO = new GenresDAO();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String genre_idStr = request.getParameter("genre_id");
        if (genre_idStr != null) {
            try {
                int genre_id = Integer.parseInt(genre_idStr);
                genresDAO.deleteGenreById(genre_id); // Implement this method in GenresDAO
                response.sendRedirect("books"); // Redirect to the book list page after deleting
            } catch (NumberFormatException e) {
                // Handle the case where genre_id is not a valid integer
                response.sendRedirect("errorPage.jsp");
            }
        } else {
            // Handle the case where genre_id is missing
            response.sendRedirect("errorPage.jsp");
        }
    }
}
