package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.BookDetailDAO;
import com.group5.se1746ks_lms.dao.BooksDAO;
import com.group5.se1746ks_lms.model.BookDetail;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.util.Random;

@WebServlet(name = "BookISBNServlet", value = "/BookISBN")
public class BookISBNServlet extends HttpServlet {
    BookDetailDAO bookDetailDAO = new BookDetailDAO();
    BooksDAO bookDAO = new BooksDAO();
    BookDetail bookDetail = new BookDetail();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if(action != null && action.equals("view")){
            String isbn = request.getParameter("isbn");
            request.setAttribute("isbn", isbn);
            request.setAttribute("bookDetails", bookDetailDAO.getBookDetailByISBN2(isbn));
            request.getRequestDispatcher("views/BookISBN.jsp").forward(request, response);

        } else if(action != null && action.equals("delete")) {
            String bookDetailId = request.getParameter("book_detail_id");
            bookDetailDAO.deleteBookDetail(bookDetailId);
            response.sendRedirect("BookISBN?action=view&isbn=" + request.getParameter("isbn"));
        }



    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String isbn = request.getParameter("isbn");
        String username = request.getParameter("username");
        int quantity = Integer.parseInt(request.getParameter("quantity"));
        for (int i = 0; i < quantity; i++) {
            bookDetail.setBook_detail_id( generateRandomId(10));
            bookDetail.setISBN(isbn);
            bookDetail.setStatus("Available");
            bookDetail.setCreated_date(new java.sql.Timestamp(System.currentTimeMillis()));
            bookDetail.setCreated_user(username);
            bookDetail.setIs_deleted(false);
            bookDetail.setIs_borrowed(false);
            bookDAO.addBookDetail(bookDetail);
        }
        response.sendRedirect("BookISBN?action=view&isbn=" + isbn);
    }

    private static String generateRandomId(int length) {
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        StringBuilder result = new StringBuilder();
        Random rnd = new Random();

        while (result.length() < length) {
            int index = (int) (rnd.nextFloat() * characters.length());
            result.append(characters.charAt(index));
        }

        return result.toString();
    }

}
