package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.BookDetailDAO;
import com.group5.se1746ks_lms.dao.BooksDAO;
import com.group5.se1746ks_lms.dao.LoansDAO;
import com.group5.se1746ks_lms.model.Loans;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;

@WebServlet(name = "LoansUpdateServlet", value = "/updateLoans")
public class LoansUpdateServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoansDAO loansDAO = new LoansDAO();
        BookDetailDAO bookDetailDAO = new BookDetailDAO();
        String username = request.getParameter("username");
        String loans_id = request.getParameter("loan_id");
        String book_detail_id = request.getParameter("book_detail_id");
        String status = request.getParameter("status");
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        switch (status) {
            case "Reserved":
                loansDAO.updateBorrowDate(loans_id, status, null, username);
                break;
            case "Borrowing":
                loansDAO.updateBorrowDate(loans_id, status, timestamp, username);
                loansDAO.updateStatusReturned(loans_id, status, username, null);
                break;

        }

        response.sendRedirect("loansDetail?loans_id="+loans_id);
    }
}
