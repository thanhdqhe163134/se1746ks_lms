package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.BooksDAO;
import com.group5.se1746ks_lms.model.BookEdition;
import com.group5.se1746ks_lms.model.Books;
import com.group5.se1746ks_lms.model.Genres;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Set;

@WebServlet("/searchBook")
public class BooksSearchServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        String query = request.getParameter("query");
        BooksDAO booksDAO = new BooksDAO();
        if(action.equalsIgnoreCase("title") && query != null){
            List<Books> bookList = booksDAO.searchBooksByTitle(query);

            request.setAttribute("bookList", bookList);
            request.getRequestDispatcher("views/BookAddSearch.jsp").forward(request, response);
        }
        else if(action.equalsIgnoreCase("isbn") && query != null){
           Long book_id = booksDAO.searchBooksIdByISBN(query);
            request.setAttribute("bookId", book_id);
            request.getRequestDispatcher("views/BookAddSearch.jsp").forward(request, response);
        }









        }

}
