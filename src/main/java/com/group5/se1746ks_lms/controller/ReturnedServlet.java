package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.LoansDAO;
import com.group5.se1746ks_lms.dao.ReturnedDAO;
import com.group5.se1746ks_lms.model.Loans;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;

@WebServlet(name = "ReturnedServlet", value = "/returned")
public class ReturnedServlet extends HttpServlet {
    ReturnedDAO returnedDAO = new ReturnedDAO();

    LoansDAO loansDAO = new LoansDAO();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String book_detail_id = request.getParameter("book_detail_id");
        if(book_detail_id != null) {
            long id = 0L;
            id = loansDAO.getLoansIDByBookDetailId(book_detail_id);
            request.setAttribute("book_detail_id", book_detail_id);
            response.sendRedirect("loansDetail?loans_id=" + id);
        }
        else {
            request.getRequestDispatcher("views/Returned.jsp").forward(request, response);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
