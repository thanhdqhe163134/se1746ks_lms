package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.FineDAO;
import com.group5.se1746ks_lms.model.Fine;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "FineServlet", value = "/fine")
public class FineServlet extends HttpServlet {
    FineDAO fineDAO = new FineDAO();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String user_id = request.getParameter("user_id");
        String sortBy = request.getParameter("sortBy");
        if(sortBy != null && sortBy.equalsIgnoreCase("null")) sortBy = null;
        int currentPage = 1;
        int itemsPerPage = 10;
        String currentPageStr = request.getParameter("currentPage");
        if (currentPageStr != null) {
            currentPage = Integer.parseInt(currentPageStr);
        }

        String itemsPerPageStr = request.getParameter("itemsPerPage");
        if (itemsPerPageStr != null) {
            itemsPerPage = Integer.parseInt(itemsPerPageStr);
        }
        int start = (currentPage - 1) * itemsPerPage;
        List<Fine> fineList;

        if(user_id == null){
            if(sortBy != null){
                fineList = fineDAO.getAllFinePagingSort(start, itemsPerPage, sortBy);
                request.setAttribute("totalPages", fineDAO.getTotalPages(itemsPerPage));
            } else {
                fineList = fineDAO.getAllFinePaging(start, itemsPerPage);
                request.setAttribute("totalPages", fineDAO.getTotalPages(itemsPerPage));
            }
            request.setAttribute("allFineList", fineList);
            request.setAttribute("currentPage", currentPage);
            request.setAttribute("itemsPerPage", itemsPerPage);
            request.setAttribute("sortBy", sortBy);
            request.getRequestDispatcher("views/Fine.jsp").forward(request, response);

        } else {
            if(sortBy != null){
                fineList = fineDAO.getFineByUserIdPagingSort(Integer.parseInt(user_id), start, itemsPerPage, sortBy);
                request.setAttribute("totalPages", fineDAO.getTotalPagesByUserId(Integer.parseInt(user_id), itemsPerPage));
            } else {
                fineList = fineDAO.getFineByUserIdPaging(Integer.parseInt(user_id), start, itemsPerPage);
                request.setAttribute("totalPages", fineDAO.getTotalPagesByUserId(Integer.parseInt(user_id), itemsPerPage));
            }
            request.setAttribute("sortBy", sortBy);
            request.setAttribute("fineList", fineList);
            request.setAttribute("user_id", user_id);
            request.setAttribute("currentPage", currentPage);
            request.setAttribute("itemsPerPage", itemsPerPage);
            request.getRequestDispatcher("views/FineUser.jsp").forward(request, response);

        }


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
