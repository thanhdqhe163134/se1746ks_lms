package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.AccountDAO;
import com.group5.se1746ks_lms.model.Account;
import de.svws_nrw.ext.jbcrypt.BCrypt;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;

@WebServlet(name = "MyProfileServlet", value = "/myProfile")
public class MyProfileServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        AccountDAO accountDAO = new AccountDAO();
        String id = request.getParameter("id");
        String changePassword = request.getParameter("changePassword");
        Account a = accountDAO.getAccountById(id);
        request.setAttribute("changePassword", changePassword);
        request.setAttribute("a", a);
        request.getRequestDispatcher("views/MyProfile.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        String changePassword = request.getParameter("changePassword");
        String current = request.getParameter("current");
        String newPass = request.getParameter("new");
        String confirm = request.getParameter("confirm");

        AccountDAO accountDAO = new AccountDAO();
        Account a = accountDAO.getAccountById(id);

        if (BCrypt.checkpw(current, a.getPassword()) == false) {
            String wrongPass = "Invalid current password";
            request.setAttribute("wrongPass", wrongPass);
            request.setAttribute("a", a);
            request.setAttribute("changePassword", changePassword);
            request.getRequestDispatcher("views/MyProfile.jsp").forward(request, response);
        } else {
            if (newPass.equals(confirm) == false) {
                String noMatch = "Password confirmation doesn't match New Password";
                request.setAttribute("noMatch", noMatch);
                request.setAttribute("a", a);
                request.setAttribute("changePassword", changePassword);
                request.getRequestDispatcher("views/MyProfile.jsp").forward(request, response);
            } else {
                String hashPassword = BCrypt.hashpw(newPass, BCrypt.gensalt());
                accountDAO.changePassword(id, hashPassword);
                response.sendRedirect("myProfile?id=" + id);
            }
        }
    }
}
