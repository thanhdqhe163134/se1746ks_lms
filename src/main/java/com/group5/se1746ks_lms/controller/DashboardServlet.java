package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.model.Account;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;

@WebServlet(name = "DashboardServlet", value = "/dashboard")
public class DashboardServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Account loggedInUser = (Account) session.getAttribute("loggedInUser");
        if (loggedInUser.getRole().equals("user")) {
            request.getRequestDispatcher("home").forward(request, response);
        } else if (loggedInUser.getRole().equals("admin")) {
            request.getRequestDispatcher("views/AdminDashboard.jsp").forward(request, response);
        } else if (loggedInUser.getRole().equals("librarian")) {
            request.getRequestDispatcher("views/LibrarianDashboard.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
