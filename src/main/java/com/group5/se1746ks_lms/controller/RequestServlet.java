package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.RequestDAO;
import com.group5.se1746ks_lms.model.Request;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.util.List;

@WebServlet("/requestList")
public class RequestServlet extends HttpServlet {
    RequestDAO requestDAO = new RequestDAO();

    Request request = new Request();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String user_idStr = request.getParameter("user_id");
        String sortBy = request.getParameter("sortBy");
        if(sortBy != null && sortBy.equalsIgnoreCase("null")) sortBy = null;
        int currentPage = 1;
        int itemsPerPage = 10; // Giá trị mặc định

        String currentPageStr = request.getParameter("currentPage");
        if (currentPageStr != null) {
            currentPage = Integer.parseInt(currentPageStr);
        }

        String itemsPerPageStr = request.getParameter("itemsPerPage");
        if (itemsPerPageStr != null) {
            itemsPerPage = Integer.parseInt(itemsPerPageStr);
        }
        int start = (currentPage - 1) * itemsPerPage;

        List<Request> reqs;
        if (user_idStr != null) {
            if (sortBy != null) {
                reqs = requestDAO.getRequestsByUser_idPagingSort(Long.parseLong(user_idStr), start, itemsPerPage, sortBy);
                request.setAttribute("totalPages", requestDAO.getTotalPagesByUser_id(Long.parseLong(user_idStr), itemsPerPage));
            } else {
                reqs = requestDAO.getRequestsByUser_idPaging(Long.parseLong(user_idStr), start, itemsPerPage);
            request.setAttribute("totalPages", requestDAO.getTotalPagesByUser_id(Long.parseLong(user_idStr), itemsPerPage));
            }
        } else {
            if (sortBy != null) {
                reqs = requestDAO.getAllRequestsWithPagingSort(start, itemsPerPage, sortBy);
                request.setAttribute("totalPages", requestDAO.getTotalPages(itemsPerPage));
            } else {
                reqs = requestDAO.getAllRequestsWithPaging(start, itemsPerPage);
                request.setAttribute("totalPages", requestDAO.getTotalPages(itemsPerPage));
            }

        }

        request.setAttribute("sortBy", sortBy);
        request.setAttribute("user_id", user_idStr);
        request.setAttribute("req", reqs);
        request.setAttribute("currentPage", currentPage);
        request.setAttribute("itemsPerPage", itemsPerPage);
        request.getRequestDispatcher("views/Request.jsp").forward(request, response);
    }



    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
