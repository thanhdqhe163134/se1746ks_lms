package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.AccountDAO;
import com.group5.se1746ks_lms.model.Account;
import com.group5.se1746ks_lms.util.GoogleOAuth;
import com.group5.se1746ks_lms.util.JWTUtil;
import de.svws_nrw.ext.jbcrypt.BCrypt;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.classic.methods.HttpUriRequestBase;
import org.apache.hc.client5.http.entity.UrlEncodedFormEntity;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.NameValuePair;
import org.apache.hc.core5.http.ParseException;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.apache.hc.core5.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@WebServlet(name = "GoogleLoginServlet", value = "/google-login")
public class GoogleLoginServlet extends HttpServlet {
    private AccountDAO accountDAO = new AccountDAO();

    GoogleOAuth GoogleOAuth = new GoogleOAuth();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String code = request.getParameter("code");
        String receivedState = request.getParameter("state");

        // Validate state
        String data = JWTUtil.decodeToken(receivedState);
        if (data == null || !data.equals("some_random_data")) {
            // Invalid state
            response.sendRedirect("error.jsp");
            return;
        }

        if (code != null && !code.isEmpty()) {
            Account userInfor = GoogleOAuth.getUserInforFromToken(code);
            String userEmail = userInfor.getEmail();
            if (userEmail.endsWith("@fpt.edu.vn")) {
                Account account = accountDAO.selectAccountByEmail(userEmail);
                if (account == null) {
                    String[] parts = userEmail.split("@");
                    String localPart = parts[0];  // phần trước dấu @
                    String student_id = localPart.substring(localPart.length() - 8);
                    String username = parts[0];
                    String password = BCrypt.hashpw(PasswordGenerator(),BCrypt.gensalt());
                    Account newAccount = new Account();
                    newAccount.setFull_name(userInfor.getFull_name());
                    newAccount.setEmail(userEmail);
                    newAccount.setStudent_id(student_id);
                    newAccount.setUsername(username);
                    newAccount.setPassword(password);
                    newAccount.setCreated_date(new Timestamp(System.currentTimeMillis()));
                    newAccount.setImg(userInfor.getImg());
                    newAccount.setRole("user");
                    newAccount.setIs_deleted(false);
                    accountDAO.createAccountByUserInfor(newAccount);
                }

                account = accountDAO.selectAccountByEmail(userEmail);

                HttpSession session = request.getSession();
                session.setAttribute("loggedInUser", account);
                response.sendRedirect("home");



            } else {
                request.setAttribute("error", "You must use FPT email to login");
                request.getRequestDispatcher("views/Login.jsp").forward(request, response);
            }
        }
    }



    private String PasswordGenerator(){
        String password = "";
        String[] characters = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n",
                "o","p","q","r","s","t","u","v","w","x","y","z","1","2","3","4",
                "5","6","7","8","9","0"};
        for(int i = 0; i < 6; i++){
            int random = (int)(Math.random()*characters.length);
            password += characters[random];
        }
        return password;
    }
}
