package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.BooksDAO;
import com.group5.se1746ks_lms.dao.CommentDAO;
import com.group5.se1746ks_lms.dao.GenresDAO;
import com.group5.se1746ks_lms.model.Books;
import com.group5.se1746ks_lms.model.Comment;
import com.group5.se1746ks_lms.model.Genres;
import com.group5.se1746ks_lms.util.ImgurUploader;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.*;

@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 10,      // 10MB
        maxRequestSize = 1024 * 1024 * 50)   // 50MB
@WebServlet("/bookDetail")
public class BookDetailServlet extends HttpServlet {
    BooksDAO booksDAO = new BooksDAO();

    GenresDAO genresDAO = new GenresDAO();

    CommentDAO commentDAO = new CommentDAO();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Comment> comments = new ArrayList<>();

        Long book_id = Long.parseLong(request.getParameter("book_id"));
        request.setAttribute("book", booksDAO.getBookById(book_id));
        request.setAttribute("allGenres", genresDAO.getAllGenres());
        String sortOrder = request.getParameter("sortOrder");
        comments = commentDAO.getAllCommentsByBookId(book_id);
        List<Comment> rootComments = new ArrayList<>();
        for (Comment comment : comments) {
            if (comment.getRep_comment() == 0) {
                rootComments.add(comment);
            }
        }
        if ("newest".equals(sortOrder)) {
            rootComments.sort(Comparator.comparing(Comment::getCreated_date).reversed());
        } else {
            rootComments.sort(Comparator.comparing(Comment::getCreated_date));
        }

        request.setAttribute("comments", comments);
        request.setAttribute("rootComments", rootComments);
        request.getRequestDispatcher("views/BookDetail.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            Long book_id = Long.parseLong(request.getParameter("book_id"));
            Books book = booksDAO.getBookById(book_id);
            if (book != null) {
                // Update the book's fields
                book.setTitle(request.getParameter("title"));
                book.setAuthor(request.getParameter("author"));
                book.setDescription(request.getParameter("description"));
                book.setTotal_copies(Integer.parseInt(request.getParameter("total_copies")));
                book.setUpdated_user(request.getParameter("updated_user"));
                book.setUpdated_date(new Timestamp(System.currentTimeMillis()));



                Part part = request.getPart("image");
                if (part != null && part.getSize() > 0 && part.getSubmittedFileName() != null && !part.getSubmittedFileName().isEmpty()) {
                    InputStream imageStream = part.getInputStream();
                    String imgURL = ImgurUploader.uploadToImgur(imageStream);
                    book.setIMG(imgURL);
                }

                String[] selectedGenres = request.getParameterValues("genres");
                String[] newGenresNames = request.getParameterValues("newGenres");

                List<String> allGenresIds = selectedGenres != null ? new ArrayList<>(Arrays.asList(selectedGenres)) : new ArrayList<>();

                if (newGenresNames != null) {
                    for (String newGenreName : newGenresNames) {
                        if (newGenreName != null && !newGenreName.trim().isEmpty()) {
                            Genres existingGenre = genresDAO.findByName(newGenreName);
                            if (existingGenre == null) {
                                Genres newGenre = genresDAO.create(newGenreName);
                                allGenresIds.add(String.valueOf(newGenre.getGenre_id()));
                            } else {
                                allGenresIds.add(String.valueOf(existingGenre.getGenre_id()));
                            }
                        }
                    }
                }

                booksDAO.updateBookGenres(book, allGenresIds.toArray(new String[0]));

                booksDAO.updateBook(book);

                response.sendRedirect("book?book_id=" + book_id);
            } else {
                response.sendRedirect("errorPage.jsp");
            }
        } catch (NumberFormatException e) {
            response.sendRedirect("errorPage.jsp");
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }
}