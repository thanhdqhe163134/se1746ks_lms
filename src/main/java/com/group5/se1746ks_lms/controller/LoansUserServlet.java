package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.LoansDAO;
import com.group5.se1746ks_lms.dao.RequestDAO;
import com.group5.se1746ks_lms.model.Loans;
import com.group5.se1746ks_lms.model.Request;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet("/loansUser")
public class LoansUserServlet extends HttpServlet {
    LoansDAO loansDAO = new LoansDAO();

    RequestDAO Re = new RequestDAO();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String error = request.getParameter("error");
        String user_idStr = request.getParameter("user_id");
        String sortBy = request.getParameter("sortBy");
        long user_id = Long.parseLong(user_idStr);
        int currentPage = 1;
        int itemsPerPage = 5;
        if (request.getParameter("currentPage") != null) {
            currentPage = Integer.parseInt(request.getParameter("currentPage"));
        }
        if (request.getParameter("itemsPerPage") != null) {
            itemsPerPage = Integer.parseInt(request.getParameter("itemsPerPage"));
        }
        int totalLoans = loansDAO.getLoansListbyUser_id(user_id).size();
        int totalPages = (int) Math.ceil((double) totalLoans / itemsPerPage);
        currentPage = currentPage > totalPages ? Math.max(totalPages, 1) : currentPage;

        List<Loans> loans = loansDAO.getLoansListPerPagebyUser_id(user_id, currentPage, itemsPerPage);
        List<Request> requests = new ArrayList<>();
        for(Loans loan: loans){
            if(loan.getIs_request())
                requests.add(Re.getRequest(loan.getLoans_id()));
        }


        if(error != null){
            request.setAttribute("error", error);
        }
        String success = request.getParameter("success");
        if(success != null){
            request.setAttribute("success", success);
        }

        Map<Long, Request> requestMap = new HashMap<>();
        for(Request req : requests) {
            if(req != null)
            requestMap.put(req.getLoan_id(), req);
        }
        request.setAttribute("requestMap", requestMap);
        request.setAttribute("requests", requests);
        request.setAttribute("user_id", user_id);
        request.setAttribute("totalPages", totalPages);
        request.setAttribute("currentPage", currentPage);
        request.setAttribute("itemsPerPage", itemsPerPage);
        request.setAttribute("sortBy", sortBy);
        request.setAttribute("loans", loans);
        request.getRequestDispatcher("views/LoansUser.jsp").forward(request,response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
