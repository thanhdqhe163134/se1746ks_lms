package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.AccountDAO;
import com.group5.se1746ks_lms.util.JWTUtil;
import de.svws_nrw.ext.jbcrypt.BCrypt;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;

@WebServlet("/reset-password")
public class AccountResetPasswordServlet extends HttpServlet {
    AccountDAO accountDAO = new AccountDAO();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String token = request.getParameter("token");

        if (token == null || token.isEmpty()) {
            // Handle invalid token
            response.sendRedirect("error.jsp");
            return;
        }

        try {
            // Decode the token to get the username
            String username = JWTUtil.decodeToken(token);

            if(username == null || !accountDAO.existsByUsername(username)) {
                // Handle invalid username
                response.sendRedirect("error.jsp");
                return;
            }
            request.setAttribute("username", username);

        request.getRequestDispatcher("views/AccountResetPassword.jsp").forward(request, response);
        } catch (Exception e) {
            // Handle error in decoding token or other issues
            response.sendRedirect("error.jsp");
        }
    }



    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("newPassword");
        String confirmPassword = request.getParameter("confirmPassword");


        if (!password.equals(confirmPassword)) {
            // Handle error
            request.setAttribute("error", "Password does not match");
            RequestDispatcher dispatcher = request.getRequestDispatcher("views/AccountResetPassword.jsp");
            dispatcher.forward(request, response);
            return;
        }

        String hashPassword = BCrypt.hashpw(password, BCrypt.gensalt());


        accountDAO.updatePasswordByUsername(username, hashPassword);

        response.sendRedirect("login?success=reset&username=" + username);
    }

}
