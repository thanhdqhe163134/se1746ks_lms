package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.AccountDAO;
import com.group5.se1746ks_lms.dao.LoansDAO;
import com.group5.se1746ks_lms.dao.RequestDAO;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.concurrent.TimeUnit;

@WebServlet(name = "RequestCreateServlet", value = "/createRequest")
public class RequestCreateServlet extends HttpServlet {
    RequestDAO requestDAO = new RequestDAO();
    LoansDAO loansDAO = new LoansDAO();

    AccountDAO accountDAO = new AccountDAO();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String loans_id = request.getParameter("loan_id");
        request.setAttribute("loan_id", loans_id);
        request.getRequestDispatcher("views/RequestCreate.jsp").forward(request, response);


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String loans_idStr = request.getParameter("loan_id");
        String extensionOption = request.getParameter("extension_option");
        String description = request.getParameter("description");
        if(description == null) description = "Pending";
        String username = request.getParameter("username");
        String user_idStr = request.getParameter("user_id");

        long loans_id = 0L;
        if (loans_idStr != null) {
            loans_id = Long.parseLong(loans_idStr);
        }
        if (user_idStr != null) {
            long user_id = Long.parseLong(user_idStr);
            try {
                if (accountDAO.CountRequest(user_id, loans_id) >= 3) {
                    String error = "You have send 3 request already. You can not send more.";
                    request.setAttribute("loan_id", loans_id);
                    request.getRequestDispatcher("views/RequestCreate.jsp").forward(request, response);
                    return;
                }
                if (accountDAO.isRateLimited(user_id)) {
                    String error = "Too many requests. Please try again later.";
                    request.setAttribute("loan_id", loans_id);
                    request.getRequestDispatcher("views/RequestCreate.jsp").forward(request, response);
                    return;
                }

            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

            Date dueDate = loansDAO.getDueDate(loans_id);
            Date newDueDate = dueDate;

        if ("add_days".equals(extensionOption)) {
            String addDaysStr = request.getParameter("extension_days");
            if (addDaysStr != null && !addDaysStr.isEmpty()) {
                int daysToAdd = Integer.parseInt(addDaysStr);
                newDueDate = new Date(dueDate.getTime() + TimeUnit.DAYS.toMillis(daysToAdd));
            }
        } else if ("choose_date".equals(extensionOption)) {
            String chosenDateStr = request.getParameter("new_due_date");
            if (chosenDateStr != null && !chosenDateStr.isEmpty()) {
                newDueDate = Date.valueOf(chosenDateStr);
            }
        }

            requestDAO.createRequest(loans_id, newDueDate, description, username);
            loansDAO.updateIsRequest(loans_id, true);

        request.setAttribute("username", username);
        response.sendRedirect("requestList?user_id=" + user_idStr);


    }
}
