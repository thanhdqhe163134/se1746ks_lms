package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.BooksDAO;
import com.group5.se1746ks_lms.model.BookDetail;
import com.group5.se1746ks_lms.model.BookEdition;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@WebServlet("/bookEdition")
public class BookEditionServlet extends HttpServlet {
    BooksDAO bookDAO = new BooksDAO();

    BookDetail bookDetail = new BookDetail();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        String publisherIdStr = request.getParameter("publisherId");
        String bookIdStr = request.getParameter("bookId");
        long bookId = 0L;
        if(bookIdStr != null && !bookIdStr.equals("null")){
            bookId = Integer.parseInt(bookIdStr);
        }
        List<BookEdition> bookEditions = new ArrayList<>();
        if(publisherIdStr != null && bookId != 0L){
            long publisherId = Long.parseLong(publisherIdStr);
            bookEditions = bookDAO.getBookEditionsByPublisherId(publisherId, bookId);
            //sort by publish year ASC
            bookEditions.sort((o1, o2) -> o1.getPublish_year() - o2.getPublish_year());




            request.setAttribute("publisher_id", publisherId);

        }
        request.setAttribute("bookEditions", bookEditions);
        if (action != null && action.equals("edit") ){
            request.getRequestDispatcher("views/BookEditionUpdate.jsp").forward(request, response);
        } else {
            request.getRequestDispatcher("views/BookEdition.jsp").forward(request, response);
        }


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String isbn = request.getParameter("ISBN");
        long publisherId = Long.parseLong(request.getParameter("publisher_id"));
        long BookId = Long.parseLong(request.getParameter("book_id"));
        int publishYear = Integer.parseInt(request.getParameter("publish_year"));
        String language = request.getParameter("language");
        int quantity = Integer.parseInt(request.getParameter("quantity"));
        String createdUser = request.getParameter("created_user");
        Timestamp createdDate = new Timestamp(System.currentTimeMillis());


        BookEdition bookEdition = new BookEdition();
        bookEdition.setISBN(isbn);
        bookEdition.setPublisher_id(publisherId);
        bookEdition.setBook_id(BookId);
        bookEdition.setCreated_date(createdDate);
        bookEdition.setCreated_user(createdUser);
        bookEdition.setPublish_year(publishYear);
        bookEdition.setLanguage(language);
        bookEdition.setIs_deleted(false);
        if(!bookDAO.existBookEdition(isbn)){
            bookDAO.addBookEdition(bookEdition);
        }

        for (int i = 0; i < quantity; i++) {
            bookDetail.setBook_detail_id( generateRandomId(10));
            bookDetail.setISBN(isbn);
            bookDetail.setStatus("Available");
            bookDetail.setCreated_date(createdDate);
            bookDetail.setCreated_user(createdUser);
            bookDetail.setIs_deleted(false);
            bookDetail.setIs_borrowed(false);
            bookDAO.addBookDetail(bookDetail);
        }
        response.sendRedirect("book?book_id="+BookId);

    }

    private static String generateRandomId(int length) {
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        StringBuilder result = new StringBuilder();
        Random rnd = new Random();

        while (result.length() < length) {
            int index = (int) (rnd.nextFloat() * characters.length());
            result.append(characters.charAt(index));
        }

        return result.toString();
    }


}
