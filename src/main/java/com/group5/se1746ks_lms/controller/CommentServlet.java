package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.CommentDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebServlet("/comment")
public class CommentServlet extends HttpServlet {


    CommentDAO commentDAO = new CommentDAO();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        String username = request.getParameter("username");


        if (action.equals("delete")) {
            Long comment_id = Long.parseLong(request.getParameter("comment_id"));
            commentDAO.deleteComment(comment_id, username);
        } else if (action.equals("edit")) {
            String content = request.getParameter("content");
            Long comment_id = Long.parseLong(request.getParameter("comment_id"));
            commentDAO.updateComment(comment_id, content, username);
        } else if (action.equals("comment")) {
            String content = request.getParameter("content");
            Long book_id = Long.parseLong(request.getParameter("book_id"));
            Long user_id = Long.parseLong(request.getParameter("user_id"));
            Long rep_comment = Long.parseLong(request.getParameter("rep_comment"));
            commentDAO.CreateComment(content, username, book_id, rep_comment, user_id);
        } else if (action.equals("restore")) {
            Long comment_id = Long.parseLong(request.getParameter("comment_id"));
            commentDAO.restoreComment(comment_id);
        }

        response.sendRedirect(request.getContextPath() + "/book?book_id=" + request.getParameter("book_id"));


    }
}
