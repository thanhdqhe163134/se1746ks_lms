package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.AccountDAO;
import com.group5.se1746ks_lms.model.Account;
import de.svws_nrw.ext.jbcrypt.BCrypt;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;

@WebServlet(name = "AccountCreateServlet", value = "/createAcc")
public class AccountCreateServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("views/AccountCreate.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String student_id = request.getParameter("student_id");
        String fullname = request.getParameter("fullname");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String dob = request.getParameter("dob");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        String address = request.getParameter("address");
        String role = request.getParameter("role");
        Timestamp create_date = new Timestamp(System.currentTimeMillis());


        String hashPassword = BCrypt.hashpw(password, BCrypt.gensalt());

        Account a = new Account(null, username, hashPassword, student_id, fullname, Date.valueOf(dob), email, phone, address, role, null, create_date, null, null, null, null, false);
        AccountDAO accountDAO = new AccountDAO();

        if (accountDAO.existsByUsername(username) == false) {
            accountDAO.createNewAccount(a);
            response.sendRedirect("accounts");
        } else {
            String err = "Username already exist.";
            request.setAttribute("err", err);
            request.setAttribute("a", a);
            request.getRequestDispatcher("views/AccountCreate.jsp").forward(request, response);
        }
    }
}
