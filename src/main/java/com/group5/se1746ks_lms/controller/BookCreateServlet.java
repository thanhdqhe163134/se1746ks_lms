package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.BooksDAO;
import com.group5.se1746ks_lms.dao.GenresDAO;
import com.group5.se1746ks_lms.model.Books;
import com.group5.se1746ks_lms.model.Genres;
import com.group5.se1746ks_lms.util.ImgurUploader;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.sql.Date;
import java.util.List;

@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 10,      // 10MB
        maxRequestSize = 1024 * 1024 * 50)   // 50MB
@WebServlet("/addBook")
public class BookCreateServlet extends HttpServlet {
    BooksDAO booksDAO = new BooksDAO();

    GenresDAO genresDAO = new GenresDAO();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("allGenres", genresDAO.getAllGenres());

        String action = request.getParameter("action");
        if(action != null && action.equals("add")){
            request.setAttribute("editMode", true);

            request.getRequestDispatcher("views/BookAdd.jsp").forward(request, response);
        } else if (action != null && action.equals("create")) {
            request.setAttribute("editMode", true);
            request.getRequestDispatcher("views/BookCreate.jsp").forward(request, response);

        }



    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String title = request.getParameter("title");
        String author = request.getParameter("author");
        int total_copies =0;
        int available_copies =0;
        int borrowed_copies =0;
        String created_user = request.getParameter("created_user");
        Timestamp created_date = new Timestamp(System.currentTimeMillis());
        String updated_user =null;
        Timestamp updated_date = null;
        boolean is_deleted = false;
        String description = request.getParameter("description");
        Part part = request.getPart("image");
        String[] selectedGenres = request.getParameterValues("genres");
        String[] newGenresNames = request.getParameterValues("newGenres");
        String[] publisherIds = request.getParameterValues("publishers");

        String newPublishYear = request.getParameter("newPublishYear");
        String newLanguage = request.getParameter("newLanguage");
        String newISBN = request.getParameter("newISBN");


        List<String> allGenresIds = selectedGenres != null ? new ArrayList<>(Arrays.asList(selectedGenres)) : new ArrayList<>();

        if (newGenresNames != null) {
            for (String newGenreName : newGenresNames) {
                if (newGenreName != null && !newGenreName.trim().isEmpty()) {
                    Genres existingGenre = genresDAO.findByName(newGenreName);
                    if (existingGenre == null) {
                        Genres newGenre = genresDAO.create(newGenreName);
                        allGenresIds.add(String.valueOf(newGenre.getGenre_id()));
                    } else {
                        allGenresIds.add(String.valueOf(existingGenre.getGenre_id()));
                    }
                }
            }
        }


        String img = "";

        if (part != null && part.getSize() > 0 && part.getSubmittedFileName() != null && !part.getSubmittedFileName().isEmpty()){
            try {
                InputStream imageStream = part.getInputStream();
                img = ImgurUploader.uploadToImgur(imageStream);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        }
        Books book = new Books(title, author,  created_user, created_date, updated_user, updated_date, is_deleted, description, img);
        Long book_id = booksDAO.addBook(book, allGenresIds.toArray(new String[0]));
        if (book_id != null) {
            response.sendRedirect("book?book_id=" + book_id + "&editMode=true");
        } else {
            response.sendRedirect("errorPage.jsp");
        }
    }
}






