package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.AccountDAO;
import com.group5.se1746ks_lms.util.EmailUtil;
import com.group5.se1746ks_lms.util.GoogleCaptcha;
import com.group5.se1746ks_lms.util.GuestRateLimited;
import com.group5.se1746ks_lms.util.JWTUtil;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;

@WebServlet("/forgot-password")
public class AccountForgetPasswordServlet extends HttpServlet {

    AccountDAO accountDAO = new AccountDAO();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("views/AccountForgetPassword.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");

        String errorMessage = GuestRateLimited.RateLimited(request.getSession());
        if (errorMessage != null) {
            request.setAttribute("error", errorMessage);
            RequestDispatcher dispatcher = request.getRequestDispatcher("views/AccountForgetPassword.jsp");
            dispatcher.forward(request, response);
            return;
        }

        String gRecaptchaResponse = request.getParameter("g-recaptcha-response");
        GoogleCaptcha captcha = new GoogleCaptcha();
        boolean validCaptcha = captcha.verifyCaptcha(gRecaptchaResponse);

        if (!validCaptcha) {
            request.setAttribute("error", "YOU ARE ROBOT, AREN'T YOU?");
            RequestDispatcher dispatcher = request.getRequestDispatcher("views/AccountForgetPassword.jsp");
            try {
                dispatcher.forward(request, response);
            } catch (ServletException e) {
                throw new RuntimeException(e);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            return;
        }


        if (accountDAO.existsByUsername(username)) {
            request.setAttribute("success", "true");
            response.sendRedirect("login");




            new Thread(() -> {


                String token = JWTUtil.createToken(username);
                String email = accountDAO.getEmailByUsername(username);
                String resetLink = "http://localhost:8080/library/reset-password?token=" + token;
                String subject = "Password Reset";
                String body = "<strong> <p>Hello, "+ username + "</p> </strong>" +
                        "<p>We have received a request to reset your password. If you did not make this request, you can safely ignore this email.</p>" +
                        "<p>Otherwise, please click on the link below to reset your password:</p>" +
                        "<strong><p><a href='" + resetLink + "' style='color: #4CAF50; text-decoration: none; font-weight: bold;'>Reset Password</a></p></strong>" +
                        "<p>Thank you</p>" +
                        "<strong><p>FPTU Library</p></strong>";
                EmailUtil.sendResetPasswordEmail(email, subject, body);
            }).start();

        } else {
            request.setAttribute("error", "Invalid username");
            RequestDispatcher dispatcher = request.getRequestDispatcher("views/AccountForgetPassword.jsp");
            dispatcher.forward(request, response);
        }
    }

}
