package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.AccountDAO;
import com.group5.se1746ks_lms.model.Account;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;

@WebServlet(name = "AccountListServlet", value = "/accounts")
public class AccountListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        AccountDAO accountDAO = new AccountDAO();

        int currentPage = 1;
        int itemsPerPage = 5;

        if (request.getParameter("currentPage") != null) {
            currentPage = Integer.parseInt(request.getParameter("currentPage"));
        }
        if (request.getParameter("itemsPerPage") != null) {
            itemsPerPage = Integer.parseInt(request.getParameter("itemsPerPage"));
        }

        String sortBy = request.getParameter("sortBy");
        ArrayList<Account> accPerPage;

        ArrayList<Account> acc = accountDAO.getAccountList();

        int totalPages = (int) Math.ceil((double) acc.size() / itemsPerPage);
        if(currentPage > totalPages) {
            currentPage = totalPages;
        }


        if (sortBy != null && sortBy.equals("newest")) {
            accPerPage = accountDAO.sortAccountList("DESC", currentPage, itemsPerPage);
        } else if (sortBy != null && sortBy.equals("oldest")){
            accPerPage = accountDAO.sortAccountList("ASC", currentPage, itemsPerPage);
        } else {
            accPerPage = accountDAO.getAccountListPerPage(currentPage, itemsPerPage);
        }


        request.setAttribute("totalPages", totalPages);
        request.setAttribute("currentPage", currentPage);
        request.setAttribute("itemsPerPage", itemsPerPage);
        request.setAttribute("sortBy", sortBy);
        request.setAttribute("acc", accPerPage);
        request.getRequestDispatcher("views/AccountList.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }
}
