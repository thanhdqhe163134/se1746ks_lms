package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.BooksDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebServlet("/deleteBook")
public class BookDeleteServlet extends HttpServlet {
    BooksDAO booksDAO = new BooksDAO();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int book_id = Integer.parseInt(req.getParameter("book_id"));
        if (booksDAO.deleteBook(book_id)) {
            resp.sendRedirect("books");


        } else {

            resp.sendRedirect("books");

        }
    }
}
