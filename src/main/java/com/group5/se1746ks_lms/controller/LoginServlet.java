package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.AccountDAO;
import com.group5.se1746ks_lms.model.Account;
import com.group5.se1746ks_lms.util.GoogleCaptcha;
import com.group5.se1746ks_lms.util.GoogleOAuth;
import com.group5.se1746ks_lms.util.GuestRateLimited;
import com.group5.se1746ks_lms.util.JWTUtil;
import de.svws_nrw.ext.jbcrypt.BCrypt;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    private AccountDAO accountDAO;

    GoogleOAuth GoogleOAuth = new GoogleOAuth();

    public void init() {
        accountDAO = new AccountDAO();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Account loggedInUser = (Account) session.getAttribute("loggedInUser");
        String action = request.getParameter("action");
        if ("google".equals(action)) {
            String googleLoginURL = GoogleOAuth.getGoogleLoginURL();
            response.sendRedirect(googleLoginURL);

        } else {

            String success = request.getParameter("success");
            String username = request.getParameter("username");

            if (loggedInUser != null) {
                response.sendRedirect("home");
            } else {
                if (success != null) {
                    if (success.equals("true")) {
                        request.setAttribute("success", "true");
                    } else if (success.equals("reset")) {
                        if (username != null) {
                            request.setAttribute("username", username);
                        }
                        request.setAttribute("success", "reset");
                    }
                }
                request.getRequestDispatcher("views/Login.jsp").forward(request, response);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        String errorMessage = GuestRateLimited.RateLimited(request.getSession());
        if (errorMessage != null) {
            request.setAttribute("error", errorMessage);
            RequestDispatcher dispatcher = request.getRequestDispatcher("views/Login.jsp");
            dispatcher.forward(request, response);
            return;
        }

        String gRecaptchaResponse = request.getParameter("g-recaptcha-response");
        GoogleCaptcha captcha = new GoogleCaptcha();
        boolean validCaptcha = captcha.verifyCaptcha(gRecaptchaResponse);

        if (!validCaptcha) {
            request.setAttribute("error", "Invalid CAPTCHA");
            request.getRequestDispatcher("views/Login.jsp").forward(request, response);
            return;
        }

        // Kiểm tra tài khoản
        Account account = accountDAO.selectAccountByUsername(username);

        if (account != null && BCrypt.checkpw(password, account.getPassword())) {
            HttpSession session = request.getSession();
            session.setAttribute("loggedInUser", account);
            if (account.getRole().equals("user")) {
                response.sendRedirect("home");
            } else {
                response.sendRedirect("dashboard");
            }
        } else {
            request.setAttribute("error", "Invalid username or password");
            request.getRequestDispatcher("views/Login.jsp").forward(request, response);
        }
    }



}

