package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.AccountDAO;
import com.group5.se1746ks_lms.dao.BooksDAO;
import com.group5.se1746ks_lms.dao.LoansDAO;
import com.group5.se1746ks_lms.model.Loans;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;

@WebServlet(name = "LoansCreateServlet", value = "/createLoans")
public class LoansCreateServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("views/LoansCreate.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String book = request.getParameter("book_code");
        String user = request.getParameter("user_id");
        String reserve = request.getParameter("reserve");
        String expiry = request.getParameter("expiry");
        String borrow = request.getParameter("borrow");
        String due = request.getParameter("due");
        String status = request.getParameter("status");
        String create_date = request.getParameter("create_date");

        Loans l = new Loans(Integer.parseInt(user), book, Timestamp.valueOf(borrow), Date.valueOf(due), Timestamp.valueOf(create_date), Date.valueOf(expiry), status, false, false, Boolean.parseBoolean(reserve));
        LoansDAO loansDAO = new LoansDAO();
        AccountDAO accountDAO = new AccountDAO();
        BooksDAO booksDAO = new BooksDAO();

        if (booksDAO.existBookDetail(book) == false) {
            String errBook = "This book is not exist";
            request.setAttribute("errBook", errBook);
            request.setAttribute("l", l);
            request.getRequestDispatcher("views/LoansCreate.jsp").forward(request, response);
        } else if (accountDAO.getAccountById(user) == null) {
            String errBook = "This user is not exist";
            request.setAttribute("errUser", errBook);
            request.setAttribute("l", l);
            request.getRequestDispatcher("views/LoansCreate.jsp").forward(request, response);
        } else {
            loansDAO.createNewLoans(l);
            response.sendRedirect("loans");
        }
    }
}
