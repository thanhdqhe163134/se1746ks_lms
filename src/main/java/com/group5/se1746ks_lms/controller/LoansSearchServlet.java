package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.AccountDAO;
import com.group5.se1746ks_lms.dao.LoansDAO;
import com.group5.se1746ks_lms.dao.RequestDAO;
import com.group5.se1746ks_lms.model.Loans;
import com.group5.se1746ks_lms.model.Request;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.stream.Collectors;

@WebServlet(name = "LoansSearchServlet", value = "/searchLoans")
public class LoansSearchServlet extends HttpServlet {
    private LoansDAO loansDAO;
    private RequestDAO requestDAO;

    public void init() {
        loansDAO = new LoansDAO();
        requestDAO = new RequestDAO();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String by = request.getParameter("by");
        String query = request.getParameter("query");
        ArrayList<Loans> loansList = loansDAO.searchLoans(by, query);

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        for (Loans l : loansList) {
            Request latestRequest = requestDAO.getRequestByLoan_id(l.getLoans_id());

            String color = ""; // Determine color based on status
            if ("Reserved".equals(l.getStatus()) && l.getExpried_date().before(new Date())) {
                color = "red";
            } else if ("Reserved".equals(l.getStatus())) {
                color = "#b8b800";
            } else if ("Borrowing".equals(l.getStatus()) && (l.getDue_date().before(new Date()) && (latestRequest == null || latestRequest.getRequest_date().before(new Date())))) {
                color = "red";
            } else if ("Borrowing".equals(l.getStatus())) {
                color = "#b8b800";
            } else if ("Returned".equals(l.getStatus())) {
                color = "green";
            } else if("Overdue".equals(l.getStatus()) && l.getNew_due_date() == null || "Expired".equals(l.getStatus()) && l.getNew_due_date() == null) {
                color = "red";
            }
            out.println("<tr>");
            out.println("<td>" + l.getLoans_id() + "</td>");
            out.println("<td>" + l.getBook_detail_id() + "</td>");
            out.println("<td><img src='" + l.getImg() + "' width='120' height='150'></td>");
            out.println("<td><a href='book?book_id=" + l.getBook_id() + "'>" + l.getTitle() + "</a></td>");
            out.println("<td>" + l.getStudent_id() + "</td>");
            out.println("<td>" + l.getCreate_date() + "</td>");
            out.println("<td style='color:" + color + ";'>" + l.getStatus() + "</td>");
            out.println("<td>" + l.getDue_date() + "</td>");

            if (latestRequest != null && !latestRequest.getIs_approved() && l.getIs_request()) {
                out.println("<td><button class='btn-submit1' type='button' onclick='loadRequestForm(" + latestRequest.getRequest_id() + ")'>New Request</button></td>");
            } else if (latestRequest != null && latestRequest.getIs_approved()) {
                out.println("<td>" + latestRequest.getRequest_date() + "</td>");
            } else {
                out.println("<td></td>");
            }

            out.println("<td class='action' style='width: 100px;'>");
            out.println("<a href='loansDetail?loans_id=" + l.getLoans_id() + "' class='view'>");
            out.println("<span><i class='fa-solid fa-eye'></i> View</span></a></td>");
            out.println("</tr>");
        }
    }

}
