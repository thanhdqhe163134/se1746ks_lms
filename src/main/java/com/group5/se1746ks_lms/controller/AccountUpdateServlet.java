package com.group5.se1746ks_lms.controller;

import com.group5.se1746ks_lms.dao.AccountDAO;
import de.svws_nrw.ext.jbcrypt.BCrypt;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.sql.Date;

@WebServlet(name = "AccountUpdateServlet", value = "/updateAcc")
public class AccountUpdateServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        String student_id = request.getParameter("student_id");
        String fullname = request.getParameter("fullname");
        String username = request.getParameter("username");
        Date dob = Date.valueOf(request.getParameter("dob"));
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        String address = request.getParameter("address");
        String role = request.getParameter("role");
        Date update_date = Date.valueOf(request.getParameter("update_date"));
        String update_user = request.getParameter("update_by");

        AccountDAO accountDAO = new AccountDAO();

        accountDAO.updateAccount(id, username, student_id, fullname, dob, email, phone, address, role, update_date, update_user);
        response.sendRedirect("accDetail?id=" + id);
    }
}
