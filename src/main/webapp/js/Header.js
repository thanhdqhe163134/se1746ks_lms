function searchBooks() {
    let query = document.getElementById('search-bar2').value;
    let resultsContainer = document.querySelector('.search-results2');


    let xhr = new XMLHttpRequest();
    xhr.open('GET', 'homeSearch?query=' + query +"&type=title", true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            resultsContainer.style.display = "block2";
            resultsContainer.innerHTML = xhr.responseText;
        }
    };
    xhr.send();
}

document.getElementById('search-type').addEventListener('change', function() {
    var searchInput = document.getElementById('search-bar');
    searchInput.setAttribute('name', this.value); // Thay đổi thuộc tính name của input
});
