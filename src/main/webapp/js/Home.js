function searchBooks1() {
    let query = document.getElementById('search-bar').value;
    let type = document.getElementById('search-type').value;
    let resultsContainer = document.querySelector('.search-results2');

    let xhr = new XMLHttpRequest();
    xhr.open('GET', 'homeSearch?query=' + encodeURIComponent(query) + "&type=" + encodeURIComponent(type), true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            resultsContainer.style.display = "block";
            resultsContainer.innerHTML = xhr.responseText;
        }
    };
    xhr.send();
}

document.getElementById('search-type').addEventListener('change', function() {
    var searchInput = document.getElementById('search-bar');
    searchInput.setAttribute('name', this.value); // Thay đổi thuộc tính name của input
});

