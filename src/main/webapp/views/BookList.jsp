<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.group5.se1746ks_lms.model.Books" %>
<%@ page import="com.group5.se1746ks_lms.model.Genres" %>
<%@ page import="java.util.Set" %>
<%@ page import="java.util.List" %>
<%@ page import="com.group5.se1746ks_lms.model.Account" %>
<%@ page import="java.util.Comparator" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="java.util.LinkedHashSet" %>

<%
    session = request.getSession(false);
    String role = null;
    String username = null;

    if (session != null) {
        Account loggedInUser = (Account) session.getAttribute("loggedInUser");
        if (loggedInUser != null) {
            role = loggedInUser.getRole(); // Lấy role từ Account
            username = loggedInUser.getUsername(); // Lấy username từ Account
        }
    }

    boolean isAdmin = "admin".equals(role); // Kiểm tra xem role có phải là admin không



%>

<head>
    <link rel="stylesheet" href="css/BookList.css">
    <script src="js/BookList.js" defer></script>
    <script src="js/Home.js" defer></script>

    <title>Book List</title>
</head>
<body>

<jsp:include page="Header.jsp" />

<%--<input type="text" id="search-bar" placeholder="Search by Title..." oninput="searchBooks()"/>--%>

<div class="wrapper">
    <div class="genre-list">
        <div class="logo">
            <a href="home">
                <img
                        src="https://cdn.haitrieu.com/wp-content/uploads/2021/10/Logo-Dai-hoc-FPT.png"
                        width="120"
                />
            </a>
        </div>
        <ul>
            <li>
                <a href="books">
                    <span class="genre">All </span>
                </a>
            </li>

            <%
                List<Genres> allGenres = (List<Genres>) request.getAttribute("allGenres");
                allGenres = allGenres.stream().sorted(Comparator.comparing(Genres::getGenre_name)).collect(Collectors.toList());

                for (Genres genre : allGenres) {
            %>
            <li>
                <div class="sidebar-items">
                    <a href="<%= "books?genre_id=" + genre.getGenre_id() %>">
                        <span class="genre"><%= genre.getGenre_name() %></span>
                    </a>
                    <% if (isAdmin) { %>
                    <form action="deleteGenre" method="post" style="display: inline">
                        <input type="hidden" name="genre_id" value="<%= genre.getGenre_id() %>"/>
                        <button type="submit" class="delete-genre-button">
                            <i class="fa-solid fa-trash"></i>
                        </button>
                    </form>
                    <% } %>
                </div>
            </li>
            <% } %>
        </ul>
    </div>


    <div class="wrapper column">

        <form class="search-block" action="books" method="get" style = " margin-top: 0px; margin-bottom: 0px;  padding-right: 20px;">
            <select id="search-type">
                <option value="title">Title</option>
                <option value="author">Author</option>
                <option value="isbn">ISBN</option>
                <option value="publisher">Publisher</option>
                <option value="language">Language</option>
            </select>
            <input type="text" id="search-bar" name="title" placeholder="Select the information that you want to search " oninput="searchBooks1()"/>
            <div class="search-results2"></div>
        </form>


    <div class="search-add-container">

        <form class="sort" action="books" method="get" id="sort">
            <span>Sort by</span>
            <select name="sortBy" onchange="document.getElementById('sort').submit();">
                <option value="all" ${sortBy == null?"selected":""}>All</option>
                <option value="az" ${sortBy.equals("title")?"selected":""}>A - Z</option>
                <option value="za" ${sortBy.equals("title_desc")?"selected":""}>Z - A</option>
                <option value="newest" ${sortBy.equals("newest")?"selected":""}>Newest</option>
                <option value="oldest" ${sortBy.equals("oldest")?"selected":""}>Oldest</option>
            </select>
        </form>

        <% if (isAdmin) { %>
        <a href="addBook?action=add">
            <button class="add-button" style="z-index: 999">Add Book</button>
        </a>
        <% } %>
    </div>



        <div class="book-container">
            <%
                List<Books> bookList = (List<Books>) request.getAttribute("bookList");
                for (Books book : bookList) {
            %>
            <div class="book-card">
                <img
                        src="<%= book.getIMG() %>"
                        class="book-img"
                        alt="<%= book.getTitle() %>"
                />
                <div class="book-content">
                    <h4 class="book-title"><%= book.getTitle() %>
                    </h4>
                    <div class="book-genres">

                        <%
                            Set<Genres> genres = book.getGenres();
                            genres = genres.stream().sorted(Comparator.comparing(Genres::getGenre_name)).collect(Collectors.toCollection(LinkedHashSet::new));
                            for (Genres genre : genres) {
                        %>
                        <a href="<%= "books?genre_id=" + genre.getGenre_id() %>">
                            <span class="book-genres"><%= genre.getGenre_name() %></span>
                        </a> <% } %>

                    </div>
                    <div class="author">
                        <p style="font-weight: 600;margin: 0;font-size: 14px;">Author:&ensp;</p>
                        <p class="book-author">
                            <a href="<%="books?author=" + book.getAuthor() %>"><%= book.getAuthor() %>
                            </a>
                        </p>
                    </div>

<%--                    <div class="publisher">--%>
<%--                        <p style="font-weight: 600; margin: 0">Publisher:&ensp;</p>--%>
<%--                        <p class="book-publisher">--%>
<%--                            <a href="<%="books?publisher=" + book.getPublisher() %>"><%= book.getPublisher() %>--%>
<%--                            </a>--%>
<%--                        </p>--%>
<%--                    </div>--%>

                </div>
                <div class="button-action">
                    <a href="<%= "book?book_id=" + book.getBook_id() %>">
                        <button class="detail-button">Detail</button>
                    </a>
                    <% if (isAdmin) { %>
                    <form action="deleteBook" method="post">
                        <input
                                type="hidden"
                                name="book_id"
                                value="<%= book.getBook_id() %>"
                        />
                        <button class="delete-button">Delete</button>
                    </form>
                    <% } %>
                </div>
            </div>

            <% } %>
        </div>

        <%
            int currentPage = (Integer) request.getAttribute("currentPage");
            int itemsPerPage = (Integer) request.getAttribute("itemsPerPage");
            int totalPages = (Integer) request.getAttribute("totalPages");
            String author = request.getParameter("author");
            String publisher = request.getParameter("publisher");
            String genre_id = request.getParameter("genre_id");
            String sortBy = request.getParameter("sortBy");

            String searchParams = "";
            if (author != null && !author.isEmpty()) {
                searchParams += "author=" + author;
            }
            if (publisher != null && !publisher.isEmpty()) {
                searchParams += "publisher=" + publisher;
            }
            if (genre_id != null && !genre_id.isEmpty()) {
                searchParams += "genre_id=" + genre_id;
            }


        %>

        <div class="pagination-container">
            <div class="pagination-nav">
                <a href="books?<%= searchParams %>&currentPage=<%= currentPage - 1 %>&itemsPerPage=<%= itemsPerPage %>&sortBy=<%= sortBy %>"
                   class="<% if(currentPage == 1) { %>disabled<% } %>">Previous</a>

                <span class="pagination-info"> <%= currentPage %> / <%= totalPages %></span>

                <a href="books?<%= searchParams %>&currentPage=<%= currentPage + 1 %>&itemsPerPage=<%= itemsPerPage %>&sortBy=<%= sortBy %>"
                   class="<% if(currentPage >= totalPages) { %>disabled<% } %>">Next</a>

            </div>
            <div class="pagination-settings">
                <form action="books" method="get" class="pagination-form" onsubmit="return validateForm();">
                    <!-- Hidden fields for search parameters -->
                    <% if (author != null && !author.isEmpty()) { %>
                    <input type="hidden" name="author" value="<%= author %>">
                    <% } %>
                    <% if (publisher != null && !publisher.isEmpty()) { %>
                    <input type="hidden" name="publisher" value="<%= publisher %>">
                    <% } %>
                    <% if (genre_id != null && !genre_id.isEmpty()) { %>
                    <input type="hidden" name="genre_id" value="<%= genre_id %>">
                    <% } %>
                    <% if (sortBy != null && !sortBy.isEmpty()) { %>
                    <input type="hidden" name="sortBy" value="<%= sortBy %>">
                    <% } %>

                    <label for="currentPage" style="margin-right: auto">Current page</label>
                    <input type="number" id="currentPage" name="currentPage" style="margin-left: 10px"
                           value="<%= currentPage %>" min="1">
                    <label for="itemsPerPage"></label>
                    <select id="itemsPerPage" name="itemsPerPage">
                        <option value="4" <%= itemsPerPage == 4 ? "selected" : "" %>>4</option>
                        <option value="8" <%= itemsPerPage == 8 ? "selected" : "" %>>8</option>
                        <option value="16" <%= itemsPerPage == 16 ? "selected" : "" %>>16</option>
                    </select>
                    <button type="submit" class="pagination-button">View</button>
                </form>
            </div>

        </div>


        <jsp:include page="Footer.jsp"/>
    </div>
</div>
<script>
    if (window.location.href.includes("books") && !window.location.href.includes("?genre_id=")) {
        document.querySelectorAll(".genre-list ul li")[0].classList.add("active");
    } else if (window.location.href.includes('/books?genre_id=')) {
        let url = window.location.href;
        let regex = /genre_id=(\d+)/;
        let match = url.match(regex);

        if (match) {
            let genre_id = match[1];
            let allListItems = document.querySelectorAll(".genre-list ul li a");
            for (let i = 0; i < allListItems.length; i++) {
                if (allListItems[i].href.includes("genre_id=" + genre_id)) {
                    allListItems[i].closest('li').classList.add("active");
                    break;
                }
            }
        }
    }


</script>

<script>
    function validateForm() {
        var currentPage = document.getElementById("currentPage").value;
        var totalPages = <%= totalPages %>;

        if (currentPage > totalPages) {
            document.getElementById("currentPage").value = totalPages;
        }

        return true;
    }
</script>

</body>
</html>