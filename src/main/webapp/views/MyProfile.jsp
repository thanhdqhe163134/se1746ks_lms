<%--
  Created by IntelliJ IDEA.
  User: tranthanhhuyen
  Date: 19/10/2023
  Time: 22:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
  <title>MANAGE ACCOUNTS</title>
  <link rel="stylesheet" href="css/Home.css">
  <link rel="stylesheet" href="css/MyProfile.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
        integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"/>
</head>
<body>
<div class="wrapper">
  <jsp:include page="Header.jsp"/>
  <div class="top-title">
    <h3>MY PROFILE</h3>
  </div>

  <div class="mainpage">
    <div class="side-bar">
      <div class="avatar">
        <img src="${a.getImg()}">
        <h2>${a.getFull_name()}</h2>
        <span>Role: ${a.getRole()}</span>
      </div>
      <div class="menu">
        <a href="myProfile?id=${sessionScope.loggedInUser.getUser_id()}">
          <i class="fa-solid fa-user"></i>
          <p>View my profile</p>
        </a>
        <a href="myProfile?changePassword=true&id=${sessionScope.loggedInUser.getUser_id()}">
          <i class="fa-solid fa-lock"></i>
          <p>Change my password</p>
        </a>
      </div>
    </div>
    <form action="myProfile?changePassword=true&id=${sessionScope.loggedInUser.getUser_id()}" method="post">
      <div class="profile-content">
        <c:if test="${changePassword != null}">
          <h2>Change my password</h2>
          <div class="pass-content">
            <h3>Current password</h3>
            <input type="password" name="current" value="" required>
            <p>${wrongPass}</p>
          </div>
          <div class="pass-content">
            <h3>New password</h3>
            <input type="password" name="new" value="" required>
          </div>
          <div class="pass-content">
            <h3>Password confirmation</h3>
            <input type="password" name="confirm" value="" required>
            <p>${noMatch}</p>
          </div>
          <div class="submit">
            <input type="submit" name="submit" value="Save password">
          </div>
        </c:if>
        <c:if test="${changePassword == null}">
          <div class="profile-list">
            <h3>User ID</h3>
            <p>${a.getUser_id()}</p>
          </div>
          <div class="profile-list">
            <h3>Username</h3>
            <p>${a.getUsername()}</p>
          </div>
          <div class="profile-list">
            <h3>Student ID</h3>
            <p>${a.getStudent_id()}</p>
          </div>
          <div class="profile-list">
            <h3>Fullname</h3>
            <p>${a.getFull_name()}</p>
          </div>
          <div class="profile-list">
            <h3>Date of birth</h3>
            <p>${a.getDob()}</p>
          </div>
          <div class="profile-list">
            <h3>Email</h3>
            <p>${a.getEmail()}</p>
          </div>
          <div class="profile-list">
            <h3>Phone Number</h3>
            <p>${a.getPhone_number()}</p>
          </div>
          <div class="profile-list">
            <h3>Address</h3>
            <p>${a.getAddress()}</p>
          </div>
          <div class="profile-list">
            <h3>Created date</h3>
            <p>${a.getCreated_date()}</p>
          </div>
          <div class="profile-list">
            <h3>Updated date</h3>
            <p>${a.getUpdated_date()}</p>
          </div>
        </c:if>
      </div>
    </form>
  </div>

  <jsp:include page="Footer.jsp"/>
</div>
</body>
</html>
