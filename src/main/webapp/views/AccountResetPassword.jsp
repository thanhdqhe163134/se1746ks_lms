<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Reset Password</title>
    <link rel="stylesheet" href="css/Login.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
          integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
</head>

<body>
<div class="wrapper">
    <div class="login">
        <img class="logo" src="https://cdn.haitrieu.com/wp-content/uploads/2021/10/Logo-Dai-hoc-FPT.png">
        <h1 class="form-header">Reset Password</h1>

        <form action="reset-password" method="post" class="form-login">
            <input type="hidden" name="username" value="${username}">
            <div class="form-input">
                <input type="password" class="enter" placeholder="New Password" name="newPassword" required>
                <i class="fa-solid fa-lock"></i>
            </div>
            <div class="form-input">
                <input type="password" class="enter" placeholder="Confirm Password" name="confirmPassword" required>
                <i class="fa-solid fa-lock"></i>
            </div>
            <p class="error-mess"><b>${error}</b></p>
            <input type="submit" value="Reset Password" class="form-submit">
            <br>
            <a href="login" class="back">
                Back to Login
            </a>
        </form>
    </div>
</div>

</body>
</html>
