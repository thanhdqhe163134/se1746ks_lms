<%@ page import="com.group5.se1746ks_lms.model.BookEdition" %>
<%@ page import="java.util.List" %>
<%@ page import="com.group5.se1746ks_lms.model.Account" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<style>
  button {
    font-size: 16px;
    padding: 4px 10px;
    border: none;
    border-radius: 5px;
    cursor: pointer;
    transition: background-color 0.3s ease;
  }

</style>

<%
  List<BookEdition> bookEditions = (List<BookEdition>) request.getAttribute("bookEditions");
  long publisher_id = (long) request.getAttribute("publisher_id");
  String[] colors = new String[bookEditions.size()];
  for(int i = 0; i < bookEditions.size(); i++) {
    int R = (int)(Math.random()*56) + 200;
    int G = (int)(Math.random()*56) + 200;
    int B= (int)(Math.random()*56) + 200;
    colors[i] = String.format("#%02x%02x%02x", R, G, B);
  }
%>
<link rel="stylesheet" href="css/BookDetail.css">

<div class="container" style="width: 90%; margin: 2% auto;">


  <br>
    <li class="row">
      <p class="name col-xs-3">
        <i class="fa fa-clock-o"></i> Publish year
      </p>
      <ul class="list">
        <% for(int i = 0; i < bookEditions.size(); i++) { %>
        <input name="publish_year" id="publish_year" type="text" value="<%= bookEditions.get(i).getPublish_year() %>" style="background-color:<%= colors[i] %>; font-size: 15px">
        <% } %>
      </ul>
    </li>
    <br>
    <li class="row">
      <p class="name col-xs-3">
        <i class="fa fa-language"></i> Language
      </p>
      <ul class="list">
        <% for(int i = 0; i < bookEditions.size(); i++) { %>
        <input name="language" id="language" type = "text" value="<%= bookEditions.get(i).getLanguage() %>" style="background-color:<%= colors[i] %>; font-size: 15px">
        <% } %>
      </ul>
    </li>
    <br>

    <li class="row">
      <p class="name col-xs-3">
        <i class="fa fa-barcode"></i> ISBN
      </p>
      <ul class="list">
        <% for(int i = 0; i < bookEditions.size(); i++) { %>

        <a href="BookISBN?action=view&isbn=<%=bookEditions.get(i).getISBN()%>">
          <button type="button" class="pulseButton" style="background-color:<%= colors[i] %>;">
            <%= bookEditions.get(i).getISBN() %>
          </button>
        </a>
        <input name="isbn" id="isbn" type = "hidden" value="<%= bookEditions.get(i).getISBN() %>">
        <% } %>
      </ul>
    </li>
  <br>
  <li class="row">
    <p class="name col-xs-3">
      <i class="fa fa-calendar"></i> Total copies
    </p>
    <ul class="list">
      <% for(int i = 0; i < bookEditions.size(); i++) { %>
      <button style="background-color:<%= colors[i] %>;" disabled><%= bookEditions.get(i).getTotal_copies() %></button>
      <% } %>
    </ul>
  </li>
  <br>

  <li class="row">
    <p class="name col-xs-3">
      <i class="fa fa-calendar"></i> Reserved copies
    </p>
    <ul class="list">
      <% for(int i = 0; i < bookEditions.size(); i++) { %>
      <button style="background-color:<%= colors[i] %>;" disabled><%= bookEditions.get(i).getBorrowed_copies() %></button>
      <% } %>
    </ul>
  <br>
    <li class="row">
        <p class="name col-xs-3">
        <i class="fa fa-calendar"></i> Available copies
        </p>
        <ul class="list">
        <% for(int i = 0; i < bookEditions.size(); i++) { %>
        <button style="background-color:<%= colors[i] %>;" disabled><%= bookEditions.get(i).getAvailable_copies() %></button>
        <% } %>
        </ul>
  <br>

  <li class="row">
    <p class="name col-xs-3">
      <i class="fa fa-calendar"></i> Created date
    </p>
    <ul class="list">
      <% for(int i = 0; i < bookEditions.size(); i++) { %>
      <button style="background-color:<%= colors[i] %>;" disabled><%= bookEditions.get(i).getCreated_date() %></button>
      <% } %>
    </ul>
  </li>
  <br>
  <li class="row">
    <p class="name col-xs-3">
      <i class="fa fa-calendar"></i> Created date
    </p>
    <ul class="list">
      <% for(int i = 0; i < bookEditions.size(); i++) { %>
      <button style="background-color:<%= colors[i] %>;" disabled><%= bookEditions.get(i).getCreated_user() %></button>
      <% } %>
    </ul>
  </li>
  <br>
  <li class="row">
    <p class="name col-xs-3">
      <i class="fa fa-calendar"></i> Updated date
    </p>
    <ul class="list">
      <% for(int i = 0; i < bookEditions.size(); i++) { %>
      <button style="background-color:<%= colors[i] %>;" disabled><%= bookEditions.get(i).getUpdated_date() %> </button>
      <% } %>
    </ul>
  </li>
  <br>
  <li class="row">
    <p class="name col-xs-3">
      <i class="fa fa-user"></i> Updated by
    </p>
    <ul class="list">
      <% for(int i = 0; i < bookEditions.size(); i++) { %>
      <button style="background-color:<%= colors[i] %>;" disabled><%= bookEditions.get(i).getUpdated_user() %></button>
      <% } %>
    </ul>
  </li>
<input type="hidden" name="publisher_id" value="<%= publisher_id %>">
</div>

<style>
  @keyframes pulseAnimation {
    0% { transform: scale(1); }
    50% { transform: scale(1.05); }
    100% { transform: scale(1); }
  }

  .pulseButton {
    animation: pulseAnimation 2s infinite;
    transition: transform 0.5s ease-in-out;
  }
</style>

