<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.group5.se1746ks_lms.model.*" %>
<%@ page import="com.group5.se1746ks_lms.dao.BooksDAO" %>
<%@ page import="java.util.*" %>
<%@ page import="java.util.stream.Collectors" %>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>



<%
    session = request.getSession(false);
    String role = "";
    String username = "";
    boolean isLoggedIn = false;
    Long user_id = null;

    if (session != null) {
        Account loggedInUser = (Account) session.getAttribute("loggedInUser");
        if (loggedInUser != null) {
            role = loggedInUser.getRole(); // Lấy role từ Account
            username = loggedInUser.getUsername(); // Lấy username từ Account
            isLoggedIn = true;
            user_id = loggedInUser.getUser_id();
        }
    }
    boolean isAdmin = "admin".equals(role); // Kiểm tra xem role có phải là admin không
%>


<%
    Books book = (Books) request.getAttribute("book");
    List<Genres> allGenres = (List<Genres>) request.getAttribute("allGenres");
    allGenres = allGenres.stream().sorted(Comparator.comparing(Genres::getGenre_name)).collect(Collectors.toList());
    Set<Genres> bookGenres = book != null ? book.getGenres() : new HashSet<>();
    boolean editMode = "true".equals(request.getParameter("editMode"));
%>
<head>
    <title><%= book.getTitle() %></title>
</head>
<script src="js/BookAdd.js" defer></script>
<script src="js/BookDetail.js" defer></script>
<link rel="stylesheet" href="css/BookDetail.css">
<link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
/>

<jsp:include page="Header.jsp"/>

<%
    String error = request.getParameter("error");
    String success = request.getParameter("success");
%>
<jsp:include page="Notification.jsp"/>


<div class="book-detail1">
        <% if (editMode) { %>
    <!-- Edit Mode -->
    <jsp:include page="BookEdit.jsp"/>

    <% } else { %>


        <div class="container" style="
    width: 90%;
    margin: 2% auto;
">
        <div class="main_content">
            <div class="book_detail">
                <div class="book_info">
                    <div class="book_avatar" itemtype="https://schema.org/ImageObject">
                        <img itemprop="image" src="<%= book.getIMG() %>" alt="<%= book.getTitle() %>"       />
                    </div>
                    <div
                            class="book_other"
                            itemscope=""
                            itemtype="http://schema.org/Book"
                    >
                        <h1 itemprop="name"><%= book.getTitle() %></h1>
                        <div class="txt">
                            <ul class="list-info">
                                <li class="author row">
                                    <p class="name col-xs-3">
                                        <i class="fa fa-user"></i> Author
                                    </p>
                                    <strong>
                                    <a class="col-xs-9" href="<%="books?author=" + book.getAuthor() %>">
                                        <%= book.getAuthor() %>
                                    </a>
                                    </strong>
                                </li>
                                <br>

                            <%
                                    List<Publisher> publishers = (List<Publisher>) request.getAttribute("publishers");
                                    %>
                                <li class="row">
                                    <p class="name col-xs-3">
                                        <i class="fa fa-rss"></i> Publisher
                                    </p>
                                        <% for(Publisher publisher : publishers){ %>
                                        <button class="btn-publisher" onclick="fetchBookEditions('<%= publisher.getPublisher_id() %>','<%= book.getBook_id() %>')" ><%= publisher.getPublisher_name() %></button>
                                        <% } %>
                                </li>



                                <form id="borrowForm" action="bookBorrow" method="post">

                                <br>
                                <div class="row" id="bookEditions">

                                </div>

                                <br>



                                <li class="row">
                                    <p class="name col-xs-3">
                                        <i class="fa fa-copy"></i> Total Copies
                                    </p>
                                    <p class="col-xs-9 number-like"><%= book.getTotal_copies() %></p>
                                </li>
                                <li class="row">
                                    <p class="name col-xs-3">
                                        <i class="fa fa-hand-paper-o"></i> Borrowed Copies
                                    </p>
                                    <p class="col-xs-9"><%= book.getCopies_borrowed() %></p>
                                </li>
                                <li class="row">
                                    <p class="name col-xs-3">
                                        <i class="fa fa-check-circle-o"></i> Available Copies
                                    </p>
                                    <p class="col-xs-9"><%= book.getCopies_available() %></p>
                                </li>
                                <% if (isAdmin) { %>
                                <li class="row">
                                    <p class="name col-xs-3">
                                        <i class="fa fa-user"></i> Created by
                                    </p>
                                    <p class="col-xs-9"><%= book.getCreated_user() %></p>
                                </li>
                                <li class="row">
                                    <p class="name col-xs-3">
                                        <i class="fa fa-calendar"></i> Created date
                                    </p>
                                    <p class="col-xs-9"><%= book.getCreated_date() %></p>
                                </li>
                                <li class="row">
                                    <p class="name col-xs-3">
                                        <i class="fa fa-user"></i> Updated by
                                    </p>
                                    <p class="col-xs-9"><%= book.getUpdated_user() %></p>
                                </li>
                                <li class="row">
                                    <p class="name col-xs-3">
                                        <i class="fa fa-calendar"></i> Updated date
                                    </p>
                                    <p class="col-xs-9"><%= book.getUpdated_date() %></p>
                                </li>
                                <% } %>
                            </ul>
                        </div>

                        <ul class="list01">
                            <%
                                Set<Genres> genres = book.getGenres();
                                genres = genres.stream().sorted(Comparator.comparing(Genres::getGenre_name)).collect(Collectors.toCollection(LinkedHashSet::new));

                                for(Genres genre : genres) {
                            %>
                            <li class="li03">
                                <a href="<%= "books?genre_id=" + genre.getGenre_id() %>"><%= genre.getGenre_name() %></a>
                            </li>
                            <% } %>
                        </ul>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <h3><i class="fa fa-info-circle"></i> Description</h3>
                <div
                        class="story-detail-info detail-content readmore-js-section readmore-js-collapsed"
                        style="/* height: 60px; */"
                >
                    <p>
                        <%= book.getDescription() %>
                    </p>
                </div>
                <br>
                <% if (isAdmin) { %>
                <a href="<%= "book?book_id=" + book.getBook_id() + "&editMode=true" %>">
                    <button class="action-button" type="button" style="font-size: 20px">Edit Book</button>
                </a>
                <% } else if(role != "" && !role.equalsIgnoreCase("librarian")){ %>
                <input type="hidden" name="book_id" value="<%=book.getBook_id()%>">
                <input type="hidden" name="user_id" value="<%= user_id %>" />
                <input type="hidden" name="username" value="<%= username %>" />
                <button class="action-button" type="submit" style="font-size: 20px">Borrow</button>
                <%}  else if(role.equals("")) {%>
                <p style="font-size: 1rem">Please <a href="login" style="color: #f18121; text-decoration: underline">login</a> to borrow this book or comment</p>
                <% } %>
                </form>



                <%--                comment user--%>
                <%!
                    public List<Comment> getRepliesForRoot(List<Comment> allComments, long rootCommentId) {
                        List<Comment> replies = new ArrayList<>();
                        for (Comment comment : allComments) {
                            if (comment.getRep_comment() == rootCommentId) {
                                replies.add(comment);
                            }
                        }
                        return replies;
                    }
                %>

                <% List<Comment> comments = (List<Comment>) request.getAttribute("comments"); %>
                <% List<Comment> rootComments = (List<Comment>) request.getAttribute("rootComments"); %>

                <div
                        id="ad_info"
                        style="
              padding-top: 20px;
              max-width: 1200px;
              margin: 0 auto;
              overflow: hidden;
            "
                ></div>
                <div class="comment-container" id="comment_list">
                    <div style="display: flex; justify-content: flex-start; align-items: center">
                         <span class="story-detail-title"
                         ><i class="fa fa-comments"></i>Comment<span
                                 class="comment-count"
                         ></span
                         ></span
                         >
                        <form action="book" method="get" id="sortForm">
                            <select name="sortOrder" onchange="document.getElementById('sortForm').submit();">
                                <option value="oldest" <%= "oldest".equals(request.getParameter("sortOrder")) ? "selected" : "" %>>Oldest</option>
                                <option value="newest" <%= "newest".equals(request.getParameter("sortOrder")) ? "selected" : "" %>>Newest</option>
                            </select>
                            <input type="hidden" name="book_id" value="<%= book.getBook_id() %>" />
                        </form>
                    </div>
                    <div class="group01 comments-container">
                        <% if(isLoggedIn) { %>
                        <form action="comment" method="post">
                                <input type="hidden" name="book_id" value="<%= book.getBook_id() %>" />
                                <input type="hidden" name="action" value="comment" />
                                <input type="hidden" name="user_id" value="<%= user_id %>" />
                                <input type="hidden" name="rep_comment" value="0" />
                                <input type="hidden" name="username" value="<%= username %>" />
                                <div style="display: flex; justify-content: space-between">
                                    <textarea
                                            name="content"
                                            placeholder="Add your comment here"
                                            style="width: 92%; min-height: 80px"
                                    ></textarea>
                                    <button
                                            style="
                    max-height: 30px;
                    width: 6%;
                    border-color: #f18121;
                    color: #f18121;
                    background-color: white;
                    border-radius: 4px;
                  "
                                    >
                                        Send
                                    </button>
                                              </div>

                            </form>
                        <% } %>
                        <div class="list-comment">
                            <%
                                for (Comment comment : rootComments) {
                                    if (comment.getRep_comment() == 0) {
                            %>
                            <article style="margin: 1% 0"
                                    class="info-comment child_3449842 parent_0 comment-main-level"
                            >
                                <div class="outsite-comment comment-main-level">
                                    <figure class="avartar-comment">
                                        <img
<%--                                                class=""--%>
                                                src="<%= comment.getIMG() != null && !comment.getIMG().isEmpty() ? comment.getIMG() : "img/default-avatar.png" %>" alt="<%= comment.getCreated_user() %>'s avatar" class="comment-avatar"
                                                style=""
                                        />
                                    </figure>
                                    <div class="item-comment">
                                        <div class="outline-content-comment">
                                            <div>
                                                <strong class="level name_4"><%= comment.getCreated_user() %></strong>
                                            </div>
                                            <div class="content-comment">
                                                <strong></strong> <%= comment.getContent() %>
                                            </div>
                                        </div>

                                        <div class="action-comment" style="margin-top: 1%">

                                            <span class="time"
                                            ><i class="fa fa-clock"></i> <%= comment.getCreated_date() %></span
                                            >
                                            <% if(isLoggedIn && !comment.getIs_deleted()) { %>
                                            <span class="reply-comment" onclick="toggleReplyForm(<%= comment.getComment_id() %>)"
                                            ><i class="fa fa-comment"></i> Reply</span
                                            >
                                            <div id="reply-form-<%= comment.getComment_id() %>" style="display:none; ">
                                                <form action="comment" method="post" style="margin-top: 1%">
                                                    <input type="hidden" name="book_id" value="<%= book.getBook_id() %>" />
                                                    <input type="hidden" name="action" value="comment" />
                                                    <input type="hidden" name="user_id" value="<%= user_id %>" />
                                                    <input type="hidden" name="rep_comment" value="<%= comment.getComment_id() %>" />
                                                    <input type="hidden" name="username" value="<%= username %>" />
                                                    <div style="display: flex; justify-content: flex-start">
                                                        <textarea name="content" placeholder="Reply to this comment..." style="width: 50%"></textarea>
                                                        <button type="submit" style="max-height: 20px;margin-left: 10px; border-color: #f18121;
    color: #f18121;
    background-color: white;
    border-radius: 4px;">Send</button>
                                                    </div>

                                                </form>
                                            </div>
                                            <% } %>

                                            <% if(!comment.getIs_deleted() && (isAdmin || username.equals(comment.getCreated_user()))) { %>
                                            <button onclick="toggleEditForm(<%= comment.getComment_id() %>)" class="btn-edit">Edit</button>
                                            <div>
                                            <div id="edit-form-<%= comment.getComment_id() %>" style="display:none;">
                                                <form action="comment" method="post">
                                                    <input type="hidden" name="action" value="edit" />
                                                    <input type="hidden" name="comment_id" value="<%= comment.getComment_id() %>" />
                                                    <input type="hidden" name="book_id" value="<%= book.getBook_id() %>" />
                                                    <input type="hidden" name="username" value="<%= username %>" />
                                                    <div style="display: flex; justify-content: flex-start">
                                                        <textarea name="content" placeholder="Edit your comment..." style="width: 50%"><%= comment.getContent() %></textarea>
                                                        <button type="submit" style="max-height: 20px;margin-left: 10px; border-color: #f18121;
                                                            color: #f18121;
                                                            background-color: white;
                                                            border-radius: 4px;">Save</button>
                                                    </div>
                                                </form>
                                            </div>

                                            <form action="comment" method="post">
                                                <input type="hidden" name="action" value="delete" />
                                                <input type="hidden" name="username" value="<%= username %>" />
                                                <input type="hidden" name="comment_id" value="<%= comment.getComment_id() %>" />
                                                <input type="hidden" name="book_id" value="<%= book.getBook_id() %>" />
                                                <button class="btn-delete" type="submit">Delete</button>
                                            </form>
                                            </div>

                                            <% } %>

                                        </div>
                                    </div>
                                </div>
                                <%
                                    // Fetch replies for this root comment
                                    List<Comment> replies = getRepliesForRoot(comments, comment.getComment_id());
                                    for (Comment reply : replies) {
                                %>
                                <div class="outsite-comment comment-main-level" style="
    margin-left: 50px;
    margin-top: 1%;
">
                                    <figure class="avartar-comment">
                                        <img
                                        <%--                                                class=""--%>
                                                src="<%= reply.getIMG() != null && !reply.getIMG().isEmpty() ? reply.getIMG() : "img/default-avatar.png" %>" alt="<%= reply.getCreated_user() %>'s avatar" class="comment-avatar"
                                                style=""
                                        />
                                    </figure>
                                    <div class="item-comment">
                                        <div class="outline-content-comment">
                                            <div>
                                                <strong class="level name_4"><%= reply.getCreated_user() %></strong>
                                            </div>
                                            <div class="content-comment">
                                                <strong></strong> <%= reply.getContent() %>
                                            </div>
                                        </div>

                                        <div class="action-comment" style="margin-top: 1%">
                        <span class="like-comment" data-id="3449842"
                        ><i class="fa fa-thumbs-up"></i>
                          <span class="total-like-comment">0</span></span
                        >
                                            <span class="time"
                                            ><i class="fa fa-clock"></i> <%= reply.getCreated_date() %></span
                                            >
                                            <% if(isLoggedIn && !reply.getIs_deleted()) { %>
                                            <span class="reply-comment" onclick="toggleReplyForm(<%= reply.getComment_id() %>)"
                                            ><i class="fa fa-comment"></i> Reply</span
                                            >
                                            <div id="reply-form-<%= reply.getComment_id() %>" style="display:none; ">
                                                <form action="comment" method="post" style="margin-top: 1%">
                                                    <input type="hidden" name="book_id" value="<%= book.getBook_id() %>" />
                                                    <input type="hidden" name="action" value="comment" />
                                                    <input type="hidden" name="user_id" value="<%= user_id %>" />
                                                    <input type="hidden" name="rep_comment" value="<%= reply.getComment_id() %>" />
                                                    <input type="hidden" name="username" value="<%= username %>" />
                                                    <div style="display: flex; justify-content: flex-start">
                                                        <textarea name="content" placeholder="Reply to this comment..." style="width: 50%"></textarea>
                                                        <button type="submit" style="max-height: 20px;margin-left: 10px; border-color: #f18121;
    color: #f18121;
    background-color: white;
    border-radius: 4px;">Send</button>
                                                    </div>

                                                </form>
                                            </div>
                                            <% } %>

                                            <% if(!reply.getIs_deleted() && (isAdmin || username.equals(reply.getCreated_user()))) { %>
                                            <button onclick="toggleEditForm(<%= reply.getComment_id() %>)" class="btn-edit">Edit</button>
                                            <div id="edit-form-<%= reply.getComment_id() %>" style="display:none;">
                                                <form action="comment" method="post">
                                                    <input type="hidden" name="action" value="edit" />
                                                    <input type="hidden" name="comment_id" value="<%= reply.getComment_id() %>" />
                                                    <input type="hidden" name="book_id" value="<%= book.getBook_id() %>" />
                                                    <input type="hidden" name="username" value="<%= username %>" />
                                                    <div style="display: flex; justify-content: flex-start">
                                                        <textarea name="content" placeholder="Edit your comment..." style="width: 50%"><%= reply.getContent() %></textarea>
                                                        <button type="submit" style="max-height: 20px;margin-left: 10px; border-color: #f18121;
                                                            color: #f18121;
                                                            background-color: white;
                                                            border-radius: 4px;">Save</button>
                                                    </div>
                                                </form>
                                            </div>
                                            <form action="comment" method="post">
                                                <input type="hidden" name="action" value="delete" />
                                                <input type="hidden" name="username" value="<%= username %>" />
                                                <input type="hidden" name="comment_id" value="<%= reply.getComment_id() %>" />
                                                <input type="hidden" name="book_id" value="<%= book.getBook_id() %>" />
                                                <button class="btn-delete" type="submit">Delete</button>
                                            </form>
                                            <% } %>

                                        </div>
                                    </div>
                                </div>
                                <% } %>
                            </article>
                            <%
                                    }
                                }
                            %>


                            <div class="page_redirect">
                                <p class="active">1</p>
                                <p onclick="loadComment(2)">2</p>
                                <p onclick="loadComment(3)">3</p>
                                <p onclick="loadComment(4)">4</p>
                                <p onclick="loadComment(5)">5</p>
                                <p onclick="loadComment(2);">
                                    <span aria-hidden="true">›</span>
                                </p>
                                <p onclick="loadComment(43);">
                                    <span aria-hidden="true">»</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                var urlComment = "https://truyenqqvn.com/frontend/comment/index";
                var urlCommentLoad = "https://truyenqqvn.com/frontend/comment/list";
                var urlCommentRemove =
                    "https://truyenqqvn.com/frontend/comment/remove";
                var urlLikeComment = "https://truyenqqvn.com/frontend/comment/like";
            </script>
        </div>
    </div>
    <% } %>
</div>
<jsp:include page="Footer.jsp"/>

            <script>
                $("#submitButton").click(function() {

                    // Danh sách đối tượng BookEdition
                    let bookEditions = ${bookEditions};

                    // Gửi AJAX
                    $.ajax({
                        url: "/bookEdition",
                        method: "POST",
                        data: {
                            bookEditions: bookEditions
                        },
                        success: function(result) {
                            alert("Saved successfully!");
                        }
                    });

                });

            </script>
