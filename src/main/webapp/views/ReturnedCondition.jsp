<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.util.*" %>
<%@ page import="com.group5.se1746ks_lms.dao.LoansDAO" %>
<%@ page import="com.group5.se1746ks_lms.model.Loans" %>
<%@ page import="com.group5.se1746ks_lms.model.Account" %>

<%-- Assume LoansDAO and Loans have been imported correctly --%>
<%
  // Here we assume that the loans_id attribute has been set by the servlet.
  String loansId = (String) request.getAttribute("loans_id");
  LoansDAO loansDAO = new LoansDAO();
  Loans loan = loansDAO.getLoansById(loansId);
%>
<%
  session = request.getSession(false);
  String role = "";
  String username = "";
  boolean isLoggedIn = false;
  Long user_id = null;

  if (session != null) {
    Account loggedInUser = (Account) session.getAttribute("loggedInUser");
    if (loggedInUser != null) {
      role = loggedInUser.getRole(); // Lấy role từ Account
      username = loggedInUser.getUsername(); // Lấy username từ Account
      isLoggedIn = true;
      user_id = loggedInUser.getUser_id();
    }
  }
  boolean isAdmin = "admin".equals(role); // Kiểm tra xem role có phải là admin không
%>

<div class="modal-content">
  <span class="close" onclick="closeModal()">&times;</span>
  <form id="returnBookForm" action="returnForm" method="post">
    <input type="hidden" name="loan_id" value="${loans_id}" />
    <input type="hidden" name="username" value="<%=username%>" />

    <div class="form-group">
      <label for="book_condition">Book Condition:</label>
      <input type="text" id="book_condition" name="book_condition" required />
    </div>

    <div class="form-group">
      <label for="fine_option">Damage or lost </label>
      <select id="fine_option" name="fine_option">
        <option value="none" selected>None</option>
        <option value="damage" selected>Damage</option>
        <option value="lost">Lost</option>
      </select>
    </div>

    <div class="form-group">
      <button type="submit" class="button-action">Submit</button>
    </div>
  </form>
</div>

<script>
  // Here you can put the Javascript needed to close the modal as shown before
  function closeModal() {
    var modal = document.getElementById('modalContainer');
    modal.style.display = 'none';
  }
</script>

<style>
  /* You can put CSS here to style your form if needed */
  .modal-content {
    /* Sample style */
    background-color: #fefefe;
    margin: 15% auto;
    padding: 20px;
    border: 1px solid #888;
    width: 80%;
  }

  .close {
    /* Sample style */
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
  }

  .close:hover,
  .close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
  }

  .form-group {
    margin-bottom: 15px;
  }
</style>
