<%@ page import="com.group5.se1746ks_lms.model.Request" %>
<%@ page import="java.util.List" %>
<%@ page import="com.group5.se1746ks_lms.model.Account" %><%--
  Created by IntelliJ IDEA.
  User: YourUserName
  Date: Today's Date
  Time: Current Time
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    session = request.getSession(false);
    Account loggedInUser = null;
    String role = null;
    if (session != null) {
        loggedInUser = (Account) session.getAttribute("loggedInUser");
        role = loggedInUser.getRole();
    }
    boolean isUserLoggedIn = loggedInUser != null;
%>
<%
    int currentPage = (Integer) request.getAttribute("currentPage");
    int itemsPerPage = (Integer) request.getAttribute("itemsPerPage");
    int totalPages = (Integer) request.getAttribute("totalPages");
    String sortBy = request.getParameter("sortBy");
    String userId = request.getParameter("user_id");
    if(totalPages == 0) {
        totalPages = 1;
    }
    if(currentPage > totalPages){
        currentPage = totalPages;
    }

%>
<html>
<head>
    <title>MANAGE REQUESTS</title>
    <link rel="stylesheet" href="css/Home.css">
    <link rel="stylesheet" href="css/LoansList.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
          integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
</head>
<body>
<div class="wrapper">
    <jsp:include page="Header.jsp"/>

    <div class="top-title">
        <h3>REQUESTS</h3>
    </div>
    <div class="container">
        <div class="body-container">
            <div class="title-box">
                <h2 class="title">Requests</h2>
            </div>
            <div class="record-list">
                <div class="search-sort">
                    <form class="sort" action="requestList" method="get" id="sort">
                        <input type="hidden" name="user_id" value="<%=userId%>">
                        <span>Sort by</span>
                        <select name="sortBy" onchange="document.getElementById('sort').submit();">
                            <option value="null" ${sortBy == null?"selected":""}>All</option>
                            <option value="DESC" ${sortBy.equals("DESC")?"selected":""}>Newest</option>
                            <option value="ASC" ${sortBy.equals("ASC")?"selected":""}>Oldest</option>
                        </select>
                    </form>
                </div>
                <table class="listing" border="1">
                    <thead>
                    <tr>
                        <th>REQUEST ID</th>
                        <th>LOAN ID</th>
                        <th>REQUEST DATE</th>
                        <th>STATUS</th>
                        <th>CREATED DATE</th>

                    </tr>
                    </thead>
                    <tbody class="list">
                    <% List<Request> requests = (List<Request>) request.getAttribute("req"); %>
                     <%for (Request req : requests) {%>
                        <tr>
                            <td><%=req.getRequest_id()%></td>
                            <td><%=req.getLoan_id()%></td>
                            <td><%=req.getRequest_date()%></td>
                            <td><%=req.getDescription()%></td>
                            <td><%=req.getCreated_date()%></td>
                        </tr>
                    <%}%>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
    <%if(isUserLoggedIn && role.equals("user")){%>
        <div class="pagination-container">
            <div class="pagination-nav">
                <a href="requestList?user_id=<%=userId%>&currentPage=<%= currentPage - 1 %>&itemsPerPage=<%= itemsPerPage %>&sortBy=<%= sortBy %>"
                   class="<% if(currentPage == 1) { %>disabled<% } %>">Previous</a>

                <span class="pagination-info"> <%= currentPage %> / <%= totalPages %></span>

                <a href="requestList?user_id=<%=userId%>&currentPage=<%= currentPage + 1 %>&itemsPerPage=<%= itemsPerPage %>&sortBy=<%= sortBy %>"
                   class="<% if(currentPage >= totalPages) { %>disabled<% } %>">Next</a>

            </div>
            <div class="pagination-settings">
                <form action="requestList" method="get" class="pagination-form" onsubmit="return validateForm();">
                    <% if (sortBy != null && !sortBy.isEmpty()) { %>
                    <input type="hidden" name="sortBy" value="<%= sortBy %>">
                    <% } %>

                    <label for="currentPage" style="margin-right: auto">Current page</label>
                    <input type="number" id="currentPage" name="currentPage" style="margin-left: 10px"
                           value="<%= currentPage %>" min="1">
                    <label for="itemsPerPage"></label>
                    <select id="itemsPerPage" name="itemsPerPage">
                        <option value="10" <%= itemsPerPage == 10 ? "selected" : "" %>>10</option>
                        <option value="20" <%= itemsPerPage == 20 ? "selected" : "" %>>20</option>
                        <option value="30" <%= itemsPerPage == 30 ? "selected" : "" %>>30</option>
                    </select>
                    <input type="hidden" name="user_id" value="<%= userId %>">
                    <button type="submit" class="pagination-button">View</button>
                </form>
            </div>
        </div>
    <%}%>
    <jsp:include page="Footer.jsp"/>
</div>
</body>
<script>
    // Your existing JavaScript code for searchRequests() function
</script>
</html>
