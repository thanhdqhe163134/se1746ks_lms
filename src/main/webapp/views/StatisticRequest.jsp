<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
  <title>Statistic</title>
  <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
  <link rel="stylesheet" href="css/Home.css">
  <link rel="stylesheet" href="css/Statistic.css">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
        integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"/>
  <jsp:include page="Header.jsp"/>

</head>
<body>
<div id="sidebar" class="sidebar">
  <button class="sidebar-item active" data-url="statistic?type=book" onclick="navigate(this)">Books</button>
  <button class="sidebar-item" data-url="statistic?type=loans" onclick="navigate(this)">Loans</button>
  <button class="sidebar-item" data-url="statistic?type=request" onclick="navigate(this)">Request</button>
  <button class="sidebar-item" data-url="statistic?type=accounts" onclick="navigate(this)">Accounts</button>
    <button class="sidebar-item" data-url="statistic?type=fine" onclick="navigate(this)">Fine</button>
</div>

<div class="content">

  <ul class="statistics-list">
    <li class="statistics-item">Total Request: ${totalRequests}</li>
    <li class="statistics-item">Total Request Pending ${totalRequestsPending}</li>
    <li class="statistics-item">Total Request Approved ${totalRequestsApproved}</li>
    <li class="statistics-item">Total Request Rejected ${totalRequestsDenied}</li>

  </ul>

  <form action="statistic" method="get">
    <select name="year">
      <option value=""></option>
      <option value="2020">2020</option>
      <option value="2021">2021</option>
      <option value="2022">2022</option>
      <option value="2023" selected>2023</option>
    </select>
    <select name="month">
      <option value=""></option>
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
      <option value="6">6</option>
      <option value="7">7</option>
      <option value="8">8</option>
      <option value="9">9</option>
      <option value="10">10</option>
      <option value="11" selected>11</option>
      <option value="12">12</option>
    </select>    <input type="hidden" name="type" value="request">
    <button type="submit">View</button>
  </form>


  <!-- Biểu đồ sách thêm vào hàng tháng -->
  <div>
    <canvas id="monthlyBookAddedChart"></canvas>
  </div>

  <!-- Biểu đồ sách thêm vào hàng ngày (nếu có) -->
  <c:if test="${not empty dailyBookAdded}">
    <div>
      <canvas id="dailyBookAddedChart"></canvas>
    </div>
  </c:if>
</div>

</body>
<jsp:include page="Footer.jsp"/>

</html>

<script>
  // Biểu đồ hàng tháng
  const monthlyDataPoints = new Array(12).fill(0);
  <c:forEach var="entry" items="${monthlyBookAdded}">
  monthlyDataPoints[parseInt("${entry.key}") - 1] = ${entry.value};
  </c:forEach>

  const monthlyData = {
    labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
    datasets: [{
      label: 'Request created per month',
      data: monthlyDataPoints,
      backgroundColor: 'rgba(0, 123, 255, 0.5)',
      borderColor: 'rgba(0, 123, 255, 1)',
      borderWidth: 1
    }]
  };

  const monthlyCtx = document.getElementById('monthlyBookAddedChart').getContext('2d');
  new Chart(monthlyCtx, {
    type: 'bar',
    data: monthlyData,
    options: {
      scales: {
        y: {
          beginAtZero: true
        }
      }
    }
  });

  <c:if test="${not empty dailyBookAdded}">
  const dailyDataPoints = new Array(31).fill(0);
  <c:forEach var="entry" items="${dailyBookAdded}">
  // Chỉ cập nhật nếu key là một ngày hợp lệ trong tháng (1-31)
  if(parseInt("${entry.key}") >= 1 && parseInt("${entry.key}") <= 31) {
    dailyDataPoints[parseInt("${entry.key}") - 1] = ${entry.value};
  }
  </c:forEach>

  const dailyLabels = Array.from({length: 31}, (_, i) => "" + (i + 1));

  const dailyData = {
    labels: dailyLabels,
    datasets: [{
      label: 'Request created per day',
      data: dailyDataPoints,
      backgroundColor: 'rgba(255, 99, 132, 0.5)',
      borderColor: 'rgba(255, 99, 132, 1)',
      borderWidth: 1
    }]
  };

  const dailyCtx = document.getElementById('dailyBookAddedChart').getContext('2d');
  new Chart(dailyCtx, {
    type: 'bar',
    data: dailyData,
    options: {
      scales: {
        y: {
          beginAtZero: true
        }
      }
    }
  });
  </c:if>

  function navigate(element) {
    var items = document.getElementsByClassName('sidebar-item');
    for (var i = 0; i < items.length; i++) {
      items[i].classList.remove('active');
    }
    element.classList.add('active');

    // Lưu mục đã chọn vào localStorage
    localStorage.setItem('selectedSidebarItem', element.getAttribute('data-url'));

    // Chuyển hướng đến URL
    var url = element.getAttribute('data-url');
    window.location.href = url;
  }

  window.onload = function() {
    var selectedUrl = localStorage.getItem('selectedSidebarItem');
    if (selectedUrl) {
      var items = document.getElementsByClassName('sidebar-item');
      // Loại bỏ highlight từ tất cả các mục
      for (var i = 0; i < items.length; i++) {
        items[i].classList.remove('active');
      }
      // Áp dụng highlight cho mục được chọn
      for (var i = 0; i < items.length; i++) {
        if (items[i].getAttribute('data-url') === selectedUrl) {
          items[i].classList.add('active');
          break;
        }
      }
    } else {
      // Nếu không có mục nào được lựa chọn, mặc định highlight mục đầu tiên hoặc một mục cụ thể
      if(items.length > 0) {
        items[0].classList.add('active'); // Hoặc chọn một mục cụ thể nếu cần
      }
    }
  };






</script>
<style>
  body {
    margin-right: 100px; /* Adjust this value to match the width of your sidebar */
    padding: 1em;
  }

  .sidebar {
    height: 10%;
    width: 100px; /* Width of sidebar */
    position: fixed;
    top: 120px;
    left: 0;
    background-color: #f1f1f1;
    padding-top: 0px; /* Space for header, adjust if necessary */
  }

  .sidebar-item {
    padding: 10px 15px;
    text-align: left;
    font-size: 15px;
    border-radius: 10px;
    background: none;
    width: 100%;
    display: block;
    transition: background-color .3s;
  }

  .sidebar-item:hover, .sidebar-item.active {
    background-color: #ff8800;
    color: white;
  }

  .content {
    margin-left: 100px; /* Same as sidebar width */
  }
</style>