<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.group5.se1746ks_lms.model.*" %>
<%@ page import="com.group5.se1746ks_lms.dao.BooksDAO" %>
<%@ page import="java.util.*" %>
<%@ page import="java.util.stream.Collectors" %>


<%
  session = request.getSession(false);
  String role = "";
  String username = "";
  boolean isLoggedIn = false;
  Long user_id = null;

  if (session != null) {
    Account loggedInUser = (Account) session.getAttribute("loggedInUser");
    if (loggedInUser != null) {
      role = loggedInUser.getRole(); // Lấy role từ Account
      username = loggedInUser.getUsername(); // Lấy username từ Account
      isLoggedIn = true;
      user_id = loggedInUser.getUser_id();
    }
  }
  boolean isAdmin = "admin".equals(role); // Kiểm tra xem role có phải là admin không
%>

<%
  Books book = (Books) request.getAttribute("book");
    if (book == null) {
        book = new Books();
    }
  List<Genres> allGenres = (List<Genres>) request.getAttribute("allGenres");
  allGenres = allGenres.stream().sorted(Comparator.comparing(Genres::getGenre_name)).collect(Collectors.toList());
  Set<Genres> bookGenres = book != null ? book.getGenres() : new HashSet<>();
  if(bookGenres == null) {
    bookGenres = new HashSet<>();
  }
  boolean editMode = "true".equals(request.getParameter("editMode"));
%>
<head>
  <title>Add A NEW BOOK</title>
</head>
<script src="js/BookAdd.js" defer></script>
<script src="js/BookDetail.js" defer></script>
<link rel="stylesheet" href="css/BookDetail.css">
<link rel="stylesheet" href="css/BookAdd.css">
<link rel="stylesheet" href="css/Search.css">
<link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
/>

<jsp:include page="Header.jsp"/>

<%
  String isExist = (String) request.getAttribute("isExist");
    if(isExist == null) {
        isExist = "";
    }
%>


<form class="search-block2" onsubmit="event.preventDefault(); searchOnEnter();">
  <input type="text" id="search-title" placeholder="Input book title" oninput="searchInstantly()"/>
  <button type="submit" style="display: none"></button>
  <div class="book-container"></div>
  <div class="search-results"></div>
</form>


<div class="book-detail1">
  <!-- Have information Mode -->
  <div class="container" style="width: 90%; margin: 2% auto;">
    <form action="${pageContext.request.contextPath}/addBook" method="post" enctype="multipart/form-data">
      <%if(book != null) { %>
      <input type="hidden" name="book_id" value="<%= book.getBook_id() %>" />
        <% } %>
      <div class="main_content">
        <div class="book_detail">
          <div class="book_info">
            <div class="book_avatar" itemtype="https://schema.org/ImageObject">
              <label>
                <input type="file" name="image" />
              </label>
            </div>
            <div class="book_other" itemscope="" itemtype="http://schema.org/Book">
              <ul class="list-info">
                <li class="row">
                  <p class="name col-xs-3">
                    <i class="fa fa-book"></i> Title
                  </p>
                  <div class="col-xs-9">
                    <%if(book.getTitle() != null)  { %>
                    <input type="text" name="title" id="title" value="<%= book.getTitle() %>" />
                        <% } else {%>
                    <input type="text" name="title" id="title" />
                        <% } %>

                  </div>
                </li>
                <br>
                <li class="author row">
                  <p class="name col-xs-3">
                    <i class="fa fa-user"></i> Author
                  </p>
                  <div class="col-xs-9">
                    <%if(book.getAuthor() != null)  { %>
                    <input type="text" name="author" value="<%= book.getAuthor() %>" />
                        <% } else {%>
                    <input type="text" name="author" />
                        <% } %>
                  </div>
                </li>

                <br>

                <li class="row">
                  <p class="name col-xs-3">
                    <i class="fa fa-info-circle"></i> Description
                  </p>
                  <div class="col-xs-9">
                    <%if(book.getDescription() != null)  { %>
                    <textarea name="description"><%= book.getDescription() %></textarea>
                        <% } else {%>
                    <textarea name="description"></textarea>
                        <% } %>
                  </div>
                </li>
              </ul>
              <div class="genres">
                <h3>Genres</h3>
                <ul class="list01">
                  <% for(Genres genre : allGenres) {
                    String checkedStr = "";
                    for(Genres bookGenre : bookGenres) {
                      if(bookGenre.getGenre_id().equals(genre.getGenre_id())) {
                        checkedStr = "checked";
                        break;
                      }
                    } %>
                  <li class="li03">
                    <a>
                      <input type="checkbox" id="genre<%= genre.getGenre_id() %>" name="genres" value="<%= genre.getGenre_id() %>" <%= checkedStr %> readonly/>
                      <label for="genre<%= genre.getGenre_id() %>"><%= genre.getGenre_name() %></label>
                    </a>
                  </li>
                  <% } %>
                </ul>
              </div>
              <br>
              <strong> <label for="newGenre">Add a new genre:</label> </strong>
              <input type="text" id="newGenre" name="newGenre">
              <button class="edit-button" type="button" onclick="addGenre()">
                <i class="fa fa-add"></i> Add
              </button>
              <br>
              <ul class="list01">
                <li class="li03">
                  <div id="newGenreList" name="newGenreList">
                  </div>
                </li>
              </ul>

              <!-- Newly added genres will appear here -->
              <input type="hidden" name="updated_user" value="<%= username %>" />
              <%
                String newPublishYear = "";
                newPublishYear = (String) session.getAttribute("newPublishYear");
                String newLanguage = "";
                newLanguage = (String) session.getAttribute("newLanguage");
                String newISBN = "";
                newISBN =(String) session.getAttribute("newISBN");
              %>
              <input type="hidden" value="<%= newPublishYear %>" name="newPublishYear">
                <input type="hidden" value="<%= newLanguage %>" name="newLanguage">
                    <input type="hidden" value="<%= newISBN %>" name="newISBN">
              <br>
              <button type="submit" class="save-button">
                <i class="fa fa-save"></i> Save All
              </button>            </div>
            <div class="clear"></div>
          </div>
        </div>
      </div>
    </form>
    <a href="books"><button type="button" class="cancel-button">Cancel</button></a>
    </div>
    </div>




<jsp:include page="Footer.jsp"/>
