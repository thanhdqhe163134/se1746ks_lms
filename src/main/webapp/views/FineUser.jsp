<%@ page import="com.group5.se1746ks_lms.model.Fine" %>
<%@ page import="java.util.List" %>
<%@ page import="com.group5.se1746ks_lms.model.Account" %>
<%@ page import="java.text.NumberFormat" %>
<%@ page import="java.util.Locale" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
  session = request.getSession(false);
  Account loggedInUser = null;
  String role = null;
  String user_id = null;
  List<Fine> fineList = null;
  if (session != null) {
    loggedInUser = (Account) session.getAttribute("loggedInUser");
    role = loggedInUser.getRole();
  }
  boolean isUserLoggedIn = loggedInUser != null;
  if(request.getAttribute("user_id") != null){
    user_id = String.valueOf(request.getAttribute("user_id"));
  }

  if(user_id != null && isUserLoggedIn){
    fineList = (List<Fine>) request.getAttribute("fineList");
  } else if(user_id == null && isUserLoggedIn && role.equals("librarian")){
    fineList = (List<Fine>) request.getAttribute("allFineList");
  }
  int currentPage = (Integer) request.getAttribute("currentPage");
  int itemsPerPage = (Integer) request.getAttribute("itemsPerPage");
  int totalPages = (Integer) request.getAttribute("totalPages");
  String sortBy = request.getParameter("sortBy");
  String userId = request.getParameter("user_id");
  if(totalPages == 0) {
    totalPages = 1;
  }
    if(currentPage > totalPages){
        currentPage = totalPages;
    }
%>

<html>
<head>
  <title>MANAGE FINES</title>
  <link rel="stylesheet" href="css/Home.css">
  <link rel="stylesheet" href="css/LoansList.css"> <!-- or appropriate CSS file for fines -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
        integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"/>
</head>
<body>
<div class="wrapper">
  <jsp:include page="Header.jsp"/>

  <div class="top-title">
    <h3>FINES</h3>
  </div>



    <%  if(isUserLoggedIn ){ %>

  <div class="container">
    <div class="body-container">
      <div class="title-box">
        <h2 class="title">Fines</h2>
      </div>

      <div class="record-list">
        <div class="search-sort">
          <form class="sort" action="fine" method="get" id="sort">
            <input type="hidden" name="user_id" value="<%=userId%>">
            <span>Sort by date created:</span>
            <select name="sortBy" onchange="document.getElementById('sort').submit();">
              <option value="null" ${sortBy == null?"selected":""}>All</option>
              <option value="DESC" ${sortBy.equals("DESC")?"selected":""}>Newest</option>
              <option value="ASC" ${sortBy.equals("ASC")?"selected":""}>Oldest</option>
            </select>
          </form>
        </div>
        <table class="listing" border="1">
          <thead>
          <tr>
            <th>FINE ID</th>
            <th>LOAN ID</th>
            <th>FINE AMOUNT</th>
            <th>DESCRIPTION</th>
            <th>CREATED DATE</th>
            <th>CREATED USER</th>
            <th>PAID DATE</th>
            <th>UPDATED USER</th>
            <th>IS PAID</th>
          </tr>
          </thead>
          <tbody class="list">
          <% for (Fine fine : fineList) { %>
          <tr>
            <td><%= fine.getFine_id() %></td>
            <td><%= fine.getLoans_id() %></td>
            <%
              NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(new Locale("vi", "VN"));
              String formattedFineAmount = currencyFormatter.format(fine.getFine_amount());
            %>
            <td><%= formattedFineAmount %></td>
            <% if (fine.getDescription() == null){ %>
            <td></td>
            <% } else { %>
            <td><%= fine.getDescription() %></td>
            <% } %>
            <td><%= fine.getCreated_date() %></td>
            <td><%= fine.getCreated_user() %></td>
            <% if(fine.getPaid_date() == null){ %>
            <td></td>
            <% } else { %>
            <td><%= fine.getPaid_date() %></td>
            <% } %>
            <%if (fine.getUpdated_user() == null){ %>
            <td></td>
            <% } else { %>
            <td><%= fine.getUpdated_user() %></td>
            <% } %>
            <td><%=fine.getIs_paid()%></td>
          </tr>
          <% } %>

          </tbody>
        </table>
      </div>
    </div>
  </div>
    <div class="pagination-container">
      <div class="pagination-nav">
        <a href="fine?user_id=<%=userId%>&currentPage=<%= currentPage - 1 %>&itemsPerPage=<%= itemsPerPage %>&sortBy=<%= sortBy %>"
           class="<% if(currentPage == 1) { %>disabled<% } %>">Previous</a>

        <span class="pagination-info"> <%= currentPage %> / <%= totalPages %></span>

        <a href="fine?user_id=<%=userId%>&currentPage=<%= currentPage + 1 %>&itemsPerPage=<%= itemsPerPage %>&sortBy=<%= sortBy %>"
           class="<% if(currentPage >= totalPages) { %>disabled<% } %>">Next</a>

      </div>
      <div class="pagination-settings">
        <form action="fine" method="get" class="pagination-form" onsubmit="return validateForm();">
          <% if (sortBy != null && !sortBy.isEmpty()) { %>
          <input type="hidden" name="sortBy" value="<%= sortBy %>">
          <% } %>

          <label for="currentPage" style="margin-right: auto">Current page</label>
          <input type="number" id="currentPage" name="currentPage" style="margin-left: 10px"
                 value="<%= currentPage %>" min="1">
          <label for="itemsPerPage"></label>
          <select id="itemsPerPage" name="itemsPerPage">
            <option value="10" <%= itemsPerPage == 10 ? "selected" : "" %>>10</option>
            <option value="20" <%= itemsPerPage == 20 ? "selected" : "" %>>20</option>
            <option value="30" <%= itemsPerPage == 30 ? "selected" : "" %>>30</option>
          </select>
          <input type="hidden" name="user_id" value="<%= userId %>">
          <button type="submit" class="pagination-button">View</button>
        </form>
      </div>
    </div>
    <%}%>


</body>
<footer>
  <jsp:include page="Footer.jsp"/>
</footer>

</html>

<style>
  .btn-submit {
    padding: 8px 16px;
    border: 1px solid #ddd;
    border-radius: 4px;
    color: white;
    cursor: pointer;
    font-size: 14px;
    background: green;

  }
  .btn-submit:hover {
    background-color: #45a049;
  }

</style>