<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
  <title>Forgot Password</title>
  <link rel="stylesheet" href="css/Login.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
        integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"/>
  <script src="https://www.google.com/recaptcha/api.js?render=6LeIeOwoAAAAALhkhd6AGDWLh4mvIFlSFeAWNkPT"></script>
  <script>
    grecaptcha.ready(function() {
      grecaptcha.execute('6LeIeOwoAAAAALhkhd6AGDWLh4mvIFlSFeAWNkPT', {action: 'forgot_password'}).then(function(token) {
        document.getElementById('g-recaptcha-response').value = token;
      });
    });
  </script>
</head>
<body>
<div class="wrapper">
  <div class="login">
    <img class="logo" src="https://cdn.haitrieu.com/wp-content/uploads/2021/10/Logo-Dai-hoc-FPT.png">
    <h1 class="form-header">Forgot Password</h1>
    <form action="forgot-password" method="post" class="form-login">
      <div class="form-input">
        <input type="text" class="enter" placeholder="Username" name="username" required>
        <i class="fa-solid fa-user"></i>
      </div>
      <c:if test="${not empty requestScope.error}">
        <p class="error-mess"><b>${error}</b></p>
      </c:if>
      <c:if test="${requestScope.success == 'true'}">
        <p class="success-mess"><b>Password reset link has been sent to your email</b></p>
      </c:if>
      <input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response">
      <input type="submit" value="Send Reset Link" class="form-submit">
      <a href="login" class="back">
        Back to Login
      </a>
    </form>
  </div>
</div>
</body>
</html>
