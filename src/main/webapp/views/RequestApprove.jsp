<%@ page import="com.group5.se1746ks_lms.model.Request" %>
<%@ page import="com.group5.se1746ks_lms.model.Account" %>
<%
    session = request.getSession(false);
    Account loggedInUser = null;
    String role = null;
    String username = null;
    if (session != null) {
        loggedInUser = (Account) session.getAttribute("loggedInUser");
        role = loggedInUser.getRole();
        username = loggedInUser.getUsername();
    }
    boolean isUserLoggedIn = loggedInUser != null;
%>
<%
    Request req = (Request) request.getAttribute("request");
%>
<div class="form">
    <div class="form-content">
        <h3>Loan ID</h3>
        <input type="text" name="loan_id" required value="<%= req.getLoan_id() %>" readonly>
    </div>
    <div class="form-content">
        <h3><label for="new_due_date">New Due Date:</label></h3>
        <input type="date" id="new_due_date" name="new_due_date" value="<%=req.getRequest_date()%>" readonly>
    </div>

    <!-- Bắt đầu thẻ form chung để gửi dữ liệu -->
    <form action="approveRequest" method="post">
        <div class="form-content">
            <h3>Description</h3>
            <textarea name="description" rows="3"></textarea>
        </div>

        <!-- Nút "Approve" -->
        <div class="form-content button-group">
            <input type="hidden" name="is_approved" value="true">
            <input type="hidden" name="request_id" value="<%=req.getRequest_id()%>">
            <input type="hidden" name="username" value="<%=username%>">
            <button type="submit" class="btn-submit btn-approve"><i class="fa fa-check"></i> Approve</button>
            <button type="button" class="btn-submit btn-not-approve" onclick="submitFormWithDisapproval(this.form)">
                <i class="fa fa-times"></i> Not Approve
            </button>
        </div>
    </form>
</div>

<script>
    // Hàm JavaScript để thay đổi giá trị và gửi form
    function submitFormWithDisapproval(form) {
        // Thay đổi giá trị của input hidden
        form.is_approved.value = "false";
        // Gửi form
        form.submit();
    }
</script>

<style>
    .form-content {
        margin-bottom: 10px;
    }

    .button-group {
        display: flex;
        justify-content: start; /* Align buttons to the start of the container */
    }

    .button-form {
        margin-right: 10px; /* Space between buttons */
    }

    .btn-submit {
        padding: 8px 16px;
        border: none;
        color: white;
        cursor: pointer;
        font-size: 14px;
    }

    .btn-approve {
        background-color: #28a745; /* Green color */
    }

    .btn-not-approve {
        background-color: #dc3545; /* Red color */
    }

    /* Style cho icon */
    .btn-submit i {
        margin-right: 5px;
    }

</style>