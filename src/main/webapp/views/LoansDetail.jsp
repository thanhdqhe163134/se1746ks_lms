<%@ page import="com.group5.se1746ks_lms.model.Account" %>
<%@ page import="com.group5.se1746ks_lms.model.Loans" %><%--
  Created by IntelliJ IDEA.
  User: tranthanhhuyen
  Date: 03/11/2023
  Time: 00:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
  session = request.getSession(false);
  String role = null;
  String username = null;
  long user_id = 0L;
  String img = null;

  if (session != null) {
    Account loggedInUser = (Account) session.getAttribute("loggedInUser");
    if (loggedInUser != null) {
      role = loggedInUser.getRole(); // Lấy role từ Account
      username = loggedInUser.getUsername(); // Lấy username từ Account
      user_id = loggedInUser.getUser_id();
      img = loggedInUser.getImg();
    }
  }

  boolean isAdmin = "admin".equals(role); // Kiểm tra xem role có phải là admin không

%>
<% Loans loan = (Loans) request.getAttribute("l");
  String status = loan.getStatus();

%>
<html>
<head>
  <title>MANAGE LOANS</title>
  <link rel="stylesheet" href="css/Home.css">
  <link rel="stylesheet" href="css/AccountCreate.css">
<%--    <link rel="stylesheet" href="css/LoanDetail.css">--%>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
        integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"/>
</head>
<body>
<div class="wrapper">
  <jsp:include page="Header.jsp"/>
  <div id="modalContainer" class="modal">
    <div class="modal-content" style="display: contents">
      <div id="formContainer">
      </div>
    </div>
  </div>
  <div class="top-title">
    <h3>MANAGE LOANS</h3>
  </div>
  <div class="title-box">
    <h2 class="title">Loans Detail</h2>

    <% if(role.equals("user")){ %>
    <a href="loansUser?user_id=${l.getUser_id()}">
                <span>
                    <i class="fa-solid fa-angle-left"></i>
                    Back to list
                </span>
    </a>
    <% } else { %>
    <a href="loans">
                <span>
                    <i class="fa-solid fa-angle-left"></i>
                    Back to list
                </span>
    </a>

    <% if(status.equals("Borrowing") || status.equals("Lost")){ %>
    <button type="button" class="button-action button-return" onclick="loadReturnForm(${l.getLoans_id()})">
      <i class="fa fa-undo" aria-hidden="true"></i> Return
    </button>
    <% } %>


    <% } %>


  </div>

  <div class="form">

    <%if(role.equals("librarian")){%>
    <form id="statusForm" action="updateLoans" method="post">
      <div class="form-content">
        <h3>Status</h3>
        <input type="radio" onchange="submitForm()" name="status" value="Reserved" <%= "Reserved".equals(status) ? "checked" : "" %> style="width: 20px !important;">Reserved
        <input type="radio" onchange="submitForm()" name="status" value="Borrowing" <%= "Borrowing".equals(status) ? "checked" : "" %> style="width: 20px !important;">Borrowing
        <input type="radio" name="status" value="Returned" <%= "Returned".equals(status) ? "checked" : "" %> style="width: 20px !important;" readonly>Returned
        <input type="radio" name="status" value="Lost" <%= "Lost".equals(status) ? "checked" : "" %> style="width: 20px !important;" readonly>Lost
        <input type="hidden" name="loan_id" value="${l.getLoans_id()}">
        <input type="hidden" name="book_detail_id" value="${l.getBook_detail_id()}">
        <input type="hidden" name="username" value="<%=username%>">
      </div>
    </form>
    <script>
      function submitForm() {
        document.getElementById('statusForm').submit();
      }
    </script>
    <% } else{%>
    <div class="form-content">
      <h3>Status</h3>
      <input type="text" name="status" class="no-change" value="${l.getStatus()}" readonly>
    </div>
    <% } %>


    <br>
    <div class="form-content">
      <h3>Record ID</h3>
      <input type="text" name="id" class="no-change" value="${l.getLoans_id()}" readonly>
    </div>
    <div class="form-content">
      <h3>Book Code</h3>
      <input type="text" name="book_code" class="no-change" value="${l.getBook_detail_id()}" readonly>
    </div>
    <div class="form-content">
      <h3>User ID</h3>
      <input type="text" name="user_id" class="no-change" value="${l.getUser_id()}" readonly>
    </div>


    <div class="form-content">
      <h3>Expiry Date</h3>
      <input type="datetime-local" name="expiry" class="no-change" value="${l.getExpried_date()}" readonly>
    </div>
    <div class="form-content">
      <h3>Borrow Date</h3>
      <input type="datetime-local" name="borrow" class="no-change" value="${l.getBorrow_date()}" readonly>
    </div>
    <div class="form-content">
      <h3>Due Date</h3>
      <input type="date" name="due" class="no-change" value="${l.getDue_date()}" readonly>
    </div>
    <div class="form-content">
      <h3>New Due Date</h3>
      <input type="date" name="due" class="no-change" value="${req.getRequest_date()}" readonly>
    </div>
    <div class="form-content">
      <h3>Return Date</h3>
      <input type="datetime-local" name="return" class="no-change" value="${l.getReturn_date()}" readonly>
    </div>
    <div class="form-content">
      <h3>Create date</h3>
      <input type="datetime-local" name="create_date" class="no-change" value="${l.getCreate_date()}" readonly>
    </div>
    <!-- Modal Container -->
    <div id="returnModal" class="modal">
      <!-- Modal content -->
      <div id="modalContent" class="modal-content">

        <!-- The content will be loaded dynamically using JavaScript -->
      </div>
    </div>

  </div>

  <jsp:include page="Footer.jsp"/>
</div>
</body>
</html>
<script>
  function loadRequestForm(loan_id) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById('formContainer').innerHTML = this.responseText;
        showModal();
      }
    };
    xhttp.open("GET", "createRequest?loan_id=" + loan_id, true);
    xhttp.send();
  }

  function showModal() {
    var modal = document.getElementById('modalContainer');
    modal.style.display = 'block';
  }

  function closeModal() {
    var modal = document.getElementById('modalContainer');
    modal.style.display = 'none';
  }

  // Close modal if clicked outside of it
  window.onclick = function(event) {
    var modal = document.getElementById('modalContainer');
    if (event.target == modal) {
      closeModal();
    }
  }


</script>
</html>

<script>
  function handleExtensionOptionChange() {
    var addDaysInput = document.getElementById('add_days_input');
    var chooseDateInput = document.getElementById('choose_date_input');

    if (document.getElementById('add_days_option').checked) {
      addDaysInput.style.display = 'block';
      chooseDateInput.style.display = 'none';
    } else if (document.getElementById('choose_date_option').checked) {
      chooseDateInput.style.display = 'block';
      addDaysInput.style.display = 'none';
    }
  }

  window.onload = handleExtensionOptionChange;


</script>
<script>
  function loadReturnForm(loanId) {
    // Đây là giả định rằng bạn đã tạo một endpoint `returnForm` trả về form HTML
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        // Đặt nội dung của modal là form trả sách
        document.getElementById('formContainer').innerHTML = this.responseText;
        // Hiển thị modal
        showModal();
      }
    };
    xhttp.open("GET", "returnForm?loan_id=" + loanId, true);
    xhttp.send();
  }

  function showModal() {
    var modal = document.getElementById('modalContainer'); // Đảm bảo ID này phải khớp với ID của modal trong HTML
    modal.style.display = 'block';
  }

  function closeModal() {
    var modal = document.getElementById('modalContainer'); // Đảm bảo ID này phải khớp với ID của modal trong HTML
    modal.style.display = 'none';
  }

</script>
<style>

  /* Modal (background) */
  .modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
  }

  /* Modal Content */
  .modal-content {
    background-color: #fefefe;
    margin: 15% auto; /* 15% from the top and centered */
    padding: 20px;
    border: 1px solid #888;
    width: 70%; /* Could be more or less, depending on screen size */
  }

  /* The Close Button */
  .close {
    color: #aaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
  }

  .close:hover,
  .close:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
  }
  .button-action {
    background-color: #4CAF50; /* Green background */
    border: none;
    color: white;
    padding: 10px 20px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
    border-radius: 5px;
    transition: background-color 0.3s ease;
  }

  .button-extend {
    background-color: #008CBA; /* Blue background */
  }

  .button-return {
    background-color: #ff6300; /* Red background */
  }

  .button-action i {
    margin-right: 5px;
  }

  .button-action:hover {
    opacity: 0.8;
  }

</style>

