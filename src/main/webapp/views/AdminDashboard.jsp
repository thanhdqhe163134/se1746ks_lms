<%--
  Created by IntelliJ IDEA.
  User: tranthanhhuyen
  Date: 04/10/2023
  Time: 16:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Admin Dashboard</title>
    <link rel="stylesheet" href="css/Dashboard.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
          integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
</head>
<body>
<div class="wrapper">
    <jsp:include page="Header.jsp"/>
    <div class="top-title">
        <h3>ADMIN DASHBOARD</h3>
    </div>

    <div class="card-box">
        <a href="books">
            <div class="card">
                <i class="fa-solid fa-book"></i>
                <div class="content"><b>Manage Books</b></div>
            </div>
        </a>
        <a href="accounts">
            <div class="card">
                <i class="fa-solid fa-users"></i>
                <div class="content"><b>Manage Accounts</b></div>
            </div>
        </a>

        <a href="statistic">
            <div class="card">
                <i class="fa-solid fa-chart-bar"></i>
                <div class="content"><b>Statistic</b></div>
            </div>
        </a>
    </div>

    <jsp:include page="Footer.jsp"/>
</div>
</body>
</html>
