<%@ page import="com.group5.se1746ks_lms.model.Account" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
  session = request.getSession(false);
  Account loggedInUser = null;
  String role = null;
  if (session != null) {
    loggedInUser = (Account) session.getAttribute("loggedInUser");
     role = loggedInUser.getRole();
  }
  boolean isUserLoggedIn = loggedInUser != null;
%>
  <link rel="stylesheet" href="css/Home.css">
  <link rel="stylesheet" href="css/AccountCreate.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
        integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"/>
  <form action="createRequest" method="post">
      <br>
      <div class="form-content">
        <h3>Loan ID</h3>
        <input type="text" name="loan_id" required value="<%= request.getAttribute("loan_id") %>" readonly style="width: 10% !important;">
      </div>
      <div class="form-content">
        <h3>Extension Options</h3>
        <div>
          <input type="radio" id="add_days_option" name="extension_option" value="add_days" onclick="handleExtensionOptionChange()" required>
          <label for="add_days_option">Add More Days</label>
        </div>
        <div>
          <input type="radio" id="choose_date_option" name="extension_option" value="choose_date" onclick="handleExtensionOptionChange()" required>
          <label for="choose_date_option">Choose New Due Date</label>
        </div>
        <br>
        <div id="add_days_input" style="display:none;">
          <label for="extension_days">Number of Days:</label>
          <input type="number" id="extension_days" name="extension_days"max="15" min="1" placeholder="Enter number of days">
        </div>
        <div id="choose_date_input" style="display:none;">
          <label for="new_due_date">New Due Date:</label>
          <input type="date" id="new_due_date" name="new_due_date">
        </div>
      </div>
<br>
      <div class="form-content">
        <input type="hidden" name="user_id" value="<%= loggedInUser.getUser_id() %>">
        <input type="hidden" name="username" value="<%= loggedInUser.getUsername() %>">
        <button type="submit" class="btn-submit1">Submit Request</button>
      </div>
  </form>


<script>
    function handleExtensionOptionChange() {
      var addDaysInput = document.getElementById('add_days_input');
      var chooseDateInput = document.getElementById('choose_date_input');

      if (document.getElementById('add_days_option').checked) {
        addDaysInput.style.display = 'block';
        chooseDateInput.style.display = 'none';
      } else if (document.getElementById('choose_date_option').checked) {
        chooseDateInput.style.display = 'block';
        addDaysInput.style.display = 'none';
      }
    }

    window.onload = handleExtensionOptionChange;

</script>
<style>
  .btn-submit1 {
    padding: 8px 16px;
    background-color: #4CAF50; /* Màu xanh lá cây */
    color: white;
    border: none;
    border-radius: 4px;
    cursor: pointer;
    font-size: 14px;
    font-weight: bold;
    text-transform: uppercase;
    outline: none;
    animation: shake 0.82s cubic-bezier(.36,.07,.19,.97) both infinite;
    transform: translate3d(0, 0, 0);
    backface-visibility: hidden;
    perspective: 1000px;
  }



  .btn-submit1:hover {
    background-color: #3e8e41; /* Màu sáng hơn khi di chuột qua */
  }

  .btn-submit1:active {
    background-color: #357a38; /* Màu tối hơn khi nhấn */
  }

</style>
