<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.group5.se1746ks_lms.model.*" %>
<%@ page import="com.group5.se1746ks_lms.dao.BooksDAO" %>
<%@ page import="java.util.*" %>
<%@ page import="java.util.stream.Collectors" %>


<%
    session = request.getSession(false);
    String role = "";
    String username = "";
    boolean isLoggedIn = false;
    Long user_id = null;

    if (session != null) {
        Account loggedInUser = (Account) session.getAttribute("loggedInUser");
        if (loggedInUser != null) {
            role = loggedInUser.getRole(); // Lấy role từ Account
            username = loggedInUser.getUsername(); // Lấy username từ Account
            isLoggedIn = true;
            user_id = loggedInUser.getUser_id();
        }
    }
    boolean isAdmin = "admin".equals(role); // Kiểm tra xem role có phải là admin không
%>

<head>
    <title>Return Book</title>
</head>
<script src="js/BookAdd.js" defer></script>
<script src="js/BookDetail.js" defer></script>
<link rel="stylesheet" href="css/BookDetail.css">
<link rel="stylesheet" href="css/BookAdd.css">
<link rel="stylesheet" href="css/Search.css">
<link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
/>

<jsp:include page="Header.jsp"/>


<form class="search-block" method="get" action="returned">
    <input type="text" id="search-bar" name="book_detail_id" placeholder="Input Book Detail ID"/>
<button class="action-button" type="submit">Submit</button>
</form>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<jsp:include page="Footer.jsp"/>
