<%--
  Created by IntelliJ IDEA.
  User: faced
  Date: 11/4/2023
  Time: 10:53 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">

<style>
  body {
    font-family: 'Arial', sans-serif;
  }

  .notification {
    position: fixed;
    top: 60px;
    right: 50px;
    z-index: 1000;
    width: 1000px;  /* Increased width */
    margin: 10px 0;
    padding: 100px;  /* Increased padding */
    border-radius: 10px;
    box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);
    transition: transform 0.3s ease, opacity 0.3s ease;
    transform-origin: top right;
    background-color: rgba(255, 255, 255, 0.9);  /* Slightly transparent background */
    font-size: 30px;
  }

  .notification .closebtn {
    margin-left: 20px;
    color: white;
    font-weight: bold;
    float: right;
    font-size: 32px;  /* Increased font size */
    line-height: 24px;
    cursor: pointer;
  }

  .notification .closebtn:hover {
    color: #ddd;
  }

  .error {
    background-color: rgba(244, 67, 54, 0.9);
    color: white;
    border-color: #f44336;
  }

  .success {
    background-color: rgba(76, 175, 80, 0.9);
    color: white;
    border-color: #4CAF50;
  }
  .closebtn {
    display: none;
    /* ... other styles ... */
  }
</style>

<script>
  window.onload = function() {
    var urlParams = new URLSearchParams(window.location.search);

    var error = urlParams.get("error");
    if (error) {
      document.getElementById("errorDiv").style.display = "block";
      document.getElementById("errorMsg").innerText = error;
      document.getElementById("errorCloseBtn").style.display = "inline";
    }

    var success = urlParams.get("success");
    if (success) {
      document.getElementById("successDiv").style.display = "block";
      document.getElementById("successMsg").innerText = success;
      document.getElementById("successCloseBtn").style.display = "inline";
    }
  };
</script>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="styles.css">
</head>
<body>
<div class="notification error" id="errorDiv">
  <span class="closebtn" id="errorCloseBtn" onclick="this.parentElement.style.display='none';">&times;</span>
  <p id="errorMsg"></p>
</div>
<div class="notification success" id="successDiv">
  <span class="closebtn" id="successCloseBtn" onclick="this.parentElement.style.display='none';">&times;</span>
  <p id="successMsg"></p>
</div>

<script src="script.js"></script>
</body>
</html>
