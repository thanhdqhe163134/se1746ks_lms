<%--
  Created by IntelliJ IDEA.
  User: tranthanhhuyen
  Date: 06/10/2023
  Time: 01:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>MANAGE ACCOUNTS</title>
    <link rel="stylesheet" href="css/Home.css">
    <link rel="stylesheet" href="css/AccountCreate.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
          integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <script src="js/ValidateData.js"></script>
</head>
<body>
<div class="wrapper">
    <jsp:include page="Header.jsp"/>
    <div class="top-title">
        <h3>MANAGE ACCOUNTS</h3>
    </div>
    <div class="title-box">
        <h2 class="title">Create new accounts</h2>
        <a href="accounts">
                <span>
                    <i class="fa-solid fa-angle-left"></i>
                    Back to list
                </span>
        </a>
    </div>
    <form class="create-form" action="createAcc" method="post">
        <br>
        <div class="form-content">
            <h3>Student ID</h3>
            <input type="text" name="student_id" value="${a.getStudent_id()}">
        </div>
        <div class="form-content">
            <h3>Fullname</h3>
            <input type="text" name="fullname" value="${a.getFull_name()}" required>
        </div>
        <div class="form-content">
            <h3>Username</h3>
            <input type="text" name="username" value="${a.getUsername()}" required>
            <p>${err}</p>
        </div>
        <div class="form-content">
            <h3>Password</h3>
            <input type="text" name="password" value="${a.getPassword()}" required>
        </div>
        <div class="form-content">
            <h3>Date of birth</h3>
            <input type="date" name="dob" value="${a.getDob()}" required>
        </div>
        <div class="form-content">
            <h3>Email</h3>
            <input type="text" name="email" id="email" oninput="validateEmail()" value="${a.getEmail()}" required>
            <p id="mess"></p>
        </div>
        <div class="form-content">
            <h3>Phone Number</h3>
            <input type="text" name="phone" value="${a.getPhone_number()}" required>
        </div>
        <div class="form-content">
            <h3>Address</h3>
            <input type="text" name="address" value="${a.getAddress()}" required>
        </div>
        <div class="form-content role">
            <h3>Role</h3>
            <input type="radio" name="role" value="user">User
            <input type="radio" name="role" value="librarian">Librarian
            <input type="radio" name="role" value="admin">Admin
        </div>
        <div class="form-content">
            <h3>Create date</h3>
            <input type="date" name="create_date" id="today" value="${a.getCreated_date()}" required>
        </div>
        <div class="form-content submit">
            <input type="submit" name="submit" id="submit" value="SUBMIT">
        </div>
    </form>
    <jsp:include page="Footer.jsp"/>
</div>
</body>
<script>
    var today = new Date();
    document.getElementById("today").value = today.toLocaleDateString("en-CA");
</script>
</html>
