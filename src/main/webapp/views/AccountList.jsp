<%--
  Created by IntelliJ IDEA.
  User: tranthanhhuyen
  Date: 06/10/2023
  Time: 00:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>MANAGE ACCOUNTS</title>
    <link rel="stylesheet" href="css/AccountList.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
          integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <script src="js/AccountList.js" defer></script>
</head>
<body>
<div class="wrapper">
    <jsp:include page="Header.jsp"/>
    <div class="top-title">
        <h3>MANAGE ACCOUNTS</h3>
    </div>
    <div class="title-box">
        <h2 class="title">User Accounts Listing</h2>
        <a href="createAcc">
                <span>
                    <i class="fa-solid fa-user-plus"></i>
                    Create new account
                </span>
        </a>
    </div>
    <div class="user-list">
        <div class="search-sort">
            <div class="search">
                <i class="fa-solid fa-magnifying-glass"></i>
                <input type="text" placeholder="Search by Username" id="search" oninput="searchAccounts()">
            </div>
            <form class="sort" action="accounts" method="get" id="sort">
                <span>Sort by date created:</span>
                <select name="sortBy" onchange="document.getElementById('sort').submit();">
                    <option ${sortBy == null?"selected":""}>None</option>
                    <option value="newest" ${sortBy.equals("newest")?"selected":""}>Newest</option>
                    <option value="oldest" ${sortBy.equals("oldest")?"selected":""}>Oldest</option>
                </select>
            </form>
        </div>
        <table class="listing" border="1">
            <thead>
            <tr>
                <th>USER ID</th>
                <th>CODE</th>
                <th>USERNAME</th>
                <th>FULLNAME</th>
                <th>EMAIL</th>
                <th>PHONE NUMBER</th>
                <th>ROLE</th>
                <th>CREATE DATE</th>
                <th>ACTION</th>
            </tr>
            </thead>
            <tbody class="list">
            <c:forEach items="${acc}" var="a">
                <tr>
                    <td>${a.getUser_id()}</td>
                    <td>${a.getStudent_id()}</td>
                    <td>${a.getUsername()}</td>
                    <td>${a.getFull_name()}</td>
                    <td>${a.getEmail()}</td>
                    <td>${a.getPhone_number()}</td>
                    <td>${a.getRole()}</td>
                    <td>${a.getCreated_date()}</td>
                    <td class="action">
                        <a href="accDetail?id=${a.getUser_id()}" class="view">
                                    <span>
                                        <i class="fa-solid fa-eye"></i>
                                        View
                                    </span>
                        </a>
                        <a href="deleteAcc?id=${a.getUser_id()}" class="delete">
                                    <span>
                                        <i class="fa-solid fa-trash-can"></i>
                                        Delete
                                    </span>
                        </a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <div class="pagination-container">
            <div class="pagination-nav">
                <a href="accounts?currentPage=${currentPage - 1}&itemsPerPage=${itemsPerPage}&sortBy=${sortBy}"
                   class="${currentPage == 1 ? "disabled" : ""}">Previous</a>
                <span class="pagination-info"> ${currentPage} / ${totalPages} </span>
                <a href="accounts?currentPage=${currentPage + 1}&itemsPerPage=${itemsPerPage}&sortBy=${sortBy}"
                   class="${currentPage >= totalPages ? "disabled" : ""}">Next</a>
            </div>
            <div class="pagination-settings">
                <form action="accounts" method="get" class="pagination-form" onsubmit="return validateForm();">
                    <c:if test="${sortBy != null && !sortBy.isEmpty()}">
                        <input type="hidden" name="sortBy" value="${sortBy}">
                    </c:if>
                    <label for="currentPage" style="margin-right: auto">Current page</label>
                    <input type="number" id="currentPage" name="currentPage" style="margin-left: 10px"
                           value="${currentPage}" min="1">
                    <label for="itemsPerPage"></label>
                    <select id="itemsPerPage" name="itemsPerPage" style="padding: 3px">
                        <option value="1" ${itemsPerPage == 1 ? "selected" : ""}>1</option>
                        <option value="5" ${itemsPerPage == 5 ? "selected" : ""}>5</option>
                        <option value="10" ${itemsPerPage == 10 ? "selected" : ""}>10</option>
                    </select>
                    <button type="submit" class="pagination-button">View</button>
                </form>
            </div>
        </div>
    </div>
    <jsp:include page="Footer.jsp"/>
</div>
</body>
<script>
    function validateForm() {
        var currentPage = document.getElementById("currentPage").value;
        var totalPages = ${totalPages};

        if (currentPage > totalPages) {
            document.getElementById("currentPage").value = totalPages;
        }

        return true;
    }
</script>
</html>
