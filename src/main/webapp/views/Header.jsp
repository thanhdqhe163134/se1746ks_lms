<%@ page import="com.group5.se1746ks_lms.model.Account" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
    session = request.getSession(false);
    String role = null;
    String username = null;
    long user_id = 0L;
    String img = null;

    if (session != null) {
        Account loggedInUser = (Account) session.getAttribute("loggedInUser");
        if (loggedInUser != null) {
            role = loggedInUser.getRole(); // Lấy role từ Account
            username = loggedInUser.getUsername(); // Lấy username từ Account
            user_id = loggedInUser.getUser_id();
            img = loggedInUser.getImg();
        }
    }

    boolean isAdmin = "admin".equals(role); // Kiểm tra xem role có phải là admin không

%>

<%
    String callingPageURI = (String) request.getAttribute("jakarta.servlet.forward.request_uri");
    boolean isHome = false;
    boolean isBooks = false;
    if (callingPageURI != null) {
        isHome = callingPageURI.endsWith("home") || callingPageURI.endsWith("/library/");
        isBooks = callingPageURI.contains("books");
    }
%>


<link rel="stylesheet" href="css/Home.css">
<link rel="stylesheet" href="css/Search.css">
<script src="js/Header.js" defer></script>


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
      integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
      crossorigin="anonymous" referrerpolicy="no-referrer"/>


<div class="header">
    <div class="logo">
        <a href="home">
            <img
                    src="https://cdn.haitrieu.com/wp-content/uploads/2021/10/Logo-Dai-hoc-FPT.png"
            />
        </a>

    </div>
    <% if (!isHome && !isBooks) { %>
    <form class="search-block2">
        <input type="text" id="search-bar2" placeholder="Search by Title"
               oninput="searchBooks()"/>
        <div class="book-container"></div>
        <div class="search-results2"></div>
    </form>
    <% } %>

    <div class="navbar">

        <ul class="menu">

            <li>
                <a href="home">
                    <span>Home</span>
                </a>
            </li>
            <li>
                <a href="books">
                    <span>Books</span>
                </a>
            </li>
            <li>
                <a href="home">
                    <span>Services</span>
                </a>
            </li>
            <li>
                <a href="home">
                    <span>About Us</span>
                </a>
            </li>
        </ul>
        <c:if test="${sessionScope.loggedInUser != null}">
            <div class="dropdown-items">
                <!-- <p style="font-weight: 600; font-size: 20px;">Admin</p> -->
                <div style="width: 12px"></div>
                <% if (img == null || img.equals("")) { %> <%
                    img = "img/default-avatar.png";
                } %>
                <img
                        src="<%=img%>"
                        class="avatar-user"
                />
                <ul class="menu-items">
                    <% if (isAdmin) { %>
                    <label for="dash">
                    <li id="dash" ><a href="dashboard">Dash board</a></li>
                    </label>
                    <% } else if (role.equals("librarian")) {%>
                    <label>
                        <li><a href="dashboard">Dash board</a></li>
                    </label>

                    <% } else {%>
                    <label>
                    <li><a href="myProfile?id=<%=user_id%>">Profile</a></li>
                    </label>
                    <%} %>
                    <% if (role.equals("user")) { %>
                    <label>
                        <li><a href="loansUser?user_id=<%=user_id%>">Borrowed Book</a></li>
                    </label>
                    <label>
                    <li><a href="requestList?user_id=<%=user_id%>">Request </a></li>
                    </label>
                    <label>
                        <li><a href="fine?user_id=<%=user_id%>">Fine </a></li>
                    </label>
                    <% } %>
                    <label>
                    <li><a href="logout">Logout</a></li>
                    </label>
                </ul>
            </div>
        </c:if>
         <c:if test="${sessionScope.loggedInUser == null}">
             <label>
            <a href="login" style="text-decoration: none">
                <span style="padding: 16px 36px; background: orange; color: white; border-radius: 8px">Login</span>
            </a>
             </label>
        </c:if>
    </div>
</div>