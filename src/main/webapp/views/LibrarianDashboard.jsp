<%--
  Created by IntelliJ IDEA.
  User: tranthanhhuyen
  Date: 20/10/2023
  Time: 02:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Librarian Dashboard</title>
    <link rel="stylesheet" href="css/Dashboard.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
          integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <style>
        .card {
            padding: 0;
        }
        .card i {
            font-size: 70px;
        }
    </style>
</head>
<body>
<div class="wrapper">
    <jsp:include page="Header.jsp"/>
    <div class="top-title">
        <h3>LIBRARIAN DASHBOARD</h3>
    </div>

    <div class="card-box" style="padding: 70px 0">
        <a href="loans">
            <div class="card">
                <i class="fa-solid fa-money-check"></i>
                <div class="content"><b>Manage Book Loans</b></div>
            </div>
        </a>

        <a href="returned">
            <div class="card">
                <i class="fa-solid fa-arrow-rotate-left"></i>
                <div class="content"><b>Manage Return</b></div>
            </div>
        </a>
        <a href="fine">
            <div class="card">
                <i class="fa-solid fa-money-bills"></i>
                <div class="content"><b>Manage Fines</b></div>
            </div>
        </a>
    </div>

    <jsp:include page="Footer.jsp"/>
</div>
</body>
</html>
