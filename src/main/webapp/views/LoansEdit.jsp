<%--
  Created by IntelliJ IDEA.
  User: tranthanhhuyen
  Date: 07/11/2023
  Time: 10:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>MANAGE LOANS</title>
    <link rel="stylesheet" href="css/Home.css">
    <link rel="stylesheet" href="css/AccountCreate.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
          integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
</head>
<body>
<<div class="wrapper">
    <jsp:include page="Header.jsp"/>
    <div class="top-title">
        <h3>MANAGE LOANS</h3>
    </div>
    <div class="title-box">
        <h2 class="title">Edit Loans</h2>
        <a href="loansDetail?loans_id=${l.getLoans_id()}">
                <span>
                    <i class="fa-solid fa-angle-left"></i>
                    Back
                </span>
        </a>
    </div>

    <form class="form" action="updateLoans" method="post">
        <br>
        <div class="form-content">
            <h3>Record ID</h3>
            <input type="text" name="id" class="no-change" value="${l.getLoans_id()}" readonly>
        </div>
        <div class="form-content">
            <h3>Book Code</h3>
            <input type="text" name="book_code" class="no-change" value="${l.getBook_detail_id()}" readonly>
        </div>
        <div class="form-content">
            <h3>User ID</h3>
            <input type="text" name="user_id" class="no-change" value="${l.getUser_id()}" readonly>
        </div>
        <div class="form-content">
            <h3>Status</h3>
            <input type="radio" name="status" value="Reserved" style="width: 20px !important;">Reserved
            <input type="radio" name="status" value="Expired" style="width: 20px !important;">Expired
            <input type="radio" name="status" value="Borrowing" style="width: 20px !important;">Borrowing
            <input type="radio" name="status" value="Returned" style="width: 20px !important;">Returned
            <input type="radio" name="status" value="Overdue" style="width: 20px !important;">Overdue
        </div>
        <div class="form-content">
            <h3>Expiry Date</h3>
            <input type="date" name="expiry"  class="no-change"  value="${l.getExpried_date()}">
        </div>
        <div class="form-content">
            <h3>Borrow Date</h3>
            <input type="date" name="borrow"  value="${l.getBorrow_date()}">
        </div>
        <div class="form-content">
            <h3>Due Date</h3>
            <input type="date" name="due" class="no-change"  value="${l.getDue_date()}">
        </div>
        <div class="form-content">
            <h3>Return Date</h3>
            <input type="date" name="return" value="${l.getReturn_date()}">
        </div>
        <div class="form-content">
            <h3>Create date</h3>
            <input type="date" name="create_date" class="no-change" value="${l.getCreate_date()}" readonly>
        </div>
        <input type="submit" name="submit" class="sub-edit" value="SAVE">
    </form>
    <jsp:include page="Footer.jsp"/>
</div>
</body>
</html>
