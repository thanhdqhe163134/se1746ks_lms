<%@ page import="com.group5.se1746ks_lms.model.Loans" %>
<%@ page import="java.util.List" %>
<%@ page import="com.group5.se1746ks_lms.model.Request" %>
<%@ page import="java.util.Date" %><%--
  Created by IntelliJ IDEA.
  User: tranthanhhuyen
  Date: 23/10/2023
  Time: 17:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>MANAGE BOOK LOANS</title>
    <link rel="stylesheet" href="css/Home.css">
    <link rel="stylesheet" href="css/LoansList.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
          integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
</head>
<body>
<div class="wrapper">
    <jsp:include page="Header.jsp"/>
    <div class="top-title">
        <h3>MANAGE BOOK LOANS</h3>
    </div>
    <div class="container">
        <div class="side-bar">
            <a class="side-box" href="myProfile?id=${sessionScope.loggedInUser.getUser_id()}">
                <p>My Profile</p>
            </a>
            <a class="side-box" href="books">
                <p>Manage Books</p>
            </a>
            <a class="side-box" href="loans">
                <p>Manage Book Loans</p>
            </a>
            <a class="side-box" HREF="fine">
                <p>Manage Fines</p>
            </a>
        </div>
        <div class="body-container">
            <div class="title-box">
                <h2 class="title">List of Book Loans Records</h2>
                <a href="createLoans">
                        <span>
                            <i class="fa-solid fa-notes-medical"></i>
                            Create new record
                        </span>
                </a>
            </div>
            <div class="record-list">
                <div class="search-sort">
                    <div class="search">
                        <span>Search by:</span>
                        <select name="searchBy" id="searchBy">
                            <option value="student_id" ${searchBy.equals("student_id")?"selected":""}>Student ID</option>
                            <option value="bookCode" ${searchBy.equals("bookCode")?"selected":""}>Book Code</option>
                            <option value="bookTitle" ${searchBy.equals("bookTitle")?"selected":""}>Title</option>
                            <option value="recordId" ${searchBy.equals("recordId")?"selected":""}>Record ID</option>
                        </select>
                        <i class="fa-solid fa-magnifying-glass"></i>
                        <input type="text" placeholder="Search" id="search" oninput="searchLoans()">
                    </div>
                    <form class="sort" action="loans" method="get" id="sort">
                        <span>Sort by date created:</span>
                        <select name="sortBy" onchange="document.getElementById('sort').submit();">
                            <option value="all" ${sortBy == null?"selected":""}>All</option>
                            <option value="newest" ${sortBy.equals("newest")?"selected":""}>Newest</option>
                            <option value="oldest" ${sortBy.equals("oldest")?"selected":""}>Oldest</option>
                        </select>
                    </form>
                </div>
                <table class="listing" border="1">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>BOOK CODE</th>
                        <th>COVER</th>
                        <th>TITLE</th>
                        <th>STUDENT ID</th>
                        <th>CREATED DATE</th>
                        <th>STATUS</th>
                        <th>DUE DATE</th>
                        <th>NEW DUE DATE</th>
                        <th>ACTION</th>
                    </tr>
                    </thead>
                    <tbody class="list">
                    <%
                        List<Loans> loansList = (List<Loans>) request.getAttribute("loans");
                        for (Loans l : loansList) {
                    %>
                    <tr>
                        <td><%= l.getLoans_id() %></td>
                        <td><%= l.getBook_detail_id() %></td>
                        <td><img src="<%= l.getImg() %>" width="120" height="150"></td>
                        <td><a href="book?book_id=<%= l.getBook_id() %>"><%= l.getTitle() %></a></td>
                       <% if(l.getStudent_id() == null){ %><td></td>
                        <%}else{%>
                        <td><%= l.getStudent_id() %></td>
                        <%}%>
                        <td><%=l.getCreate_date()%></td>
                        <td style="<%
    String color = "";
    if ("Reserved".equals(l.getStatus()) && l.getExpried_date().before(new Date())) {
        color = "red";
    } else if ("Reserved".equals(l.getStatus())) {
        color = "#b8b800";
    } else if ("Borrowing".equals(l.getStatus()) && (l.getDue_date().before(new Date()) && (l.getNew_due_date() == null || l.getNew_due_date().before(new Date())))) {
        color = "red";
    } else if ("Borrowing".equals(l.getStatus())) {
        color = "#b8b800";
    } else if ("Returned".equals(l.getStatus())) {
        color = "green";
    } else if("Overdue".equals(l.getStatus()) && l.getNew_due_date() == null || "Expired".equals(l.getStatus()) || "Lost".equals(l.getStatus())){
        color = "red";
    }
%>color: <%=color%>;"><%= l.getStatus() %></td>

                        <td><%= l.getDue_date() %></td>
                        <%if(l.getNew_due_date() == null && l.getIs_request() && !l.getRequest_status().equalsIgnoreCase("Rejected")){%>
                        <td>    <button class="btn-submit1" type="button" onclick="loadRequestForm(<%=l.getRequest_id()%>)">New Request</button>
                        </td>
                        <%}else if(l.getNew_due_date() != null){%>
                        <td><%= l.getNew_due_date() %></td>
                        <%} else {%>
                        <td></td>
                        <%}%>

                        <td class="action" style="width: 100px;">
                            <a href="loansDetail?loans_id=<%= l.getLoans_id() %>" class="view">
                <span>
                    <i class="fa-solid fa-eye"></i>
                    View
                </span>
                            </a>
                        </td>
                    </tr>
                    <%
                        }
                    %>

                    </tbody>
                </table>
                <div class="pagination-container">
                    <div class="pagination-nav">
                        <a href="loans?currentPage=${currentPage - 1}&itemsPerPage=${itemsPerPage}&sortBy=${sortBy}"
                           class="${currentPage == 1 ? "disabled" : ""}">Previous</a>
                        <span class="pagination-info"> ${currentPage} / ${totalPages} </span>
                        <a href="loans?currentPage=${currentPage + 1}&itemsPerPage=${itemsPerPage}&sortBy=${sortBy}"
                           class="${currentPage >= totalPages ? "disabled" : ""}">Next</a>
                    </div>
                    <div class="pagination-settings">
                        <form action="loans" method="get" class="pagination-form" onsubmit="return validateForm();">
                            <c:if test="${sortBy != null && !sortBy.isEmpty()}">
                                <input type="hidden" name="sortBy" value="${sortBy}">
                            </c:if>
                            <label for="currentPage" style="margin-right: auto">Current page</label>
                            <input type="number" id="currentPage" name="currentPage" style="margin-left: 10px"
                                   value="${currentPage}" min="1">
                            <label for="itemsPerPage"></label>
                            <select id="itemsPerPage" name="itemsPerPage" style="padding: 3px">
                                <option value="5" ${itemsPerPage == 5 ? "selected" : ""}>5</option>
                                <option value="10" ${itemsPerPage == 10 ? "selected" : ""}>10</option>
                                <option value="20" ${itemsPerPage == 20 ? "selected" : ""}>20</option>
                            </select>
                            <button type="submit" class="pagination-button">View</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modalContainer" class="modal">
        <div class="modal-content">
            <div id="formContainer">
                <!-- Form sẽ được tải vào đây -->
            </div>
        </div>
    </div>
    <jsp:include page="Footer.jsp"/>
</div>
</body>
<script>
    function searchLoans() {
        let by = document.getElementById('searchBy').value;
        let query = document.getElementById('search').value;
        let xhr = new XMLHttpRequest();
        xhr.open('GET', 'searchLoans?by=' + by + '&query=' + query, true);
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                document.querySelector('.list').innerHTML = xhr.responseText;
            }
        };
        xhr.send();
    }

    function validateForm() {
        var currentPage = document.getElementById("currentPage").value;
        var totalPages = ${totalPages};

        if (currentPage > totalPages) {
            document.getElementById("currentPage").value = totalPages;
        }

        return true;
    }
</script>
<script>
    function loadRequestForm(requestId) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('formContainer').innerHTML = this.responseText;
                showModal();
            }
        };
        xhttp.open("GET", "approveRequest?request_id=" + requestId, true);
        xhttp.send();
    }

    function showModal() {
        var modal = document.getElementById('modalContainer');
        modal.style.display = 'block';
    }

    function closeModal() {
        var modal = document.getElementById('modalContainer');
        modal.style.display = 'none';
    }

    window.onclick = function(event) {
        var modal = document.getElementById('modalContainer');
        if (event.target == modal) {
            closeModal();
        }
    }
</script>
</html>
<style>
    /* Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
        background-color: #fefefe;
        margin: 15% auto; /* 15% from the top and centered */
        padding: 20px;
        border: 1px solid #888;
        width: 20%; /* Could be more or less, depending on screen size */
    }

    /* The Close Button */
    .close {
        color: #aaa;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: black;
        text-decoration: none;
        cursor: pointer;
    }
    .btn-submit1 {
        padding: 8px 16px;
        background-color: #4CAF50; /* Màu xanh lá cây */
        color: white;
        border: none;
        border-radius: 4px;
        cursor: pointer;
        font-size: 14px;
        font-weight: bold;
        text-transform: uppercase;
        outline: none;
        animation: shake 0.82s cubic-bezier(.36,.07,.19,.97) both infinite;
        transform: translate3d(0, 0, 0);
        backface-visibility: hidden;
        perspective: 1000px;
    }

    @keyframes shake {
        10%, 90% {
            transform: translate3d(-0.5px, 0, 0);
        }

        20%, 80% {
            transform: translate3d(0.5px, 0, 0);
        }

        30%, 50%, 70% {
            transform: translate3d(-0.5px, 0, 0);
        }

        40%, 60% {
            transform: translate3d(0.5px, 0, 0);
        }
    }

    .btn-submit1:hover {
        background-color: #3e8e41; /* Màu sáng hơn khi di chuột qua */
    }

    .btn-submit1:active {
        background-color: #357a38; /* Màu tối hơn khi nhấn */
    }

</style>

<script>
    function submitFormWithDisapproval(form) {
        form.is_approved.value = "false";
        form.submit();
    }
</script>