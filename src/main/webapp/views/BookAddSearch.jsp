<%@ page import="com.group5.se1746ks_lms.model.Books" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Set" %>
<%@ page import="com.group5.se1746ks_lms.model.Genres" %>

<link rel="stylesheet" href="css/Search.css">


<% List<Books> bookList = (List<Books>) request.getAttribute("bookList"); %>

<% for (Books book : bookList) { %>
<div class='book-card2' onclick="window.location.href='book?book_id=<%=book.getBook_id()%>&&editMode=true'">
    <img src='<%=book.getIMG()%>' alt='<%=book.getTitle()%>' class='book-img2'>
    <h3 class='book-title2'><%=book.getTitle()%></h3>
    <p class='book-author2'><%=book.getAuthor()%></p>
    <div class='book-genres2'>
        <% Set<Genres> genres = book.getGenres();
            for (Genres genre : genres) { %>
        <span class='genre2'><%=genre.getGenre_name()%></span>
        <% } %>
    </div>
</div>
<% } %>