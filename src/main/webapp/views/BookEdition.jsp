<%@ page import="com.group5.se1746ks_lms.model.BookEdition" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<style>
    button {
        font-size: 16px;
        padding: 4px 10px;
        border: none;
        border-radius: 5px;
        cursor: pointer;
        transition: background-color 0.3s ease;
    }


</style>

<%
    List<BookEdition> bookEditions = (List<BookEdition>) request.getAttribute("bookEditions");
    String[] colors = new String[bookEditions.size()];
    for(int i = 0; i < bookEditions.size(); i++) {
        int R = (int)(Math.random()*150) + 105;
        int G = (int)(Math.random()*150) + 105;
        int B= (int)(Math.random()*150) + 105;
        colors[i] = String.format("#%02x%02x%02x", R, G, B);
    }
%>
<link rel="stylesheet" href="css/BookDetail.css">
<li class="row">
    <p class="name col-xs-3">
        <i class="fa fa-clock-o"></i> Publish year
    </p>
    <ul class="list">
        <% for(int i = 0; i < bookEditions.size(); i++) { %>
        <button type="button" class="btn-publisher_year <%= i == 0 ? "selected-item" : "" %>" data-isbn="<%= bookEditions.get(i).getISBN() %>" style="background-color:<%= colors[i] %>;">
            <%= bookEditions.get(i).getPublish_year() %>
        </button>
        <% } %>
    </ul>
    <br>
</li>
<br>
<li class="row">
    <p class="name col-xs-3">
        <i class="fa fa-language"></i> Language
    </p>
    <ul class="list">
        <% for(int i = 0; i < bookEditions.size(); i++) { %>
        <button  type="button" class="btn-language <%= i == 0 ? "selected-item" : "" %>" style="background-color:<%= colors[i] %>; ">
            <%= bookEditions.get(i).getLanguage() %>
        </button>
        <% } %>
    </ul>
</li>
<br>
<li class="row">
    <p class="name col-xs-3">
        <i class="fa fa-language"></i> Available Copies
    </p>
    <ul class="list">
        <% for(int i = 0; i < bookEditions.size(); i++) { %>
        <button  type="button" class="btn-language <%= i == 0 ? "selected-item" : "" %>" style="background-color:<%= colors[i] %>; ">
            <%= bookEditions.get(i).getAvailable_copies() %>
        </button>
        <% } %>
    </ul>
</li>


