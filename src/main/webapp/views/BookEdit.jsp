





<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.group5.se1746ks_lms.model.*" %>
<%@ page import="com.group5.se1746ks_lms.dao.BooksDAO" %>
<%@ page import="java.util.*" %>
<%@ page import="java.util.stream.Collectors" %>


<%
    session = request.getSession(false);
    String role = "";
    String username = "";
    boolean isLoggedIn = false;
    Long user_id = null;

    if (session != null) {
        Account loggedInUser = (Account) session.getAttribute("loggedInUser");
        if (loggedInUser != null) {
            role = loggedInUser.getRole(); // Lấy role từ Account
            username = loggedInUser.getUsername(); // Lấy username từ Account
            isLoggedIn = true;
            user_id = loggedInUser.getUser_id();
        }
    }
    boolean isAdmin = "admin".equals(role); // Kiểm tra xem role có phải là admin không
%>

<%
    Books book = (Books) request.getAttribute("book");
    List<Genres> allGenres = (List<Genres>) request.getAttribute("allGenres");
    allGenres = allGenres.stream().sorted(Comparator.comparing(Genres::getGenre_name)).collect(Collectors.toList());
    Set<Genres> bookGenres = book != null ? book.getGenres() : new HashSet<>();
    boolean editMode = "true".equals(request.getParameter("editMode"));
%>
<head>
    <title><%= book.getTitle() %></title>
</head>
<script src="js/BookAdd.js" defer></script>
<script src="js/BookDetail.js" defer></script>
<link rel="stylesheet" href="css/BookDetail.css">
<link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
/>

<div class="container" style="width: 90%; margin: 2% auto;">
    <form action="${pageContext.request.contextPath}/book" method="post" enctype="multipart/form-data">
        <input type="hidden" name="book_id" value="<%= book.getBook_id() %>" />
        <div class="main_content">
            <div class="book_detail">
                <div class="book_info">
                    <div class="book_avatar" itemtype="https://schema.org/ImageObject">
                        <label>
                            <img itemprop="image" src="<%= book.getIMG() %>" alt="<%= book.getTitle() %>" />
                            <input type="file" name="image" />
                        </label>
                    </div>
                    <div class="book_other" itemscope="" itemtype="http://schema.org/Book">
                        <ul class="list-info">
                            <li class="row">
                                <p class="name col-xs-3">
                                    <i class="fa fa-book"></i> Title
                                </p>
                                <div class="col-xs-9">
                                    <input type="text" name="title" id="title" value="<%= book.getTitle() %>" />
                                </div>
                            </li>
                            <br>
                            <li class="author row">
                                <p class="name col-xs-3">
                                    <i class="fa fa-user"></i> Author
                                </p>
                                <div class="col-xs-9">
                                    <input type="text" name="author" value="<%= book.getAuthor() %>" />
                                </div>
                            </li>

                            <br>

                            <%
                                List<Publisher> publishers = (List<Publisher>) request.getAttribute("publishers");
                            %>
                            <li class="row">
                                <p class="name col-xs-3">
                                    <i class="fa fa-rss"></i> Publisher
                                </p>
                                <% for(Publisher publisher : publishers){ %>
                                <button class="btn-publisher" onclick="fetchBookEditionsUpdate('<%= publisher.getPublisher_id() %>', '<%= book.getBook_id() %>'); return false;">
                                    <%= publisher.getPublisher_name()%>
                                </button>                                    <% } %>
                            </li>




                            <br>
                            <button class="edit-button" type="button" id="add-edition-button">
                                <a class="fa fa-add"></a> Add more edition
                            </button>


                            <br>
                            <div class="row" id="bookEditions">

                            </div>

                            <br>



                            <br>


                            <br>
                            <li class="row">
                                <p class="name col-xs-3">
                                    <i class="fa fa-info-circle"></i> Description
                                </p>
                                <div class="col-xs-9">
                                    <textarea name="description"><%= book.getDescription() %></textarea>
                                </div>
                            </li>
                        </ul>
                        <div class="genres">
                            <h3>Genres</h3>
                            <ul class="list01">
                                <% for(Genres genre : allGenres) {
                                    String checkedStr = "";
                                    for(Genres bookGenre : bookGenres) {
                                        if(bookGenre.getGenre_id().equals(genre.getGenre_id())) {
                                            checkedStr = "checked";
                                            break;
                                        }
                                    } %>
                                <li class="li03">
                                    <a>
                                        <input type="checkbox" id="genre<%= genre.getGenre_id() %>" name="genres" value="<%= genre.getGenre_id() %>" <%= checkedStr %> readonly/>
                                        <label for="genre<%= genre.getGenre_id() %>"><%= genre.getGenre_name() %></label>
                                    </a>
                                </li>
                                <% } %>
                            </ul>
                        </div>
                        <br>
                        <strong> <label for="newGenre">Add a new genre:</label> </strong>
                        <input type="text" id="newGenre" name="newGenre">
                        <button class="edit-button" type="button" onclick="addGenre()">
                            <i class="fa fa-add"></i> Add
                        </button>
                        <br>
                        <ul class="list01">
                            <li class="li03">
                                <div id="newGenreList" name="newGenreList">
                                </div>
                            </li>
                        </ul>
                        <br>

                        <!-- Newly added genres will appear here -->
                        <input type="hidden" name="updated_user" value="<%= username %>" />
                        <button type="submit" class="save-button">
                            <i class="fa fa-save"></i> Save All
                        </button>
                    </div>
                    <div class="clear"></div>
                </div>
                <a href="<%= "book?book_id=" + book.getBook_id() %>"><button type="button" class="cancel-button">
                    <i class="fa fa-close" style="size: 20px;"> </i>
                </button></a>

            </div>

        </div>

    </form>
        <% List<Publisher> allPublishers = (List<Publisher>) request.getAttribute("allPublishers"); %>

    <div class="container" style="width: 90%; margin: 2% auto;">
        <div class="main_content">
            <div class="book_detail">
                <div class="book_info">


                    <div id="myModal" class="modal">
                        <div class="modal-content">
                            <span class="close">&times;</span>
                            <form method="post" action="bookEdition">
                                <div class="row" style="margin-left: 14px;
    margin-top: 20px;
    margin-right: 14px;">
                                    <p class="name col-xs-3">
                                        <i class="fa fa-rss"></i> Publisher
                                    </p>
                                    <ul class="list">
                                        <select name="publisher_id" style="width: fit-content">
                                            <% for (Publisher publisher : allPublishers) { %>
                                            <option value="<%= publisher.getPublisher_id() %>"><%= publisher.getPublisher_name() %></option>
                                            <% } %>
                                        </select>
                                    </ul>
                                </div>
                                <br>
                                <div class="row" style="margin-left: 14px;
    margin-top: 14px;
    margin-right: 14px;">
                                    <p class="name col-xs-9" >
                                        <i class="fa fa-clock-o"></i> Publish year
                                    </p>
                                    <ul class="list">
                                        <input name="publish_year" id="publish_year" type="number" style="font-size: 15px; margin-top: 5px">
                                    </ul>
                                </div>
                                <br>
                                <div class="row" style="margin-left: 14px;
    margin-top: 14px;
    margin-right: 14px;">
                                    <p class="name col-xs-3">
                                        <i class="fa fa-language"></i> Language
                                    </p>
                                    <ul class="list">
                                        <input name="language" id="language" type="text" style="font-size: 15px">
                                    </ul>
                                </div>
                                <br>
                                <div class="row" style="margin-left: 14px;
    margin-top: 14px;
    margin-right: 14px;">
                                    <p class="name col-xs-3">
                                        <i class="fa fa-barcode"></i> ISBN
                                    </p>
                                    <ul class="list">
                                        <input name="ISBN" id="ISBN" type="text" style="font-size: 15px">
                                    </ul>
                                </div>
                                <br>
                                <div class="row" style="margin-left: 14px;
    margin-top: 14px;
    margin-right: 14px;">
                                    <p class="name col-xs-3">
                                        <i class="fa fa-number"></i> Quantity
                                    </p>
                                    <ul class="list">
                                        <input name="quantity" id="quantity" type="number" style="font-size: 15px">
                                    </ul>
                                </div>
                                <br>
                                <div class="row" style="text-align: right; margin: 14px 14px" >
                                    <input type="hidden" name="book_id" value="<%=book.getBook_id()%>">
                                    <input type="hidden" name="created_user" value="<%=username%>">
                                    <button class="edit-button" type="submit"id="submit-button">
                                        <i class="fa fa-add"></i> Add
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function addGenre() {
            var newGenreList = document.getElementById('newGenreList');
            var newGenre = document.getElementById('newGenre');
            if (newGenre.value.trim() === '') return;

            // Create a new anchor element
            var genreElement = document.createElement('a');
            genreElement.textContent = newGenre.value;

            // Create a new hidden input element
            var hiddenInput = document.createElement('input');
            hiddenInput.type = 'hidden';
            hiddenInput.name = 'newGenres';  // This name should match the parameter you want to retrieve on the server side
            hiddenInput.value = newGenre.value;

            // Append elements
            newGenreList.appendChild(genreElement);
            newGenreList.appendChild(hiddenInput);

            // Clear the input field
            newGenre.value = '';
        }

    </script>