<%@ page import="com.group5.se1746ks_lms.model.Loans" %>
<%@ page import="com.group5.se1746ks_lms.model.Request" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %><%--
  Created by IntelliJ IDEA.
  User: tranthanhhuyen
  Date: 23/10/2023
  Time: 17:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
  <title>MANAGE BOOK LOANS</title>
  <link rel="stylesheet" href="css/Home.css">
  <link rel="stylesheet" href="css/LoansList.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
        integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"/>
</head>
<body>
<div class="wrapper">
  <jsp:include page="Header.jsp"/>

  <div class="top-title">
    <h3>LOANS</h3>
  </div>
  <div class="container">
    <div class="body-container">
      <div class="title-box">
        <h2 class="title">Loans</h2>
      </div>
      <div class="record-list">
        <div class="search-sort">
          <div class="search">
            <span>Search by:</span>
            <select name="searchBy" id="searchBy">
              <option value="bookCode" ${searchBy.equals("bookCode")?"selected":""}>Book Code</option>
              <option value="bookTitle" ${searchBy.equals("bookTitle")?"selected":""}>Title</option>
              <option value="recordId" ${searchBy.equals("recordId")?"selected":""}>Record ID</option>
              <option value="userId" ${searchBy.equals("userId")?"selected":""}>User ID</option>
            </select>
            <i class="fa-solid fa-magnifying-glass"></i>
            <input type="text" placeholder="Search" id="search" oninput="searchLoans()">
          </div>
          <form class="sort" action="loans" method="post" id="sort">
            <span>Sort by date created:</span>
            <select name="sortBy" onchange="document.getElementById('sort').submit();">
              <option value="all" ${sortBy == null?"selected":""}>All</option>
              <option value="newest" ${sortBy.equals("newest")?"selected":""}>Newest</option>
              <option value="oldest" ${sortBy.equals("oldest")?"selected":""}>Oldest</option>
            </select>
          </form>
        </div>
        <table class="listing" border="1">
          <thead>
          <tr>
            <th>NO</th>
            <th>BOOK CODE</th>
            <th>COVER</th>
            <th>TITLE</th>
            <th>CREATE DATE</th>
            <th>DUE DATE</th>
            <th>NEW DUE DATE</th>
            <th>STATUS</th>
            <th>EXPIRED DATE</th>
            <th>ACTION</th>
          </tr>
          </thead>
          <tbody class="list">
          <%     List<Loans> loansList = (List<Loans>) request.getAttribute("loans");
            List<Request> requests = (List<Request>) request.getAttribute("requests");

          for (int i = 0; i < loansList.size(); i++) {
          Loans l = loansList.get(i);
          %>

          <tr>
            <td><%= i + 1 %></td>
            <td><%= l.getBook_detail_id() %></td>
            <td><img src="<%= l.getImg() %>" width="120" height="150"></td>
            <td><a href="book?book_id=<%= l.getBook_id() %>"><%= l.getTitle() %></a></td>
            <td><%= l.getCreate_date() %></td>
            <td><%= l.getDue_date() %></td>
            <%
              boolean found = false;
              for (Request req : requests) {
                if (req != null && req.getLoan_id() == l.getLoans_id()) {
                  found = true;
            %><td><%= req.getRequest_date() %></td><%
                break;
              }
            }
            if (!found) {
          %><td><% 
            if (!l.getStatus().equals("Expired") && !l.getStatus().equals("Overdue") && !l.getStatus().equals("Returned") && !l.getStatus().equals("Reserved") &&  !l.getStatus().equals("Lost")) {
          %>
            <button type="button" class="button-action button-extend" onclick="loadRequestForm(<%= l.getLoans_id() %>)">
              <i class="fa fa-clock" aria-hidden="true"></i> Extend
            </button>
            <%
              }
            %></td><%
            }
          %>



            <td style="<%
    String color = "";
    if ("Reserved".equals(l.getStatus()) && l.getExpried_date().before(new Date())) {
        color = "red";
    } else if ("Reserved".equals(l.getStatus())) {
        color = "#b8b800";
    } else if ("Borrowing".equals(l.getStatus()) && (l.getDue_date().before(new Date()) && (l.getNew_due_date() == null || l.getNew_due_date().before(new Date())))) {
        color = "red";
    } else if ("Borrowing".equals(l.getStatus())) {
        color = "#b8b800";
    } else if ("Returned".equals(l.getStatus())) {
        color = "green";
    } else if("Overdue".equals(l.getStatus()) && l.getNew_due_date() == null || "Expired".equals(l.getStatus()) && l.getNew_due_date() == null || "Lost".equals(l.getStatus()) && l.getNew_due_date() == null){
        color = "red";
    }
%>color: <%=color%>;"><%= l.getStatus() %></td>
            <td><%= l.getExpried_date() %></td>
            <td class="action" style="width: 100px;">
              <a href="loansDetail?loans_id=<%= l.getLoans_id() %>" class="view">
                <span>
                    <i class="fa-solid fa-eye"></i>
                    View
                </span>
              </a>
            </td>
          </tr>
          <%
            }
          %>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <%
    long user_id = (long) request.getAttribute("user_id");

  %>
  <div class="pagination-container">
    <div class="pagination-nav">
      <a href="loansUser?user_id=<%=user_id%>&currentPage=${currentPage - 1}&itemsPerPage=${itemsPerPage}&sortBy=${sortBy}"
         class="${currentPage == 1 ? "disabled" : ""}">Previous</a>
      <span class="pagination-info"> ${currentPage} / ${totalPages} </span>
      <a href="loansUser?user_id=<%=user_id%>&currentPage=${currentPage + 1}&itemsPerPage=${itemsPerPage}&sortBy=${sortBy}"
         class="${currentPage >= totalPages ? "disabled" : ""}">Next</a>
    </div>
    <div class="pagination-settings">
      <form action="loansUser" method="get" class="pagination-form" onsubmit="return validateForm();">
        <input type="hidden" name="user_id" value="<%=user_id%>">
        <c:if test="${sortBy != null && !sortBy.isEmpty()}">
          <input type="hidden" name="sortBy" value="${sortBy}">
        </c:if>
        <label for="currentPage" style="margin-right: auto">Current page</label>
        <input type="number" id="currentPage" name="currentPage" style="margin-left: 10px"
               value="${currentPage}" min="1">
        <label for="itemsPerPage"></label>
        <select id="itemsPerPage" name="itemsPerPage" style="padding: 3px">
          <option value="1" ${itemsPerPage == 1 ? "selected" : ""}>1</option>
          <option value="5" ${itemsPerPage == 5 ? "selected" : ""}>5</option>
          <option value="10" ${itemsPerPage == 10 ? "selected" : ""}>10</option>
        </select>

        <button type="submit" class="pagination-button">View</button>
      </form>
    </div>
  </div>
  <div id="modalContainer" class="modal">
    <div class="modal-content">
      <div id="formContainer">
      </div>
    </div>
  </div>
  <jsp:include page="Footer.jsp"/>
</div>
</body>
<script>
  function searchLoans() {
    let by = document.getElementById('searchBy').value;
    let query = document.getElementById('search').value;
    let xhr = new XMLHttpRequest();
    xhr.open('GET', 'searchLoans?by=' + by + '&query=' + query, true);
    xhr.onreadystatechange = function () {
      if (xhr.readyState == 4 && xhr.status == 200) {
        document.getElementsByClassName('list')[0].innerHTML = xhr.responseText;
      }
    };
    xhr.send();
  }
</script>
<script>
  function loadRequestForm(loan_id) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById('formContainer').innerHTML = this.responseText;
        showModal();
      }
    };
    xhttp.open("GET", "createRequest?loan_id=" + loan_id, true);
    xhttp.send();
  }

  function showModal() {
    var modal = document.getElementById('modalContainer');
    modal.style.display = 'block';
  }

  function closeModal() {
    var modal = document.getElementById('modalContainer');
    modal.style.display = 'none';
  }

  // Close modal if clicked outside of it
  window.onclick = function(event) {
    var modal = document.getElementById('modalContainer');
    if (event.target == modal) {
      closeModal();
    }
  }


</script>
</html>

<script>
  function handleExtensionOptionChange() {
    var addDaysInput = document.getElementById('add_days_input');
    var chooseDateInput = document.getElementById('choose_date_input');

    if (document.getElementById('add_days_option').checked) {
      addDaysInput.style.display = 'block';
      chooseDateInput.style.display = 'none';
    } else if (document.getElementById('choose_date_option').checked) {
      chooseDateInput.style.display = 'block';
      addDaysInput.style.display = 'none';
    }
  }

  window.onload = handleExtensionOptionChange;

</script>
<style>

  /* Modal (background) */
  .modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
  }

  /* Modal Content */
  .modal-content {
    background-color: #fefefe;
    margin: 15% auto; /* 15% from the top and centered */
    padding: 20px;
    border: 1px solid #888;
    width: 70%; /* Could be more or less, depending on screen size */
  }

  /* The Close Button */
  .close {
    color: #aaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
  }

  .close:hover,
  .close:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
  }

  .button-action {
    background-color: #4CAF50; /* Green background */
    border: none;
    color: white;
    padding: 10px 20px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
    border-radius: 5px;
    transition: background-color 0.3s ease;
  }

  .button-extend {
    background-color: #008CBA; /* Blue background */
  }

  .button-return {
    background-color: #ff6300; /* Red background */
  }

  .button-action i {
    margin-right: 5px;
  }

  .button-action:hover {
    opacity: 0.8;
  }

</style>
