<%--
  Created by IntelliJ IDEA.
  User: faced
  Date: 9/30/2023
  Time: 2:16 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>LOGIN</title>
    <link rel="stylesheet" href="css/Login.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
          integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <script src="https://www.google.com/recaptcha/api.js?render=6LeIeOwoAAAAALhkhd6AGDWLh4mvIFlSFeAWNkPT"></script>
    <script>
        grecaptcha.ready(function() {
            grecaptcha.execute('6LeIeOwoAAAAALhkhd6AGDWLh4mvIFlSFeAWNkPT', {action: 'login'}).then(function(token) {
                document.getElementById('g-recaptcha-response').value = token;
            });
        });
    </script>
</head>

<body>
<div class="wrapper">
    <div class="login">
        <img class="logo" src="https://cdn.haitrieu.com/wp-content/uploads/2021/10/Logo-Dai-hoc-FPT.png">
        <h1 class="form-header">LOGIN</h1>
        <%
            String success = request.getParameter("success");
            if(success != null) {
            if(success.equals("true")) {%>
        <p class="success-mess"><b>Password reset link has been sent to your email</b></p>
        <%} else if(success.equals("reset")) {%>
        <p class="success-mess"><b>Password reset successfully</b></p>
        <%} }%>

        <form action="login" method="post" class="form-login">
            <div class="form-input">
                <%
                    String username = request.getParameter("username");
                %>
                <input type="text" class="enter" placeholder="Username" name="username" value="<%=username != null ? username : ""%>">
                <i class="fa-solid fa-user"></i>

            </div>
            <div class="form-input">
                <input type="password" class="enter" placeholder="Password" name="password">
                <i class="fa-solid fa-lock"></i>
            </div>
            <p class="error-mess"><b>${error}</b></p>
            <input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response">
            <input type="submit" value="Login" class="form-submit">
            <a href="login?action=google" class="google-login-btn">
                <img src="https://upload.wikimedia.org/wikipedia/commons/5/53/Google_%22G%22_Logo.svg" alt="Google Logo"> Login with Google
            </a>

            <br>
            <a href="forgot-password" class="forgot-password">Forget password?</a>

            <a href="home" class="back">
                Back Homepage
            </a>
        </form>
    </div>
</div>

</body>
</html>