<%@ page import="com.group5.se1746ks_lms.model.Account" %><%--
  Created by IntelliJ IDEA.
  User: faced
  Date: 9/30/2023
  Time: 1:29 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>FPT Library</title>
    <link rel="stylesheet" href="css/Home.css">
    <link rel="stylesheet" href="css/Search.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
          integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <script src="js/Home.js" defer></script>

</head>
<body>
<div class="wrapper">
    <jsp:include page="Header.jsp"/>


    <div class="banner">
        <div class="welcome">
            <h1>WELCOME TO
                <span>FPTU LIBRARY</span>
            </h1>
            <p>Open: 08:15 - 21:00 Weekday | 08:00 - 12:00 & 13:00 - 17:00 Weekend</p>
        </div>
        <form class="search-block" action="books" method="get">
            <select id="search-type">
                <option value="title">Title</option>
                <option value="author">Author</option>
                <option value="isbn">ISBN</option>
                <option value="publisher">Publisher</option>
                <option value="language">Language</option>
            </select>
            <input type="text" id="search-bar" name="title" placeholder="Select the information that you want to search " oninput="searchBooks1()"/>
            <div class="search-results2"></div>
        </form>


    </div>

    <div class="main-section">
        <div class="title">
            <h2>How it works</h2>
        </div>
        <div class="block-step">
            <div class="step">
                <i class="fa-solid fa-magnifying-glass"></i>
                <h2>Search for book of choice</h2>
            </div>
            <div class="next">
                <i class="fa-solid fa-angle-right"></i>
            </div>
            <div class="step">
                <i class="fa-regular fa-rectangle-list"></i>
                <h2>Read rules for borrowing book</h2>
            </div>
            <div class="next">
                <i class="fa-solid fa-angle-right"></i>
            </div>
            <div class="step">
                <i class="fa-solid fa-circle-plus"></i>
                <h2 style="margin-bottom: 0;">Reserve</h2>
                <h2 style="margin-top: 0;">book</h2>
            </div>
            <div class="next">
                <i class="fa-solid fa-angle-right"></i>
            </div>
            <div class="step">
                <i class="fa-solid fa-right-from-bracket"></i>
                <h2 style="margin-bottom: 0;">Check-out</h2>
                <h2 style="margin-top: 0;">book</h2>
            </div>
        </div>
    </div>

    <jsp:include page="Footer.jsp"/>

</div>
</body>
</html>
