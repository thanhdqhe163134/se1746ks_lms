<%--
  Created by IntelliJ IDEA.
  User: tranthanhhuyen
  Date: 12/10/2023
  Time: 21:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
  <title>MANAGE ACCOUNTS</title>
  <link rel="stylesheet" href="css/Home.css">
  <link rel="stylesheet" href="css/AccountCreate.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
        integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"/>
</head>
<body>
<div class="wrapper">
  <jsp:include page="Header.jsp"/>
  <div class="top-title">
    <h3>MANAGE ACCOUNTS</h3>
  </div>
  <div class="title-box">
    <h2 class="title">Account Details</h2>
    <a href="accounts">
                <span>
                    <i class="fa-solid fa-angle-left"></i>
                    Back to list
                </span>
    </a>
  </div>
  <form class="form" action="updateAcc" method="post">
    <br>
    <div class="form-content">
      <h3>ID</h3>
      <input type="text" name="id" class="no-change" value="${a.getUser_id()}" readonly>
    </div>
    <div class="form-content">
      <h3>Student ID</h3>
      <input type="text" name="student_id" value="${a.getStudent_id()}" >
    </div>
    <div class="form-content">
      <h3>Fullname</h3>
      <input type="text" name="fullname" value="${a.getFull_name()}" required>
    </div>
    <div class="form-content">
      <h3>Username</h3>
      <input type="text" name="username" value="${a.getUsername()}" required>
    </div>
    <div class="form-content">
      <h3>Date of Birth</h3>
      <input type="date" name="dob" value="${a.getDob()}"required >
    </div>
    <div class="form-content">
      <h3>Email</h3>
      <input type="text" name="email" value="${a.getEmail()}" required>
    </div>
    <div class="form-content">
      <h3>Phone Number</h3>
      <input type="text" name="phone" value="${a.getPhone_number()}" >
    </div>
    <div class="form-content">
      <h3>Address</h3>
      <input type="text" name="address" value="${a.getAddress()}" >
    </div>
    <div class="form-content role">
      <h3>Role</h3>
      <c:if test="${a.getRole() != 'admin'}">
        <input type="radio" name="role" value="user" ${a.getRole().equals("user")?"checked":""}>User
        <input type="radio" name="role" value="librarian" ${a.getRole().equals("librarian")?"checked":""}>Librarian
        <input type="radio" name="role" value="admin" ${a.getRole().equals("admin")?"checked":""}>Admin
      </c:if>
      <c:if test="${a.getRole() == 'admin'}">
        <input type="radio" name="role" class="no-change" value="user" ${a.getRole().equals("user")?"checked":""}>User
        <input type="radio" name="role" class="no-change" value="librarian" ${a.getRole().equals("librarian")?"checked":""}>Librarian
        <input type="radio" name="role" class="no-change" value="admin" ${a.getRole().equals("admin")?"checked":""}>Admin
      </c:if>
    </div>
    <div class="form-content">
      <h3>Create date</h3>
      <input type="date" name="create_date" class="no-change" value="${a.getCreated_date()}" readonly>
    </div>
    <div class="form-content">
      <h3>Update date</h3>
      <input type="date" name="update_date" id="today" class="no-change" value="${a.getUpdated_date()}" readonly>
    </div>
    <div class="form-content">
      <h3>Update By</h3>
      <input type="text" name="update_by" class="no-change" value="${sessionScope.loggedInUser.getUsername()}" readonly>
    </div>
    <c:if test="${a.getIs_deleted() == true}">
      <div class="form-content">
        <h3>Delete date</h3>
        <input type="date" name="deleted_date" class="no-change" value="${a.getDeleted_date()}" readonly>
      </div>
      <div class="form-content">
        <h3>Delete By</h3>
        <input type="text" name="deleted_by" class="no-change" value="${sessionScope.loggedInUser.getUsername()}" readonly>
      </div>
    </c:if>
    <a href="accDetail?editMode=true&id=${a.getUser_id()}" class="form-content submit">
      <input type="submit" name="submit" value="EDIT">
    </a>
  </form>
  <jsp:include page="Footer.jsp"/>
</div>
</body>
<script>
  var today = new Date();
  document.getElementById("today").value = today.toLocaleDateString("en-CA");
</script>
</html>
