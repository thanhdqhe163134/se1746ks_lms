<link rel="stylesheet" href="css/Home.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
      integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
      crossorigin="anonymous" referrerpolicy="no-referrer"/>


<div class="footer">
    <div class="footer-block">
        <div class="contact">
            <div class="contact-title">
                <h2>FPT UNIVERSITY LIBRARY</h2>
            </div>
            <div class="contact-info">
                <i class="fa fa-location-arrow"></i>
                <span> 107 Delta, FPT University, Hoa Lac Hi-tech Park, Km29, Thang Long Boulevard, Thach Hoa,</span><br>
                <span>Thach That, Ha Noi, Vietnam</span>
            </div>
            <div class="contact-info">
                <i class="fa fa-envelope"></i>
                <span>
                    <a href="mailto:thuvien_fu_hoalac@fpt.edu.vn"> thuvien_fu_hoalac@fpt.edu.vn</a>
                </span>
            </div>
            <div class="contact-info">
                <i class="fa fa-phone"></i>
                <span> 02466 805 912</span>
            </div>
        </div>
        <div class="follow">
            <h2 class="footer-widget-title">Follow us</h2>
            <div class="social-contact">
                <ul>
                    <li class="fb">
                        <a href="https://www.facebook.com/thuvienfu">
                            <i class="fa-brands fa-square-facebook"></i>
                        </a>
                    </li>
                    <li class="ytb">
                        <a href="https://www.youtube.com/channel/UCXavXXYzplrnIeVl0-wR3uw">
                            <i class="fa-brands fa-youtube"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
