<%@ page import="com.group5.se1746ks_lms.model.BookDetail" %>
<%@ page import="java.util.List" %>
<%@ page import="com.group5.se1746ks_lms.model.Account" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>MANAGE BOOK DETAIL</title>
    <link rel="stylesheet" href="css/Home.css">
    <link rel="stylesheet" href="css/AccountList.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
          integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <script src="js/AccountList.js" defer></script>
    <% List<BookDetail> list = (List<BookDetail>) request.getAttribute("bookDetails"); %>
    <%
        session = request.getSession(false);
        String role = "";
        String username = "";
        boolean isLoggedIn = false;
        Long user_id = null;

        if (session != null) {
            Account loggedInUser = (Account) session.getAttribute("loggedInUser");
            if (loggedInUser != null) {
                role = loggedInUser.getRole(); // Lấy role từ Account
                username = loggedInUser.getUsername(); // Lấy username từ Account
                isLoggedIn = true;
                user_id = loggedInUser.getUser_id();
            }
        }
        boolean isAdmin = "admin".equals(role); // Kiểm tra xem role có phải là admin không
    %>
</head>
<body>
<div class="wrapper">
    <jsp:include page="Header.jsp"/>
    <div class="top-title">
        <h3>MANAGE BOOK DETAIL</h3>
    </div>
    <div class="title-box">
        <form method="post" action="BookISBN">
            <input type="number" name="quantity" placeholder="Quantity" style="
    height: 35px;" required>
            <input type="hidden" name="isbn" value="<%=request.getAttribute("isbn")%>">
            <input type="hidden" name="username" value="<%=username%>">
            <button type="submit" class="btn-submit">Add</button>
        </form>

    </div>
    <div class="user-list">
        <div class="search-sort">
            <form class="sort" action="accounts" method="post" id="sort">
                <span>Sort by date created:</span>
                <select style="width: 100px; height: auto" name="sortBy" onchange="document.getElementById('sort').submit();">
                    <option value="newest" ${sortBy.equals("newest")?"selected":""}>Newest</option>
                    <option value="oldest" ${sortBy.equals("oldest")?"selected":""}>Oldest</option>
                </select>
            </form>
        </div>
        <table class="listing" border="1">
            <thead>
            <tr>
                <th>ID</th>
                <th>ISBN</th>
                <th>Status</th>
                <th>Is Borrowed</th>
                <th>Created Date</th>
                <th>Created User</th>
                <th>Updated Date</th>
                <th>Updated User</th>
                <th>Is Deleted</th>
                <th>ACTION</th>
            </tr>
            </thead>
            <tbody class="list">
            <% for (BookDetail b : list) { %>
                <tr>
                    <td><%= b.getBook_detail_id() %></td>
                    <td><%= b.getISBN() %></td>
                    <td><%= b.getStatus() %></td>

                    <td style="<%= b.getIs_borrowed() ? "color: green;" : "color: red;" %>">
                        <%= b.getIs_borrowed() %>
                    </td>
                    <td><%=b.getCreated_date()%></td>
                    <td><%=b.getCreated_user()%></td>
                    <td><%=b.getUpdated_date()%></td>
                    <td><%=b.getUpdated_user()%></td>
                    <td style="<%= b.getIs_deleted() ? "color: green;" : "color: red;" %>">
                        <%= b.getIs_deleted() %>
                    </td>                    <td class="action">
                        <a href="BookISBN?action=delete&book_detail_id=<%=b.getBook_detail_id()%>" class="delete">
                                    <span>
                                        <i class="fa-solid fa-trash-can"></i>
                                        Delete
                                    </span>
                        </a>
                    </td>
                </tr>
            <% } %>
            </tbody>
        </table>
    </div>
    <jsp:include page="Footer.jsp"/>
</div>
</body>
</html>
<style>
    .btn-submit {
        padding: 8px 16px;
        border: 1px solid #ddd;
        border-radius: 4px;
        color: white;
        cursor: pointer;
        font-size: 14px;
        background: green;

    }
    .btn-submit:hover {
        background-color: #45a049;
    }

</style>